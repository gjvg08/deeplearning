import torch
import torchvision
import time


class MitNet(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.conv_in = torch.nn.Conv2d(in_channels=1, out_channels=24, kernel_size=3, stride=1, padding=0)
        self.max_pool1 = torch.nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_interm = torch.nn.Conv2d(in_channels=24, out_channels=36, kernel_size=3, stride=1, padding=0)
        self.max_pool2 = torch.nn.MaxPool2d(kernel_size=2, stride=2)
        # flatten
        self.lin_interm = torch.nn.Linear(in_features=900, out_features=128)
        self.relu = torch.nn.ReLU()
        self.lin_out = torch.nn.Linear(in_features=128, out_features=10)
        self.logsoftmax = torch.nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.conv_in(x)
        x = self.max_pool1(x)
        x = self.conv_interm(x)
        x = self.max_pool2(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin_interm(x)
        x = self.relu(x)
        x = self.lin_out(x)
        x = self.logsoftmax(x)

        return x


class SimpleLinearNet(torch.nn.Module):
    def __init__(self):
        super().__init__()
        # flatten
        self.lin = torch.nn.Linear(in_features=784, out_features=10)
        self.logsoftmax = torch.nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        x = self.lin(x)
        x = self.logsoftmax(x)

        return x


if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("Device:", device)
    model = MitNet().to(device)

    num_epochs = 4
    some_data = torchvision.datasets.MNIST("mnist_test", download=True, train=True,
                                           transform=torchvision.transforms.ToTensor())
    dloader = torch.utils.data.DataLoader(some_data, batch_size=12)

    criterion = torch.nn.CrossEntropyLoss()
    optim = torch.optim.SGD(params=model.parameters(), lr=0.1, momentum=0, weight_decay=0.)

    start_time = time.time()
    for epoch in range(num_epochs):
        model.train()
        for inputs, labels in dloader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            optim.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optim.step()

        correct = 0
        total = 0

        # Set the model to evaluation mode
        model.eval()

        # Disable gradient computation
        with torch.no_grad():
            for inputs, labels in dloader:
                inputs = inputs.to(device)
                labels = labels.to(device)

                outputs = model(inputs)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()

        # Calculate the accuracy
        accuracy = correct / total

        # Print the accuracy for the current epoch
        print(f"Accuracy for epoch {epoch + 1}: {round(accuracy * 100,2)}%")

    end_time = time.time()
    total_time = end_time - start_time
    print(f"Total training time: {total_time} seconds")
