import os
import torch

class rnaDataset(torch.utils.data.Dataset):
    def __init__(self, labels, st_dir, transform=None, target_transform=None):
        self.labels = labels
        self.st_dir = st_dir
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        return len(self.img_labels)

    def __getitem__(self, idx):
        rna_path = os.path.join(self.st_dir, os.fsdecode(os.listdir(self.st_dir)[idx]))
        rna_seq = read_st_file(rna_path)
        label = self.img_labels.iloc[idx, 1]
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label
