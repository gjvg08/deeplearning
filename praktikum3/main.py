import numpy as np
import torch
import torchvision
import matplotlib.pyplot as plt




if __name__ == '__main__':
    some_data = torchvision.datasets.MNIST("mnist_test", download=True, train=True, transform=torchvision.transforms.ToTensor())
    dloader = torch.utils.data.DataLoader(some_data, batch_size=5)

    figure = plt.figure(figsize=(8, 8))
    cols, rows = 3, 3
    for i in range(1, cols * rows + 1):
        sample_idx = torch.randint(len(some_data), size=(1,)).item()
        img, label = some_data[sample_idx]
        figure.add_subplot(rows, cols, i)
        plt.title(label)
        plt.axis("off")
        plt.imshow(img.squeeze(), cmap="gray")
    plt.show()
