import numpy as np
import os
import collections as cs


def read_st_file(filepath):
    rna_seq = ""
    with open(filepath, "r") as file:
        for line in file.readlines():
            if line.startswith("#"):
                continue
            rna_seq = line.rstrip()
            break
    return rna_seq


def read_st_dir(dirpath):
    rna_seqs = dict()
    directory = os.fsencode(dirpath)
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_id = int(filename.split(".")[0].split("_")[2])
        filepath = dirpath + "/" + filename
        rna_seqs[file_id] = read_st_file(filepath)
    return rna_seqs


def get_alphabet(rna_seqs):
    seq_lst = [seq for seq in rna_seqs.values()]
    all_seqs = "".join(seq_lst)
    alphabet_occurences = cs.Counter(all_seqs)
    return alphabet_occurences


def rna_to_onehot(rna_seq, alphabet):
    alphabet_size = len(alphabet.keys())
    arr = np.zeros((len(rna_seq), alphabet_size), dtype=np.int8)
    base_to_idx = {base: idx for idx, base in enumerate(alphabet.keys())}
    for i, base in enumerate(rna_seq):
        arr[i][base_to_idx[base]] = 1
    return arr


if __name__ == '__main__':
    # test_st_rna = read_st_file("bpRNA_Daten/bpRNA_CRW_226.st")
    # print(test_st_rna)
    rna_seqs = read_st_dir("bpRNA_Daten")
    alphabet = get_alphabet(rna_seqs)
    encoded_rna_seqs = {k: rna_to_onehot(v, alphabet) for k, v in rna_seqs.items()}
    print("Number of different bases: {}".format(len(alphabet.keys())))
    # print(encoded_rna_seqs)
    print(np.shape(encoded_rna_seqs[226]))
    print(encoded_rna_seqs[226])


# Jede Klammer kriegt eine Zahl bzw. jede structure einen Wert. Ähnliche Werte ähnliche Strukturen?
