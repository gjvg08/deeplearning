#Name: bpRNA_CRW_11279
#Length:  1205 
#PageNumber: 2
GCCCAUAAGACUGGGAUAACUCCGGGAAACCGGGGCUAAUACCGGAUAACAUUUUGAACCGCAUGGUUCGAAAUUGAAAGGCGGCUUCGGCUGUCACUUAUGGAUGGACCCGCGUCGCAUUAGCUAGUUGGUGAGGUAACGGCUCACCAAGGCAACGAUGCGUAGCCGACCUGAGAGGGUGAUCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCCGCAAUGGACGAAAGUCUGACGGAGCAACGCCGCGUGAGUGAUGAAGGCUUUCGGGUCGUAAAACUCUGUUGUUAGGGAAGAACAAGUGCUAGUUGAAUAAGCUGGCACCUUGACGGUACCUAACCAGAAAGCCACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUGGCAAGCGUUAUCCGGAAUUAUUGGGCGUAAAGCGCGCGCAGGUGGUUUCUUAAGUCUGAUGUGAAAGCCCACGGCUCAACCGUGGAGGGUCAUUGGAAACUGGGAGACUUGAGUGCAGAAGAGGAAAGUGGAAUUCCAUGUGUAGCGGUGAAAUGCGUAGAGAUAUGGAGGAACACCAGUGGCGAAGGCGACUUUCUGGUCUGUAACUGACACUGAGGCGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAAGUGUUAGAGGGUUUCCGCCCCUUAGUGCUGAAGUUAACGCAUUAAGCACUCCGCCUGGGGAGUACGGCCGCAAGGCUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCUCCUGAAAACCCUAGAGAUAGGGCUUCUCCUUCGGGAGCAGAGUGACAGGUGGUCAUGGUUGUCGUCAGCUCGUGUCUGAGAUGUUGGGUUAAGUCCCGCCAACGAGCGCAACCCUUGAUCUUAGUUGCCAUCAUUAAGUUGGGCACUCUAAGGUGACUGCCGGUGACAAACCGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGACCUGGGCUACACACGUGCUACAAUGGACGGUACAAAGAGCUGCCAAGACCGCGAGGUGGAGCUAAUCUCAUAAAACCGUUCAGUUCGGAUUGUAGGCUGCAACUCGCCUACA
..(((((((.((((....(((((((....))))))).....)))).....((((((((((....)))))))))).....((((((....)))))).)))))))............(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((.......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))...............................((((((((((((....((((........))))........(((((((.....((((((.((..(((((((....))))))).((((....))))))))).))).((.(((..(((((((((...(((((((((...)))..((((......))))...)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...)).)))))....)))))))...)).))))))))))....((((((.....(((..((....(((....)))...))....))).....))))))..........(((((((........)))))))
EESSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHSSSSSSSSSSMMMMMSSSSSSHHHHSSSSSSMSSSSSSSXXXXXXXXXXXXSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSBSSMMSSSSSSSHHHHSSSSSSSMSSSSHHHHSSSSSSSSSBSSSMSSBSSSIISSSSSSSSSMMMSSSSSSSSSHHHSSSMMSSSSHHHHHHSSSSMMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSISSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSXXXXSSSSSSIIIIISSSIISSIIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSXXXXXXXXXXSSSSSSSHHHHHHHHSSSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 3..9 "CCAUAAG" 97..103 "CUUAUGG"
S2 11..14 "CUGG" 42..45 "CCGG"
S3 19..25 "ACUCCGG" 30..36 "CCGGGGC"
S4 51..60 "AUUUUGAACC" 65..74 "GGUUCGAAAU"
S5 80..85 "GGCGGC" 90..95 "GCUGUC"
S6 116..118 "CGC" 160..162 "GCG"
S7 121..121 "U" 159..159 "U"
S8 123..125 "GCU" 151..153 "GGC"
S9 128..135 "UUGGUGAG" 143..150 "CUCACCAA"
S10 165..168 "GCCG" 184..187 "CGGC"
S11 169..172 "ACCU" 177..180 "GGGU"
S12 192..197 "CUGGGA" 208..213 "GCCCAG"
S13 215..218 "CUCC" 223..226 "GGAG"
S14 243..243 "U" 269..269 "A"
S15 245..247 "CCG" 266..268 "CGG"
S16 251..255 "UGGAC" 260..264 "GUCUG"
S17 282..285 "GAGU" 309..312 "ACUC"
S18 292..295 "GGCU" 300..303 "GGUC"
S19 314..314 "G" 373..373 "G"
S20 317..322 "GUUAGG" 365..370 "CCUAAC"
S21 326..326 "G" 360..360 "C"
S22 332..339 "GUGCUAGU" 347..354 "GCUGGCAC"
S23 377..381 "GCCAC" 418..422 "GUGGC"
S24 388..392 "CUACG" 413..417 "CGUAG"
S25 398..399 "GC" 404..405 "GC"
S26 444..446 "GGC" 759..761 "GCC"
S27 454..457 "GCGC" 638..641 "GCGC"
S28 459..459 "C" 635..635 "G"
S29 461..463 "GGU" 632..634 "ACU"
S30 465..471 "GUUUCUU" 522..528 "GGGAGAC"
S31 473..474 "AG" 520..521 "CU"
S32 475..483 "UCUGAUGUG" 509..517 "GUCAUUGGA"
S33 488..494 "CCCACGG" 500..506 "CCGUGGA"
S34 532..534 "AGU" 626..628 "ACU"
S35 535..540 "GCAGAA" 619..624 "GUCUGU"
S36 543..549 "GGAAAGU" 611..617 "ACUUUCU"
S37 550..551 "GG" 593..594 "AC"
S38 554..561 "UUCCAUGU" 583..590 "AUAUGGAG"
S39 565..567 "GCG" 574..576 "UGC"
S40 599..599 "G" 610..610 "G"
S41 602..603 "GC" 608..609 "GC"
S42 646..651 "GCGUGG" 682..687 "CCACGC"
S43 655..656 "GC" 680..681 "GU"
S44 660..663 "CAGG" 673..676 "CCUG"
S45 698..703 "GAGUGC" 752..757 "GCACUC"
S46 706..717 "AGUGUUAGAGGG" 724..735 "CCCCUUAGUGCU"
S47 739..740 "GU" 745..746 "GC"
S48 763..765 "GGG" 788..790 "CUC"
S49 772..775 "GGCC" 780..783 "GGCU"
S50 822..831 "GGAGCAUGUG" 1105..1114 "CACGUGCUAC"
S51 832..833 "GU" 1102..1103 "AC"
S52 838..841 "UUCG" 850..853 "CGAA"
S53 862..868 "CCAGGUC" 1092..1098 "GACCUGG"
S54 874..876 "AUC" 922..924 "GAC"
S55 877..879 "CUC" 918..920 "GAG"
S56 881..882 "UG" 916..917 "CA"
S57 885..891 "AACCCUA" 896..902 "UAGGGCU"
S58 904..907 "CUCC" 912..915 "GGAG"
S59 926..927 "GG" 1086..1087 "CC"
S60 929..931 "GGU" 1083..1085 "GCC"
S61 934..935 "UG" 1080..1081 "CA"
S62 936..938 "GUU" 1074..1076 "AAU"
S63 939..942 "GUCG" 1069..1072 "CGUC"
S64 946..951 "GCUCGU" 980..985 "ACGAGC"
S65 952..954 "GUC" 958..960 "GAU"
S66 963..966 "UGGG" 973..976 "CCCG"
S67 991..994 "CCUU" 1061..1064 "GGGG"
S68 996..1002 "AUCUUAG" 1026..1032 "CUAAGGU"
S69 1006..1007 "CC" 1020..1021 "GG"
S70 1010..1011 "CA" 1018..1019 "UG"
S71 1035..1042 "CUGCCGGU" 1048..1055 "ACCGGAGG"
S72 1119..1124 "GACGGU" 1168..1173 "ACCGUU"
S73 1130..1132 "GAG" 1160..1162 "CUC"
S74 1135..1136 "GC" 1154..1155 "GC"
S75 1141..1143 "ACC" 1148..1150 "GGU"
S76 1184..1190 "UGUAGGC" 1199..1205 "GCCUACA"
H1 26..29 "GAAA" (25,30) G:C 
H2 61..64 "GCAU" (60,65) C:G 
H3 86..89 "UUCG" (85,90) C:G 
H4 136..142 "GUAACGG" (135,143) G:C 
H5 173..176 "GAGA" (172,177) U:G 
H6 198..207 "CUGAGACACG" (197,208) A:G 
H7 219..222 "UACG" (218,223) C:G 
H8 256..259 "GAAA" (255,260) C:G 
H9 296..299 "UUCG" (295,300) U:G 
H10 340..346 "UGAAUAA" (339,347) U:G 
H11 400..403 "AGCC" (399,404) C:G PK{1}
H12 495..499 "CUCAA" (494,500) G:C 
H13 568..573 "GUGAAA" (567,574) G:U 
H14 604..607 "GAAG" (603,608) C:G 
H15 664..672 "AUUAGAUAC" (663,673) G:C 
H16 718..723 "UUUCCG" (717,724) G:C 
H17 741..744 "UAAC" (740,745) U:G PK{2}
H18 776..779 "GCAA" (775,780) C:G 
H19 842..849 "AAGCAACG" (841,850) G:C 
H20 892..895 "GAGA" (891,896) A:U 
H21 908..911 "UUCG" (907,912) C:G 
H22 955..957 "UGA" (954,958) C:G 
H23 967..972 "UUAAGU" (966,973) G:C 
H24 1012..1017 "UUAAGU" (1011,1018) A:U 
H25 1043..1047 "GACAA" (1042,1048) U:A 
H26 1144..1147 "GCGA" (1143,1148) C:G 
H27 1191..1198 "UGCAACUC" (1190,1199) C:G 
B1 119..120 "AU" (118,160) C:G (159,121) U:U 
B2 126..127 "AG" (125,151) U:G (150,128) A:U 
B3 181..183 "GAU" (180,169) U:A (168,184) G:C 
B4 244..244 "U" (243,269) U:A (268,245) G:C 
B5 382..387 "GGCUAA" (381,418) C:G (417,388) G:C PK{1}
B6 460..460 "A" (459,635) C:G (634,461) U:G 
B7 472..472 "A" (471,522) U:G (521,473) U:A 
B8 518..519 "AA" (517,475) A:U (474,520) G:C 
B9 600..601 "UG" (599,610) G:G (609,602) C:G 
B10 625..625 "A" (624,535) U:G (534,626) U:A 
B11 652..654 "GGA" (651,682) G:C (681,655) U:G 
B12 880..880 "C" (879,918) C:G (917,881) A:U 
B13 921..921 "U" (920,877) G:C (876,922) C:G 
B14 928..928 "U" (927,1086) G:C (1085,929) C:G 
B15 1008..1009 "AU" (1007,1020) C:G (1019,1010) G:C 
B16 1073..1073 "A" (1072,939) C:G (938,1074) U:A 
B17 1077..1079 "CAU" (1076,936) U:G (935,1080) G:C 
B18 1104..1104 "A" (1103,832) C:G (831,1105) G:C 
I1.1 15..18 "GAUA" (14,42) G:C 
I1.2 37..41 "UAAUA" (36,19) C:A 
I2.1 122..122 "A" (121,159) U:U 
I2.2 154..158 "AACGA" (153,123) C:G 
I3.1 248..250 "CAA" (247,266) G:C 
I3.2 265..265 "A" (264,251) G:U 
I4.1 286..291 "GAUGAA" (285,309) U:A 
I4.2 304..308 "GUAAA" (303,292) C:G 
I5.1 315..316 "UU" (314,373) G:G 
I5.2 371..372 "CA" (370,317) C:G 
I6.1 323..325 "GAA" (322,365) G:C 
I6.2 361..364 "GGUA" (360,326) C:G 
I7.1 327..331 "AACAA" (326,360) G:C 
I7.2 355..359 "CUUGA" (354,332) C:G 
I8.1 393..397 "UGCCA" (392,413) G:C 
I8.2 406..412 "GGUAAUA" (405,398) C:G 
I9.1 458..458 "G" (457,638) C:G 
I9.2 636..637 "AG" (635,459) G:C 
I10.1 484..487 "AAAG" (483,509) G:G 
I10.2 507..508 "GG" (506,488) A:C 
I11.1 541..542 "GA" (540,619) A:G 
I11.2 618..618 "G" (617,543) U:G 
I12.1 552..553 "AA" (551,593) G:A 
I12.2 591..592 "GA" (590,554) G:U 
I13.1 562..564 "GUA" (561,583) U:A 
I13.2 577..582 "GUAGAG" (576,565) C:G 
I14.1 657..659 "AAA" (656,680) C:G 
I14.2 677..679 "GUA" (676,660) G:C 
I15.1 766..771 "GAGUAC" (765,788) G:C 
I15.2 784..787 "GAAA" (783,772) U:G 
I16.1 932..933 "CA" (931,1083) U:G 
I16.2 1082..1082 "U" (1081,934) A:U 
I17.1 1003..1005 "UUG" (1002,1026) G:C 
I17.2 1022..1025 "CACU" (1021,1006) G:C 
I18.1 1125..1129 "ACAAA" (1124,1168) U:A 
I18.2 1163..1167 "AUAAA" (1162,1130) C:G 
I19.1 1133..1134 "CU" (1132,1160) G:C 
I19.2 1156..1159 "UAAU" (1155,1135) C:G 
I20.1 1137..1140 "CAAG" (1136,1154) C:G 
I20.2 1151..1153 "GGA" (1150,1141) U:A 
M1.1 10..10 "A" (9,97) G:C (45,11) G:C 
M1.2 46..50 "AUAAC" (45,11) G:C (74,51) U:A 
M1.3 75..79 "UGAAA" (74,51) U:A (95,80) C:G 
M1.4 96..96 "A" (95,80) C:G (9,97) G:C 
M2.1 447..453 "GUAAAGC" (446,759) C:G (641,454) C:G PK{2}
M2.2 642..645 "GAAA" (641,454) C:G (687,646) C:G 
M2.3 688..697 "CGUAAACGAU" (687,646) C:G (757,698) C:G 
M2.4 758..758 "C" (757,698) C:G (446,759) C:G 
M3.1 464..464 "G" (463,632) U:A (528,465) C:G 
M3.2 529..531 "UUG" (528,465) C:G (628,532) U:A 
M3.3 629..631 "GAC" (628,532) U:A (463,632) U:A 
M4.1 550..549 "" (549,611) U:A (594,550) C:G 
M4.2 595..598 "ACCA" (594,550) C:G (610,599) G:G 
M4.3 611..610 "" (610,599) G:G (549,611) U:A 
M5.1 704..705 "UA" (703,752) C:G (735,706) U:A 
M5.2 736..738 "GAA" (735,706) U:A (746,739) C:G 
M5.3 747..751 "AUUAA" (746,739) C:G (703,752) C:G 
M6.1 834..837 "UUAA" (833,1102) U:A (853,838) A:U 
M6.2 854..861 "GAACCUUA" (853,838) A:U (1098,862) G:C 
M6.3 1099..1101 "GCU" (1098,862) G:C (833,1102) U:A 
M7.1 869..873 "UUGAC" (868,1092) C:G (924,874) C:A 
M7.2 925..925 "A" (924,874) C:A (1087,926) C:G 
M7.3 1088..1091 "UUAU" (1087,926) C:G (868,1092) C:G 
M8.1 883..884 "AA" (882,916) G:C (902,885) U:A 
M8.2 903..903 "U" (902,885) U:A (915,904) G:C 
M8.3 916..915 "" (915,904) G:C (882,916) G:C 
M9.1 943..945 "UCA" (942,1069) G:C (985,946) C:G 
M9.2 986..990 "GCAAC" (985,946) C:G (1064,991) G:C 
M9.3 1065..1068 "AUGA" (1064,991) G:C (942,1069) G:C 
M10.1 952..951 "" (951,980) U:A (960,952) U:G 
M10.2 961..962 "GU" (960,952) U:G (976,963) G:U 
M10.3 977..979 "CCA" (976,963) G:U (951,980) U:A 
M11.1 995..995 "G" (994,1061) U:G (1032,996) U:A 
M11.2 1033..1034 "GA" (1032,996) U:A (1055,1035) G:C 
M11.3 1056..1060 "AAGGU" (1055,1035) G:C (994,1061) U:G 
X1 104..115 "AUGGACCCGCGU" (103,3) G:C (162,116) G:C 
X2 163..164 "UA" (162,116) G:C (187,165) C:G 
X3 188..191 "CACA" (187,165) C:G (213,192) G:C 
X5 227..242 "GCAGCAGUAGGGAAUC" (226,215) G:C (269,243) A:U 
X6 270..281 "GCAACGCCGCGU" (269,243) A:U (312,282) C:G 
X8 374..376 "AAA" (373,314) G:G (422,377) C:G 
X9 423..443 "AAGCGUUAUCCGGAAUUAUUG" (422,377) C:G (761,444) C:G 
X11 791..821 "AAAGGAAUUGACGGGGGCCCGCACAAGCGGU" (790,763) C:G (1114,822) C:G 
X12 1115..1118 "AAUG" (1114,822) C:G (1173,1119) U:G 
X13 1174..1183 "CAGUUCGGAU" (1173,1119) U:G (1205,1184) A:U 
E1 1..2 "GC" 
PK1 3bp 382..384 401..403 B5 382..387 H11 400..403
PK2 2bp 447..448 743..744 M2.1 447..453 H17 741..744
PK1.1 382 G 403 C
PK1.2 383 G 402 C
PK1.3 384 C 401 G
PK2.1 447 G 744 C
PK2.2 448 U 743 A
NCBP1 540 A 619 G S35
NCBP2 940 U 1071 U S63
NCBP3 488 C 506 A S33
NCBP4 874 A 924 C S54
NCBP5 197 A 208 G S12
NCBP6 483 G 509 G S32
NCBP7 886 A 901 C S57
NCBP8 314 G 373 G S19
NCBP9 1037 G 1053 A S71
NCBP10 599 G 610 G S40
NCBP11 714 A 727 C S46
NCBP12 551 G 593 A S37
NCBP13 823 G 1113 A S50
NCBP14 121 U 159 U S7
NCBP15 482 U 510 U S32
NCBP16 19 A 36 C S3
segment1 7bp 3..9 CCAUAAG 97..103 CUUAUGG
segment2 11bp 11..25 CUGGGAUAACUCCGG 30..45 CCGGGGCUAAUACCGG
segment3 10bp 51..60 AUUUUGAACC 65..74 GGUUCGAAAU
segment4 6bp 80..85 GGCGGC 90..95 GCUGUC
segment5 15bp 116..135 CGCAUUAGCUAGUUGGUGAG 143..162 CUCACCAAGGCAACGAUGCG
segment6 8bp 165..172 GCCGACCU 177..187 GGGUGAUCGGC
segment7 6bp 192..197 CUGGGA 208..213 GCCCAG
segment8 4bp 215..218 CUCC 223..226 GGAG
segment9 9bp 243..255 UUCCGCAAUGGAC 260..269 GUCUGACGGA
segment10 8bp 282..295 GAGUGAUGAAGGCU 300..312 GGUCGUAAAACUC
segment11 16bp 314..339 GUUGUUAGGGAAGAACAAGUGCUAGU 347..373 GCUGGCACCUUGACGGUACCUAACCAG
segment12 12bp 377..399 GCCACGGCUAACUACGUGCCAGC 404..422 GCGGUAAUACGUAGGUGGC
segment13 3bp 444..446 GGC 759..761 GCC
segment14 8bp 454..463 GCGCGCAGGU 632..641 ACUGAGGCGC
segment15 25bp 465..494 GUUUCUUAAGUCUGAUGUGAAAGCCCACGG 500..528 CCGUGGAGGGUCAUUGGAAACUGGGAGAC
segment16 16bp 532..549 AGUGCAGAAGAGGAAAGU 611..628 ACUUUCUGGUCUGUAACU
segment17 13bp 550..567 GGAAUUCCAUGUGUAGCG 574..594 UGCGUAGAGAUAUGGAGGAAC
segment18 3bp 599..603 GUGGC 608..610 GCG
segment19 12bp 646..663 GCGUGGGGAGCAAACAGG 673..687 CCUGGUAGUCCACGC
segment20 6bp 698..703 GAGUGC 752..757 GCACUC
segment21 12bp 706..717 AGUGUUAGAGGG 724..735 CCCCUUAGUGCU
segment22 2bp 739..740 GU 745..746 GC
segment23 7bp 763..775 GGGGAGUACGGCC 780..790 GGCUGAAACUC
segment24 12bp 822..833 GGAGCAUGUGGU 1102..1114 ACACACGUGCUAC
segment25 4bp 838..841 UUCG 850..853 CGAA
segment26 7bp 862..868 CCAGGUC 1092..1098 GACCUGG
segment27 8bp 874..882 AUCCUCCUG 916..924 CAGAGUGAC
segment28 7bp 885..891 AACCCUA 896..902 UAGGGCU
segment29 4bp 904..907 CUCC 912..915 GGAG
segment30 14bp 926..942 GGUGGUCAUGGUUGUCG 1069..1087 CGUCAAAUCAUCAUGCCCC
segment31 6bp 946..951 GCUCGU 980..985 ACGAGC
segment32 3bp 952..954 GUC 958..960 GAU
segment33 4bp 963..966 UGGG 973..976 CCCG
segment34 4bp 991..994 CCUU 1061..1064 GGGG
segment35 11bp 996..1011 AUCUUAGUUGCCAUCA 1018..1032 UGGGCACUCUAAGGU
segment36 8bp 1035..1042 CUGCCGGU 1048..1055 ACCGGAGG
segment37 14bp 1119..1143 GACGGUACAAAGAGCUGCCAAGACC 1148..1173 GGUGGAGCUAAUCUCAUAAAACCGUU
segment38 7bp 1184..1190 UGUAGGC 1199..1205 GCCUACA
