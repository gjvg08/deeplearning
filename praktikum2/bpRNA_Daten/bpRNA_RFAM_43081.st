#Name: bpRNA_RFAM_43081
#Length:  1331 
#PageNumber: 2
CACCAGGUUGAUUCUGCCUGACUGUGACGCUAGCCUCGGGGAUUAAGCCAUGCAAGUCUGUGAAGUUUCGUUAUGAAACAGUGAACGGCUCAGUAAAACACGUCUAUCUACCCACUUGUCUAAUAUAACCCCGGGAAACUGUGGCUAAUAUUGUGGAUGAGGGCGUGACCUAUCAGCUAGUCGGUACGGUAAAGGCGUACGGAGGCGAUGACGGGUAACGGGGAAUAUGGGUUUUAUUCCGGAGAGGGAGCCUGAGAGAUGGCUACCACUCCAAGGACGGCAGAGCGCGAAACUUACCCAAUGAACAUUGAGGUAGUUACGGGGUGUGACAUUCUAUGACCGUGUGUUAAAAGCAGGGUUAAGACAUUGGAGGGCAAGUCUGGUGCCAGCAGCCGCGGUAAUUUCCAGCUCCAAUAGCGUCUGUGUUUAUUGCUGCGGUUAAAACGUGCGUAGUCUGGUCUAUGGCUUGGGUGUAAUCCGUGCUUUUCUAGUACGGGUUAUUUCAGGUACCUAGACCUUGAGACGGGGAUGUGGUAAUGUUAUUUGGUAGCGAGAGGUGAAAUUCGAUGACCUACCAAGGAGCAACAGAGGCGAAAGCGAUUACCAGGAACCGAUCUGACGAUCAAGCGCGUGAGCAGGAUUAUCGAAGAGGAUUAGAGACCCACGUAUUCCUUGCAGUAAACGAUGCCUACAGUGUGGUGCUUAAUUGCAUUGCGCGAACGAAAGCUAGUGUAUGGGCUCCGGGGAUAGUACGGACGCAAGUUUAAACUUGAAGAAAUUGACGGAAGGACACCACAAGGAGUGGAGUGUGCGGGUUAAUUUGACUCAACGCGGAAACCUUACCCGAGCAAGUAAUAGUCCUAAGAUGUAUGUGCAGAGCAGAGCAUUACCGUGGUGCAUGGCCGUCGUUAACACCUGGAGUGAUCUGUCUGAUUAAAUUCGGUAACGUGUGAGAGGUUGAGCCCGAUGAACUAGCUUGCACAAUAGCUACAUAGUUGGGGACUGCAUGUGUGAGCAUGAGGAAGUAGCGCCCGAUAACAGGUCUGUGAUGCCCGUAGAUGUCCGGGGCUCCACGCGCACUACAAUGAGGUGUGGUUUAUCUUAGUGUUUAACACCUCGUAGUUGGGAUUGAUGCUUGUAACAGUGUCAUGAACGUGGAAUUCCUAGUAGUUGGUAGUCACUAACGACUAACGAAUGCGUCCCUGUUCUUUGUACACACCGCCCGUCGUUAUCUAAGAUGGAAGUGCGGGUGAAGACACGAUGGCAGUAGGCUAGGGUAACUUAGUCUAUAGUGUAAAAACGGGCCUUAGUGGAAGAAUCCGUGCACGUAA
...(((((.......))))).((((((((((.(((((((((.....(((.(((..((.(((...((......))...)))))......(((.......[((((((.((...(((((....(.(....(((((((....))))))).....)).)))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))...((((((((....))))...))))))).((((((..........)))))).(.(....))].)))).)).).......(((...(((....)).)))).)).)))))).............((((((.........)))))).......((((((.......((((.....((....))........)))))))))).))))))))))..........(((.......((((...(((.................................................................(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...)))...))))....((((((...((...((((.........))))...))))))))..........((((((.((((((((((....))))))))))..((....))..)))....))).))).(((......((((....))))...)))........(((((.(((((((..((..(((((((((((((((((....(.((........)))........(((((((.....((((((......((((..)))).....))))))...((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((..............................((....))....((((((((....)))))))).....))))....)))).)))...).))))))).....)))))))...)).))))))))))...((((((....................))))))......(...((((((((......))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).))..........(.((((((((..(((((((((((((((.((((...)))).)))))))))))))..))))))))..)....))......(.....((((....)))))........
EEESSSSSHHHHHHHSSSSSXSSSSSSSSSSMSSSSSSSSSIIIIISSSBSSSMMSSBSSSIIISSHHHHHHSSIIISSSSSMMMMMMSSSMMMMMMMMSSSSSSBSSBBBSSSSSIIIISBSIIIISSSSSSSHHHHSSSSSSSIIIIISSISSSBBSSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSBSHHHHSSMMSSSSBSSISMMMMMMMSSSBBBSSSHHHHSSBSSSSMSSBSSSSSSMMMMMMMMMMMMMSSSSSSHHHHHHHHHSSSSSSMMMMMMMSSSSSSBBBBBBBSSSSIIIIISSHHHHSSIIIIIIIISSSSSSSSSSMSSSSSSSSSSXXXXXXXXXXSSSMMMMMMMSSSSIIISSSIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIISSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSIIISSSIIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMSSSSSSSSSSHHHHSSSSSSSSSSMMSSHHHHSSMMSSSBBBBSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIISSSXXXXXXXXSSSSSBSSSSSSSIISSMMSSSSSSSSSSSSSSSSSMMMMSBSSHHHHHHHHSSSMMMMMMMMSSSSSSSMMMMMSSSSSSIIIIIISSSSHHSSSSIIIIISSSSSSMMMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSHHHHSSMMMMSSSSSSSSHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSBSSSSSSSMMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSHHHHHHHHHHHHHHHHHHHHSSSSSSMMMMMMSIIISSSSSSSSHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXXXXXXXXXSBSSSSSSSSBBSSSSSSSSSSSSSSSISSSSHHHSSSSISSSSSSSSSSSSSBBSSSSSSSSBBSBBBBSSXXXXXXSBBBBBSSSSHHHHSSSSSEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 4..8 "CAGGU" 16..20 "GCCUG"
S2 22..31 "CUGUGACGCU" 416..425 "AGCGUCUGUG"
S3 33..38 "GCCUCG" 320..325 "CGGGGU"
S4 39..40 "GG" 317..318 "UU"
S5 41..41 "G" 288..288 "C"
S6 47..48 "GC" 285..286 "GC"
S7 49..49 "C" 283..283 "G"
S8 51..53 "UGC" 280..282 "GCA"
S9 56..57 "GU" 81..82 "GU"
S10 59..61 "UGU" 78..80 "ACA"
S11 65..66 "GU" 73..74 "AU"
S12 89..91 "CUC" 242..244 "GAG"
S13 100..105 "ACGUCU" 163..168 "GCGUGA"
S14 107..108 "UC" 161..162 "GG"
S15 112..113 "CC" 159..160 "GA"
S16 114..116 "ACU" 154..156 "UGG"
S17 121..121 "U" 152..152 "U"
S18 123..123 "A" 151..151 "U"
S19 128..134 "ACCCCGG" 139..145 "CUGUGGC"
S20 169..171 "CCU" 213..215 "GGG"
S21 174..174 "C" 212..212 "C"
S22 176..178 "GCU" 204..206 "GGC"
S23 181..188 "UCGGUACG" 196..203 "CGUACGGA"
S24 219..222 "CGGG" 238..241 "UCCG"
S25 223..226 "GAAU" 231..234 "GUUU"
S26 246..251 "GGGAGC" 262..267 "GCUACC"
S27 269..269 "C" 277..277 "A"
S28 271..271 "C" 276..276 "G"
S29 296..298 "ACC" 313..315 "GUA"
S30 302..302 "U" 312..312 "G"
S31 303..304 "GA" 309..310 "UG"
S32 339..344 "ACCGUG" 354..359 "CAGGGU"
S33 367..372 "UUGGAG" 409..414 "CUCCAA"
S34 380..383 "CUGG" 405..408 "CCAG"
S35 389..390 "GC" 395..396 "GC"
S36 436..438 "CGG" 741..743 "CCG"
S37 446..449 "GUGC" 627..630 "GCGC"
S38 453..455 "GUC" 621..623 "GAU"
S39 521..523 "AGA" 615..617 "UCU"
S40 524..529 "CGGGGA" 608..613 "GAACCG"
S41 532..538 "UGGUAAU" 600..606 "AUUACCA"
S42 539..540 "GU" 582..583 "GC"
S43 543..550 "UUUGGUAG" 572..579 "CUACCAAG"
S44 554..556 "GAG" 563..565 "UUC"
S45 588..588 "G" 599..599 "G"
S46 591..592 "GC" 597..598 "GC"
S47 635..640 "GCAGGA" 671..676 "CCUUGC"
S48 644..645 "UC" 669..670 "UU"
S49 649..652 "GAGG" 662..665 "CCAC"
S50 687..689 "GCC" 737..739 "GGC"
S51 690..692 "UAC" 730..732 "GUG"
S52 694..703 "GUGUGGUGCU" 708..717 "UGCAUUGCGC"
S53 720..721 "AC" 726..727 "GC"
S54 745..747 "GGA" 769..771 "CUU"
S55 754..757 "GGAC" 762..765 "GUUU"
S56 780..781 "UG" 1215..1216 "CA"
S57 782..784 "ACG" 1211..1213 "UGU"
S58 786..792 "AAGGACA" 1204..1210 "UGUUCUU"
S59 795..796 "AC" 1199..1200 "GU"
S60 799..803 "GGAGU" 1160..1164 "AUUCC"
S61 804..813 "GGAGUGUGCG" 1074..1083 "CGCGCACUAC"
S62 814..815 "GG" 1071..1072 "CC"
S63 820..820 "U" 834..834 "G"
S64 822..823 "UG" 832..833 "CG"
S65 843..849 "CCCGAGC" 1061..1067 "GUCCGGG"
S66 855..860 "AUAGUC" 882..887 "GAGCAU"
S67 867..870 "UGUA" 873..876 "UGCA"
S68 891..892 "CG" 1054..1055 "CG"
S69 894..897 "GGUG" 1050..1053 "UGCC"
S70 900..900 "U" 1049..1049 "A"
S71 901..901 "G" 1047..1047 "U"
S72 902..904 "GCC" 1041..1043 "GGU"
S73 905..908 "GUCG" 1036..1039 "UAAC"
S74 912..917 "ACACCU" 946..951 "ACGUGU"
S75 918..920 "GGA" 925..927 "UCU"
S76 930..933 "CUGA" 940..943 "UCGG"
S77 957..960 "GUUG" 1028..1031 "GCGC"
S78 991..992 "CA" 997..998 "UG"
S79 1003..1010 "CUGCAUGU" 1015..1022 "GCAUGAGG"
S80 1087..1092 "GAGGUG" 1113..1118 "CACCUC"
S81 1125..1125 "G" 1154..1154 "C"
S82 1129..1136 "UUGAUGCU" 1143..1150 "AGUGUCAU"
S83 1170..1177 "GUUGGUAG" 1185..1192 "CGACUAAC"
S84 1227..1227 "C" 1298..1298 "A"
S85 1229..1229 "U" 1297..1297 "A"
S86 1230..1230 "U" 1292..1292 "G"
S87 1231..1236 "AUCUAA" 1284..1289 "AGUCUA"
S88 1239..1240 "UG" 1282..1283 "UU"
S89 1241..1253 "GAAGUGCGGGUGA" 1267..1279 "GUAGGCUAGGGUA"
S90 1255..1258 "GACA" 1262..1265 "UGGC"
S91 1305..1305 "C" 1323..1323 "U"
S92 1311..1314 "UGGA" 1319..1322 "UCCG"
H1 9..15 "UGAUUCU" (8,16) U:G 
H2 67..72 "UUCGUU" (66,73) U:A 
H3 135..138 "GAAA" (134,139) G:C 
H4 189..195 "GUAAAGG" (188,196) G:C 
H5 227..230 "AUGG" (226,231) U:G 
H6 252..261 "CUGAGAGAUG" (251,262) C:G 
H7 272..275 "CAAG" (271,276) C:G 
H8 305..308 "ACAU" (304,309) A:U 
H9 345..353 "UGUUAAAAG" (344,354) G:C 
H10 391..394 "AGCC" (390,395) C:G 
H11 557..562 "GUGAAA" (556,563) G:U 
H12 593..596 "GAAA" (592,597) C:G 
H13 653..661 "AUUAGAGAC" (652,662) G:C 
H14 704..707 "UAAU" (703,708) U:U 
H15 722..725 "GAAA" (721,726) C:G 
H16 758..761 "GCAA" (757,762) C:G 
H17 824..831 "ACUCAACG" (823,832) G:C 
H18 871..872 "UG" (870,873) A:U 
H19 921..924 "GUGA" (920,925) A:U 
H20 934..939 "UUAAAU" (933,940) A:U 
H21 993..996 "UAGU" (992,997) A:U 
H22 1011..1014 "GUGA" (1010,1015) U:G 
H23 1093..1112 "UGGUUUAUCUUAGUGUUUAA" (1092,1113) G:C 
H24 1137..1142 "UGUAAC" (1136,1143) U:A 
H25 1178..1184 "UCACUAA" (1177,1185) G:C 
H26 1259..1261 "CGA" (1258,1262) A:U 
H27 1315..1318 "AGAA" (1314,1319) A:U 
B1 50..50 "A" (49,283) C:G (282,51) A:U 
B2 58..58 "C" (57,81) U:G (80,59) A:U 
B3 106..106 "A" (105,163) U:G (162,107) G:U 
B4 109..111 "UAC" (108,161) C:G (160,112) A:C 
B5 122..122 "A" (121,152) U:U (151,123) U:A 
B6 157..158 "AU" (156,114) G:A (113,159) C:G 
B7 172..173 "AU" (171,213) U:G (212,174) C:C 
B8 179..180 "AG" (178,204) U:G (203,181) A:U 
B9 235..237 "UAU" (234,223) U:G (222,238) G:U 
B10 270..270 "U" (269,277) C:A (276,271) G:C 
B11 284..284 "A" (283,49) G:C (48,285) C:G 
B12 299..301 "CAA" (298,313) C:G (312,302) G:U 
B13 311..311 "A" (310,303) G:G (302,312) U:G 
B14 319..319 "A" (318,39) U:G (38,320) G:C 
B15 373..379 "GGCAAGU" (372,409) G:C (408,380) G:C 
B16 589..590 "AG" (588,599) G:G (598,591) C:G 
B17 614..614 "A" (613,524) G:C (523,615) A:U 
B18 641..643 "UUA" (640,671) A:C (670,644) U:U 
B19 733..736 "UAUG" (732,690) G:U (689,737) C:G 
B20 785..785 "G" (784,1211) G:U (1210,786) U:A 
B21 821..821 "U" (820,834) U:G (833,822) G:U 
B22 893..893 "U" (892,1054) G:C (1053,894) C:G 
B23 898..899 "CA" (897,1050) G:U (1049,900) A:U 
B24 1040..1040 "A" (1039,905) C:G (904,1041) C:G 
B25 1044..1046 "CUG" (1043,902) U:G (901,1047) G:U 
B26 1048..1048 "G" (1047,901) U:G (900,1049) U:A 
B27 1073..1073 "A" (1072,814) C:G (813,1074) G:C 
B28 1214..1214 "A" (1213,782) U:A (781,1215) G:C 
B29 1228..1228 "G" (1227,1298) C:A (1297,1229) A:U 
B30 1237..1238 "GA" (1236,1284) A:A (1283,1239) U:U 
B31 1280..1281 "AC" (1279,1241) A:G (1240,1282) G:U 
B32 1290..1291 "UA" (1289,1231) A:A (1230,1292) U:G 
B33 1293..1296 "UGUA" (1292,1230) G:U (1229,1297) U:A 
B34 1306..1310 "CUUAG" (1305,1323) C:U (1322,1311) G:U 
I1.1 42..46 "AUUAA" (41,288) G:C 
I1.2 287..287 "G" (286,47) C:G 
I2.1 62..64 "GAA" (61,78) U:A 
I2.2 75..77 "GAA" (74,65) U:G 
I3.1 117..120 "UGUC" (116,154) U:U 
I3.2 153..153 "G" (152,121) U:U 
I4.1 124..127 "UAUA" (123,151) A:U 
I4.2 146..150 "UAAUA" (145,128) C:A 
I5.1 175..175 "A" (174,212) C:C 
I5.2 207..211 "GAUGA" (206,176) C:G 
I6.1 384..388 "UGCCA" (383,405) G:C 
I6.2 397..404 "GGUAAUUU" (396,389) C:G 
I7.1 450..452 "GUA" (449,627) C:G 
I7.2 624..626 "CAA" (623,453) U:G 
I8.1 456..520 "UGGUCUAUGGCUUGGGUGUAAUCCGUGCUUUUCUAGUACGGGUUAUUUCAGGUACCUAGACCUUG" (455,621) C:G 
I8.2 618..620 "GAC" (617,521) U:A 
I9.1 530..531 "UG" (529,608) A:G 
I9.2 607..607 "G" (606,532) A:U 
I10.1 541..542 "UA" (540,582) U:G 
I10.2 580..581 "GA" (579,543) G:U 
I11.1 551..553 "CGA" (550,572) G:C 
I11.2 566..571 "GAUGAC" (565,554) C:G 
I12.1 646..648 "GAA" (645,669) C:U 
I12.2 666..668 "GUA" (665,649) C:G 
I13.1 748..753 "UAGUAC" (747,769) A:C 
I13.2 766..768 "AAA" (765,754) U:G 
I14.1 793..794 "CC" (792,1204) A:U 
I14.2 1201..1203 "CCC" (1200,795) U:A 
I15.1 861..866 "CUAAGA" (860,882) C:G 
I15.2 877..881 "GAGCA" (876,867) A:U 
I16.1 1126..1128 "GGA" (1125,1154) G:C 
I16.2 1151..1153 "GAA" (1150,1129) U:U 
I17.1 1254..1254 "A" (1253,1267) A:G 
I17.2 1266..1266 "A" (1265,1255) C:G 
M1.1 32..32 "A" (31,416) U:A (325,33) U:G 
M1.2 326..338 "GUGACAUUCUAUG" (325,33) U:G (359,339) U:A 
M1.3 360..366 "UAAGACA" (359,339) U:A (414,367) A:U 
M1.4 415..415 "U" (414,367) A:U (31,416) U:A 
M2.1 41..40 "" (40,317) G:U (288,41) C:G 
M2.2 289..295 "GAAACUU" (288,41) C:G (315,296) A:A 
M2.3 316..316 "G" (315,296) A:A (40,317) G:U 
M3.1 54..55 "AA" (53,280) C:G (82,56) U:G 
M3.2 83..88 "GAACGG" (82,56) U:G (244,89) G:C 
M3.3 245..245 "A" (244,89) G:C (267,246) C:G 
M3.4 268..268 "A" (267,246) C:G (277,269) A:C 
M3.5 278..279 "CG" (277,269) A:C (53,280) C:G PK{1}
M4.1 92..99 "AGUAAAAC" (91,242) C:G (168,100) A:A PK{1}
M4.2 169..168 "" (168,100) A:A (215,169) G:C 
M4.3 216..218 "UAA" (215,169) G:C (241,219) G:C 
M4.4 242..241 "" (241,219) G:C (91,242) C:G 
M5.1 439..445 "UUAAAAC" (438,741) G:C (630,446) C:G 
M5.2 631..634 "GUGA" (630,446) C:G (676,635) C:G 
M5.3 677..686 "AGUAAACGAU" (676,635) C:G (739,687) C:G 
M5.4 740..740 "U" (739,687) C:G (438,741) G:C 
M6.1 539..538 "" (538,600) U:A (583,539) C:G 
M6.2 584..587 "AACA" (583,539) C:G (599,588) G:G 
M6.3 600..599 "" (599,588) G:G (538,600) U:A 
M7.1 693..693 "A" (692,730) C:G (717,694) C:G 
M7.2 718..719 "GA" (717,694) C:G (727,720) C:A 
M7.3 728..729 "UA" (727,720) C:A (692,730) C:G 
M8.1 797..798 "AA" (796,1199) C:G (1164,799) C:G 
M8.2 1165..1169 "UAGUA" (1164,799) C:G (1192,1170) C:G 
M8.3 1193..1198 "GAAUGC" (1192,1170) C:G (796,1199) C:G 
M9.1 804..803 "" (803,1160) U:A (1083,804) C:G 
M9.2 1084..1086 "AAU" (1083,804) C:G (1118,1087) C:G 
M9.3 1119..1124 "GUAGUU" (1118,1087) C:G (1154,1125) C:G 
M9.4 1155..1159 "GUGGA" (1154,1125) C:G (803,1160) U:A 
M10.1 816..819 "UUAA" (815,1071) G:C (834,820) G:U 
M10.2 835..842 "AAACCUUA" (834,820) G:U (1067,843) G:C 
M10.3 1068..1070 "GCU" (1067,843) G:C (815,1071) G:C 
M11.1 850..854 "AAGUA" (849,1061) C:G (887,855) U:A 
M11.2 888..890 "UAC" (887,855) U:A (1055,891) G:C 
M11.3 1056..1060 "UAGAU" (1055,891) G:C (849,1061) C:G 
M12.1 909..911 "UUA" (908,1036) G:U (951,912) U:A 
M12.2 952..956 "GAGAG" (951,912) U:A (1031,957) C:G 
M12.3 1032..1035 "CCGA" (1031,957) C:G (908,1036) G:U 
M13.1 918..917 "" (917,946) U:A (927,918) U:G 
M13.2 928..929 "GU" (927,918) U:G (943,930) G:C 
M13.3 944..945 "UA" (943,930) G:C (917,946) U:A 
M14.1 961..990 "AGCCCGAUGAACUAGCUUGCACAAUAGCUA" (960,1028) G:G (998,991) G:C 
M14.2 999..1002 "GGGA" (998,991) G:C (1022,1003) G:C 
M14.3 1023..1027 "AAGUA" (1022,1003) G:C (960,1028) G:G 
X2 426..435 "UUUAUUGCUG" (425,22) G:C (743,436) G:C 
X4 772..779 "GAAGAAAU" (771,745) U:G (1216,780) A:U 
X5 1217..1226 "CACCGCCCGU" (1216,780) A:U (1298,1227) A:C 
X6 1299..1304 "AACGGG" (1298,1227) A:C (1323,1305) U:C 
E1 1..3 "CAC" 
E2 1324..1331 "GCACGUAA" 
PK1 1bp 99..99 278..278 M4.1 92..99 M3.5 278..279
PK1.1 99 C 278 C
NCBP1 959 U 1029 C S77
NCBP2 805 G 1082 A S61
NCBP3 1250 G 1270 G S89
NCBP4 1244 G 1276 G S89
NCBP5 1233 C 1287 C S87
NCBP6 527 G 610 A S40
NCBP7 1246 G 1274 A S89
NCBP8 23 U 424 U S2
NCBP9 720 A 727 C S53
NCBP10 1232 U 1288 U S87
NCBP11 342 G 356 G S32
NCBP12 116 U 154 U S16
NCBP13 100 A 168 A S13
NCBP14 1247 C 1273 U S89
NCBP15 644 U 670 U S48
NCBP16 296 A 315 A S29
NCBP17 1129 U 1150 U S82
NCBP18 916 C 947 C S74
NCBP19 1239 U 1283 U S88
NCBP20 907 C 1037 A S73
NCBP21 650 A 664 A S49
NCBP22 112 C 160 A S15
NCBP23 588 G 599 G S45
NCBP24 1305 C 1323 U S91
NCBP25 1236 A 1284 A S87
NCBP26 645 C 669 U S48
NCBP27 1176 A 1186 G S83
NCBP28 128 A 145 C S19
NCBP29 1005 G 1020 A S79
NCBP30 303 G 310 G S31
NCBP31 960 G 1028 G S77
NCBP32 114 A 156 G S16
NCBP33 24 G 423 G S2
NCBP34 858 G 884 G S66
NCBP35 104 C 164 C S13
NCBP36 131 C 142 U S19
NCBP37 747 A 769 C S54
NCBP38 847 A 1063 C S65
NCBP39 1241 G 1279 A S89
NCBP40 174 C 212 C S21
NCBP41 25 U 422 U S2
NCBP42 640 A 671 C S47
NCBP43 1243 A 1277 G S89
NCBP44 1253 A 1267 G S89
NCBP45 269 C 277 A S27
NCBP46 183 G 201 G S23
NCBP47 1234 U 1286 U S87
NCBP48 1235 A 1285 G S87
NCBP49 529 A 608 G S40
NCBP50 857 A 885 C S66
NCBP51 1231 A 1289 A S87
NCBP52 1249 G 1271 G S89
NCBP53 121 U 152 U S17
NCBP54 1227 C 1298 A S84
NCBP55 297 C 314 U S29
NCBP56 1256 A 1264 G S90
NCBP57 703 U 708 U S52
NCBP58 248 G 265 A S26
NCBP59 528 G 609 A S40
NCBP60 99 C 278 C PK1.1
segment1 5bp 4..8 CAGGU 16..20 GCCUG
segment2 10bp 22..31 CUGUGACGCU 416..425 AGCGUCUGUG
segment3 8bp 33..40 GCCUCGGG 317..325 UUACGGGGU
segment4 7bp 41..53 GAUUAAGCCAUGC 280..288 GCAGAGCGC
segment5 7bp 56..66 GUCUGUGAAGU 73..82 AUGAAACAGU
segment6 3bp 89..91 CUC 242..244 GAG
segment7 22bp 100..134 ACGUCUAUCUACCCACUUGUCUAAUAUAACCCCGG 139..168 CUGUGGCUAAUAUUGUGGAUGAGGGCGUGA
segment8 15bp 169..188 CCUAUCAGCUAGUCGGUACG 196..215 CGUACGGAGGCGAUGACGGG
segment9 8bp 219..226 CGGGGAAU 231..241 GUUUUAUUCCG
segment10 6bp 246..251 GGGAGC 262..267 GCUACC
segment11 2bp 269..271 CUC 276..277 GA
segment12 6bp 296..304 ACCCAAUGA 309..315 UGAGGUA
segment13 6bp 339..344 ACCGUG 354..359 CAGGGU
segment14 12bp 367..390 UUGGAGGGCAAGUCUGGUGCCAGC 395..414 GCGGUAAUUUCCAGCUCCAA
segment15 3bp 436..438 CGG 741..743 CCG
segment16 23bp 446..538 GUGCGUAGUCUGGUCUAUGGCUUGGGUGUAAUCCGUGCUUUUCUAGUACGGGUUAUUUCAGGUACCUAGACCUUGAGACGGGGAUGUGGUAAU 600..630 AUUACCAGGAACCGAUCUGACGAUCAAGCGC
segment17 13bp 539..556 GUUAUUUGGUAGCGAGAG 563..583 UUCGAUGACCUACCAAGGAGC
segment18 3bp 588..592 GAGGC 597..599 GCG
segment19 12bp 635..652 GCAGGAUUAUCGAAGAGG 662..676 CCACGUAUUCCUUGC
segment20 6bp 687..692 GCCUAC 730..739 GUGUAUGGGC
segment21 10bp 694..703 GUGUGGUGCU 708..717 UGCAUUGCGC
segment22 2bp 720..721 AC 726..727 GC
segment23 7bp 745..757 GGAUAGUACGGAC 762..771 GUUUAAACUU
segment24 14bp 780..796 UGACGGAAGGACACCAC 1199..1216 GUCCCUGUUCUUUGUACA
segment25 5bp 799..803 GGAGU 1160..1164 AUUCC
segment26 12bp 804..815 GGAGUGUGCGGG 1071..1083 CCACGCGCACUAC
segment27 3bp 820..823 UUUG 832..834 CGG
segment28 7bp 843..849 CCCGAGC 1061..1067 GUCCGGG
segment29 10bp 855..870 AUAGUCCUAAGAUGUA 873..887 UGCAGAGCAGAGCAU
segment30 15bp 891..908 CGUGGUGCAUGGCCGUCG 1036..1055 UAACAGGUCUGUGAUGCCCG
segment31 6bp 912..917 ACACCU 946..951 ACGUGU
segment32 3bp 918..920 GGA 925..927 UCU
segment33 4bp 930..933 CUGA 940..943 UCGG
segment34 4bp 957..960 GUUG 1028..1031 GCGC
segment35 2bp 991..992 CA 997..998 UG
segment36 8bp 1003..1010 CUGCAUGU 1015..1022 GCAUGAGG
segment37 6bp 1087..1092 GAGGUG 1113..1118 CACCUC
segment38 9bp 1125..1136 GGGAUUGAUGCU 1143..1154 AGUGUCAUGAAC
segment39 8bp 1170..1177 GUUGGUAG 1185..1192 CGACUAAC
segment40 28bp 1227..1258 CGUUAUCUAAGAUGGAAGUGCGGGUGAAGACA 1262..1298 UGGCAGUAGGCUAGGGUAACUUAGUCUAUAGUGUAAA
segment41 5bp 1305..1314 CCUUAGUGGA 1319..1323 UCCGU
