#Name: bpRNA_CRW_7800
#Length:  1175 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
CGAACGCUGGCGGCAGGCCUAACACAUGCAAGUCGAGCCUUAGCGGCGGACGGGUGAGUAACGCGUGGGAACGUGCCCUUCACUACGGAAUAGUCCCGGGAAACUGGGUUUAAUACCGUAUACGCCCUUCGGGGGAAAGAAUUUCGGUGAAGGAUCGGCCCGCGUUAGAUUAGGUAGUUGGUGGGGUAAUGGCCUACCAAGCCUACGAUCUAUAGCUGGUUUGAGAGGAUGAUCAGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGAGGAAUCUUAGACAAUGGGGGCAACCCUGAUCUAGCCAUGCCGCGUGAACGAUGAAGGCCUUAGGGUUGUAAAGUUCUUUCGCCUGGGAAGAUAAUGACGGUACCAGGUAAAGAAGCGGAAUUACUGGGCGUAAAGCGCGCGUAGGCGGGCUGUUAAGUCAGAGGUGAAAUNCCAGGGCUCAACCCUNGAACUGCCUUUGAUACUGGCAGCCUUGAGGUCUGGAGAGGUGAGUGGAAUUCCGAGUGUAGAGGUGAAAUUCGUAGAUAUUCGGAAGAACACCAGUGGCGAAGGCGGCUCACUGGCCAGAUACUGACGCUGAGGUGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAAUGCCAGUCGUCGGGCAGCAUGNCUGUUCGGUGACACACCUAACGGAGUAAGCAUUCCGCCUGGGGAGUACGGCCGCAAGGUUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGCAGAACCUUACCAACCCUUNGACAUCCCUGUCGACAGGUGACAGGUGCUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUCGGUUAAGUCCGGCAACGAGCGCAACCCACGUCCUUAGUUGCCAGCAUUCAGUUNGGGCACUCUAAGGAAACUGCCGAUGAUAAGUCGGAGGAAGGUGUGGAUGACGUCAAGUCCUCAUGGCCCUUACGGGUUGGGCUACACACGUGCUACAAUGGCAGUGACAAUGGGUUAAUCCCAAAAANACUGUCUCAGUUCGGAUUGUCCUCUGCAACUCGAGGGCAUGAAGUCGGAAUCGCUAGUAAUCGCGUAACAGCAUGACGCGGUGAAUACGU
.........(((((((((....(((.(((..(((..((....)))))......(((......((((((((.((...(((((((.((((....(((((((....))))))).....)))).....(((....)))....((...)).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).)).))))))..((((......((((....)))).....)))).[.(((((((...(.......)....))))))..])..............((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.....))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))......................((..(((((((((((((((((....((((........))))........(((((((......(((((((....)))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((.......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((.....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))
EEEEEEEEESSSSSSSSSIIIISSSBSSSMMSSSBBSSHHHHSSSSSMMMMMMSSSMMMMMMSSSSSSSSBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSHHHHSSSMMMMSSHHHSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSBSSSSSSXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHSIIIISSSSSSBBBSXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXSSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMMSSSSSSSHHHHSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSHHHHHSSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 10..15 "GCGGCA" 324..329 "UGCCGC"
S2 16..17 "GG" 321..322 "CC"
S3 18..18 "C" 287..287 "G"
S4 23..25 "CAC" 283..285 "GUG"
S5 27..29 "UGC" 280..282 "GCA"
S6 32..34 "GUC" 45..47 "GGC"
S7 37..38 "GC" 43..44 "GC"
S8 54..56 "GUG" 238..240 "CAC"
S9 63..70 "GCGUGGGA" 158..165 "GCCCGCGU"
S10 72..73 "CG" 156..157 "CG"
S11 77..83 "CCUUCAC" 147..153 "GUGAAGG"
S12 85..88 "ACGG" 116..119 "CCGU"
S13 93..99 "GUCCCGG" 104..110 "CUGGGUU"
S14 125..127 "CCC" 132..134 "GGG"
S15 139..140 "GA" 144..145 "UC"
S16 166..168 "UAG" 210..212 "CUA"
S17 171..171 "U" 209..209 "U"
S18 173..175 "GGU" 201..203 "GCC"
S19 178..185 "UUGGUGGG" 193..200 "CCUACCAA"
S20 215..218 "GCUG" 234..237 "CAGC"
S21 219..222 "GUUU" 227..230 "GGAU"
S22 242..247 "CUGGGA" 258..263 "GCCCAG"
S23 265..268 "CUCC" 273..276 "GGAG"
S24 293..293 "U" 319..319 "A"
S25 295..297 "AGA" 316..318 "UCU"
S26 301..305 "UGGGG" 310..314 "CCCUG"
S27 332..335 "GAAC" 359..362 "GUUC"
S28 342..345 "GGCC" 350..353 "GGUU"
S29 366..366 "C" 398..398 "G"
S30 367..372 "GCCUGG" 389..394 "CCAGGU"
S31 376..376 "G" 384..384 "C"
S32 413..415 "GGC" 727..729 "GCC"
S33 421..421 "G" 726..726 "C"
S34 423..426 "GCGC" 607..610 "GUGC"
S35 428..428 "U" 604..604 "G"
S36 430..432 "GGC" 601..603 "GCU"
S37 434..440 "GGCUGUU" 491..497 "GGCAGCC"
S38 442..443 "AG" 489..490 "CU"
S39 444..452 "UCAGAGGUG" 478..486 "UGCCUUUGA"
S40 457..463 "NCCAGGG" 469..475 "CCCUNGA"
S41 501..503 "AGG" 595..597 "ACU"
S42 504..509 "UCUGGA" 588..593 "GCCAGA"
S43 512..518 "GGUGAGU" 580..586 "GCUCACU"
S44 519..520 "GG" 562..563 "AC"
S45 523..530 "UUCCGAGU" 552..559 "AUUCGGAA"
S46 534..536 "GAG" 543..545 "UUC"
S47 568..568 "G" 579..579 "G"
S48 571..572 "GC" 577..578 "GC"
S49 615..620 "GCGUGG" 651..656 "CCACGC"
S50 624..625 "GC" 649..650 "GU"
S51 629..632 "CAGG" 642..645 "CCUG"
S52 667..672 "GAAUGC" 720..725 "GCAUUC"
S53 675..686 "GUCGUCGGGCAG" 692..703 "CUGUUCGGUGAC"
S54 707..708 "CC" 713..714 "GG"
S55 731..733 "GGG" 756..758 "CUC"
S56 740..743 "GGCC" 748..751 "GGUU"
S57 781..782 "AC" 1174..1175 "GU"
S58 785..789 "GCGGU" 1135..1139 "AUCGC"
S59 790..799 "GGAGCAUGUG" 1044..1053 "CACGUGCUAC"
S60 800..801 "GU" 1041..1042 "AC"
S61 806..809 "UUCG" 818..821 "CGCA"
S62 830..836 "CCAACCC" 1031..1037 "GGGUUGG"
S63 843..845 "AUC" 859..861 "GAC"
S64 846..849 "CCUG" 854..857 "CAGG"
S65 863..864 "GG" 1025..1026 "CC"
S66 866..869 "GCUG" 1021..1024 "UGGC"
S67 872..873 "UG" 1019..1020 "CA"
S68 874..876 "GCU" 1013..1015 "AGU"
S69 877..880 "GUCG" 1008..1011 "CGUC"
S70 884..889 "GCUCGU" 918..923 "ACGAGC"
S71 890..892 "GUC" 897..899 "GAU"
S72 902..905 "UCGG" 912..915 "CCGG"
S73 929..932 "CCAC" 1000..1003 "GUGG"
S74 934..940 "UCCUUAG" 965..971 "CUAAGGA"
S75 944..945 "CC" 959..960 "GG"
S76 948..949 "CA" 957..958 "NG"
S77 974..981 "CUGCCGAU" 987..994 "GUCGGAGG"
S78 1057..1063 "GGCAGUG" 1085..1091 "NACUGUC"
S79 1069..1071 "GGG" 1077..1079 "CCC"
S80 1098..1098 "C" 1129..1129 "G"
S81 1102..1109 "UUGUCCUC" 1118..1125 "GAGGGCAU"
S82 1145..1152 "AUCGCGUA" 1160..1167 "GACGCGGU"
H1 39..42 "CUUA" (38,43) C:G 
H2 100..103 "GAAA" (99,104) G:C 
H3 128..131 "UUCG" (127,132) C:G 
H4 141..143 "AUU" (140,144) A:U 
H5 186..192 "GUAAUGG" (185,193) G:C 
H6 223..226 "GAGA" (222,227) U:G 
H7 248..257 "CUGAGACACG" (247,258) A:G 
H8 269..272 "UACG" (268,273) C:G 
H9 306..309 "GCAA" (305,310) G:C 
H10 346..349 "UUAG" (345,350) C:G 
H11 377..383 "AUAAUGA" (376,384) G:C 
H12 464..468 "CUCAA" (463,469) G:C 
H13 537..542 "GUGAAA" (536,543) G:U 
H14 573..576 "GAAG" (572,577) C:G 
H15 633..641 "AUUAGAUAC" (632,642) G:C 
H16 687..691 "CAUGN" (686,692) G:C 
H17 709..712 "UAAC" (708,713) C:G PK{2}
H18 744..747 "GCAA" (743,748) C:G 
H19 810..817 "AAGCAACG" (809,818) G:C 
H20 850..853 "UCGA" (849,854) G:C 
H21 893..896 "GUGA" (892,897) C:G 
H22 906..911 "UUAAGU" (905,912) G:C 
H23 950..956 "UUCAGUU" (949,957) A:N 
H24 982..986 "GAUAA" (981,987) U:G 
H25 1072..1076 "UUAAU" (1071,1077) G:C 
H26 1110..1117 "UGCAACUC" (1109,1118) C:G 
H27 1153..1159 "ACAGCAU" (1152,1160) A:G 
B1 26..26 "A" (25,283) C:G (282,27) A:U 
B2 35..36 "GA" (34,45) C:G (44,37) C:G 
B3 71..71 "A" (70,158) A:G (157,72) G:C 
B4 169..170 "AU" (168,210) G:C (209,171) U:U 
B5 176..177 "AG" (175,201) U:G (200,178) A:U 
B6 231..233 "GAU" (230,219) U:G (218,234) G:C 
B7 294..294 "U" (293,319) U:A (318,295) U:A 
B8 323..323 "A" (322,16) C:G (15,324) A:U 
B9 395..397 "AAA" (394,367) U:G (366,398) C:G PK{1}
B10 416..420 "GUAAA" (415,727) C:G (726,421) C:G PK{2}
B11 429..429 "A" (428,604) U:G (603,430) U:G 
B12 441..441 "A" (440,491) U:G (490,442) U:A 
B13 487..488 "UA" (486,444) A:U (443,489) G:C 
B14 569..570 "UG" (568,579) G:G (578,571) C:G 
B15 594..594 "U" (593,504) A:U (503,595) G:A 
B16 621..623 "GGA" (620,651) G:C (650,624) U:G 
B17 858..858 "U" (857,846) G:C (845,859) C:G 
B18 865..865 "U" (864,1025) G:C (1024,866) C:G 
B19 870..871 "CA" (869,1021) G:U (1020,872) A:U 
B20 946..947 "AG" (945,959) C:G (958,948) G:C 
B21 1012..1012 "A" (1011,877) C:G (876,1013) U:A 
B22 1016..1018 "CCU" (1015,874) U:G (873,1019) G:C 
B23 1043..1043 "A" (1042,800) C:G (799,1044) G:C 
I1.1 19..22 "CUAA" (18,287) C:G 
I1.2 286..286 "A" (285,23) G:C 
I2.1 74..76 "UGC" (73,156) G:C 
I2.2 154..155 "AU" (153,77) G:C 
I3.1 89..92 "AAUA" (88,116) G:C 
I3.2 111..115 "UAAUA" (110,93) U:G 
I4.1 172..172 "A" (171,209) U:U 
I4.2 204..208 "UACGA" (203,173) C:G 
I5.1 298..300 "CAA" (297,316) A:U 
I5.2 315..315 "A" (314,301) G:U 
I6.1 336..341 "GAUGAA" (335,359) C:G 
I6.2 354..358 "GUAAA" (353,342) U:G 
I7.1 373..375 "GAA" (372,389) G:C 
I7.2 385..388 "GGUA" (384,376) C:G 
I8.1 427..427 "G" (426,607) C:G 
I8.2 605..606 "AG" (604,428) G:U 
I9.1 453..456 "AAAU" (452,478) G:U 
I9.2 476..477 "AC" (475,457) A:N 
I10.1 510..511 "GA" (509,588) A:G 
I10.2 587..587 "G" (586,512) U:G 
I11.1 521..522 "AA" (520,562) G:A 
I11.2 560..561 "GA" (559,523) A:U 
I12.1 531..533 "GUA" (530,552) U:A 
I12.2 546..551 "GUAGAU" (545,534) C:G 
I13.1 626..628 "AAA" (625,649) C:G 
I13.2 646..648 "GUA" (645,629) G:C 
I14.1 734..739 "GAGUAC" (733,756) G:C 
I14.2 752..755 "AAAA" (751,740) U:G 
I15.1 941..943 "UUG" (940,965) G:C 
I15.2 961..964 "CACU" (960,944) G:C 
I16.1 1064..1068 "ACAAU" (1063,1085) G:N 
I16.2 1080..1084 "AAAAA" (1079,1069) C:G 
I17.1 1099..1101 "GGA" (1098,1129) C:G 
I17.2 1126..1128 "GAA" (1125,1102) U:U 
M1.1 18..17 "" (17,321) G:C (287,18) G:C 
M1.2 288..292 "GAAUC" (287,18) G:C (319,293) A:U 
M1.3 320..320 "G" (319,293) A:U (17,321) G:C 
M2.1 30..31 "AA" (29,280) C:G (47,32) C:G 
M2.2 48..53 "GGACGG" (47,32) C:G (240,54) C:G 
M2.3 241..241 "A" (240,54) C:G (263,242) G:C 
M2.4 264..264 "A" (263,242) G:C (276,265) G:C 
M2.5 277..279 "GCA" (276,265) G:C (29,280) C:G 
M3.1 57..62 "AGUAAC" (56,238) G:C (165,63) U:G 
M3.2 166..165 "" (165,63) U:G (212,166) A:U 
M3.3 213..214 "UA" (212,166) A:U (237,215) C:G 
M3.4 238..237 "" (237,215) C:G (56,238) G:C 
M4.1 84..84 "U" (83,147) C:G (119,85) U:A 
M4.2 120..124 "AUACG" (119,85) U:A (134,125) G:C 
M4.3 135..138 "GAAA" (134,125) G:C (145,139) C:G 
M4.4 146..146 "G" (145,139) C:G (83,147) C:G 
M5.1 422..422 "C" (421,726) G:C (610,423) C:G 
M5.2 611..614 "GAAA" (610,423) C:G (656,615) C:G 
M5.3 657..666 "CGUAAACGAU" (656,615) C:G (725,667) C:G 
M5.4 726..725 "" (725,667) C:G (421,726) G:C 
M6.1 433..433 "G" (432,601) C:G (497,434) C:G 
M6.2 498..500 "UUG" (497,434) C:G (597,501) U:A 
M6.3 598..600 "GAC" (597,501) U:A (432,601) C:G 
M7.1 519..518 "" (518,580) U:G (563,519) C:G 
M7.2 564..567 "ACCA" (563,519) C:G (579,568) G:G 
M7.3 580..579 "" (579,568) G:G (518,580) U:G 
M8.1 673..674 "CA" (672,720) C:G (703,675) C:G 
M8.2 704..706 "ACA" (703,675) C:G (714,707) G:C 
M8.3 715..719 "AGUAA" (714,707) G:C (672,720) C:G 
M9.1 783..784 "AA" (782,1174) C:G (1139,785) C:G 
M9.2 1140..1144 "UAGUA" (1139,785) C:G (1167,1145) U:A 
M9.3 1168..1173 "GAAUAC" (1167,1145) U:A (782,1174) C:G 
M10.1 790..789 "" (789,1135) U:A (1053,790) C:G 
M10.2 1054..1056 "AAU" (1053,790) C:G (1091,1057) C:G 
M10.3 1092..1097 "UCAGUU" (1091,1057) C:G (1129,1098) G:C 
M10.4 1130..1134 "UCGGA" (1129,1098) G:C (789,1135) U:A 
M11.1 802..805 "UUAA" (801,1041) U:A (821,806) A:U 
M11.2 822..829 "GAACCUUA" (821,806) A:U (1037,830) G:C 
M11.3 1038..1040 "GCU" (1037,830) G:C (801,1041) U:A 
M12.1 837..842 "UUNGAC" (836,1031) C:G (861,843) C:A 
M12.2 862..862 "A" (861,843) C:A (1026,863) C:G 
M12.3 1027..1030 "UUAC" (1026,863) C:G (836,1031) C:G 
M13.1 881..883 "UCA" (880,1008) G:C (923,884) C:G 
M13.2 924..928 "GCAAC" (923,884) C:G (1003,929) G:C 
M13.3 1004..1007 "AUGA" (1003,929) G:C (880,1008) G:C 
M14.1 890..889 "" (889,918) U:A (899,890) U:G 
M14.2 900..901 "GU" (899,890) U:G (915,902) G:U 
M14.3 916..917 "CA" (915,902) G:U (889,918) U:A 
M15.1 933..933 "G" (932,1000) C:G (971,934) A:U 
M15.2 972..973 "AA" (971,934) A:U (994,974) G:C 
M15.3 995..999 "AAGGU" (994,974) G:C (932,1000) C:G 
X1 330..331 "GU" (329,10) C:G (362,332) C:G 
X2 363..365 "UUU" (362,332) C:G (398,366) G:C PK{1}
X3 399..412 "AAGCGGAAUUACUG" (398,366) G:C (729,413) C:G 
X5 759..780 "AAAGGAAUUGACGGGGGCCCGC" (758,731) C:G (1175,781) U:A 
E1 1..9 "CGAACGCUG" 
PK1 1bp 364..364 397..397 X2 363..365 B9 395..397
PK2 2bp 416..417 711..712 B10 416..420 H17 709..712
PK1.1 364 U 397 A
PK2.1 416 G 712 C
PK2.2 417 U 711 A
NCBP1 1152 A 1160 G S82
NCBP2 509 A 588 G S42
NCBP3 976 G 992 A S77
NCBP4 94 U 109 U S13
NCBP5 568 G 579 G S47
NCBP6 791 G 1052 A S59
NCBP7 171 U 209 U S17
NCBP8 1063 G 1085 N S78
NCBP9 843 A 861 C S63
NCBP10 520 G 562 A S44
NCBP11 503 G 595 A S41
NCBP12 878 U 1010 U S69
NCBP13 1102 U 1125 U S81
NCBP14 459 C 473 N S40
NCBP15 247 A 258 G S22
NCBP16 457 N 475 A S40
NCBP17 949 A 957 N S76
NCBP18 70 A 158 G S9
NCBP19 807 U 820 C S61
segment1 8bp 10..17 GCGGCAGG 321..329 CCAUGCCGC
segment2 7bp 18..29 CCUAACACAUGC 280..287 GCAGUGAG
segment3 5bp 32..38 GUCGAGC 43..47 GCGGC
segment4 3bp 54..56 GUG 238..240 CAC
segment5 17bp 63..83 GCGUGGGAACGUGCCCUUCAC 147..165 GUGAAGGAUCGGCCCGCGU
segment6 11bp 85..99 ACGGAAUAGUCCCGG 104..119 CUGGGUUUAAUACCGU
segment7 3bp 125..127 CCC 132..134 GGG
segment8 2bp 139..140 GA 144..145 UC
segment9 15bp 166..185 UAGAUUAGGUAGUUGGUGGG 193..212 CCUACCAAGCCUACGAUCUA
segment10 8bp 215..222 GCUGGUUU 227..237 GGAUGAUCAGC
segment11 6bp 242..247 CUGGGA 258..263 GCCCAG
segment12 4bp 265..268 CUCC 273..276 GGAG
segment13 9bp 293..305 UUAGACAAUGGGG 310..319 CCCUGAUCUA
segment14 8bp 332..345 GAACGAUGAAGGCC 350..362 GGUUGUAAAGUUC
segment15 8bp 366..376 CGCCUGGGAAG 384..398 CGGUACCAGGUAAAG
segment16 4bp 413..421 GGCGUAAAG 726..729 CGCC
segment17 8bp 423..432 GCGCGUAGGC 601..610 GCUGAGGUGC
segment18 25bp 434..463 GGCUGUUAAGUCAGAGGUGAAAUNCCAGGG 469..497 CCCUNGAACUGCCUUUGAUACUGGCAGCC
segment19 16bp 501..518 AGGUCUGGAGAGGUGAGU 580..597 GCUCACUGGCCAGAUACU
segment20 13bp 519..536 GGAAUUCCGAGUGUAGAG 543..563 UUCGUAGAUAUUCGGAAGAAC
segment21 3bp 568..572 GUGGC 577..579 GCG
segment22 12bp 615..632 GCGUGGGGAGCAAACAGG 642..656 CCUGGUAGUCCACGC
segment23 6bp 667..672 GAAUGC 720..725 GCAUUC
segment24 12bp 675..686 GUCGUCGGGCAG 692..703 CUGUUCGGUGAC
segment25 2bp 707..708 CC 713..714 GG
segment26 7bp 731..743 GGGGAGUACGGCC 748..758 GGUUAAAACUC
segment27 2bp 781..782 AC 1174..1175 GU
segment28 5bp 785..789 GCGGU 1135..1139 AUCGC
segment29 12bp 790..801 GGAGCAUGUGGU 1041..1053 ACACACGUGCUAC
segment30 4bp 806..809 UUCG 818..821 CGCA
segment31 7bp 830..836 CCAACCC 1031..1037 GGGUUGG
segment32 7bp 843..849 AUCCCUG 854..861 CAGGUGAC
segment33 15bp 863..880 GGUGCUGCAUGGCUGUCG 1008..1026 CGUCAAGUCCUCAUGGCCC
segment34 6bp 884..889 GCUCGU 918..923 ACGAGC
segment35 3bp 890..892 GUC 897..899 GAU
segment36 4bp 902..905 UCGG 912..915 CCGG
segment37 4bp 929..932 CCAC 1000..1003 GUGG
segment38 11bp 934..949 UCCUUAGUUGCCAGCA 957..971 NGGGCACUCUAAGGA
segment39 8bp 974..981 CUGCCGAU 987..994 GUCGGAGG
segment40 10bp 1057..1071 GGCAGUGACAAUGGG 1077..1091 CCCAAAAANACUGUC
segment41 9bp 1098..1109 CGGAUUGUCCUC 1118..1129 GAGGGCAUGAAG
segment42 8bp 1145..1152 AUCGCGUA 1160..1167 GACGCGGU
