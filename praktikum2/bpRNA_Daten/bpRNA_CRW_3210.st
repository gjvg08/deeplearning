#Name: bpRNA_CRW_3210
#Length:  1173 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
UCACCAAGGCGACGACGGGUAGCCGGCCUGAGAGGGCGACCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGGAAGCCUGAUGCAGCGACGCCGCGUGAGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCAGCAGGGAAGAAGCGCAAGUGACGGUACCUGCAGAAGAAGCGCCGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGCGCAAGCGUUGUCCGGAAUUAUUGGGCGUAAAGAGCUCGUAGGCGGUCUGUCGCGUCGGAUGUGAAAGCCCGGGGCUUAACCCCGGGUCUGCAUUCGAUACGGGCAGACUAGAGUGUGGUAGGGGAGAUCGGAAUUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAGGAACACCGGUGGCGAAGGCGGAUCUCUGGGCCAUUACUGACGCUGAGGAGCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGGUGGGAACUAGGUGUUGGCGACAUUCCACGUCGUCGGUGCCGCAGCUAACGCAUUAAGUUCCCCGCCUGGGGAGUACGGCCGCAAGGCUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCAGCGGAGCAUGUGGCUUAAUUCGACGCAACGCGAAGAACCUUACCAAGGCUUGACAUCGCCCGGAAAGCAUCAGAGAUGGUGCCCCCCUUGUGGUCGGGUGACAGGUGGUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUUCUGUGUUGCCAGCAACUCCUUCGGGAGGUUGGGGACUCACAGGAGACUGCCGGGGUCAACUCGGAGGAAGGUGGGGACGACGUCAAGUCAUCAUGCCCCUUAUGUCUUGGGCUGCACACGUGCUACAAUGGCAGGUACAAUGAGCUGCGAUGUCGCAAGGCGGAGCGAAUCUCAAAAAGCCUGUCUCAGUUCGGAUUGGGGUCUGCAACUCGACCCCAUGAAGUCGGAGUUGCUAGUAAUCGCAGAUCAGCAUUGCUGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACGUCACGAAAGUCGGUAACACCCGAAGCCGGUGGCCCAACCCC
.....................((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(............)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.......))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....(((((((((..(((((((....))))))).((.....))))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..(((.............)))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((...(((((..((...(((....)))...))....)))))...)))))))......(...((((((((........))))))))...).....))))).....((((((((........))))))))......))...)))))))))).)).......................................................
EEEEEEEEEEEEEEEEEEEEESSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHHHHHHSIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSHHHHHSSSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSSHHHHHHHHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIISSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 22..25 "GCCG" 41..44 "CGGC"
S2 26..29 "GCCU" 34..37 "GGGC"
S3 49..54 "CUGGGA" 65..70 "GCCCAG"
S4 72..75 "CUCC" 80..83 "GGAG"
S5 100..100 "U" 126..126 "A"
S6 102..104 "GCA" 123..125 "UGC"
S7 108..112 "UGGGC" 117..121 "GCCUG"
S8 139..142 "GAGG" 166..169 "CCUC"
S9 149..152 "GGCC" 157..160 "GGUU"
S10 173..173 "C" 210..210 "G"
S11 174..179 "AGCAGG" 201..206 "CCUGCA"
S12 183..183 "G" 196..196 "C"
S13 213..217 "GCGCC" 254..258 "GGCGC"
S14 224..228 "CUACG" 249..253 "CGUAG"
S15 234..235 "GC" 240..241 "GC"
S16 280..282 "GGC" 596..598 "GCC"
S17 288..288 "G" 595..595 "C"
S18 290..293 "GCUC" 474..477 "GAGC"
S19 295..295 "U" 471..471 "G"
S20 297..299 "GGC" 468..470 "GCU"
S21 301..307 "GUCUGUC" 358..364 "GGCAGAC"
S22 309..310 "CG" 356..357 "CG"
S23 311..319 "UCGGAUGUG" 345..353 "UGCAUUCGA"
S24 324..330 "CCCGGGG" 336..342 "CCCCGGG"
S25 368..370 "AGU" 462..464 "ACU"
S26 371..376 "GUGGUA" 455..460 "GGCCAU"
S27 379..385 "GGAGAUC" 447..453 "GAUCUCU"
S28 386..387 "GG" 429..430 "AC"
S29 390..397 "UUCCUGGU" 419..426 "AUCAGGAG"
S30 401..403 "GCG" 410..412 "UGC"
S31 435..435 "G" 446..446 "G"
S32 438..439 "GC" 444..445 "GC"
S33 482..487 "GCGUGG" 518..523 "CCACGC"
S34 491..492 "GC" 516..517 "GU"
S35 496..499 "CAGG" 509..512 "CCUG"
S36 534..539 "GGGAAC" 589..594 "GUUCCC"
S37 542..553 "GGUGUUGGCGAC" 561..572 "GUCGUCGGUGCC"
S38 576..577 "GC" 582..583 "GC"
S39 600..602 "GGG" 625..627 "CUC"
S40 609..612 "GGCC" 617..620 "GGCU"
S41 636..637 "UG" 1117..1118 "CA"
S42 638..640 "ACG" 1113..1115 "UGU"
S43 642..648 "GGGCCCG" 1106..1112 "CGGGCCU"
S44 650..651 "AC" 1101..1102 "GU"
S45 654..658 "GCAGC" 1061..1065 "GUUGC"
S46 659..668 "GGAGCAUGUG" 949..958 "CACGUGCUAC"
S47 669..670 "GC" 946..947 "GC"
S48 675..678 "UUCG" 687..690 "CGAA"
S49 699..705 "CCAAGGC" 936..942 "GUCUUGG"
S50 711..719 "AUCGCCCGG" 750..758 "UCGGGUGAC"
S51 722..728 "AGCAUCA" 733..739 "UGGUGCC"
S52 741..742 "CC" 748..749 "GG"
S53 760..761 "GG" 930..931 "CC"
S54 763..766 "GGUG" 926..929 "UGCC"
S55 769..770 "UG" 924..925 "CA"
S56 771..773 "GCU" 918..920 "AGU"
S57 774..777 "GUCG" 913..916 "CGUC"
S58 781..786 "GCUCGU" 815..820 "ACGAGC"
S59 787..789 "GUC" 794..796 "GAU"
S60 799..802 "UGGG" 809..812 "CCCG"
S61 826..829 "CCUU" 905..908 "GGGG"
S62 831..837 "UUCUGUG" 870..876 "CACAGGA"
S63 841..842 "CC" 864..865 "GG"
S64 845..847 "CAA" 861..863 "UUG"
S65 879..886 "CUGCCGGG" 892..899 "CUCGGAGG"
S66 962..968 "GGCAGGU" 1011..1017 "GCCUGUC"
S67 972..976 "AUGAG" 1003..1007 "CUCAA"
S68 979..980 "GC" 997..998 "GC"
S69 984..986 "GUC" 991..993 "GGC"
S70 1024..1024 "C" 1055..1055 "G"
S71 1028..1035 "UUGGGGUC" 1044..1051 "GACCCCAU"
S72 1071..1078 "AUCGCAGA" 1087..1094 "GCUGCGGU"
H1 30..33 "GAGA" (29,34) U:G 
H2 55..64 "CUGAGACACG" (54,65) A:G 
H3 76..79 "UACG" (75,80) C:G 
H4 113..116 "GGAA" (112,117) C:G 
H5 153..156 "UUCG" (152,157) C:G 
H6 184..195 "AAGCGCAAGUGA" (183,196) G:C 
H7 236..239 "AGCC" (235,240) C:G PK{2}
H8 331..335 "CUUAA" (330,336) G:C 
H9 404..409 "GUGAAA" (403,410) G:U 
H10 440..443 "GAAG" (439,444) C:G 
H11 500..508 "AUUAGAUAC" (499,509) G:C 
H12 554..560 "AUUCCAC" (553,561) C:G 
H13 578..581 "UAAC" (577,582) C:G PK{3}
H14 613..616 "GCAA" (612,617) C:G 
H15 679..686 "ACGCAACG" (678,687) G:C 
H16 729..732 "GAGA" (728,733) A:U 
H17 743..747 "CUUGU" (742,748) C:G 
H18 790..793 "GUGA" (789,794) C:G 
H19 803..808 "UUAAGU" (802,809) G:C 
H20 848..860 "CUCCUUCGGGAGG" (847,861) A:U 
H21 887..891 "GUCAA" (886,892) G:C 
H22 987..990 "GCAA" (986,991) C:G 
H23 1036..1043 "UGCAACUC" (1035,1044) C:G 
H24 1079..1086 "UCAGCAUU" (1078,1087) A:G 
B1 38..40 "GAC" (37,26) C:G (25,41) G:C 
B2 101..101 "U" (100,126) U:A (125,102) C:G 
B3 207..209 "GAA" (206,174) A:A (173,210) C:G PK{1}
B4 218..223 "GGCUAA" (217,254) C:G (253,224) G:C PK{2}
B5 283..287 "GUAAA" (282,596) C:G (595,288) C:G PK{3}
B6 296..296 "A" (295,471) U:G (470,297) U:G 
B7 308..308 "G" (307,358) C:G (357,309) G:C 
B8 354..355 "UA" (353,311) A:U (310,356) G:C 
B9 436..437 "UG" (435,446) G:G (445,438) C:G 
B10 461..461 "U" (460,371) U:G (370,462) U:A 
B11 488..490 "GGA" (487,518) G:C (517,491) U:G 
B12 641..641 "G" (640,1113) G:U (1112,642) U:G 
B13 762..762 "U" (761,930) G:C (929,763) C:G 
B14 767..768 "CA" (766,926) G:U (925,769) A:U 
B15 843..844 "AG" (842,864) C:G (863,845) G:C 
B16 917..917 "A" (916,774) C:G (773,918) U:A 
B17 921..923 "CAU" (920,771) U:G (770,924) G:C 
B18 948..948 "A" (947,669) C:G (668,949) G:C 
B19 1116..1116 "A" (1115,638) U:A (637,1117) G:C 
I1.1 105..107 "CAA" (104,123) A:U 
I1.2 122..122 "A" (121,108) G:U 
I2.1 143..148 "GAUGAC" (142,166) G:C 
I2.2 161..165 "GUAAA" (160,149) U:G 
I3.1 180..182 "GAA" (179,201) G:C 
I3.2 197..200 "GGUA" (196,183) C:G 
I4.1 229..233 "UGCCA" (228,249) G:C 
I4.2 242..248 "GGUAAUA" (241,234) C:G 
I5.1 294..294 "G" (293,474) C:G 
I5.2 472..473 "AG" (471,295) G:U 
I6.1 320..323 "AAAG" (319,345) G:U 
I6.2 343..344 "UC" (342,324) G:C 
I7.1 377..378 "GG" (376,455) A:G 
I7.2 454..454 "G" (453,379) U:G 
I8.1 388..389 "AA" (387,429) G:A 
I8.2 427..428 "GA" (426,390) G:U 
I9.1 398..400 "GUA" (397,419) U:A 
I9.2 413..418 "GCAGAU" (412,401) C:G 
I10.1 493..495 "GAA" (492,516) C:G 
I10.2 513..515 "GUA" (512,496) G:C 
I11.1 603..608 "GAGUAC" (602,625) G:C 
I11.2 621..624 "AAAA" (620,609) U:G 
I12.1 649..649 "C" (648,1106) G:C 
I12.2 1103..1105 "UCC" (1102,650) U:A 
I13.1 838..840 "UUG" (837,870) G:C 
I13.2 866..869 "GACU" (865,841) G:C 
I14.1 969..971 "ACA" (968,1011) U:G 
I14.2 1008..1010 "AAA" (1007,972) A:A 
I15.1 977..978 "CU" (976,1003) G:C 
I15.2 999..1002 "GAAU" (998,979) C:G 
I16.1 981..983 "GAU" (980,997) C:G 
I16.2 994..996 "GGA" (993,984) C:G 
I17.1 1025..1027 "GGA" (1024,1055) C:G 
I17.2 1052..1054 "GAA" (1051,1028) U:U 
M1.1 289..289 "A" (288,595) G:C (477,290) C:G 
M1.2 478..481 "GAAA" (477,290) C:G (523,482) C:G 
M1.3 524..533 "CGUAAACGGU" (523,482) C:G (594,534) C:G 
M1.4 595..594 "" (594,534) C:G (288,595) G:C 
M2.1 300..300 "G" (299,468) C:G (364,301) C:G 
M2.2 365..367 "UAG" (364,301) C:G (464,368) U:A 
M2.3 465..467 "GAC" (464,368) U:A (299,468) C:G 
M3.1 386..385 "" (385,447) C:G (430,386) C:G 
M3.2 431..434 "ACCG" (430,386) C:G (446,435) G:G 
M3.3 447..446 "" (446,435) G:G (385,447) C:G 
M4.1 540..541 "UA" (539,589) C:G (572,542) C:G 
M4.2 573..575 "GCA" (572,542) C:G (583,576) C:G 
M4.3 584..588 "AUUAA" (583,576) C:G (539,589) C:G 
M5.1 652..653 "AA" (651,1101) C:G (1065,654) C:G 
M5.2 1066..1070 "UAGUA" (1065,654) C:G (1094,1071) U:A 
M5.3 1095..1100 "GAAUAC" (1094,1071) U:A (651,1101) C:G 
M6.1 659..658 "" (658,1061) C:G (958,659) C:G 
M6.2 959..961 "AAU" (958,659) C:G (1017,962) C:G 
M6.3 1018..1023 "UCAGUU" (1017,962) C:G (1055,1024) G:C 
M6.4 1056..1060 "UCGGA" (1055,1024) G:C (658,1061) C:G 
M7.1 671..674 "UUAA" (670,946) C:G (690,675) A:U 
M7.2 691..698 "GAACCUUA" (690,675) A:U (942,699) G:C 
M7.3 943..945 "GCU" (942,699) G:C (670,946) C:G 
M8.1 706..710 "UUGAC" (705,936) C:G (758,711) C:A 
M8.2 759..759 "A" (758,711) C:A (931,760) C:G 
M8.3 932..935 "UUAU" (931,760) C:G (705,936) C:G 
M9.1 720..721 "AA" (719,750) G:U (739,722) C:A 
M9.2 740..740 "C" (739,722) C:A (749,741) G:C 
M9.3 750..749 "" (749,741) G:C (719,750) G:U 
M10.1 778..780 "UCA" (777,913) G:C (820,781) C:G 
M10.2 821..825 "GCAAC" (820,781) C:G (908,826) G:C 
M10.3 909..912 "ACGA" (908,826) G:C (777,913) G:C 
M11.1 787..786 "" (786,815) U:A (796,787) U:G 
M11.2 797..798 "GU" (796,787) U:G (812,799) G:U 
M11.3 813..814 "CA" (812,799) G:U (786,815) U:A 
M12.1 830..830 "G" (829,905) U:G (876,831) A:U 
M12.2 877..878 "GA" (876,831) A:U (899,879) G:C 
M12.3 900..904 "AAGGU" (899,879) G:C (829,905) U:G 
X1 45..48 "CACA" (44,22) C:G (70,49) G:C 
X3 84..99 "GCAGCAGUGGGGAAUA" (83,72) G:C (126,100) A:U 
X4 127..138 "GCGACGCCGCGU" (126,100) A:U (169,139) C:G 
X5 170..172 "UUU" (169,139) C:G (210,173) G:C PK{1}
X6 211..212 "AA" (210,173) G:C (258,213) C:G 
X7 259..279 "AAGCGUUGUCCGGAAUUAUUG" (258,213) C:G (598,280) C:G 
X9 628..635 "AAAGGAAU" (627,600) C:G (1118,636) A:U 
E1 1..21 "UCACCAAGGCGACGACGGGUA" 
E2 1119..1173 "CACCGCCCGUCACGUCACGAAAGUCGGUAACACCCGAAGCCGGUGGCCCAACCCC" 
PK1 1bp 171..171 209..209 X5 170..172 B3 207..209
PK2 3bp 218..220 237..239 B4 218..223 H7 236..239
PK3 2bp 283..284 580..581 B5 283..287 H13 578..581
PK1.1 171 U 209 A
PK2.1 218 G 239 C
PK2.2 219 G 238 C
PK2.3 220 C 237 G
PK3.1 283 G 581 C
PK3.2 284 U 580 A
NCBP1 722 A 739 C S51
NCBP2 881 G 897 A S65
NCBP3 775 U 915 U S57
NCBP4 660 G 957 A S46
NCBP5 387 G 429 A S28
NCBP6 1028 U 1051 U S71
NCBP7 174 A 206 A S11
NCBP8 1078 A 1087 G S72
NCBP9 711 A 758 C S50
NCBP10 376 A 455 G S26
NCBP11 435 G 446 G S31
NCBP12 54 A 65 G S3
NCBP13 972 A 1007 A S67
segment1 8bp 22..29 GCCGGCCU 34..44 GGGCGACCGGC
segment2 6bp 49..54 CUGGGA 65..70 GCCCAG
segment3 4bp 72..75 CUCC 80..83 GGAG
segment4 9bp 100..112 UUGCACAAUGGGC 117..126 GCCUGAUGCA
segment5 8bp 139..152 GAGGGAUGACGGCC 157..169 GGUUGUAAACCUC
segment6 8bp 173..183 CAGCAGGGAAG 196..210 CGGUACCUGCAGAAG
segment7 12bp 213..235 GCGCCGGCUAACUACGUGCCAGC 240..258 GCGGUAAUACGUAGGGCGC
segment8 4bp 280..288 GGCGUAAAG 595..598 CGCC
segment9 8bp 290..299 GCUCGUAGGC 468..477 GCUGAGGAGC
segment10 25bp 301..330 GUCUGUCGCGUCGGAUGUGAAAGCCCGGGG 336..364 CCCCGGGUCUGCAUUCGAUACGGGCAGAC
segment11 16bp 368..385 AGUGUGGUAGGGGAGAUC 447..464 GAUCUCUGGGCCAUUACU
segment12 13bp 386..403 GGAAUUCCUGGUGUAGCG 410..430 UGCGCAGAUAUCAGGAGGAAC
segment13 3bp 435..439 GUGGC 444..446 GCG
segment14 12bp 482..499 GCGUGGGGAGCGAACAGG 509..523 CCUGGUAGUCCACGC
segment15 6bp 534..539 GGGAAC 589..594 GUUCCC
segment16 12bp 542..553 GGUGUUGGCGAC 561..572 GUCGUCGGUGCC
segment17 2bp 576..577 GC 582..583 GC
segment18 7bp 600..612 GGGGAGUACGGCC 617..627 GGCUAAAACUC
segment19 14bp 636..651 UGACGGGGGCCCGCAC 1101..1118 GUUCCCGGGCCUUGUACA
segment20 5bp 654..658 GCAGC 1061..1065 GUUGC
segment21 12bp 659..670 GGAGCAUGUGGC 946..958 GCACACGUGCUAC
segment22 4bp 675..678 UUCG 687..690 CGAA
segment23 7bp 699..705 CCAAGGC 936..942 GUCUUGG
segment24 9bp 711..719 AUCGCCCGG 750..758 UCGGGUGAC
segment25 7bp 722..728 AGCAUCA 733..739 UGGUGCC
segment26 2bp 741..742 CC 748..749 GG
segment27 15bp 760..777 GGUGGUGCAUGGCUGUCG 913..931 CGUCAAGUCAUCAUGCCCC
segment28 6bp 781..786 GCUCGU 815..820 ACGAGC
segment29 3bp 787..789 GUC 794..796 GAU
segment30 4bp 799..802 UGGG 809..812 CCCG
segment31 4bp 826..829 CCUU 905..908 GGGG
segment32 12bp 831..847 UUCUGUGUUGCCAGCAA 861..876 UUGGGGACUCACAGGA
segment33 8bp 879..886 CUGCCGGG 892..899 CUCGGAGG
segment34 17bp 962..986 GGCAGGUACAAUGAGCUGCGAUGUC 991..1017 GGCGGAGCGAAUCUCAAAAAGCCUGUC
segment35 9bp 1024..1035 CGGAUUGGGGUC 1044..1055 GACCCCAUGAAG
segment36 8bp 1071..1078 AUCGCAGA 1087..1094 GCUGCGGU
