#Name: bpRNA_CRW_2337
#Length:  1179 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
UACUCGAGUGGCGAACGGGUGAGUAACACGUGGGUGAUCUGCCCUGCACUUUGGGAUAAGCCUGGGAAACUGGGUCUAAUACCGAAUAUUUCCUUCUGGUUGCAUGGCUGGGAGGGGAAAGCUUUUGCGGUGUGGGAUGGGCCCGCGGCCUAUCAGCUUGUUGGUGAGGUUAUGGCUUACCAAGGCGACGACGGGUAGCCGGCCUGAGAGGGUGACCGGCCACACUGGGACUGAGAUACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGCAAGCCUGAUGCAGCGACGCCGCGUGGGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCAGUAGGGACGAAGCGCAAGUGACGGUACCUAUAGAAGAAGGACCGGCCAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGUCCGAGCGUUGUCCGGAAUUACUGGGCGUAAAGAGCUCGUAGGUGGUUUGUCGCGUUGUCCGUGAAAACUCACAGCUUAACUGUGGGCGUGCGGGCGAUACGGGCAGACUUGAGUACUGCAGGGGAGACUGGAAUUCCUGGUGUAGCGGUGGAAUGCGCAGAUAUCAGGAGGAACACCGGUGGCGAAGGCGGGUCUCUGGGCAGUAACUGACGCUGAGGAGCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGGUGGGUACUAGGUGUGGGUUUCCUUCCUUGGGAUCCGUGCCGUAGCUAACGCAUUAAGUACCCCGCCUGGGGAGUACGGCCGCAAGGCUAAAACUCAAAGAAAUUGACGGGGGCCCGCACAAGCGGCGGAGCAUGUGGAUUAAUUCGAUGCAACGCGAAGAACCUUACCUGGGUUUGACAUGCACAGGACGACUGCAGAGAUGUGGUUUCCCUUGUGGCCUGUGUGCAGGUGGUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUCUCAUGUUGCCAGCGCGUUAUGGCGGGGACUCGUGAGAGACUGCCGGGGUCAACUCGGAGGAAGGUGGGGAUGACGUCAAGUCAUCAUGCCCCUUAUGUCCAGGGCUUCACACAUGCUACAAUGGCCGGUACAAAGGGCUGCGAUGCCGUGAGGUGGAGCGAAUCCUUUCAA
..................(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....(((((((.(((....))).)))))))....((....)).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(..((....))..)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.......))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))...............................((((((((((((....((((........))))........(((((((.....(((((((((..(((((((....))))))).((.....))))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..(((......)))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))............((((((..((...(((....)))...))....))))))..
EEEEEEEEEEEEEEEEEESSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSISSSHHHHSSSISSSSSSSMMMMSSHHHHSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMMMMMMMMMMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMMMMMMMMMMMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMMMSSSSSSSIIISIISSHHHHSSIISIIIISSSSSSBBBSMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMMMMMMMMMMMMMMMMMMMMMSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSMSSSIIIIIISSSSHHHHSSSSIIIISSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSHHHHHSSSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSSHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMMMMMMMMMMSSSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSSEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 19..21 "GUG" 221..223 "CAC"
S2 28..35 "ACGUGGGU" 141..148 "GCCCGCGG"
S3 38..39 "UC" 139..140 "GG"
S4 43..49 "CCUGCAC" 130..136 "GUGUGGG"
S5 51..54 "UUGG" 82..85 "CCGA"
S6 59..65 "AGCCUGG" 70..76 "CUGGGUC"
S7 91..97 "UCCUUCU" 110..116 "GGGAGGG"
S8 99..101 "GUU" 106..108 "GGC"
S9 121..122 "GC" 127..128 "GC"
S10 149..151 "CCU" 193..195 "GGG"
S11 154..154 "C" 192..192 "C"
S12 156..158 "GCU" 184..186 "GGC"
S13 161..168 "UUGGUGAG" 176..183 "CUUACCAA"
S14 198..201 "GCCG" 217..220 "CGGC"
S15 202..205 "GCCU" 210..213 "GGGU"
S16 225..230 "CUGGGA" 241..246 "GCCCAG"
S17 248..251 "CUCC" 256..259 "GGAG"
S18 276..276 "U" 302..302 "A"
S19 278..280 "GCA" 299..301 "UGC"
S20 284..288 "UGGGC" 293..297 "GCCUG"
S21 315..318 "GGGG" 342..345 "CCUC"
S22 325..328 "GGCC" 333..336 "GGUU"
S23 349..349 "C" 386..386 "G"
S24 350..355 "AGUAGG" 377..382 "CCUAUA"
S25 359..359 "G" 372..372 "C"
S26 362..363 "GC" 368..369 "GU"
S27 389..393 "GGACC" 430..434 "GGUCC"
S28 400..404 "CUACG" 425..429 "CGUAG"
S29 410..411 "GC" 416..417 "GC"
S30 456..458 "GGC" 772..774 "GCC"
S31 464..464 "G" 771..771 "C"
S32 466..469 "GCUC" 650..653 "GAGC"
S33 471..471 "U" 647..647 "G"
S34 473..475 "GGU" 644..646 "GCU"
S35 477..483 "GUUUGUC" 534..540 "GGCAGAC"
S36 485..486 "CG" 532..533 "CG"
S37 487..495 "UUGUCCGUG" 521..529 "UGCGGGCGA"
S38 500..506 "CUCACAG" 512..518 "CUGUGGG"
S39 544..546 "AGU" 638..640 "ACU"
S40 547..552 "ACUGCA" 631..636 "GGCAGU"
S41 555..561 "GGAGACU" 623..629 "GGUCUCU"
S42 562..563 "GG" 605..606 "AC"
S43 566..573 "UUCCUGGU" 595..602 "AUCAGGAG"
S44 577..579 "GCG" 586..588 "UGC"
S45 611..611 "G" 622..622 "G"
S46 614..615 "GC" 620..621 "GC"
S47 658..663 "GCGUGG" 694..699 "CCACGC"
S48 667..668 "GC" 692..693 "GU"
S49 672..675 "CAGG" 685..688 "CCUG"
S50 710..715 "GGGUAC" 765..770 "GUACCC"
S51 718..729 "GGUGUGGGUUUC" 737..748 "GGGAUCCGUGCC"
S52 752..753 "GC" 758..759 "GC"
S53 776..778 "GGG" 801..803 "CUC"
S54 785..788 "GGCC" 793..796 "GGCU"
S55 835..844 "GGAGCAUGUG" 1118..1127 "CACAUGCUAC"
S56 845..846 "GA" 1115..1116 "UC"
S57 851..854 "UUCG" 863..866 "CGAA"
S58 875..881 "CCUGGGU" 1105..1111 "GUCCAGG"
S59 887..895 "AUGCACAGG" 926..934 "CCUGUGUGC"
S60 898..904 "GACUGCA" 909..915 "UGUGGUU"
S61 917..918 "CC" 924..925 "GG"
S62 936..937 "GG" 1099..1100 "CC"
S63 939..942 "GGUG" 1095..1098 "UGCC"
S64 945..946 "UG" 1093..1094 "CA"
S65 947..949 "GCU" 1087..1089 "AGU"
S66 950..953 "GUCG" 1082..1085 "CGUC"
S67 957..962 "GCUCGU" 991..996 "ACGAGC"
S68 963..965 "GUC" 970..972 "GAU"
S69 975..978 "UGGG" 985..988 "CCCG"
S70 1002..1005 "CCUU" 1074..1077 "GGGG"
S71 1007..1013 "UCUCAUG" 1039..1045 "CGUGAGA"
S72 1017..1018 "CC" 1033..1034 "GG"
S73 1021..1023 "CGC" 1030..1032 "GCG"
S74 1048..1055 "CUGCCGGG" 1061..1068 "CUCGGAGG"
S75 1140..1145 "AAAGGG" 1172..1177 "CCUUUC"
S76 1148..1149 "GC" 1166..1167 "GC"
S77 1153..1155 "GCC" 1160..1162 "GGU"
H1 66..69 "GAAA" (65,70) G:C 
H2 102..105 "GCAU" (101,106) U:G 
H3 123..126 "UUUU" (122,127) C:G 
H4 169..175 "GUUAUGG" (168,176) G:C 
H5 206..209 "GAGA" (205,210) U:G 
H6 231..240 "CUGAGAUACG" (230,241) A:G 
H7 252..255 "UACG" (251,256) C:G 
H8 289..292 "GCAA" (288,293) C:G 
H9 329..332 "UUCG" (328,333) C:G 
H10 364..367 "GCAA" (363,368) C:G 
H11 412..415 "AGCC" (411,416) C:G PK{2}
H12 507..511 "CUUAA" (506,512) G:C 
H13 580..585 "GUGGAA" (579,586) G:U 
H14 616..619 "GAAG" (615,620) C:G 
H15 676..684 "AUUAGAUAC" (675,685) G:C 
H16 730..736 "CUUCCUU" (729,737) C:G 
H17 754..757 "UAAC" (753,758) C:G PK{3}
H18 789..792 "GCAA" (788,793) C:G 
H19 855..862 "AUGCAACG" (854,863) G:C 
H20 905..908 "GAGA" (904,909) A:U 
H21 919..923 "CUUGU" (918,924) C:G 
H22 966..969 "GUGA" (965,970) C:G 
H23 979..984 "UUAAGU" (978,985) G:C 
H24 1024..1029 "GUUAUG" (1023,1030) C:G 
H25 1056..1060 "GUCAA" (1055,1061) G:C 
H26 1156..1159 "GUGA" (1155,1160) C:G 
B1 36..37 "GA" (35,141) U:G (140,38) G:U 
B2 152..153 "AU" (151,193) U:G (192,154) C:C 
B3 159..160 "UG" (158,184) U:G (183,161) A:U 
B4 214..216 "GAC" (213,202) U:G (201,217) G:C 
B5 277..277 "U" (276,302) U:A (301,278) C:G 
B6 383..385 "GAA" (382,350) A:A (349,386) C:G PK{1}
B7 394..399 "GGCCAA" (393,430) C:G (429,400) G:C PK{2}
B8 459..463 "GUAAA" (458,772) C:G (771,464) C:G PK{3}
B9 472..472 "A" (471,647) U:G (646,473) U:G 
B10 484..484 "G" (483,534) C:G (533,485) G:C 
B11 530..531 "UA" (529,487) A:U (486,532) G:C 
B12 612..613 "UG" (611,622) G:G (621,614) C:G 
B13 637..637 "A" (636,547) U:A (546,638) U:A 
B14 664..666 "GGA" (663,694) G:C (693,667) U:G 
B15 938..938 "U" (937,1099) G:C (1098,939) C:G 
B16 943..944 "CA" (942,1095) G:U (1094,945) A:U 
B17 1019..1020 "AG" (1018,1033) C:G (1032,1021) G:C 
B18 1086..1086 "A" (1085,950) C:G (949,1087) U:A 
B19 1090..1092 "CAU" (1089,947) U:G (946,1093) G:C 
B20 1117..1117 "A" (1116,845) C:G (844,1118) G:C 
I1.1 40..42 "UGC" (39,139) C:G 
I1.2 137..138 "AU" (136,43) G:C 
I2.1 55..58 "GAUA" (54,82) G:C 
I2.2 77..81 "UAAUA" (76,59) C:A 
I3.1 98..98 "G" (97,110) U:G 
I3.2 109..109 "U" (108,99) C:G 
I4.1 155..155 "A" (154,192) C:C 
I4.2 187..191 "GACGA" (186,156) C:G 
I5.1 281..283 "CAA" (280,299) A:U 
I5.2 298..298 "A" (297,284) G:U 
I6.1 319..324 "GAUGAC" (318,342) G:C 
I6.2 337..341 "GUAAA" (336,325) U:G 
I7.1 356..358 "GAC" (355,377) G:C 
I7.2 373..376 "GGUA" (372,359) C:G 
I8.1 360..361 "AA" (359,372) G:C 
I8.2 370..371 "GA" (369,362) U:G 
I9.1 405..409 "UGCCA" (404,425) G:C 
I9.2 418..424 "GGUAAUA" (417,410) C:G 
I10.1 470..470 "G" (469,650) C:G 
I10.2 648..649 "AG" (647,471) G:U 
I11.1 496..499 "AAAA" (495,521) G:U 
I11.2 519..520 "CG" (518,500) G:C 
I12.1 553..554 "GG" (552,631) A:G 
I12.2 630..630 "G" (629,555) U:G 
I13.1 564..565 "AA" (563,605) G:A 
I13.2 603..604 "GA" (602,566) G:U 
I14.1 574..576 "GUA" (573,595) U:A 
I14.2 589..594 "GCAGAU" (588,577) C:G 
I15.1 669..671 "GAA" (668,692) C:G 
I15.2 689..691 "GUA" (688,672) G:C 
I16.1 779..784 "GAGUAC" (778,801) G:C 
I16.2 797..800 "AAAA" (796,785) U:G 
I17.1 1014..1016 "UUG" (1013,1039) G:C 
I17.2 1035..1038 "GACU" (1034,1017) G:C 
I18.1 1146..1147 "CU" (1145,1172) G:C 
I18.2 1168..1171 "GAAU" (1167,1148) C:G 
I19.1 1150..1152 "GAU" (1149,1166) C:G 
I19.2 1163..1165 "GGA" (1162,1153) U:G 
M1.1 22..27 "AGUAAC" (21,221) G:C (148,28) G:A 
M1.2 149..148 "" (148,28) G:A (195,149) G:C 
M1.3 196..197 "UA" (195,149) G:C (220,198) C:G 
M1.4 221..220 "" (220,198) C:G (21,221) G:C 
M2.1 50..50 "U" (49,130) C:G (85,51) A:U 
M2.2 86..90 "AUAUU" (85,51) A:U (116,91) G:U 
M2.3 117..120 "GAAA" (116,91) G:U (128,121) C:G 
M2.4 129..129 "G" (128,121) C:G (49,130) C:G 
M3.1 224..224 "A" (223,19) C:G (246,225) G:C 
M3.2 247..247 "A" (246,225) G:C (259,248) G:C 
M3.3 260..275 "GCAGCAGUGGGGAAUA" (259,248) G:C (302,276) A:U 
M3.4 303..314 "GCGACGCCGCGU" (302,276) A:U (345,315) C:G 
M3.5 346..348 "UUU" (345,315) C:G (386,349) G:C PK{1}
M3.6 387..388 "AA" (386,349) G:C (434,389) C:G 
M3.7 435..455 "GAGCGUUGUCCGGAAUUACUG" (434,389) C:G (774,456) C:G 
M3.8 775..775 "U" (774,456) C:G (803,776) C:G 
M3.9 804..834 "AAAGAAAUUGACGGGGGCCCGCACAAGCGGC" (803,776) C:G (1127,835) C:G 
M3.10 1128..1139 "AAUGGCCGGUAC" (1127,835) C:G (1177,1140) C:A 
M4.1 465..465 "A" (464,771) G:C (653,466) C:G 
M4.2 654..657 "GAAA" (653,466) C:G (699,658) C:G 
M4.3 700..709 "CGUAAACGGU" (699,658) C:G (770,710) C:G 
M4.4 771..770 "" (770,710) C:G (464,771) G:C 
M5.1 476..476 "G" (475,644) U:G (540,477) C:G 
M5.2 541..543 "UUG" (540,477) C:G (640,544) U:A 
M5.3 641..643 "GAC" (640,544) U:A (475,644) U:G 
M6.1 562..561 "" (561,623) U:G (606,562) C:G 
M6.2 607..610 "ACCG" (606,562) C:G (622,611) G:G 
M6.3 623..622 "" (622,611) G:G (561,623) U:G 
M7.1 716..717 "UA" (715,765) C:G (748,718) C:G 
M7.2 749..751 "GUA" (748,718) C:G (759,752) C:G 
M7.3 760..764 "AUUAA" (759,752) C:G (715,765) C:G 
M8.1 847..850 "UUAA" (846,1115) A:U (866,851) A:U 
M8.2 867..874 "GAACCUUA" (866,851) A:U (1111,875) G:C 
M8.3 1112..1114 "GCU" (1111,875) G:C (846,1115) A:U 
M9.1 882..886 "UUGAC" (881,1105) U:G (934,887) C:A 
M9.2 935..935 "A" (934,887) C:A (1100,936) C:G 
M9.3 1101..1104 "UUAU" (1100,936) C:G (881,1105) U:G 
M10.1 896..897 "AC" (895,926) G:C (915,898) U:G 
M10.2 916..916 "U" (915,898) U:G (925,917) G:C 
M10.3 926..925 "" (925,917) G:C (895,926) G:C 
M11.1 954..956 "UCA" (953,1082) G:C (996,957) C:G 
M11.2 997..1001 "GCAAC" (996,957) C:G (1077,1002) G:C 
M11.3 1078..1081 "AUGA" (1077,1002) G:C (953,1082) G:C 
M12.1 963..962 "" (962,991) U:A (972,963) U:G 
M12.2 973..974 "GU" (972,963) U:G (988,975) G:U 
M12.3 989..990 "CA" (988,975) G:U (962,991) U:A 
M13.1 1006..1006 "G" (1005,1074) U:G (1045,1007) A:U 
M13.2 1046..1047 "GA" (1045,1007) A:U (1068,1048) G:C 
M13.3 1069..1073 "AAGGU" (1068,1048) G:C (1005,1074) U:G 
E1 1..18 "UACUCGAGUGGCGAACGG" 
E2 1178..1179 "AA" 
PK1 1bp 347..347 385..385 M3.5 346..348 B6 383..385
PK2 3bp 394..396 413..415 B7 394..399 H11 412..415
PK3 2bp 459..460 756..757 B8 459..463 H17 754..757
PK1.1 347 U 385 A
PK2.1 394 G 415 C
PK2.2 395 G 414 C
PK2.3 396 C 413 G
PK3.1 459 G 757 C
PK3.2 460 U 756 A
NCBP1 1050 G 1066 A S74
NCBP2 350 A 382 A S24
NCBP3 1140 A 1177 C S75
NCBP4 887 A 934 C S59
NCBP5 28 A 148 G S2
NCBP6 951 U 1084 U S66
NCBP7 611 G 622 G S45
NCBP8 154 C 192 C S11
NCBP9 552 A 631 G S40
NCBP10 230 A 241 G S16
NCBP11 563 G 605 A S42
NCBP12 836 G 1126 A S55
NCBP13 59 A 76 C S6
segment1 3bp 19..21 GUG 221..223 CAC
segment2 17bp 28..49 ACGUGGGUGAUCUGCCCUGCAC 130..148 GUGUGGGAUGGGCCCGCGG
segment3 11bp 51..65 UUGGGAUAAGCCUGG 70..85 CUGGGUCUAAUACCGA
segment4 10bp 91..101 UCCUUCUGGUU 106..116 GGCUGGGAGGG
segment5 2bp 121..122 GC 127..128 GC
segment6 15bp 149..168 CCUAUCAGCUUGUUGGUGAG 176..195 CUUACCAAGGCGACGACGGG
segment7 8bp 198..205 GCCGGCCU 210..220 GGGUGACCGGC
segment8 6bp 225..230 CUGGGA 241..246 GCCCAG
segment9 4bp 248..251 CUCC 256..259 GGAG
segment10 9bp 276..288 UUGCACAAUGGGC 293..302 GCCUGAUGCA
segment11 8bp 315..328 GGGGGAUGACGGCC 333..345 GGUUGUAAACCUC
segment12 10bp 349..363 CAGUAGGGACGAAGC 368..386 GUGACGGUACCUAUAGAAG
segment13 12bp 389..411 GGACCGGCCAACUACGUGCCAGC 416..434 GCGGUAAUACGUAGGGUCC
segment14 4bp 456..464 GGCGUAAAG 771..774 CGCC
segment15 8bp 466..475 GCUCGUAGGU 644..653 GCUGAGGAGC
segment16 25bp 477..506 GUUUGUCGCGUUGUCCGUGAAAACUCACAG 512..540 CUGUGGGCGUGCGGGCGAUACGGGCAGAC
segment17 16bp 544..561 AGUACUGCAGGGGAGACU 623..640 GGUCUCUGGGCAGUAACU
segment18 13bp 562..579 GGAAUUCCUGGUGUAGCG 586..606 UGCGCAGAUAUCAGGAGGAAC
segment19 3bp 611..615 GUGGC 620..622 GCG
segment20 12bp 658..675 GCGUGGGGAGCGAACAGG 685..699 CCUGGUAGUCCACGC
segment21 6bp 710..715 GGGUAC 765..770 GUACCC
segment22 12bp 718..729 GGUGUGGGUUUC 737..748 GGGAUCCGUGCC
segment23 2bp 752..753 GC 758..759 GC
segment24 7bp 776..788 GGGGAGUACGGCC 793..803 GGCUAAAACUC
segment25 12bp 835..846 GGAGCAUGUGGA 1115..1127 UCACACAUGCUAC
segment26 4bp 851..854 UUCG 863..866 CGAA
segment27 7bp 875..881 CCUGGGU 1105..1111 GUCCAGG
segment28 9bp 887..895 AUGCACAGG 926..934 CCUGUGUGC
segment29 7bp 898..904 GACUGCA 909..915 UGUGGUU
segment30 2bp 917..918 CC 924..925 GG
segment31 15bp 936..953 GGUGGUGCAUGGCUGUCG 1082..1100 CGUCAAGUCAUCAUGCCCC
segment32 6bp 957..962 GCUCGU 991..996 ACGAGC
segment33 3bp 963..965 GUC 970..972 GAU
segment34 4bp 975..978 UGGG 985..988 CCCG
segment35 4bp 1002..1005 CCUU 1074..1077 GGGG
segment36 12bp 1007..1023 UCUCAUGUUGCCAGCGC 1030..1045 GCGGGGACUCGUGAGA
segment37 8bp 1048..1055 CUGCCGGG 1061..1068 CUCGGAGG
segment38 11bp 1140..1155 AAAGGGCUGCGAUGCC 1160..1177 GGUGGAGCGAAUCCUUUC
