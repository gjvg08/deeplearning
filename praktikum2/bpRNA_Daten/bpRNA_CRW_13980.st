#Name: bpRNA_CRW_13980
#Length:  1115 
#PageNumber: 2
CGCAAUGGGCGAAAGCCUGACGGAGCAACGCCGCGUGAGUGAUGAAGGUUUUCGGAUCGUAAAGCUCUGUUGCCAGGGAAGAACGUCCGGUAGAGUAACUGCUACCGGAGUGACGGUACCUGAGAAGAAAGCCCCGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGGGCAAGCGUUGUCCGGAAUUAUUGGGCGUAAAGCGCGCGCAGGCGGCUAUUUAAGUCUGGUGUUUAAACCUUGGGCUCAACCUGAGGUCGCACUGGAAACUGGGUGGCUUGAGUACAGAAGAGGAAAGUGGAAUUCCACGUGUAGCGGUGAAAUGCGUAGAGAUGUGGAGGAACACCAGUGGCGAAGGCGACUUUCUGGGCUGUAACUGACGCUGAGGCGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAGGUGUUAGGGGUUUCGAUACCCUUGGUGCCGAAGUUAACACAGUAAGCACUCCGCCUGGGGAGUACGGUCGCAAGACUGAAACUCAAAGGAAUUGACGGGGACCCGCACAAGCAGUGGAGUAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCCUCUGAAUCCUCUAGAGAUAGAGGCGGCCUUCGGGACAGAGGAGACAGGUGGUGCUUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGACUUUAGUUGCCAGCAGGUAGUGCUGGGCACUCUAGAGUGACUGCCGGUGACAAACCGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGACCUGGGCUACACACGUACUACAAUGGCCGGUACAACGGGAAGCGAAGCCGCGAGGUGGAGCCAAUCCCAGCAAAGCCGGUCUCAGUUCGGAUUGCAGGCUGCAACUCGCCUGCAUGAAGUCGGAAUUGCUAGUAAUCGCGGAUCAGCAUGCCGCGGUGAAUACGUUCCCGGGUCUUGUACACACCGCCCGUCACACCACGAGAGUUUACAACACCCGAAGUCGGUGGGGUAACCCGCAAGGGAGCCAGCCGCCGAAGGUGGGGUAGA
((...(((((....))))).))..............((((......((((....)))).....)))).(..((((((...(.....((((((((......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((.(....(((((((.....)))))))..))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....((((((.((..(((((((....)))))))..((....)).)).))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((........))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((...(...(((....)))...).....)))......)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).)).......................(((((((((((....((((((.((((..(((....))).))))))))))...)))))))))))
SSIIISSSSSHHHHSSSSSISSXXXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSBSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSISSMMSSSSSSSHHHHSSSSSSSMMSSHHHHSSMSSISSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIIISIIISSSHHHHSSSIIISIIIIISSSIIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSSSIIIISSSSSSBSSSSIISSSHHHHSSSISSSSSSSSSSIIISSSSSSSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..2 "CG" 21..22 "CG"
S2 6..10 "UGGGC" 15..19 "GCCUG"
S3 37..40 "GAGU" 64..67 "GCUC"
S4 47..50 "GGUU" 55..58 "GAUC"
S5 69..69 "G" 127..127 "G"
S6 72..77 "GCCAGG" 119..124 "CCUGAG"
S7 81..81 "G" 114..114 "C"
S8 87..94 "CCGGUAGA" 101..108 "GCUACCGG"
S9 131..135 "GCCCC" 172..176 "GGGGC"
S10 142..146 "CUACG" 167..171 "CGUAG"
S11 152..153 "GC" 158..159 "GC"
S12 198..200 "GGC" 512..514 "GCC"
S13 208..211 "GCGC" 391..394 "GCGC"
S14 213..213 "C" 388..388 "G"
S15 215..217 "GGC" 385..387 "GCU"
S16 219..225 "GCUAUUU" 275..281 "GGGUGGC"
S17 227..228 "AG" 273..274 "CU"
S18 229..235 "UCUGGUG" 264..270 "CACUGGA"
S19 237..237 "U" 263..263 "G"
S20 242..248 "CCUUGGG" 254..260 "CCUGAGG"
S21 285..287 "AGU" 379..381 "ACU"
S22 288..293 "ACAGAA" 372..377 "GGCUGU"
S23 296..302 "GGAAAGU" 364..370 "ACUUUCU"
S24 303..304 "GG" 346..347 "AC"
S25 307..314 "UUCCACGU" 336..343 "AUGUGGAG"
S26 318..320 "GCG" 327..329 "UGC"
S27 352..352 "G" 363..363 "G"
S28 355..356 "GC" 361..362 "GC"
S29 399..404 "GCGUGG" 435..440 "CCACGC"
S30 408..409 "GC" 433..434 "GU"
S31 413..416 "CAGG" 426..429 "CCUG"
S32 451..456 "GAGUGC" 505..510 "GCACUC"
S33 459..470 "GGUGUUAGGGGU" 477..488 "ACCCUUGGUGCC"
S34 492..493 "GU" 498..499 "AC"
S35 516..518 "GGG" 541..543 "CUC"
S36 525..528 "GGUC" 533..536 "GACU"
S37 552..553 "UG" 1028..1029 "CA"
S38 554..556 "ACG" 1024..1026 "UGU"
S39 558..564 "GGACCCG" 1017..1023 "CGGGUCU"
S40 566..567 "AC" 1012..1013 "GU"
S41 570..574 "GCAGU" 973..977 "AUUGC"
S42 575..584 "GGAGUAUGUG" 860..869 "CACGUACUAC"
S43 585..586 "GU" 857..858 "AC"
S44 591..594 "UUCG" 603..606 "CGAA"
S45 615..621 "CCAGGUC" 847..853 "GACCUGG"
S46 627..629 "AUC" 674..676 "GAC"
S47 630..632 "CCU" 670..672 "AGG"
S48 634..635 "UG" 667..668 "CA"
S49 638..644 "UCCUCUA" 649..655 "UAGAGGC"
S50 658..659 "CC" 664..665 "GG"
S51 678..679 "GG" 841..842 "CC"
S52 681..684 "GGUG" 837..840 "UGCC"
S53 687..688 "UG" 835..836 "CA"
S54 689..691 "GUU" 829..831 "AAU"
S55 692..695 "GUCG" 824..827 "CGUC"
S56 699..704 "GCUCGU" 733..738 "ACGAGC"
S57 705..707 "GUC" 712..714 "GAU"
S58 717..720 "UGGG" 727..730 "CCCG"
S59 744..747 "CCUU" 816..819 "GGGG"
S60 749..755 "ACUUUAG" 781..787 "CUAGAGU"
S61 759..760 "CC" 775..776 "GG"
S62 763..764 "CA" 773..774 "UG"
S63 790..797 "CUGCCGGU" 803..810 "ACCGGAGG"
S64 873..879 "GGCCGGU" 923..929 "GCCGGUC"
S65 885..887 "GGG" 914..916 "CCC"
S66 891..891 "C" 908..908 "G"
S67 895..897 "GCC" 902..904 "GGU"
S68 936..936 "C" 967..967 "G"
S69 940..947 "UUGCAGGC" 956..963 "GCCUGCAU"
S70 983..990 "AUCGCGGA" 998..1005 "GCCGCGGU"
S71 1053..1063 "UUUACAACACC" 1105..1115 "GGUGGGGUAGA"
S72 1068..1073 "GUCGGU" 1096..1101 "GCCGCC"
S73 1075..1078 "GGGU" 1092..1095 "GCCA"
S74 1081..1083 "CCC" 1088..1090 "GGG"
H1 11..14 "GAAA" (10,15) C:G 
H2 51..54 "UUCG" (50,55) U:G 
H3 95..100 "GUAACU" (94,101) A:G 
H4 154..157 "AGCC" (153,158) C:G PK{1}
H5 249..253 "CUCAA" (248,254) G:C 
H6 321..326 "GUGAAA" (320,327) G:U 
H7 357..360 "GAAG" (356,361) C:G 
H8 417..425 "AUUAGAUAC" (416,426) G:C 
H9 471..476 "UUCGAU" (470,477) U:A 
H10 494..497 "UAAC" (493,498) U:A PK{2}
H11 529..532 "GCAA" (528,533) C:G 
H12 595..602 "AAGCAACG" (594,603) G:C 
H13 645..648 "GAGA" (644,649) A:U 
H14 660..663 "UUCG" (659,664) C:G 
H15 708..711 "GUGA" (707,712) C:G 
H16 721..726 "UUAAGU" (720,727) G:C 
H17 765..772 "GGUAGUGC" (764,773) A:U 
H18 798..802 "GACAA" (797,803) U:A 
H19 898..901 "GCGA" (897,902) C:G 
H20 948..955 "UGCAACUC" (947,956) C:G 
H21 991..997 "UCAGCAU" (990,998) A:G 
H22 1084..1087 "GCAA" (1083,1088) C:G 
B1 136..141 "GGCUAA" (135,172) C:G (171,142) G:C PK{1}
B2 214..214 "A" (213,388) C:G (387,215) U:G 
B3 226..226 "A" (225,275) U:G (274,227) U:A 
B4 236..236 "U" (235,264) G:C (263,237) G:U 
B5 271..272 "AA" (270,229) A:U (228,273) G:C 
B6 353..354 "UG" (352,363) G:G (362,355) C:G 
B7 378..378 "A" (377,288) U:A (287,379) U:A 
B8 405..407 "GGA" (404,435) G:C (434,408) U:G 
B9 557..557 "G" (556,1024) G:U (1023,558) U:G 
B10 673..673 "A" (672,630) G:C (629,674) C:G 
B11 680..680 "U" (679,841) G:C (840,681) C:G 
B12 685..686 "CU" (684,837) G:U (836,687) A:U 
B13 761..762 "AG" (760,775) C:G (774,763) G:C 
B14 828..828 "A" (827,692) C:G (691,829) U:A 
B15 832..834 "CAU" (831,689) U:G (688,835) G:C 
B16 859..859 "A" (858,585) C:G (584,860) G:C 
B17 1027..1027 "A" (1026,554) U:A (553,1028) G:C 
B18 1074..1074 "G" (1073,1096) U:G (1095,1075) A:G 
I1.1 3..5 "CAA" (2,21) G:C 
I1.2 20..20 "A" (19,6) G:U 
I2.1 41..46 "GAUGAA" (40,64) U:G 
I2.2 59..63 "GUAAA" (58,47) C:G 
I3.1 70..71 "UU" (69,127) G:G 
I3.2 125..126 "AA" (124,72) G:G 
I4.1 78..80 "GAA" (77,119) G:C 
I4.2 115..118 "GGUA" (114,81) C:G 
I5.1 82..86 "AACGU" (81,114) G:C 
I5.2 109..113 "AGUGA" (108,87) G:C 
I6.1 147..151 "UGCCA" (146,167) G:C 
I6.2 160..166 "GGUAAUA" (159,152) C:G 
I7.1 212..212 "G" (211,391) C:G 
I7.2 389..390 "AG" (388,213) G:C 
I8.1 238..241 "UAAA" (237,263) U:G 
I8.2 261..262 "UC" (260,242) G:C 
I9.1 294..295 "GA" (293,372) A:G 
I9.2 371..371 "G" (370,296) U:G 
I10.1 305..306 "AA" (304,346) G:A 
I10.2 344..345 "GA" (343,307) G:U 
I11.1 315..317 "GUA" (314,336) U:A 
I11.2 330..335 "GUAGAG" (329,318) C:G 
I12.1 410..412 "AAA" (409,433) C:G 
I12.2 430..432 "GUA" (429,413) G:C 
I13.1 519..524 "GAGUAC" (518,541) G:C 
I13.2 537..540 "GAAA" (536,525) U:G 
I14.1 565..565 "C" (564,1017) G:C 
I14.2 1014..1016 "UCC" (1013,566) U:A 
I15.1 633..633 "C" (632,670) U:A 
I15.2 669..669 "G" (668,634) A:U 
I16.1 756..758 "UUG" (755,781) G:C 
I16.2 777..780 "CACU" (776,759) G:C 
I17.1 880..884 "ACAAC" (879,923) U:G 
I17.2 917..922 "AGCAAA" (916,885) C:G 
I18.1 888..890 "AAG" (887,914) G:C 
I18.2 909..913 "CCAAU" (908,891) G:C 
I19.1 892..894 "GAA" (891,908) C:G 
I19.2 905..907 "GGA" (904,895) U:G 
I20.1 937..939 "GGA" (936,967) C:G 
I20.2 964..966 "GAA" (963,940) U:U 
I21.1 1064..1067 "CGAA" (1063,1105) C:G 
I21.2 1102..1104 "GAA" (1101,1068) C:G 
I22.1 1079..1080 "AA" (1078,1092) U:G 
I22.2 1091..1091 "A" (1090,1081) G:C 
M1.1 201..207 "GUAAAGC" (200,512) C:G (394,208) C:G PK{2}
M1.2 395..398 "GAAA" (394,208) C:G (440,399) C:G 
M1.3 441..450 "CGUAAACGAU" (440,399) C:G (510,451) C:G 
M1.4 511..511 "C" (510,451) C:G (200,512) C:G 
M2.1 218..218 "G" (217,385) C:G (281,219) C:G 
M2.2 282..284 "UUG" (281,219) C:G (381,285) U:A 
M2.3 382..384 "GAC" (381,285) U:A (217,385) C:G 
M3.1 303..302 "" (302,364) U:A (347,303) C:G 
M3.2 348..351 "ACCA" (347,303) C:G (363,352) G:G 
M3.3 364..363 "" (363,352) G:G (302,364) U:A 
M4.1 457..458 "UA" (456,505) C:G (488,459) C:G 
M4.2 489..491 "GAA" (488,459) C:G (499,492) C:G 
M4.3 500..504 "AGUAA" (499,492) C:G (456,505) C:G 
M5.1 568..569 "AA" (567,1012) C:G (977,570) C:G 
M5.2 978..982 "UAGUA" (977,570) C:G (1005,983) U:A 
M5.3 1006..1011 "GAAUAC" (1005,983) U:A (567,1012) C:G 
M6.1 575..574 "" (574,973) U:A (869,575) C:G 
M6.2 870..872 "AAU" (869,575) C:G (929,873) C:G 
M6.3 930..935 "UCAGUU" (929,873) C:G (967,936) G:C 
M6.4 968..972 "UCGGA" (967,936) G:C (574,973) U:A 
M7.1 587..590 "UUAA" (586,857) U:A (606,591) A:U 
M7.2 607..614 "GAACCUUA" (606,591) A:U (853,615) G:C 
M7.3 854..856 "GCU" (853,615) G:C (586,857) U:A 
M8.1 622..626 "UUGAC" (621,847) C:G (676,627) C:A 
M8.2 677..677 "A" (676,627) C:A (842,678) C:G 
M8.3 843..846 "UUAU" (842,678) C:G (621,847) C:G 
M9.1 636..637 "AA" (635,667) G:C (655,638) C:U 
M9.2 656..657 "GG" (655,638) C:U (665,658) G:C 
M9.3 666..666 "A" (665,658) G:C (635,667) G:C 
M10.1 696..698 "UCA" (695,824) G:C (738,699) C:G 
M10.2 739..743 "GCAAC" (738,699) C:G (819,744) G:C 
M10.3 820..823 "AUGA" (819,744) G:C (695,824) G:C 
M11.1 705..704 "" (704,733) U:A (714,705) U:G 
M11.2 715..716 "GU" (714,705) U:G (730,717) G:U 
M11.3 731..732 "CA" (730,717) G:U (704,733) U:A 
M12.1 748..748 "G" (747,816) U:G (787,749) U:A 
M12.2 788..789 "GA" (787,749) U:A (810,790) G:C 
M12.3 811..815 "AAGGU" (810,790) G:C (747,816) U:G 
X1 23..36 "GAGCAACGCCGCGU" (22,1) G:C (67,37) C:G 
X3 128..130 "AAA" (127,69) G:G (176,131) C:G 
X4 177..197 "AAGCGUUGUCCGGAAUUAUUG" (176,131) C:G (514,198) C:G 
X6 544..551 "AAAGGAAU" (543,516) C:G (1029,552) A:U 
X7 1030..1052 "CACCGCCCGUCACACCACGAGAG" (1029,552) A:U (1115,1053) A:U 
PK1 3bp 136..138 155..157 B1 136..141 H4 154..157
PK2 2bp 201..202 496..497 M1.1 201..207 H10 494..497
PK1.1 136 G 157 C
PK1.2 137 G 156 C
PK1.3 138 C 155 G
PK2.1 201 G 497 C
PK2.2 202 U 496 A
NCBP1 627 A 676 C S46
NCBP2 940 U 963 U S69
NCBP3 638 U 655 C S49
NCBP4 1075 G 1095 A S73
NCBP5 94 A 101 G S8
NCBP6 990 A 998 G S70
NCBP7 69 G 127 G S5
NCBP8 352 G 363 G S27
NCBP9 72 G 124 G S6
NCBP10 293 A 372 G S22
NCBP11 1069 U 1100 C S72
NCBP12 292 A 373 G S22
NCBP13 73 C 123 A S6
NCBP14 1059 A 1109 G S71
NCBP15 693 U 826 U S55
NCBP16 576 G 868 A S42
NCBP17 1058 A 1110 G S71
NCBP18 792 G 808 A S63
NCBP19 304 G 346 A S24
segment1 7bp 1..10 CGCAAUGGGC 15..22 GCCUGACG
segment2 8bp 37..50 GAGUGAUGAAGGUU 55..67 GAUCGUAAAGCUC
segment3 16bp 69..94 GUUGCCAGGGAAGAACGUCCGGUAGA 101..127 GCUACCGGAGUGACGGUACCUGAGAAG
segment4 12bp 131..153 GCCCCGGCUAACUACGUGCCAGC 158..176 GCGGUAAUACGUAGGGGGC
segment5 3bp 198..200 GGC 512..514 GCC
segment6 8bp 208..217 GCGCGCAGGC 385..394 GCUGAGGCGC
segment7 24bp 219..248 GCUAUUUAAGUCUGGUGUUUAAACCUUGGG 254..281 CCUGAGGUCGCACUGGAAACUGGGUGGC
segment8 16bp 285..302 AGUACAGAAGAGGAAAGU 364..381 ACUUUCUGGGCUGUAACU
segment9 13bp 303..320 GGAAUUCCACGUGUAGCG 327..347 UGCGUAGAGAUGUGGAGGAAC
segment10 3bp 352..356 GUGGC 361..363 GCG
segment11 12bp 399..416 GCGUGGGGAGCAAACAGG 426..440 CCUGGUAGUCCACGC
segment12 6bp 451..456 GAGUGC 505..510 GCACUC
segment13 12bp 459..470 GGUGUUAGGGGU 477..488 ACCCUUGGUGCC
segment14 2bp 492..493 GU 498..499 AC
segment15 7bp 516..528 GGGGAGUACGGUC 533..543 GACUGAAACUC
segment16 14bp 552..567 UGACGGGGACCCGCAC 1012..1029 GUUCCCGGGUCUUGUACA
segment17 5bp 570..574 GCAGU 973..977 AUUGC
segment18 12bp 575..586 GGAGUAUGUGGU 857..869 ACACACGUACUAC
segment19 4bp 591..594 UUCG 603..606 CGAA
segment20 7bp 615..621 CCAGGUC 847..853 GACCUGG
segment21 8bp 627..635 AUCCCUCUG 667..676 CAGAGGAGAC
segment22 7bp 638..644 UCCUCUA 649..655 UAGAGGC
segment23 2bp 658..659 CC 664..665 GG
segment24 15bp 678..695 GGUGGUGCUUGGUUGUCG 824..842 CGUCAAAUCAUCAUGCCCC
segment25 6bp 699..704 GCUCGU 733..738 ACGAGC
segment26 3bp 705..707 GUC 712..714 GAU
segment27 4bp 717..720 UGGG 727..730 CCCG
segment28 4bp 744..747 CCUU 816..819 GGGG
segment29 11bp 749..764 ACUUUAGUUGCCAGCA 773..787 UGGGCACUCUAGAGU
segment30 8bp 790..797 CUGCCGGU 803..810 ACCGGAGG
segment31 14bp 873..897 GGCCGGUACAACGGGAAGCGAAGCC 902..929 GGUGGAGCCAAUCCCAGCAAAGCCGGUC
segment32 9bp 936..947 CGGAUUGCAGGC 956..967 GCCUGCAUGAAG
segment33 8bp 983..990 AUCGCGGA 998..1005 GCCGCGGU
segment34 24bp 1053..1083 UUUACAACACCCGAAGUCGGUGGGGUAACCC 1088..1115 GGGAGCCAGCCGCCGAAGGUGGGGUAGA
