#Name: bpRNA_CRW_9625
#Length:  1090 
#PageNumber: 2
GACGAACGCUGGCGGCGUGCCUAAUACAUGCAAGUAGAACGCUGAAGACUUUAGCUUGCUAAAGUUGGAAGAGUUGCGAACGGGUGAGUAACGCGUAGGUAACCUGCCUACUAGCGGGGGAUAACUAUUGGAAACGAUAGCUAAUACCGCAUAACAGCAUUUAACCCAUGUUAGAUGCUUGAAAGGAGCAAUUGCUUCACUAGUAGAUGGACCUGCGUUGUAUUAGCUAGUUGGUGAGGUAACGGCUCACCAAGGCGACGAUACAUAGCCGACCUGAGAGGGUGAUCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCGGCAAUGGGGGCAACCCUGACCGAGCAACGCCGCGUGAGUGAAGAAGGUUUUCGGAUCGUAAAGCUCUGUUGUAAGAGAAGAACGUGUGUGAGAGUGGAAAGUUCACACAGUGACGGUAACUUACCAGAAAGGGACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUCCCGAGCGUUGUCCGGAUUUAUUGGGCGUAAAGCGAGCGCAGGCGGUUUAAUAAGUCUGAAGUUAAAGGCAGUGGCUUAACCAUUGUUCGCUUUGGAAACUGUUAGACUUGAGUGCAGAAGGGGAGAGUGGAAUUCCAUGUGUAGCGGUGAAAUGCGUAGAUAUAUGGAGGAACACCGGUGGCGAAAGCGGCUCUCUGGUCUGUAACUGACGCUGAGGCUCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAGGUGUUAGGCCCUUUCCGGGGCUUAGUGCCGCAGCUAACGCAUUAAGCACUCCGCCUGGGGAGUACGACCGCAAGGUUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCCGAUGCUAUUCCUAGAGAUAGGAAGUUUCUUCGGAACAUCGGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCA
(((.((((((.(((((((((....(((.(((..(((..(((((.((...(((((....)))))...)).))))))))......(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....((((((((((....)))))))))).....(((((....))))).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).)).))))))..((((......((((....)))).....)))).(..((((((...(.....((((((((.......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).)))))))))...........((([[.....((((.(.(((.(((((((.((((((((((.....(((((((.....)))))))..))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))...............................................((((........))))....................((((((.((..(((((((....))))))).(((....))))).))).)))......................(((((((((....)))..((((......))))..))))))...
SSSBSSSSSSMSSSSSSSSSIIIISSSBSSSMMSSSBBSSSSSISSIIISSSSSHHHHSSSSSIIISSISSSSSSSSMMMMMMSSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHSSSSSSSSSSMMMMMSSSSSHHHHSSSSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSBSSSSSSMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMSIISSSSSSIIISIIIIISSSSSSSSHHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISMMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMSSSSSSSSSMMMMMMMMMMMSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSIIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSMSSSIIIIIISSSSHHHHSSSSIIIISSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSSSHHHHHHHHSSSSMMMMMMMMMMMMMMMMMMMMSSSSSSISSMMSSSSSSSHHHHSSSSSSSMSSSHHHHSSSSSISSSBSSSMMMMMMMMMMMMMMMMMMMMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..3 "GAC" 533..535 "GUC"
S2 5..10 "AACGCU" 527..532 "AGCGUU"
S3 12..17 "GCGGCG" 377..382 "CGCCGC"
S4 18..19 "UG" 374..375 "CA"
S5 20..20 "C" 340..340 "G"
S6 25..27 "UAC" 336..338 "GUA"
S7 29..31 "UGC" 333..335 "GCA"
S8 34..36 "GUA" 75..77 "UGC"
S9 39..43 "ACGCU" 70..74 "AGAGU"
S10 45..46 "AA" 67..68 "GG"
S11 50..54 "UUUAG" 59..63 "CUAAA"
S12 84..86 "GUG" 291..293 "CAC"
S13 93..100 "GCGUAGGU" 211..218 "ACCUGCGU"
S14 103..104 "CC" 209..210 "GG"
S15 108..114 "CUACUAG" 200..206 "CUAGUAG"
S16 116..119 "GGGG" 147..150 "CCGC"
S17 124..130 "ACUAUUG" 135..141 "CGAUAGC"
S18 156..165 "AGCAUUUAAC" 170..179 "GUUAGAUGCU"
S19 185..189 "GGAGC" 194..198 "GCUUC"
S20 219..221 "UGU" 263..265 "ACA"
S21 224..224 "U" 262..262 "U"
S22 226..228 "GCU" 254..256 "GGC"
S23 231..238 "UUGGUGAG" 246..253 "CUCACCAA"
S24 268..271 "GCCG" 287..290 "CGGC"
S25 272..275 "ACCU" 280..283 "GGGU"
S26 295..300 "CUGGGA" 311..316 "GCCCAG"
S27 318..321 "CUCC" 326..329 "GGAG"
S28 346..346 "U" 372..372 "A"
S29 348..350 "CGG" 369..371 "CCG"
S30 354..358 "UGGGG" 363..367 "CCCUG"
S31 385..388 "GAGU" 412..415 "GCUC"
S32 395..398 "GGUU" 403..406 "GAUC"
S33 417..417 "G" 476..476 "G"
S34 420..425 "GUAAGA" 468..473 "ACUUAC"
S35 429..429 "G" 463..463 "C"
S36 435..442 "GUGUGAGA" 450..457 "GUUCACAC"
S37 480..484 "GGGAC" 521..525 "GUCCC"
S38 491..495 "CUACG" 516..520 "CGUAG"
S39 501..502 "GC" 507..508 "GC"
S40 547..549 "GGC" 861..863 "GCC"
S41 557..560 "GAGC" 740..743 "GCUC"
S42 562..562 "C" 737..737 "G"
S43 564..566 "GGC" 734..736 "GCU"
S44 568..574 "GUUUAAU" 624..630 "GUUAGAC"
S45 576..577 "AG" 622..623 "CU"
S46 578..585 "UCUGAAGU" 612..619 "GCUUUGGA"
S47 591..597 "GCAGUGG" 603..609 "CCAUUGU"
S48 634..636 "AGU" 728..730 "ACU"
S49 637..642 "GCAGAA" 721..726 "GUCUGU"
S50 645..651 "GGAGAGU" 713..719 "GCUCUCU"
S51 652..653 "GG" 695..696 "AC"
S52 656..663 "UUCCAUGU" 685..692 "AUAUGGAG"
S53 667..669 "GCG" 676..678 "UGC"
S54 701..701 "G" 712..712 "G"
S55 704..705 "GC" 710..711 "GC"
S56 748..753 "GCGUGG" 784..789 "CCACGC"
S57 757..758 "GC" 782..783 "GU"
S58 762..765 "CAGG" 775..778 "CCUG"
S59 800..805 "GAGUGC" 854..859 "GCACUC"
S60 808..819 "GGUGUUAGGCCC" 826..837 "GGGCUUAGUGCC"
S61 841..842 "GC" 847..848 "GC"
S62 865..867 "GGG" 890..892 "CUC"
S63 874..877 "GACC" 882..885 "GGUU"
S64 940..943 "UUCG" 952..955 "CGAA"
S65 976..978 "AUC" 1023..1025 "GAC"
S66 979..981 "CCG" 1019..1021 "CGG"
S67 983..984 "UG" 1016..1017 "CA"
S68 987..993 "AUUCCUA" 998..1004 "UAGGAAG"
S69 1006..1008 "UUC" 1013..1015 "GAA"
S70 1048..1053 "GCUCGU" 1082..1087 "ACGAGC"
S71 1054..1056 "GUC" 1061..1063 "GAU"
S72 1066..1069 "UGGG" 1076..1079 "CCCG"
H1 55..58 "CUUG" (54,59) G:C 
H2 131..134 "GAAA" (130,135) G:C 
H3 166..169 "CCAU" (165,170) C:G 
H4 190..193 "AAUU" (189,194) C:G 
H5 239..245 "GUAACGG" (238,246) G:C 
H6 276..279 "GAGA" (275,280) U:G 
H7 301..310 "CUGAGACACG" (300,311) A:G 
H8 322..325 "UACG" (321,326) C:G 
H9 359..362 "GCAA" (358,363) G:C 
H10 399..402 "UUCG" (398,403) U:G 
H11 443..449 "GUGGAAA" (442,450) A:G 
H12 503..506 "AGCC" (502,507) C:G PK{1}
H13 598..602 "CUUAA" (597,603) G:C 
H14 670..675 "GUGAAA" (669,676) G:U 
H15 706..709 "GAAA" (705,710) C:G 
H16 766..774 "AUUAGAUAC" (765,775) G:C 
H17 820..825 "UUUCCG" (819,826) C:G 
H18 843..846 "UAAC" (842,847) C:G PK{2}
H19 878..881 "GCAA" (877,882) C:G 
H20 944..951 "AAGCAACG" (943,952) G:C 
H21 994..997 "GAGA" (993,998) A:U 
H22 1009..1012 "UUCG" (1008,1013) C:G 
H23 1057..1060 "GUGA" (1056,1061) C:G 
H24 1070..1075 "UUAAGU" (1069,1076) G:C 
B1 4..4 "G" (3,533) C:G (532,5) U:A 
B2 28..28 "A" (27,336) C:G (335,29) A:U 
B3 37..38 "GA" (36,75) A:U (74,39) U:A 
B4 101..102 "AA" (100,211) U:A (210,103) G:C 
B5 222..223 "AU" (221,263) U:A (262,224) U:U 
B6 229..230 "AG" (228,254) U:G (253,231) A:U 
B7 284..286 "GAU" (283,272) U:A (271,287) G:C 
B8 347..347 "U" (346,372) U:A (371,348) G:C 
B9 376..376 "A" (375,18) A:U (17,377) G:C 
B10 485..490 "GGCUAA" (484,521) C:G (520,491) G:C PK{1}
B11 563..563 "A" (562,737) C:G (736,564) U:G 
B12 575..575 "A" (574,624) U:G (623,576) U:A 
B13 620..621 "AA" (619,578) A:U (577,622) G:C 
B14 702..703 "UG" (701,712) G:G (711,704) C:G 
B15 727..727 "A" (726,637) U:G (636,728) U:A 
B16 754..756 "GGA" (753,784) G:C (783,757) U:G 
B17 1022..1022 "U" (1021,979) G:C (978,1023) C:G 
I1.1 21..24 "CUAA" (20,340) C:G 
I1.2 339..339 "G" (338,25) A:U 
I2.1 44..44 "G" (43,70) U:A 
I2.2 69..69 "A" (68,45) G:A 
I3.1 47..49 "GAC" (46,67) A:G 
I3.2 64..66 "GUU" (63,50) A:U 
I4.1 105..107 "UGC" (104,209) C:G 
I4.2 207..208 "AU" (206,108) G:C 
I5.1 120..123 "GAUA" (119,147) G:C 
I5.2 142..146 "UAAUA" (141,124) C:A 
I6.1 225..225 "A" (224,262) U:U 
I6.2 257..261 "GACGA" (256,226) C:G 
I7.1 351..353 "CAA" (350,369) G:C 
I7.2 368..368 "A" (367,354) G:U 
I8.1 389..394 "GAAGAA" (388,412) U:G 
I8.2 407..411 "GUAAA" (406,395) C:G 
I9.1 418..419 "UU" (417,476) G:G 
I9.2 474..475 "CA" (473,420) C:G 
I10.1 426..428 "GAA" (425,468) A:A 
I10.2 464..467 "GGUA" (463,429) C:G 
I11.1 430..434 "AACGU" (429,463) G:C 
I11.2 458..462 "AGUGA" (457,435) C:G 
I12.1 496..500 "UGCCA" (495,516) G:C 
I12.2 509..515 "GGUAAUA" (508,501) C:G 
I13.1 561..561 "G" (560,740) C:G 
I13.2 738..739 "AG" (737,562) G:C 
I14.1 586..590 "UAAAG" (585,612) U:G 
I14.2 610..611 "UC" (609,591) U:G 
I15.1 643..644 "GG" (642,721) A:G 
I15.2 720..720 "G" (719,645) U:G 
I16.1 654..655 "AA" (653,695) G:A 
I16.2 693..694 "GA" (692,656) G:U 
I17.1 664..666 "GUA" (663,685) U:A 
I17.2 679..684 "GUAGAU" (678,667) C:G 
I18.1 759..761 "AAA" (758,782) C:G 
I18.2 779..781 "GUA" (778,762) G:C 
I19.1 868..873 "GAGUAC" (867,890) G:C 
I19.2 886..889 "GAAA" (885,874) U:G 
I20.1 982..982 "A" (981,1019) G:C 
I20.2 1018..1018 "U" (1017,983) A:U 
M1.1 11..11 "G" (10,527) U:A (382,12) C:G 
M1.2 383..384 "GU" (382,12) C:G (415,385) C:G 
M1.3 416..416 "U" (415,385) C:G (476,417) G:G 
M1.4 477..479 "AAA" (476,417) G:G (525,480) C:G 
M1.5 526..526 "G" (525,480) C:G (10,527) U:A 
M2.1 20..19 "" (19,374) G:C (340,20) G:C 
M2.2 341..345 "GAAUC" (340,20) G:C (372,346) A:U 
M2.3 373..373 "G" (372,346) A:U (19,374) G:C 
M3.1 32..33 "AA" (31,333) C:G (77,34) C:G 
M3.2 78..83 "GAACGG" (77,34) C:G (293,84) C:G 
M3.3 294..294 "A" (293,84) C:G (316,295) G:C 
M3.4 317..317 "A" (316,295) G:C (329,318) G:C 
M3.5 330..332 "GCA" (329,318) G:C (31,333) C:G 
M4.1 87..92 "AGUAAC" (86,291) G:C (218,93) U:G 
M4.2 219..218 "" (218,93) U:G (265,219) A:U 
M4.3 266..267 "UA" (265,219) A:U (290,268) C:G 
M4.4 291..290 "" (290,268) C:G (86,291) G:C 
M5.1 115..115 "C" (114,200) G:C (150,116) C:G 
M5.2 151..155 "AUAAC" (150,116) C:G (179,156) U:A 
M5.3 180..184 "UGAAA" (179,156) U:A (198,185) C:G 
M5.4 199..199 "A" (198,185) C:G (114,200) G:C 
M6.1 536..546 "CGGAUUUAUUG" (535,1) C:G (863,547) C:G 
M6.2 864..864 "U" (863,547) C:G (892,865) C:G 
M6.3 893..939 "AAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAA" (892,865) C:G (955,940) A:U 
M6.4 956..975 "GAACCUUACCAGGUCUUGAC" (955,940) A:U (1025,976) C:A 
M6.5 1026..1047 "AGGUGGUGCAUGGUUGUCGUCA" (1025,976) C:A (1087,1048) C:G 
M7.1 550..556 "GUAAAGC" (549,861) C:G (743,557) C:G PK{2}
M7.2 744..747 "GAAA" (743,557) C:G (789,748) C:G 
M7.3 790..799 "CGUAAACGAU" (789,748) C:G (859,800) C:G 
M7.4 860..860 "C" (859,800) C:G (549,861) C:G 
M8.1 567..567 "G" (566,734) C:G (630,568) C:G 
M8.2 631..633 "UUG" (630,568) C:G (730,634) U:A 
M8.3 731..733 "GAC" (730,634) U:A (566,734) C:G 
M9.1 652..651 "" (651,713) U:G (696,652) C:G 
M9.2 697..700 "ACCG" (696,652) C:G (712,701) G:G 
M9.3 713..712 "" (712,701) G:G (651,713) U:G 
M10.1 806..807 "UA" (805,854) C:G (837,808) C:G 
M10.2 838..840 "GCA" (837,808) C:G (848,841) C:G 
M10.3 849..853 "AUUAA" (848,841) C:G (805,854) C:G 
M11.1 985..986 "CU" (984,1016) G:C (1004,987) G:A 
M11.2 1005..1005 "U" (1004,987) G:A (1015,1006) A:U 
M11.3 1016..1015 "" (1015,1006) A:U (984,1016) G:C 
M12.1 1054..1053 "" (1053,1082) U:A (1063,1054) U:G 
M12.2 1064..1065 "GU" (1063,1054) U:G (1079,1066) G:U 
M12.3 1080..1081 "CA" (1079,1066) G:U (1053,1082) U:A 
E1 1088..1090 "GCA" 
PK1 3bp 485..487 504..506 B10 485..490 H12 503..506
PK2 2bp 550..551 845..846 M7.1 550..556 H18 843..846
PK1.1 485 G 506 C
PK1.2 486 G 505 C
PK1.3 487 C 504 G
PK2.1 550 G 846 C
PK2.2 551 U 845 A
NCBP1 124 A 141 C S17
NCBP2 653 G 695 A S51
NCBP3 976 A 1025 C S65
NCBP4 417 G 476 G S33
NCBP5 45 A 68 G S10
NCBP6 300 A 311 G S26
NCBP7 224 U 262 U S21
NCBP8 425 A 468 A S34
NCBP9 442 A 450 G S36
NCBP10 642 A 721 G S49
NCBP11 701 G 712 G S54
NCBP12 41 G 72 A S9
NCBP13 46 A 67 G S10
NCBP14 987 A 1004 G S68
NCBP15 117 G 149 G S16
segment1 9bp 1..10 GACGAACGCU 527..535 AGCGUUGUC
segment2 8bp 12..19 GCGGCGUG 374..382 CAACGCCGC
segment3 7bp 20..31 CCUAAUACAUGC 333..340 GCAGUAGG
segment4 15bp 34..54 GUAGAACGCUGAAGACUUUAG 59..77 CUAAAGUUGGAAGAGUUGC
segment5 3bp 84..86 GUG 291..293 CAC
segment6 17bp 93..114 GCGUAGGUAACCUGCCUACUAG 200..218 CUAGUAGAUGGACCUGCGU
segment7 11bp 116..130 GGGGGAUAACUAUUG 135..150 CGAUAGCUAAUACCGC
segment8 10bp 156..165 AGCAUUUAAC 170..179 GUUAGAUGCU
segment9 5bp 185..189 GGAGC 194..198 GCUUC
segment10 15bp 219..238 UGUAUUAGCUAGUUGGUGAG 246..265 CUCACCAAGGCGACGAUACA
segment11 8bp 268..275 GCCGACCU 280..290 GGGUGAUCGGC
segment12 6bp 295..300 CUGGGA 311..316 GCCCAG
segment13 4bp 318..321 CUCC 326..329 GGAG
segment14 9bp 346..358 UUCGGCAAUGGGG 363..372 CCCUGACCGA
segment15 8bp 385..398 GAGUGAAGAAGGUU 403..415 GAUCGUAAAGCUC
segment16 16bp 417..442 GUUGUAAGAGAAGAACGUGUGUGAGA 450..476 GUUCACACAGUGACGGUAACUUACCAG
segment17 12bp 480..502 GGGACGGCUAACUACGUGCCAGC 507..525 GCGGUAAUACGUAGGUCCC
segment18 3bp 547..549 GGC 861..863 GCC
segment19 8bp 557..566 GAGCGCAGGC 734..743 GCUGAGGCUC
segment20 24bp 568..597 GUUUAAUAAGUCUGAAGUUAAAGGCAGUGG 603..630 CCAUUGUUCGCUUUGGAAACUGUUAGAC
segment21 16bp 634..651 AGUGCAGAAGGGGAGAGU 713..730 GCUCUCUGGUCUGUAACU
segment22 13bp 652..669 GGAAUUCCAUGUGUAGCG 676..696 UGCGUAGAUAUAUGGAGGAAC
segment23 3bp 701..705 GUGGC 710..712 GCG
segment24 12bp 748..765 GCGUGGGGAGCAAACAGG 775..789 CCUGGUAGUCCACGC
segment25 6bp 800..805 GAGUGC 854..859 GCACUC
segment26 12bp 808..819 GGUGUUAGGCCC 826..837 GGGCUUAGUGCC
segment27 2bp 841..842 GC 847..848 GC
segment28 7bp 865..877 GGGGAGUACGACC 882..892 GGUUGAAACUC
segment29 4bp 940..943 UUCG 952..955 CGAA
segment30 8bp 976..984 AUCCCGAUG 1016..1025 CAUCGGUGAC
segment31 7bp 987..993 AUUCCUA 998..1004 UAGGAAG
segment32 3bp 1006..1008 UUC 1013..1015 GAA
segment33 6bp 1048..1053 GCUCGU 1082..1087 ACGAGC
segment34 3bp 1054..1056 GUC 1061..1063 GAU
segment35 4bp 1066..1069 UGGG 1076..1079 CCCG
