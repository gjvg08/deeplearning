#Name: bpRNA_CRW_3147
#Length:  1187 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
UCGCAUGGUGGGGGGUGGAAAGCUUUUUGUGGUUUUGGAUGGACUCGCGGCCUAUCAGCUUGUUGGUGAGGUAAUGGCUCACCAAGGCGACGACGGGUAGCCGGCCUGAGAGGGUGACCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGAAAGCCUGAUGCAGCGACGCCGCGUGAGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCAGUAGGGAAGAAGCGAAAGUGACGGUACCUGCAGAAGAAGCGCCGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGCGCAAGCGUUAUCCGGAAUUAUUGGGCGUAAGAGCUCGUAGGCGGUUUGUCGCGUCUGCCGUGAAAGUCCGGGGCUCAACUCCGGAUCUGCGGUGGGUACGGGCAGACUAGAGUGAUGUAGGGGAGACUGGAAUUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAGGAACACCGAUGGCGAAGGCAGGUCUCUGGGCAUUAACUGACGCUGAGGAGCGAAAGCAUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCAUGCCGUAAACGUUGGGCACUAGGUGUGGGGGACAUUCCACGUUUUCCGCGCCGUAGCUAACGCAUUAAGUGCCCCGCCUGGGGAGUACGGCCGCAAGGCUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGCGGAGCAUGCGGAUUAAUUCGAUGCAACGCGAAGAACCUUACCAAGGCUUGACAUGGGCCGGACCGGGCUGGAAACAGUCCUUCCCCUUUGGGGCCGGUUCACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUCGUUCCAUGUUGCCAGCGCGUAAUGGCGGGGACUCAUGGGAGACUGCCGGGGUCAACUCGGAGGAAGGUGGGGACGACGUCAAAUCAUCAUGCCCCUUAUGUCUUGGGCUUCACGCAUGCUACAAUGGCCGGUACAAAGGGUUGCGAUACUGUGAGGUGGAGCUAAUCCCAAAAAGCCGGUCUCAGUUCGGAUUGGGGUCUGCAACUCGACCCCAUGAAGUCGGAGUCGCUAGUAAUCGCAGAUCAGCAACGCUGCGGUGAAUACGUUCCCGGGCCUUGU
((....)).............((.....))....................(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(..((....))..)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[..(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.......))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))..........(((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....(((((((((..(((((((....)))))))...((...))..))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..(((......)))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((...(((((..((...(((....)))...))....)))))...)))))))......(...((((((((........))))))))...).....))))).....((((((((........))))))))......))...))))))))))
SSHHHHSSXXXXXXXXXXXXXSSHHHHHSSXXXXXXXXXXXXXXXXXXXXSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISIISSHHHHSSIISIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMMMSSHHHSSMMSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSSHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIISSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..2 "UC" 7..8 "GG"
S2 22..23 "GC" 29..30 "GU"
S3 51..53 "CCU" 95..97 "GGG"
S4 56..56 "C" 94..94 "C"
S5 58..60 "GCU" 86..88 "GGC"
S6 63..70 "UUGGUGAG" 78..85 "CUCACCAA"
S7 100..103 "GCCG" 119..122 "CGGC"
S8 104..107 "GCCU" 112..115 "GGGU"
S9 127..132 "CUGGGA" 143..148 "GCCCAG"
S10 150..153 "CUCC" 158..161 "GGAG"
S11 178..178 "U" 204..204 "A"
S12 180..182 "GCA" 201..203 "UGC"
S13 186..190 "UGGGC" 195..199 "GCCUG"
S14 217..220 "GAGG" 244..247 "CCUC"
S15 227..230 "GGCC" 235..238 "GGUU"
S16 251..251 "C" 288..288 "G"
S17 252..257 "AGUAGG" 279..284 "CCUGCA"
S18 261..261 "G" 274..274 "C"
S19 264..265 "GC" 270..271 "GU"
S20 291..295 "GCGCC" 332..336 "GGCGC"
S21 302..306 "CUACG" 327..331 "CGUAG"
S22 312..313 "GC" 318..319 "GC"
S23 358..360 "GGC" 673..675 "GCC"
S24 365..365 "G" 672..672 "C"
S25 367..370 "GCUC" 551..554 "GAGC"
S26 372..372 "U" 548..548 "G"
S27 374..376 "GGC" 545..547 "GCU"
S28 378..384 "GUUUGUC" 435..441 "GGCAGAC"
S29 386..387 "CG" 433..434 "CG"
S30 388..396 "UCUGCCGUG" 422..430 "UGCGGUGGG"
S31 401..407 "UCCGGGG" 413..419 "CUCCGGA"
S32 445..447 "AGU" 539..541 "ACU"
S33 448..453 "GAUGUA" 532..537 "GGCAUU"
S34 456..462 "GGAGACU" 524..530 "GGUCUCU"
S35 463..464 "GG" 506..507 "AC"
S36 467..474 "UUCCUGGU" 496..503 "AUCAGGAG"
S37 478..480 "GCG" 487..489 "UGC"
S38 512..512 "A" 523..523 "A"
S39 515..516 "GC" 521..522 "GC"
S40 559..564 "GCAUGG" 595..600 "CCAUGC"
S41 568..569 "GC" 593..594 "GU"
S42 573..576 "CAGG" 586..589 "CCUG"
S43 611..616 "GGGCAC" 666..671 "GUGCCC"
S44 619..630 "GGUGUGGGGGAC" 638..649 "GUUUUCCGCGCC"
S45 653..654 "GC" 659..660 "GC"
S46 677..679 "GGG" 702..704 "CUC"
S47 686..689 "GGCC" 694..697 "GGCU"
S48 715..717 "ACG" 1185..1187 "UGU"
S49 719..725 "GGGCCCG" 1178..1184 "CGGGCCU"
S50 727..728 "AC" 1173..1174 "GU"
S51 731..735 "GCGGC" 1133..1137 "GUCGC"
S52 736..745 "GGAGCAUGCG" 1021..1030 "CGCAUGCUAC"
S53 746..747 "GA" 1018..1019 "UC"
S54 752..755 "UUCG" 764..767 "CGAA"
S55 776..782 "CCAAGGC" 1008..1014 "GUCUUGG"
S56 788..796 "AUGGGCCGG" 829..837 "CCGGUUCAC"
S57 799..805 "CGGGCUG" 810..816 "CAGUCCU"
S58 820..821 "CC" 825..826 "GG"
S59 839..840 "GG" 1002..1003 "CC"
S60 842..845 "GGUG" 998..1001 "UGCC"
S61 848..849 "UG" 996..997 "CA"
S62 850..852 "GUU" 990..992 "AAU"
S63 853..856 "GUCG" 985..988 "CGUC"
S64 860..865 "GCUCGU" 894..899 "ACGAGC"
S65 866..868 "GUC" 873..875 "GAU"
S66 878..881 "UGGG" 888..891 "CCCG"
S67 905..908 "CCUC" 977..980 "GGGG"
S68 910..916 "UUCCAUG" 942..948 "CAUGGGA"
S69 920..921 "CC" 936..937 "GG"
S70 924..926 "CGC" 933..935 "GCG"
S71 951..958 "CUGCCGGG" 964..971 "CUCGGAGG"
S72 1034..1040 "GGCCGGU" 1083..1089 "GCCGGUC"
S73 1044..1048 "AAGGG" 1075..1079 "CCCAA"
S74 1051..1052 "GC" 1069..1070 "GC"
S75 1056..1058 "ACU" 1063..1065 "GGU"
S76 1096..1096 "C" 1127..1127 "G"
S77 1100..1107 "UUGGGGUC" 1116..1123 "GACCCCAU"
S78 1143..1150 "AUCGCAGA" 1159..1166 "GCUGCGGU"
H1 3..6 "GCAU" (2,7) C:G 
H2 24..28 "UUUUU" (23,29) C:G 
H3 71..77 "GUAAUGG" (70,78) G:C 
H4 108..111 "GAGA" (107,112) U:G 
H5 133..142 "CUGAGACACG" (132,143) A:G 
H6 154..157 "UACG" (153,158) C:G 
H7 191..194 "GAAA" (190,195) C:G 
H8 231..234 "UUCG" (230,235) C:G 
H9 266..269 "GAAA" (265,270) C:G 
H10 314..317 "AGCC" (313,318) C:G PK{2}
H11 408..412 "CUCAA" (407,413) G:C 
H12 481..486 "GUGAAA" (480,487) G:U 
H13 517..520 "GAAG" (516,521) C:G 
H14 577..585 "AUUAGAUAC" (576,586) G:C 
H15 631..637 "AUUCCAC" (630,638) C:G 
H16 655..658 "UAAC" (654,659) C:G PK{3}
H17 690..693 "GCAA" (689,694) C:G 
H18 756..763 "AUGCAACG" (755,764) G:C 
H19 806..809 "GAAA" (805,810) G:C 
H20 822..824 "UUU" (821,825) C:G 
H21 869..872 "GUGA" (868,873) C:G 
H22 882..887 "UUAAGU" (881,888) G:C 
H23 927..932 "GUAAUG" (926,933) C:G 
H24 959..963 "GUCAA" (958,964) G:C 
H25 1059..1062 "GUGA" (1058,1063) U:G 
H26 1108..1115 "UGCAACUC" (1107,1116) C:G 
H27 1151..1158 "UCAGCAAC" (1150,1159) A:G 
B1 54..55 "AU" (53,95) U:G (94,56) C:C 
B2 61..62 "UG" (60,86) U:G (85,63) A:U 
B3 116..118 "GAC" (115,104) U:G (103,119) G:C 
B4 179..179 "U" (178,204) U:A (203,180) C:G 
B5 285..287 "GAA" (284,252) A:A (251,288) C:G PK{1}
B6 296..301 "GGCUAA" (295,332) C:G (331,302) G:C PK{2}
B7 361..364 "GUAA" (360,673) C:G (672,365) C:G PK{3}
B8 373..373 "A" (372,548) U:G (547,374) U:G 
B9 385..385 "G" (384,435) C:G (434,386) G:C 
B10 431..432 "UA" (430,388) G:U (387,433) G:C 
B11 513..514 "UG" (512,523) A:A (522,515) C:G 
B12 538..538 "A" (537,448) U:G (447,539) U:A 
B13 565..567 "GGA" (564,595) G:C (594,568) U:G 
B14 718..718 "G" (717,1185) G:U (1184,719) U:G 
B15 841..841 "U" (840,1002) G:C (1001,842) C:G 
B16 846..847 "CA" (845,998) G:U (997,848) A:U 
B17 922..923 "AG" (921,936) C:G (935,924) G:C 
B18 989..989 "A" (988,853) C:G (852,990) U:A 
B19 993..995 "CAU" (992,850) U:G (849,996) G:C 
B20 1020..1020 "A" (1019,746) C:G (745,1021) G:C 
I1.1 57..57 "A" (56,94) C:C 
I1.2 89..93 "GACGA" (88,58) C:G 
I2.1 183..185 "CAA" (182,201) A:U 
I2.2 200..200 "A" (199,186) G:U 
I3.1 221..226 "GAUGAC" (220,244) G:C 
I3.2 239..243 "GUAAA" (238,227) U:G 
I4.1 258..260 "GAA" (257,279) G:C 
I4.2 275..278 "GGUA" (274,261) C:G 
I5.1 262..263 "AA" (261,274) G:C 
I5.2 272..273 "GA" (271,264) U:G 
I6.1 307..311 "UGCCA" (306,327) G:C 
I6.2 320..326 "GGUAAUA" (319,312) C:G 
I7.1 371..371 "G" (370,551) C:G 
I7.2 549..550 "AG" (548,372) G:U 
I8.1 397..400 "AAAG" (396,422) G:U 
I8.2 420..421 "UC" (419,401) A:U 
I9.1 454..455 "GG" (453,532) A:G 
I9.2 531..531 "G" (530,456) U:G 
I10.1 465..466 "AA" (464,506) G:A 
I10.2 504..505 "GA" (503,467) G:U 
I11.1 475..477 "GUA" (474,496) U:A 
I11.2 490..495 "GCAGAU" (489,478) C:G 
I12.1 570..572 "GAA" (569,593) C:G 
I12.2 590..592 "GUA" (589,573) G:C 
I13.1 680..685 "GAGUAC" (679,702) G:C 
I13.2 698..701 "AAAA" (697,686) U:G 
I14.1 726..726 "C" (725,1178) G:C 
I14.2 1175..1177 "UCC" (1174,727) U:A 
I15.1 917..919 "UUG" (916,942) G:C 
I15.2 938..941 "GACU" (937,920) G:C 
I16.1 1041..1043 "ACA" (1040,1083) U:G 
I16.2 1080..1082 "AAA" (1079,1044) A:A 
I17.1 1049..1050 "UU" (1048,1075) G:C 
I17.2 1071..1074 "UAAU" (1070,1051) C:G 
I18.1 1053..1055 "GAU" (1052,1069) C:G 
I18.2 1066..1068 "GGA" (1065,1056) U:A 
I19.1 1097..1099 "GGA" (1096,1127) C:G 
I19.2 1124..1126 "GAA" (1123,1100) U:U 
M1.1 366..366 "A" (365,672) G:C (554,367) C:G 
M1.2 555..558 "GAAA" (554,367) C:G (600,559) C:G 
M1.3 601..610 "CGUAAACGUU" (600,559) C:G (671,611) C:G 
M1.4 672..671 "" (671,611) C:G (365,672) G:C 
M2.1 377..377 "G" (376,545) C:G (441,378) C:G 
M2.2 442..444 "UAG" (441,378) C:G (541,445) U:A 
M2.3 542..544 "GAC" (541,445) U:A (376,545) C:G 
M3.1 463..462 "" (462,524) U:G (507,463) C:G 
M3.2 508..511 "ACCG" (507,463) C:G (523,512) A:A 
M3.3 524..523 "" (523,512) A:A (462,524) U:G 
M4.1 617..618 "UA" (616,666) C:G (649,619) C:G 
M4.2 650..652 "GUA" (649,619) C:G (660,653) C:G 
M4.3 661..665 "AUUAA" (660,653) C:G (616,666) C:G 
M5.1 729..730 "AA" (728,1173) C:G (1137,731) C:G 
M5.2 1138..1142 "UAGUA" (1137,731) C:G (1166,1143) U:A 
M5.3 1167..1172 "GAAUAC" (1166,1143) U:A (728,1173) C:G 
M6.1 736..735 "" (735,1133) C:G (1030,736) C:G 
M6.2 1031..1033 "AAU" (1030,736) C:G (1089,1034) C:G 
M6.3 1090..1095 "UCAGUU" (1089,1034) C:G (1127,1096) G:C 
M6.4 1128..1132 "UCGGA" (1127,1096) G:C (735,1133) C:G 
M7.1 748..751 "UUAA" (747,1018) A:U (767,752) A:U 
M7.2 768..775 "GAACCUUA" (767,752) A:U (1014,776) G:C 
M7.3 1015..1017 "GCU" (1014,776) G:C (747,1018) A:U 
M8.1 783..787 "UUGAC" (782,1008) C:G (837,788) C:A 
M8.2 838..838 "A" (837,788) C:A (1003,839) C:G 
M8.3 1004..1007 "UUAU" (1003,839) C:G (782,1008) C:G 
M9.1 797..798 "AC" (796,829) G:C (816,799) U:C 
M9.2 817..819 "UCC" (816,799) U:C (826,820) G:C 
M9.3 827..828 "GG" (826,820) G:C (796,829) G:C 
M10.1 857..859 "UCA" (856,985) G:C (899,860) C:G 
M10.2 900..904 "GCAAC" (899,860) C:G (980,905) G:C 
M10.3 981..984 "ACGA" (980,905) G:C (856,985) G:C 
M11.1 866..865 "" (865,894) U:A (875,866) U:G 
M11.2 876..877 "GU" (875,866) U:G (891,878) G:U 
M11.3 892..893 "CA" (891,878) G:U (865,894) U:A 
M12.1 909..909 "G" (908,977) C:G (948,910) A:U 
M12.2 949..950 "GA" (948,910) A:U (971,951) G:C 
M12.3 972..976 "AAGGU" (971,951) G:C (908,977) C:G 
X1 9..21 "UGGGGGGUGGAAA" (8,1) G:U (30,22) U:G 
X2 31..50 "GGUUUUGGAUGGACUCGCGG" (30,22) U:G (97,51) G:C 
X3 98..99 "UA" (97,51) G:C (122,100) C:G 
X4 123..126 "CACA" (122,100) C:G (148,127) G:C 
X6 162..177 "GCAGCAGUGGGGAAUA" (161,150) G:C (204,178) A:U 
X7 205..216 "GCGACGCCGCGU" (204,178) A:U (247,217) C:G 
X8 248..250 "UUU" (247,217) C:G (288,251) G:C PK{1}
X9 289..290 "AA" (288,251) G:C (336,291) C:G 
X10 337..357 "AAGCGUUAUCCGGAAUUAUUG" (336,291) C:G (675,358) C:G 
X12 705..714 "AAAGGAAUUG" (704,677) C:G (1187,715) U:A 
PK1 1bp 249..249 287..287 X8 248..250 B5 285..287
PK2 3bp 296..298 315..317 B6 296..301 H10 314..317
PK3 2bp 361..362 657..658 B7 361..364 H16 655..658
PK1.1 249 U 287 A
PK2.1 296 G 317 C
PK2.2 297 G 316 C
PK2.3 298 C 315 G
PK3.1 361 G 658 C
PK3.2 362 U 657 A
NCBP1 737 G 1029 A S52
NCBP2 512 A 523 A S38
NCBP3 453 A 532 G S33
NCBP4 953 G 969 A S71
NCBP5 854 U 987 U S63
NCBP6 788 A 837 C S56
NCBP7 1045 A 1078 A S73
NCBP8 1044 A 1079 A S73
NCBP9 464 G 506 A S35
NCBP10 132 A 143 G S9
NCBP11 1150 A 1159 G S78
NCBP12 56 C 94 C S4
NCBP13 252 A 284 A S17
NCBP14 799 C 816 U S57
NCBP15 1100 U 1123 U S77
segment1 2bp 1..2 UC 7..8 GG
segment2 2bp 22..23 GC 29..30 GU
segment3 15bp 51..70 CCUAUCAGCUUGUUGGUGAG 78..97 CUCACCAAGGCGACGACGGG
segment4 8bp 100..107 GCCGGCCU 112..122 GGGUGACCGGC
segment5 6bp 127..132 CUGGGA 143..148 GCCCAG
segment6 4bp 150..153 CUCC 158..161 GGAG
segment7 9bp 178..190 UUGCACAAUGGGC 195..204 GCCUGAUGCA
segment8 8bp 217..230 GAGGGAUGACGGCC 235..247 GGUUGUAAACCUC
segment9 10bp 251..265 CAGUAGGGAAGAAGC 270..288 GUGACGGUACCUGCAGAAG
segment10 12bp 291..313 GCGCCGGCUAACUACGUGCCAGC 318..336 GCGGUAAUACGUAGGGCGC
segment11 4bp 358..365 GGCGUAAG 672..675 CGCC
segment12 8bp 367..376 GCUCGUAGGC 545..554 GCUGAGGAGC
segment13 25bp 378..407 GUUUGUCGCGUCUGCCGUGAAAGUCCGGGG 413..441 CUCCGGAUCUGCGGUGGGUACGGGCAGAC
segment14 16bp 445..462 AGUGAUGUAGGGGAGACU 524..541 GGUCUCUGGGCAUUAACU
segment15 13bp 463..480 GGAAUUCCUGGUGUAGCG 487..507 UGCGCAGAUAUCAGGAGGAAC
segment16 3bp 512..516 AUGGC 521..523 GCA
segment17 12bp 559..576 GCAUGGGGAGCGAACAGG 586..600 CCUGGUAGUCCAUGC
segment18 6bp 611..616 GGGCAC 666..671 GUGCCC
segment19 12bp 619..630 GGUGUGGGGGAC 638..649 GUUUUCCGCGCC
segment20 2bp 653..654 GC 659..660 GC
segment21 7bp 677..689 GGGGAGUACGGCC 694..704 GGCUAAAACUC
segment22 12bp 715..728 ACGGGGGCCCGCAC 1173..1187 GUUCCCGGGCCUUGU
segment23 5bp 731..735 GCGGC 1133..1137 GUCGC
segment24 12bp 736..747 GGAGCAUGCGGA 1018..1030 UCACGCAUGCUAC
segment25 4bp 752..755 UUCG 764..767 CGAA
segment26 7bp 776..782 CCAAGGC 1008..1014 GUCUUGG
segment27 9bp 788..796 AUGGGCCGG 829..837 CCGGUUCAC
segment28 7bp 799..805 CGGGCUG 810..816 CAGUCCU
segment29 2bp 820..821 CC 825..826 GG
segment30 15bp 839..856 GGUGGUGCAUGGUUGUCG 985..1003 CGUCAAAUCAUCAUGCCCC
segment31 6bp 860..865 GCUCGU 894..899 ACGAGC
segment32 3bp 866..868 GUC 873..875 GAU
segment33 4bp 878..881 UGGG 888..891 CCCG
segment34 4bp 905..908 CCUC 977..980 GGGG
segment35 12bp 910..926 UUCCAUGUUGCCAGCGC 933..948 GCGGGGACUCAUGGGA
segment36 8bp 951..958 CUGCCGGG 964..971 CUCGGAGG
segment37 17bp 1034..1058 GGCCGGUACAAAGGGUUGCGAUACU 1063..1089 GGUGGAGCUAAUCCCAAAAAGCCGGUC
segment38 9bp 1096..1107 CGGAUUGGGGUC 1116..1127 GACCCCAUGAAG
segment39 8bp 1143..1150 AUCGCAGA 1159..1166 GCUGCGGU
