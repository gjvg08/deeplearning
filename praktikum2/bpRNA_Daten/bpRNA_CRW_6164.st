#Name: bpRNA_CRW_6164
#Length:  1220 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
CACAUGCAAGUCGAACGGACCCUUCGGGGUUAGUGGCGGACGGGUGAGUAACACGUGGGAACGUGCCUUUAGGUUCGGAAUAGCUCCUGGAAACGGGUGGUAAUGCCGAAUGUGCCCUUCGGGGGAAAGAUUUAUCGCCUUUAGAGCGGCCCGCGUCUGAUUAGCUAGUUGGUGAGGUAACGGCUCACCAAGGCGACGAUCAGUAGCUGGUCUGAGAGGAUGACCAGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUCUUGCGCAAUGGGCGAAAGCCUGACGCAGCCAUGCCGCGUGAAUGAUGAAGGUCUUAGGAUUGUAAAAUUCUUUCACCGGGGACGAUAAUGACGGUACCCGGAGAAGAAGCCCCGGCUAACUUCGUGCCAGCAGCCGCGGUAAUACGAAGGGGGCUAGCGUUGCUCGGAAUUACUGGGCGUAAAGGGCGCGUAGGCGGAUCGUUAAGUCAGAGGUGAAAUCCCAGGGCUCAACCCUGGAACUGCCUUUGAUACUGGCGAUCUUGAGUAUGAGAGAGGUAUGUGGAACUUCGAGUGUAGAGGUGAAAUUCGUAGAUAUUCGGAAGAACACCAGUGGCGAAGGCGACAUACUGGCUCAUUACUGACGCUGGGGCGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAAUGAUGAUUGCUAGUUGUCGGGCUGCAUGCAGUUCGGUGACGCAGCUAACGCAUUAAGCAAUCCGCCUGGGGAGUACGGUCGCAAGAUUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGCAGAACCUUACCACCUUUUGACAUGCCUGGACCGCCACGGAGACGUGGUUUUCCCUUCGGGGACUAGGACACAGGUGCUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUCGCCAUUAGUUGCCAUCAUUUAGUUGGGAACUCUAAUGGGACUGCCGGGCUAAGCCGGAGGAAGGUGGGGAUGACGUCAAGUCCUCAUGGCCCUUACAGGGUGGGCUACACACGUGCUACAAUGGCAACUACAGAGGGUUAAUCCUUAAAGUUGUCUCAGUUCGGAUUGUCCUCUGCAACUCGAGGGCAUGAAGUUGGAAUCGCUAGUAAUCGC
(((.(((..(((..((((((((....)))))))))))......(((......((((((((.((...(((((((.((((....(((((((....))))))).....)))).....(((....)))....((....)).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).......(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(.......)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((....))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))..........................(((((((((((((((((....((((........))))........(((((((.....(((((((...(((((((....)))))))..(((....)))..)))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..(((((((......))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...((((((......(((.....)))....))))))......(...((((((((........))))))))...).....)))))..........
SSSBSSSMMSSSBBSSSSSSSSHHHHSSSSSSSSSSSMMMMMMSSSMMMMMMSSSSSSSSBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSHHHHSSSMMMMSSHHHHSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHSIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSMMMSSSSSSSHHHHSSSSSSSMMSSSHHHHSSSMMSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSHHHHHHSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSIIIIIISSSHHHHHSSSIIIISSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..3 "CAC" 274..276 "GUG"
S2 5..7 "UGC" 271..273 "GCA"
S3 10..12 "GUC" 35..37 "GGC"
S4 15..22 "ACGGACCC" 27..34 "GGGUUAGU"
S5 44..46 "GUG" 229..231 "CAC"
S6 53..60 "ACGUGGGA" 149..156 "GCCCGCGU"
S7 62..63 "CG" 147..148 "CG"
S8 67..73 "CUUUAGG" 138..144 "CCUUUAG"
S9 75..78 "UCGG" 106..109 "CCGA"
S10 83..89 "GCUCCUG" 94..100 "CGGGUGG"
S11 115..117 "CCC" 122..124 "GGG"
S12 129..130 "GA" 135..136 "UC"
S13 157..159 "CUG" 201..203 "CAG"
S14 162..162 "U" 200..200 "U"
S15 164..166 "GCU" 192..194 "GGC"
S16 169..176 "UUGGUGAG" 184..191 "CUCACCAA"
S17 206..209 "GCUG" 225..228 "CAGC"
S18 210..213 "GUCU" 218..221 "GGAU"
S19 233..238 "CUGGGA" 249..254 "GCCCAG"
S20 256..259 "CUCC" 264..267 "GGAG"
S21 284..284 "U" 310..310 "A"
S22 286..288 "GCG" 307..309 "CGC"
S23 292..296 "UGGGC" 301..305 "GCCUG"
S24 323..326 "GAAU" 350..353 "AUUC"
S25 333..336 "GGUC" 341..344 "GAUU"
S26 357..357 "C" 389..389 "G"
S27 358..363 "ACCGGG" 380..385 "CCCGGA"
S28 367..367 "G" 375..375 "C"
S29 392..396 "GCCCC" 433..437 "GGGGC"
S30 403..407 "CUUCG" 428..432 "CGAAG"
S31 413..414 "GC" 419..420 "GC"
S32 459..461 "GGC" 772..774 "GCC"
S33 467..467 "G" 771..771 "C"
S34 469..472 "GCGC" 653..656 "GCGC"
S35 474..474 "U" 650..650 "G"
S36 476..478 "GGC" 647..649 "GCU"
S37 480..486 "GAUCGUU" 537..543 "GGCGAUC"
S38 488..489 "AG" 535..536 "CU"
S39 490..498 "UCAGAGGUG" 524..532 "UGCCUUUGA"
S40 503..509 "CCCAGGG" 515..521 "CCCUGGA"
S41 547..549 "AGU" 641..643 "ACU"
S42 550..555 "AUGAGA" 634..639 "GCUCAU"
S43 558..564 "GGUAUGU" 626..632 "ACAUACU"
S44 565..566 "GG" 608..609 "AC"
S45 569..576 "CUUCGAGU" 598..605 "AUUCGGAA"
S46 580..582 "GAG" 589..591 "UUC"
S47 614..614 "G" 625..625 "G"
S48 617..618 "GC" 623..624 "GC"
S49 661..666 "GCGUGG" 697..702 "CCACGC"
S50 670..671 "GC" 695..696 "GU"
S51 675..678 "CAGG" 688..691 "CCUG"
S52 713..718 "GAUUGC" 765..770 "GCAAUC"
S53 721..732 "GUUGUCGGGCUG" 737..748 "CAGUUCGGUGAC"
S54 752..753 "GC" 758..759 "GC"
S55 776..778 "GGG" 801..803 "CUC"
S56 785..788 "GGUC" 793..796 "GAUU"
S57 830..834 "GCGGU" 1206..1210 "AUCGC"
S58 835..844 "GGAGCAUGUG" 1117..1126 "CACGUGCUAC"
S59 845..846 "GU" 1114..1115 "AC"
S60 851..854 "UUCG" 863..866 "CGCA"
S61 875..881 "CCACCUU" 1104..1110 "AGGGUGG"
S62 887..889 "AUG" 934..936 "CAC"
S63 890..893 "CCUG" 929..932 "UAGG"
S64 897..903 "CGCCACG" 908..914 "CGUGGUU"
S65 917..919 "CCC" 924..926 "GGG"
S66 938..939 "GG" 1098..1099 "CC"
S67 941..944 "GCUG" 1094..1097 "UGGC"
S68 947..948 "UG" 1092..1093 "CA"
S69 949..951 "GCU" 1086..1088 "AGU"
S70 952..955 "GUCG" 1081..1084 "CGUC"
S71 959..964 "GCUCGU" 993..998 "ACGAGC"
S72 965..967 "GUC" 972..974 "GAU"
S73 977..980 "UGGG" 987..990 "CCCG"
S74 1004..1007 "CCUC" 1073..1076 "GGGG"
S75 1009..1015 "CCAUUAG" 1039..1045 "CUAAUGG"
S76 1019..1020 "CC" 1033..1034 "GG"
S77 1023..1024 "CA" 1031..1032 "UG"
S78 1048..1054 "CUGCCGG" 1061..1067 "CCGGAGG"
S79 1130..1135 "GGCAAC" 1157..1162 "GUUGUC"
S80 1142..1144 "GGG" 1150..1152 "CCU"
S81 1169..1169 "C" 1200..1200 "G"
S82 1173..1180 "UUGUCCUC" 1189..1196 "GAGGGCAU"
H1 23..26 "UUCG" (22,27) C:G 
H2 90..93 "GAAA" (89,94) G:C 
H3 118..121 "UUCG" (117,122) C:G 
H4 131..134 "UUUA" (130,135) A:U 
H5 177..183 "GUAACGG" (176,184) G:C 
H6 214..217 "GAGA" (213,218) U:G 
H7 239..248 "CUGAGACACG" (238,249) A:G 
H8 260..263 "UACG" (259,264) C:G 
H9 297..300 "GAAA" (296,301) C:G 
H10 337..340 "UUAG" (336,341) C:G 
H11 368..374 "AUAAUGA" (367,375) G:C 
H12 415..418 "AGCC" (414,419) C:G PK{2}
H13 510..514 "CUCAA" (509,515) G:C 
H14 583..588 "GUGAAA" (582,589) G:U 
H15 619..622 "GAAG" (618,623) C:G 
H16 679..687 "AUUAGAUAC" (678,688) G:C 
H17 733..736 "CAUG" (732,737) G:C 
H18 754..757 "UAAC" (753,758) C:G PK{3}
H19 789..792 "GCAA" (788,793) C:G 
H20 855..862 "AAGCAACG" (854,863) G:C 
H21 904..907 "GAGA" (903,908) G:C 
H22 920..923 "UUCG" (919,924) C:G 
H23 968..971 "GUGA" (967,972) C:G 
H24 981..986 "UUAAGU" (980,987) G:C 
H25 1025..1030 "UUUAGU" (1024,1031) A:U 
H26 1055..1060 "GCUAAG" (1054,1061) G:C 
H27 1145..1149 "UUAAU" (1144,1150) G:C 
H28 1181..1188 "UGCAACUC" (1180,1189) C:G 
B1 4..4 "A" (3,274) C:G (273,5) A:U 
B2 13..14 "GA" (12,35) C:G (34,15) U:A 
B3 61..61 "A" (60,149) A:G (148,62) G:C 
B4 160..161 "AU" (159,201) G:C (200,162) U:U 
B5 167..168 "AG" (166,192) U:G (191,169) A:U 
B6 222..224 "GAC" (221,210) U:G (209,225) G:C 
B7 285..285 "U" (284,310) U:A (309,286) C:G 
B8 386..388 "GAA" (385,358) A:A (357,389) C:G PK{1}
B9 397..402 "GGCUAA" (396,433) C:G (432,403) G:C PK{2}
B10 462..466 "GUAAA" (461,772) C:G (771,467) C:G PK{3}
B11 475..475 "A" (474,650) U:G (649,476) U:G 
B12 487..487 "A" (486,537) U:G (536,488) U:A 
B13 533..534 "UA" (532,490) A:U (489,535) G:C 
B14 615..616 "UG" (614,625) G:G (624,617) C:G 
B15 640..640 "U" (639,550) U:A (549,641) U:A 
B16 667..669 "GGA" (666,697) G:C (696,670) U:G 
B17 933..933 "A" (932,890) G:C (889,934) G:C 
B18 940..940 "U" (939,1098) G:C (1097,941) C:G 
B19 945..946 "CA" (944,1094) G:U (1093,947) A:U 
B20 1021..1022 "AU" (1020,1033) C:G (1032,1023) G:C 
B21 1085..1085 "A" (1084,952) C:G (951,1086) U:A 
B22 1089..1091 "CCU" (1088,949) U:G (948,1092) G:C 
B23 1116..1116 "A" (1115,845) C:G (844,1117) G:C 
I1.1 64..66 "UGC" (63,147) G:C 
I1.2 145..146 "AG" (144,67) G:C 
I2.1 79..82 "AAUA" (78,106) G:C 
I2.2 101..105 "UAAUG" (100,83) G:G 
I3.1 163..163 "A" (162,200) U:U 
I3.2 195..199 "GACGA" (194,164) C:G 
I4.1 289..291 "CAA" (288,307) G:C 
I4.2 306..306 "A" (305,292) G:U 
I5.1 327..332 "GAUGAA" (326,350) U:A 
I5.2 345..349 "GUAAA" (344,333) U:G 
I6.1 364..366 "GAC" (363,380) G:C 
I6.2 376..379 "GGUA" (375,367) C:G 
I7.1 408..412 "UGCCA" (407,428) G:C 
I7.2 421..427 "GGUAAUA" (420,413) C:G 
I8.1 473..473 "G" (472,653) C:G 
I8.2 651..652 "GG" (650,474) G:U 
I9.1 499..502 "AAAU" (498,524) G:U 
I9.2 522..523 "AC" (521,503) A:C 
I10.1 556..557 "GA" (555,634) A:G 
I10.2 633..633 "G" (632,558) U:G 
I11.1 567..568 "AA" (566,608) G:A 
I11.2 606..607 "GA" (605,569) A:C 
I12.1 577..579 "GUA" (576,598) U:A 
I12.2 592..597 "GUAGAU" (591,580) C:G 
I13.1 672..674 "AAA" (671,695) C:G 
I13.2 692..694 "GUA" (691,675) G:C 
I14.1 779..784 "GAGUAC" (778,801) G:C 
I14.2 797..800 "AAAA" (796,785) U:G 
I15.1 1016..1018 "UUG" (1015,1039) G:C 
I15.2 1035..1038 "AACU" (1034,1019) G:C 
I16.1 1136..1141 "UACAGA" (1135,1157) C:G 
I16.2 1153..1156 "UAAA" (1152,1142) U:G 
I17.1 1170..1172 "GGA" (1169,1200) C:G 
I17.2 1197..1199 "GAA" (1196,1173) U:U 
M1.1 8..9 "AA" (7,271) C:G (37,10) C:G 
M1.2 38..43 "GGACGG" (37,10) C:G (231,44) C:G 
M1.3 232..232 "A" (231,44) C:G (254,233) G:C 
M1.4 255..255 "A" (254,233) G:C (267,256) G:C 
M1.5 268..270 "GCA" (267,256) G:C (7,271) C:G 
M2.1 47..52 "AGUAAC" (46,229) G:C (156,53) U:A 
M2.2 157..156 "" (156,53) U:A (203,157) G:C 
M2.3 204..205 "UA" (203,157) G:C (228,206) C:G 
M2.4 229..228 "" (228,206) C:G (46,229) G:C 
M3.1 74..74 "U" (73,138) G:C (109,75) A:U 
M3.2 110..114 "AUGUG" (109,75) A:U (124,115) G:C 
M3.3 125..128 "GAAA" (124,115) G:C (136,129) C:G 
M3.4 137..137 "G" (136,129) C:G (73,138) G:C 
M4.1 468..468 "G" (467,771) G:C (656,469) C:G 
M4.2 657..660 "GAAA" (656,469) C:G (702,661) C:G 
M4.3 703..712 "CGUAAAUGAU" (702,661) C:G (770,713) C:G 
M4.4 771..770 "" (770,713) C:G (467,771) G:C 
M5.1 479..479 "G" (478,647) C:G (543,480) C:G 
M5.2 544..546 "UUG" (543,480) C:G (643,547) U:A 
M5.3 644..646 "GAC" (643,547) U:A (478,647) C:G 
M6.1 565..564 "" (564,626) U:A (609,565) C:G 
M6.2 610..613 "ACCA" (609,565) C:G (625,614) G:G 
M6.3 626..625 "" (625,614) G:G (564,626) U:A 
M7.1 719..720 "UA" (718,765) C:G (748,721) C:G 
M7.2 749..751 "GCA" (748,721) C:G (759,752) C:G 
M7.3 760..764 "AUUAA" (759,752) C:G (718,765) C:G 
M8.1 835..834 "" (834,1206) U:A (1126,835) C:G 
M8.2 1127..1129 "AAU" (1126,835) C:G (1162,1130) C:G 
M8.3 1163..1168 "UCAGUU" (1162,1130) C:G (1200,1169) G:C 
M8.4 1201..1205 "UUGGA" (1200,1169) G:C (834,1206) U:A 
M9.1 847..850 "UUAA" (846,1114) U:A (866,851) A:U 
M9.2 867..874 "GAACCUUA" (866,851) A:U (1110,875) G:C 
M9.3 1111..1113 "GCU" (1110,875) G:C (846,1114) U:A 
M10.1 882..886 "UUGAC" (881,1104) U:A (936,887) C:A 
M10.2 937..937 "A" (936,887) C:A (1099,938) C:G 
M10.3 1100..1103 "UUAC" (1099,938) C:G (881,1104) U:A 
M11.1 894..896 "GAC" (893,929) G:U (914,897) U:C 
M11.2 915..916 "UU" (914,897) U:C (926,917) G:C 
M11.3 927..928 "AC" (926,917) G:C (893,929) G:U 
M12.1 956..958 "UCA" (955,1081) G:C (998,959) C:G 
M12.2 999..1003 "GCAAC" (998,959) C:G (1076,1004) G:C 
M12.3 1077..1080 "AUGA" (1076,1004) G:C (955,1081) G:C 
M13.1 965..964 "" (964,993) U:A (974,965) U:G 
M13.2 975..976 "GU" (974,965) U:G (990,977) G:U 
M13.3 991..992 "CA" (990,977) G:U (964,993) U:A 
M14.1 1008..1008 "G" (1007,1073) C:G (1045,1009) G:C 
M14.2 1046..1047 "GA" (1045,1009) G:C (1067,1048) G:C 
M14.3 1068..1072 "AAGGU" (1067,1048) G:C (1007,1073) C:G 
X1 277..283 "GGGAAUC" (276,1) G:C (310,284) A:U 
X2 311..322 "GCCAUGCCGCGU" (310,284) A:U (353,323) C:G 
X3 354..356 "UUU" (353,323) C:G (389,357) G:C PK{1}
X4 390..391 "AA" (389,357) G:C (437,392) C:G 
X5 438..458 "UAGCGUUGCUCGGAAUUACUG" (437,392) C:G (774,459) C:G 
X7 804..829 "AAAGGAAUUGACGGGGGCCCGCACAA" (803,776) C:G (1210,830) C:G 
E1 1211..1220 "UAGUAAUCGC" 
PK1 1bp 355..355 388..388 X3 354..356 B8 386..388
PK2 3bp 397..399 416..418 B9 397..402 H12 415..418
PK3 2bp 462..463 756..757 B10 462..466 H18 754..757
PK1.1 355 U 388 A
PK2.1 397 G 418 C
PK2.2 398 G 417 C
PK2.3 399 C 416 G
PK3.1 462 G 757 C
PK3.2 463 U 756 A
NCBP1 358 A 385 A S27
NCBP2 1050 G 1065 A S78
NCBP3 569 C 605 A S45
NCBP4 953 U 1083 U S70
NCBP5 897 C 914 U S64
NCBP6 1173 U 1196 U S82
NCBP7 60 A 149 G S6
NCBP8 17 G 32 A S4
NCBP9 69 U 142 U S8
NCBP10 887 A 936 C S62
NCBP11 566 G 608 A S44
NCBP12 614 G 625 G S47
NCBP13 85 U 98 U S10
NCBP14 555 A 634 G S42
NCBP15 162 U 200 U S14
NCBP16 503 C 521 A S40
NCBP17 83 G 100 G S10
NCBP18 836 G 1125 A S58
NCBP19 238 A 249 G S19
NCBP20 852 U 865 C S60
NCBP21 70 U 141 U S8
segment1 6bp 1..7 CACAUGC 271..276 GCAGUG
segment2 11bp 10..22 GUCGAACGGACCC 27..37 GGGUUAGUGGC
segment3 3bp 44..46 GUG 229..231 CAC
segment4 17bp 53..73 ACGUGGGAACGUGCCUUUAGG 138..156 CCUUUAGAGCGGCCCGCGU
segment5 11bp 75..89 UCGGAAUAGCUCCUG 94..109 CGGGUGGUAAUGCCGA
segment6 3bp 115..117 CCC 122..124 GGG
segment7 2bp 129..130 GA 135..136 UC
segment8 15bp 157..176 CUGAUUAGCUAGUUGGUGAG 184..203 CUCACCAAGGCGACGAUCAG
segment9 8bp 206..213 GCUGGUCU 218..228 GGAUGACCAGC
segment10 6bp 233..238 CUGGGA 249..254 GCCCAG
segment11 4bp 256..259 CUCC 264..267 GGAG
segment12 9bp 284..296 UUGCGCAAUGGGC 301..310 GCCUGACGCA
segment13 8bp 323..336 GAAUGAUGAAGGUC 341..353 GAUUGUAAAAUUC
segment14 8bp 357..367 CACCGGGGACG 375..389 CGGUACCCGGAGAAG
segment15 12bp 392..414 GCCCCGGCUAACUUCGUGCCAGC 419..437 GCGGUAAUACGAAGGGGGC
segment16 4bp 459..467 GGCGUAAAG 771..774 CGCC
segment17 8bp 469..478 GCGCGUAGGC 647..656 GCUGGGGCGC
segment18 25bp 480..509 GAUCGUUAAGUCAGAGGUGAAAUCCCAGGG 515..543 CCCUGGAACUGCCUUUGAUACUGGCGAUC
segment19 16bp 547..564 AGUAUGAGAGAGGUAUGU 626..643 ACAUACUGGCUCAUUACU
segment20 13bp 565..582 GGAACUUCGAGUGUAGAG 589..609 UUCGUAGAUAUUCGGAAGAAC
segment21 3bp 614..618 GUGGC 623..625 GCG
segment22 12bp 661..678 GCGUGGGGAGCAAACAGG 688..702 CCUGGUAGUCCACGC
segment23 6bp 713..718 GAUUGC 765..770 GCAAUC
segment24 12bp 721..732 GUUGUCGGGCUG 737..748 CAGUUCGGUGAC
segment25 2bp 752..753 GC 758..759 GC
segment26 7bp 776..788 GGGGAGUACGGUC 793..803 GAUUAAAACUC
segment27 5bp 830..834 GCGGU 1206..1210 AUCGC
segment28 12bp 835..846 GGAGCAUGUGGU 1114..1126 ACACACGUGCUAC
segment29 4bp 851..854 UUCG 863..866 CGCA
segment30 7bp 875..881 CCACCUU 1104..1110 AGGGUGG
segment31 7bp 887..893 AUGCCUG 929..936 UAGGACAC
segment32 7bp 897..903 CGCCACG 908..914 CGUGGUU
segment33 3bp 917..919 CCC 924..926 GGG
segment34 15bp 938..955 GGUGCUGCAUGGCUGUCG 1081..1099 CGUCAAGUCCUCAUGGCCC
segment35 6bp 959..964 GCUCGU 993..998 ACGAGC
segment36 3bp 965..967 GUC 972..974 GAU
segment37 4bp 977..980 UGGG 987..990 CCCG
segment38 4bp 1004..1007 CCUC 1073..1076 GGGG
segment39 11bp 1009..1024 CCAUUAGUUGCCAUCA 1031..1045 UGGGAACUCUAAUGG
segment40 7bp 1048..1054 CUGCCGG 1061..1067 CCGGAGG
segment41 9bp 1130..1144 GGCAACUACAGAGGG 1150..1162 CCUUAAAGUUGUC
segment42 9bp 1169..1180 CGGAUUGUCCUC 1189..1200 GAGGGCAUGAAG
