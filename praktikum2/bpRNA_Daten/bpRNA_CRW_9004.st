#Name: bpRNA_CRW_9004
#Length:  1225 
#PageNumber: 2
GCGGCGGAGGGUGAGUAACACGUGGGUAACCUACCUAUAAGACUGGGAUAACUUCGGGAAACCGGAGCUAAUACCGGAUACCAUUUGGAACCGCAUGGUUCUAAAGUGAAAGAUGGUUUUGCUAUCACUUAUAGAUGGACCCGCGCCGUAUUAGCUAGUUGGUAAGGUAACGGCUUACCAAGGCAACGAUACGUAGCCGACCUGAGAGGGUGAUCGGCCACACUGGAACUGAGACACGGUCCAGACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCCGCAAUGGGCGAAAGCCUGACGGAGCAACGCCGCGUGAGUGAUGAAGGGUUUCGGCUCGUAAAACUCUGUUAUUAGGGAAGAACAAAUGUGUAAGUAACUGUGCACAUCUUGACGGUACCUAAUCAGAAAGCCACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUGGCAAGCGUUAUCCGGAAUUAUUGGGCGUAAAGCGCGCGUAGGCGGUUUCUUAAGUCUGAUGUGAAAGCCCCACGGCUCAACCGUGGAGGGUCAUUGGAAACUGGGAAACUUGAGUGCAGAAGAGGAAAGUGGAAUUCCAUGUGUAGCGGUGAAAUGCGCAGAGAUAUGGAGGAACACCAGUGGCGAAGGCGACUUUCUGGUCUGUAACUGACGCUGAUGUGCGAAAGCGUGGGGAUCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAAGUGUUAGGGGGUUUCCGCCCCUUAGUGCUGCAGCUAACGCAUUAAGCACUCCGCCUGGGGAGUACGACCGCAAGGUUGAAACUCAAAGGAAUUGACGGGGACCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAAAUCUUGACAUCCUUUGAAAACUCUAGAGAUAGAGCCUUCCCCUUCGGGGGACAAAGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUAAGCUUAGUUGCCAUCAUUAAGUUGGGCACUCUAGGUUGACUGCCGGUGACAAACCGGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGAUUUGGGCUACACACGUGCUACAAUGGACAAUACAAAGGGGCAGCUAAACCGSGAGGUCAUGCAAAUCCCAUAAAGUUGUUCUCAGUUCGGAUUGUAGU
..........(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....((((((((((....)))))))))).....((((((...)))))).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((((.....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))...............................((((((((((((....((((........))))........(((((((.....((((((((..(((((((....))))))).((.((....)).))))))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....))))).))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((...((...(((....)))...))....))).....))))))).................
EEEEEEEEEESSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHSSSSSSSSSSMMMMMSSSSSSHHHSSSSSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMMMMMMMMMMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMMMMMMMMMMMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMSIISSSSSSIIISIIIIISSSSSSSSHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISMMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMMMMMMMMMMMMMMMMMMMMMSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSMSSSIIIIIISSSSHHHHSSSSIIIISSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSISSHHHHSSISSSSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSBSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 11..13 "GUG" 219..221 "CAC"
S2 20..27 "ACGUGGGU" 139..146 "ACCCGCGC"
S3 30..31 "CC" 137..138 "GG"
S4 35..41 "CUAUAAG" 128..134 "CUUAUAG"
S5 43..46 "CUGG" 74..77 "CCGG"
S6 51..57 "ACUUCGG" 62..68 "CCGGAGC"
S7 83..92 "AUUUGGAACC" 97..106 "GGUUCUAAAG"
S8 112..117 "GAUGGU" 121..126 "GCUAUC"
S9 147..149 "CGU" 191..193 "ACG"
S10 152..152 "U" 190..190 "U"
S11 154..156 "GCU" 182..184 "GGC"
S12 159..166 "UUGGUAAG" 174..181 "CUUACCAA"
S13 196..199 "GCCG" 215..218 "CGGC"
S14 200..203 "ACCU" 208..211 "GGGU"
S15 223..228 "CUGGAA" 239..244 "GUCCAG"
S16 246..249 "CUCC" 254..257 "GGAG"
S17 274..274 "U" 300..300 "A"
S18 276..278 "CCG" 297..299 "CGG"
S19 282..286 "UGGGC" 291..295 "GCCUG"
S20 313..316 "GAGU" 340..343 "ACUC"
S21 323..326 "GGGU" 331..334 "GCUC"
S22 345..345 "G" 403..403 "G"
S23 348..353 "AUUAGG" 395..400 "CCUAAU"
S24 357..357 "G" 390..390 "C"
S25 363..370 "AUGUGUAA" 377..384 "GUGCACAU"
S26 407..411 "GCCAC" 448..452 "GUGGC"
S27 418..422 "CUACG" 443..447 "CGUAG"
S28 428..429 "GC" 434..435 "GC"
S29 474..476 "GGC" 790..792 "GCC"
S30 484..487 "GCGC" 669..672 "GUGC"
S31 489..489 "U" 666..666 "G"
S32 491..493 "GGC" 663..665 "GCU"
S33 495..501 "GUUUCUU" 553..559 "GGGAAAC"
S34 503..504 "AG" 551..552 "CU"
S35 505..513 "UCUGAUGUG" 540..548 "GUCAUUGGA"
S36 519..525 "CCCACGG" 531..537 "CCGUGGA"
S37 563..565 "AGU" 657..659 "ACU"
S38 566..571 "GCAGAA" 650..655 "GUCUGU"
S39 574..580 "GGAAAGU" 642..648 "ACUUUCU"
S40 581..582 "GG" 624..625 "AC"
S41 585..592 "UUCCAUGU" 614..621 "AUAUGGAG"
S42 596..598 "GCG" 605..607 "UGC"
S43 630..630 "G" 641..641 "G"
S44 633..634 "GC" 639..640 "GC"
S45 677..682 "GCGUGG" 713..718 "CCACGC"
S46 686..687 "UC" 711..712 "GU"
S47 691..694 "CAGG" 704..707 "CCUG"
S48 729..734 "GAGUGC" 783..788 "GCACUC"
S49 737..748 "AGUGUUAGGGGG" 755..766 "CCCCUUAGUGCU"
S50 770..771 "GC" 776..777 "GC"
S51 794..796 "GGG" 819..821 "CUC"
S52 803..806 "GACC" 811..814 "GGUU"
S53 853..862 "GGAGCAUGUG" 1139..1148 "CACGUGCUAC"
S54 863..864 "GU" 1136..1137 "AC"
S55 869..872 "UUCG" 881..884 "CGAA"
S56 893..899 "CCAAAUC" 1126..1132 "GAUUUGG"
S57 905..907 "AUC" 954..956 "GAC"
S58 908..912 "CUUUG" 948..952 "CAAAG"
S59 915..921 "AACUCUA" 926..932 "UAGAGCC"
S60 934..935 "UC" 946..947 "GA"
S61 937..938 "CC" 943..944 "GG"
S62 958..959 "GG" 1120..1121 "CC"
S63 961..964 "GGUG" 1116..1119 "UGCC"
S64 967..968 "UG" 1114..1115 "CA"
S65 969..971 "GUU" 1108..1110 "AAU"
S66 972..975 "GUCG" 1103..1106 "CGUC"
S67 979..984 "GCUCGU" 1013..1018 "ACGAGC"
S68 985..987 "GUC" 992..994 "GAU"
S69 997..1000 "UGGG" 1007..1010 "CCCG"
S70 1024..1027 "CCUU" 1095..1098 "GGGG"
S71 1029..1035 "AGCUUAG" 1059..1065 "CUAGGUU"
S72 1039..1040 "CC" 1053..1054 "GG"
S73 1043..1044 "CA" 1051..1052 "UG"
S74 1068..1070 "CUG" 1087..1089 "AGG"
S75 1071..1075 "CCGGU" 1081..1085 "ACCGG"
S76 1152..1158 "GGACAAU" 1202..1208 "GUUGUUC"
S77 1164..1166 "GGG" 1194..1196 "CCC"
S78 1170..1171 "GC" 1188..1189 "GC"
S79 1175..1177 "ACC" 1182..1184 "GGU"
H1 58..61 "GAAA" (57,62) G:C 
H2 93..96 "GCAU" (92,97) C:G 
H3 118..120 "UUU" (117,121) U:G 
H4 167..173 "GUAACGG" (166,174) G:C 
H5 204..207 "GAGA" (203,208) U:G 
H6 229..238 "CUGAGACACG" (228,239) A:G 
H7 250..253 "UACG" (249,254) C:G 
H8 287..290 "GAAA" (286,291) C:G 
H9 327..330 "UUCG" (326,331) U:G 
H10 371..376 "GUAACU" (370,377) A:G 
H11 430..433 "AGCC" (429,434) C:G PK{1}
H12 526..530 "CUCAA" (525,531) G:C 
H13 599..604 "GUGAAA" (598,605) G:U 
H14 635..638 "GAAG" (634,639) C:G 
H15 695..703 "AUUAGAUAC" (694,704) G:C 
H16 749..754 "UUUCCG" (748,755) G:C 
H17 772..775 "UAAC" (771,776) C:G PK{2}
H18 807..810 "GCAA" (806,811) C:G 
H19 873..880 "AAGCAACG" (872,881) G:C 
H20 922..925 "GAGA" (921,926) A:U 
H21 939..942 "UUCG" (938,943) C:G 
H22 988..991 "GUGA" (987,992) C:G 
H23 1001..1006 "UUAAGU" (1000,1007) G:C 
H24 1045..1050 "UUAAGU" (1044,1051) A:U 
H25 1076..1080 "GACAA" (1075,1081) U:A 
H26 1178..1181 "GSGA" (1177,1182) C:G 
B1 28..29 "AA" (27,139) U:A (138,30) G:C 
B2 150..151 "AU" (149,191) U:A (190,152) U:U 
B3 157..158 "AG" (156,182) U:G (181,159) A:U 
B4 212..214 "GAU" (211,200) U:A (199,215) G:C 
B5 275..275 "U" (274,300) U:A (299,276) G:C 
B6 412..417 "GGCUAA" (411,448) C:G (447,418) G:C PK{1}
B7 490..490 "A" (489,666) U:G (665,491) U:G 
B8 502..502 "A" (501,553) U:G (552,503) U:A 
B9 549..550 "AA" (548,505) A:U (504,551) G:C 
B10 631..632 "UG" (630,641) G:G (640,633) C:G 
B11 656..656 "A" (655,566) U:G (565,657) U:A 
B12 683..685 "GGA" (682,713) G:C (712,686) U:U 
B13 953..953 "U" (952,908) G:C (907,954) C:G 
B14 960..960 "U" (959,1120) G:C (1119,961) C:G 
B15 965..966 "CA" (964,1116) G:U (1115,967) A:U 
B16 1041..1042 "AU" (1040,1053) C:G (1052,1043) G:C 
B17 1086..1086 "G" (1085,1071) G:C (1070,1087) G:A 
B18 1107..1107 "A" (1106,972) C:G (971,1108) U:A 
B19 1111..1113 "CAU" (1110,969) U:G (968,1114) G:C 
B20 1138..1138 "A" (1137,863) C:G (862,1139) G:C 
I1.1 32..34 "UAC" (31,137) C:G 
I1.2 135..136 "AU" (134,35) G:C 
I2.1 47..50 "GAUA" (46,74) G:C 
I2.2 69..73 "UAAUA" (68,51) C:A 
I3.1 153..153 "A" (152,190) U:U 
I3.2 185..189 "AACGA" (184,154) C:G 
I4.1 279..281 "CAA" (278,297) G:C 
I4.2 296..296 "A" (295,282) G:U 
I5.1 317..322 "GAUGAA" (316,340) U:A 
I5.2 335..339 "GUAAA" (334,323) C:G 
I6.1 346..347 "UU" (345,403) G:G 
I6.2 401..402 "CA" (400,348) U:A 
I7.1 354..356 "GAA" (353,395) G:C 
I7.2 391..394 "GGUA" (390,357) C:G 
I8.1 358..362 "AACAA" (357,390) G:C 
I8.2 385..389 "CUUGA" (384,363) U:A 
I9.1 423..427 "UGCCA" (422,443) G:C 
I9.2 436..442 "GGUAAUA" (435,428) C:G 
I10.1 488..488 "G" (487,669) C:G 
I10.2 667..668 "AU" (666,489) G:U 
I11.1 514..518 "AAAGC" (513,540) G:G 
I11.2 538..539 "GG" (537,519) A:C 
I12.1 572..573 "GA" (571,650) A:G 
I12.2 649..649 "G" (648,574) U:G 
I13.1 583..584 "AA" (582,624) G:A 
I13.2 622..623 "GA" (621,585) G:U 
I14.1 593..595 "GUA" (592,614) U:A 
I14.2 608..613 "GCAGAG" (607,596) C:G 
I15.1 688..690 "AAA" (687,711) C:G 
I15.2 708..710 "GUA" (707,691) G:C 
I16.1 797..802 "GAGUAC" (796,819) G:C 
I16.2 815..818 "GAAA" (814,803) U:G 
I17.1 936..936 "C" (935,946) C:G 
I17.2 945..945 "G" (944,937) G:C 
I18.1 1036..1038 "UUG" (1035,1059) G:C 
I18.2 1055..1058 "CACU" (1054,1039) G:C 
I19.1 1159..1163 "ACAAA" (1158,1202) U:G 
I19.2 1197..1201 "AUAAA" (1196,1164) C:G 
I20.1 1167..1169 "GCA" (1166,1194) G:C 
I20.2 1190..1193 "AAAU" (1189,1170) C:G 
I21.1 1172..1174 "UAA" (1171,1188) C:G 
I21.2 1185..1187 "CAU" (1184,1175) U:A 
M1.1 14..19 "AGUAAC" (13,219) G:C (146,20) C:A 
M1.2 147..146 "" (146,20) C:A (193,147) G:C 
M1.3 194..195 "UA" (193,147) G:C (218,196) C:G 
M1.4 219..218 "" (218,196) C:G (13,219) G:C 
M2.1 42..42 "A" (41,128) G:C (77,43) G:C 
M2.2 78..82 "AUACC" (77,43) G:C (106,83) G:A 
M2.3 107..111 "UGAAA" (106,83) G:A (126,112) C:G 
M2.4 127..127 "A" (126,112) C:G (41,128) G:C 
M3.1 222..222 "A" (221,11) C:G (244,223) G:C 
M3.2 245..245 "A" (244,223) G:C (257,246) G:C 
M3.3 258..273 "GCAGCAGUAGGGAAUC" (257,246) G:C (300,274) A:U 
M3.4 301..312 "GCAACGCCGCGU" (300,274) A:U (343,313) C:G 
M3.5 344..344 "U" (343,313) C:G (403,345) G:G 
M3.6 404..406 "AAA" (403,345) G:G (452,407) C:G 
M3.7 453..473 "AAGCGUUAUCCGGAAUUAUUG" (452,407) C:G (792,474) C:G 
M3.8 793..793 "U" (792,474) C:G (821,794) C:G 
M3.9 822..852 "AAAGGAAUUGACGGGGACCCGCACAAGCGGU" (821,794) C:G (1148,853) C:G 
M3.10 1149..1151 "AAU" (1148,853) C:G (1208,1152) C:G 
M4.1 477..483 "GUAAAGC" (476,790) C:G (672,484) C:G PK{2}
M4.2 673..676 "GAAA" (672,484) C:G (718,677) C:G 
M4.3 719..728 "CGUAAACGAU" (718,677) C:G (788,729) C:G 
M4.4 789..789 "C" (788,729) C:G (476,790) C:G 
M5.1 494..494 "G" (493,663) C:G (559,495) C:G 
M5.2 560..562 "UUG" (559,495) C:G (659,563) U:A 
M5.3 660..662 "GAC" (659,563) U:A (493,663) C:G 
M6.1 581..580 "" (580,642) U:A (625,581) C:G 
M6.2 626..629 "ACCA" (625,581) C:G (641,630) G:G 
M6.3 642..641 "" (641,630) G:G (580,642) U:A 
M7.1 735..736 "UA" (734,783) C:G (766,737) U:A 
M7.2 767..769 "GCA" (766,737) U:A (777,770) C:G 
M7.3 778..782 "AUUAA" (777,770) C:G (734,783) C:G 
M8.1 865..868 "UUAA" (864,1136) U:A (884,869) A:U 
M8.2 885..892 "GAACCUUA" (884,869) A:U (1132,893) G:C 
M8.3 1133..1135 "GCU" (1132,893) G:C (864,1136) U:A 
M9.1 900..904 "UUGAC" (899,1126) C:G (956,905) C:A 
M9.2 957..957 "A" (956,905) C:A (1121,958) C:G 
M9.3 1122..1125 "UUAU" (1121,958) C:G (899,1126) C:G 
M10.1 913..914 "AA" (912,948) G:C (932,915) C:A 
M10.2 933..933 "U" (932,915) C:A (947,934) A:U 
M10.3 948..947 "" (947,934) A:U (912,948) G:C 
M11.1 976..978 "UCA" (975,1103) G:C (1018,979) C:G 
M11.2 1019..1023 "GCAAC" (1018,979) C:G (1098,1024) G:C 
M11.3 1099..1102 "AUGA" (1098,1024) G:C (975,1103) G:C 
M12.1 985..984 "" (984,1013) U:A (994,985) U:G 
M12.2 995..996 "GU" (994,985) U:G (1010,997) G:U 
M12.3 1011..1012 "CA" (1010,997) G:U (984,1013) U:A 
M13.1 1028..1028 "A" (1027,1095) U:G (1065,1029) U:A 
M13.2 1066..1067 "GA" (1065,1029) U:A (1089,1068) G:C 
M13.3 1090..1094 "AAGGU" (1089,1068) G:C (1027,1095) U:G 
E1 1..10 "GCGGCGGAGG" 
E2 1209..1225 "UCAGUUCGGAUUGUAGU" 
PK1 3bp 412..414 431..433 B6 412..417 H11 430..433
PK2 2bp 477..478 774..775 M4.1 477..483 H17 772..775
PK1.1 412 G 433 C
PK1.2 413 G 432 C
PK1.3 414 C 431 G
PK2.1 477 G 775 C
PK2.2 478 U 774 A
NCBP1 512 U 541 U S35
NCBP2 571 A 650 G S38
NCBP3 686 U 712 U S46
NCBP4 152 U 190 U S10
NCBP5 370 A 377 G S25
NCBP6 51 A 68 C S6
NCBP7 916 A 931 C S59
NCBP8 582 G 624 A S40
NCBP9 20 A 146 C S2
NCBP10 519 C 537 A S36
NCBP11 513 G 540 G S35
NCBP12 854 G 1147 A S53
NCBP13 905 A 956 C S57
NCBP14 83 A 106 G S7
NCBP15 630 G 641 G S43
NCBP16 973 U 1105 U S66
NCBP17 228 A 239 G S15
NCBP18 345 G 403 G S22
NCBP19 915 A 932 C S59
NCBP20 1070 G 1087 A S74
segment1 3bp 11..13 GUG 219..221 CAC
segment2 17bp 20..41 ACGUGGGUAACCUACCUAUAAG 128..146 CUUAUAGAUGGACCCGCGC
segment3 11bp 43..57 CUGGGAUAACUUCGG 62..77 CCGGAGCUAAUACCGG
segment4 10bp 83..92 AUUUGGAACC 97..106 GGUUCUAAAG
segment5 6bp 112..117 GAUGGU 121..126 GCUAUC
segment6 15bp 147..166 CGUAUUAGCUAGUUGGUAAG 174..193 CUUACCAAGGCAACGAUACG
segment7 8bp 196..203 GCCGACCU 208..218 GGGUGAUCGGC
segment8 6bp 223..228 CUGGAA 239..244 GUCCAG
segment9 4bp 246..249 CUCC 254..257 GGAG
segment10 9bp 274..286 UUCCGCAAUGGGC 291..300 GCCUGACGGA
segment11 8bp 313..326 GAGUGAUGAAGGGU 331..343 GCUCGUAAAACUC
segment12 16bp 345..370 GUUAUUAGGGAAGAACAAAUGUGUAA 377..403 GUGCACAUCUUGACGGUACCUAAUCAG
segment13 12bp 407..429 GCCACGGCUAACUACGUGCCAGC 434..452 GCGGUAAUACGUAGGUGGC
segment14 3bp 474..476 GGC 790..792 GCC
segment15 8bp 484..493 GCGCGUAGGC 663..672 GCUGAUGUGC
segment16 25bp 495..525 GUUUCUUAAGUCUGAUGUGAAAGCCCCACGG 531..559 CCGUGGAGGGUCAUUGGAAACUGGGAAAC
segment17 16bp 563..580 AGUGCAGAAGAGGAAAGU 642..659 ACUUUCUGGUCUGUAACU
segment18 13bp 581..598 GGAAUUCCAUGUGUAGCG 605..625 UGCGCAGAGAUAUGGAGGAAC
segment19 3bp 630..634 GUGGC 639..641 GCG
segment20 12bp 677..694 GCGUGGGGAUCAAACAGG 704..718 CCUGGUAGUCCACGC
segment21 6bp 729..734 GAGUGC 783..788 GCACUC
segment22 12bp 737..748 AGUGUUAGGGGG 755..766 CCCCUUAGUGCU
segment23 2bp 770..771 GC 776..777 GC
segment24 7bp 794..806 GGGGAGUACGACC 811..821 GGUUGAAACUC
segment25 12bp 853..864 GGAGCAUGUGGU 1136..1148 ACACACGUGCUAC
segment26 4bp 869..872 UUCG 881..884 CGAA
segment27 7bp 893..899 CCAAAUC 1126..1132 GAUUUGG
segment28 8bp 905..912 AUCCUUUG 948..956 CAAAGUGAC
segment29 7bp 915..921 AACUCUA 926..932 UAGAGCC
segment30 4bp 934..938 UCCCC 943..947 GGGGA
segment31 15bp 958..975 GGUGGUGCAUGGUUGUCG 1103..1121 CGUCAAAUCAUCAUGCCCC
segment32 6bp 979..984 GCUCGU 1013..1018 ACGAGC
segment33 3bp 985..987 GUC 992..994 GAU
segment34 4bp 997..1000 UGGG 1007..1010 CCCG
segment35 4bp 1024..1027 CCUU 1095..1098 GGGG
segment36 11bp 1029..1044 AGCUUAGUUGCCAUCA 1051..1065 UGGGCACUCUAGGUU
segment37 8bp 1068..1075 CUGCCGGU 1081..1089 ACCGGGAGG
segment38 15bp 1152..1177 GGACAAUACAAAGGGGCAGCUAAACC 1182..1208 GGUCAUGCAAAUCCCAUAAAGUUGUUC
