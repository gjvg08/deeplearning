#Name: bpRNA_CRW_9233
#Length:  1213 
#PageNumber: 2
ACUUACAGAUGGACCCGCGGCGCAUUAGCUAGUUGGUGAGGUAACGGCUCACCAAGGCGACGAUGCGUAGCCGACCUGAGAGGUGAUCGGCCACACUGGGACUGAAACACGGCCCAGAUUCCUACGGGGAGGCAGCAGUAGGGAAUCUUUCCGCAAUGGAUGAAAGUCUGACGGAGCAACGCGGCGUGACUGAUGAAUGUUUUCGGAUCGUAAAACUCUGUUGUUAGGGAAGAACAAGUACCGUUCGAAUAGGGCGGCACCUUGACGGUACCUAACCAGAAAGCCACGGCUAAUUACGUGCCAGCAGCCGCGGUAAUACGUAGGUGACAAGCGUUGUCCGGAAUUAUUGGGCGUAAAGCGCGCGCAGGCGGUUUCUUAAGUCUGAUGUGAAAGCCCCCGGCUCAACCGGGGAGGGUCAUUGGAAACUGGGGAACUUGAGUGCAGAAGAGGAGAGUGGAAUUCCACGUGUAGCGGUGAAAUGCGUAGAGAUGUGGAGGAACACCAGUGGCGAAGGCGACUCUCUGGUCUGUAACUGACGCUGAGGCGCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAAGUGUUAGAGGGUUUCCGCCCUUUAGUGCUGCAGCAAACGCAUUAAGCACUCCGCCUGGGGAGUACGGUCGCAAGACUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCUCUGACAACCCUAGAGAUAGGGCUUCCCCUUCGGGGGCAGAGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUCGGUUAAGUCCCGCAACGAGUCGCAACCCUUGUAUCUUAGUUGCCAACAUUCAAUUGGGCACUCUAAAGUGACUGCCGGUGACAAACCGGAAGAAAGUGGGGAUGACCUCCAAUCAUCAUGCCCCUUAUGACCUGGGCUACACACGUGCUACAAUGGGCAGAACAAAGGGCAGCGAAGCCGCGAGGCUAAGCCAAUCCCACAAAUCUGUUCUCAGUUCGGAUCGCAGUCUGCAACUCGACUGCGUGAAGCUGGAAUCGCUAGUAAUCGCGGAUCAGCAUGCCGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACACCACGAGAGUUUGUAACA
....................(((..(.(((..((((((((.......))))))))))).....))))..(((((.((....)))...))))....((((((..........)))))).((((....)).)).................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((.......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....((((((((..(((((((....))))))).((((....))))))))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..))))).).....((((..(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).))................................
EEEEEEEEEEEEEEEEEEEESSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSBSSHHHHSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSBSSXXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSSSHHHHSSSSSSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSBSMMMMMSSSSMMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 21..23 "CGC" 65..67 "GCG"
S2 26..26 "U" 64..64 "U"
S3 28..30 "GCU" 56..58 "GGC"
S4 33..40 "UUGGUGAG" 48..55 "CUCACCAA"
S5 70..73 "GCCG" 88..91 "CGGC"
S6 74..74 "A" 84..84 "U"
S7 76..77 "CU" 82..83 "GG"
S8 96..101 "CUGGGA" 112..117 "GCCCAG"
S9 119..120 "UU" 130..131 "AG"
S10 121..122 "CC" 127..128 "GG"
S11 149..149 "U" 175..175 "A"
S12 151..153 "CCG" 172..174 "CGG"
S13 157..161 "UGGAU" 166..170 "GUCUG"
S14 188..191 "GACU" 215..218 "ACUC"
S15 198..201 "UGUU" 206..209 "GAUC"
S16 220..220 "G" 279..279 "G"
S17 223..228 "GUUAGG" 271..276 "CCUAAC"
S18 232..232 "G" 266..266 "C"
S19 238..245 "GUACCGUU" 253..260 "GGCGGCAC"
S20 283..287 "GCCAC" 324..328 "GUGAC"
S21 294..298 "UUACG" 319..323 "CGUAG"
S22 304..305 "GC" 310..311 "GC"
S23 350..352 "GGC" 665..667 "GCC"
S24 360..363 "GCGC" 544..547 "GCGC"
S25 365..365 "C" 541..541 "G"
S26 367..369 "GGC" 538..540 "GCU"
S27 371..377 "GUUUCUU" 428..434 "GGGGAAC"
S28 379..380 "AG" 426..427 "CU"
S29 381..389 "UCUGAUGUG" 415..423 "GUCAUUGGA"
S30 394..400 "CCCCCGG" 406..412 "CCGGGGA"
S31 438..440 "AGU" 532..534 "ACU"
S32 441..446 "GCAGAA" 525..530 "GUCUGU"
S33 449..455 "GGAGAGU" 517..523 "ACUCUCU"
S34 456..457 "GG" 499..500 "AC"
S35 460..467 "UUCCACGU" 489..496 "AUGUGGAG"
S36 471..473 "GCG" 480..482 "UGC"
S37 505..505 "G" 516..516 "G"
S38 508..509 "GC" 514..515 "GC"
S39 552..557 "GCGUGG" 588..593 "CCACGC"
S40 561..562 "GC" 586..587 "GU"
S41 566..569 "CAGG" 579..582 "CCUG"
S42 604..609 "GAGUGC" 658..663 "GCACUC"
S43 612..623 "AGUGUUAGAGGG" 630..641 "CCCUUUAGUGCU"
S44 645..646 "GC" 651..652 "GC"
S45 669..671 "GGG" 694..696 "CUC"
S46 678..681 "GGUC" 686..689 "GACU"
S47 705..706 "UG" 1180..1181 "CA"
S48 707..709 "ACG" 1176..1178 "UGU"
S49 711..717 "GGGCCCG" 1169..1175 "CGGGCCU"
S50 719..720 "AC" 1164..1165 "GU"
S51 723..727 "GCGGU" 1125..1129 "AUCGC"
S52 728..737 "GGAGCAUGUG" 1013..1022 "CACGUGCUAC"
S53 738..739 "GU" 1010..1011 "AC"
S54 744..747 "UUCG" 756..759 "CGAA"
S55 768..774 "CCAGGUC" 1000..1006 "GACCUGG"
S56 780..782 "AUC" 827..829 "GAC"
S57 783..787 "CUCUG" 821..825 "CAGAG"
S58 790..796 "AACCCUA" 801..807 "UAGGGCU"
S59 809..812 "CCCC" 817..820 "GGGG"
S60 831..832 "GG" 994..995 "CC"
S61 834..837 "GGUG" 990..993 "UGCC"
S62 840..841 "UG" 988..989 "CA"
S63 842..844 "GUU" 982..984 "AAU"
S64 845..848 "GUCG" 977..980 "CCUC"
S65 852..852 "G" 892..892 "C"
S66 853..857 "CUCGU" 886..890 "ACGAG"
S67 858..860 "GUC" 865..867 "GAU"
S68 870..873 "UCGG" 880..883 "CCCG"
S69 898..901 "CCUU" 969..972 "GGGG"
S70 904..910 "AUCUUAG" 934..940 "CUAAAGU"
S71 914..915 "CC" 928..929 "GG"
S72 918..919 "CA" 926..927 "UG"
S73 943..950 "CUGCCGGU" 956..963 "ACCGGAAG"
S74 1026..1032 "GGGCAGA" 1075..1081 "UCUGUUC"
S75 1038..1040 "GGG" 1067..1069 "CCC"
S76 1043..1044 "GC" 1061..1062 "GC"
S77 1048..1050 "GCC" 1055..1057 "GGC"
S78 1088..1088 "C" 1119..1119 "G"
S79 1092..1099 "UCGCAGUC" 1108..1115 "GACUGCGU"
S80 1135..1142 "AUCGCGGA" 1150..1157 "GCCGCGGU"
H1 41..47 "GUAACGG" (40,48) G:C 
H2 78..81 "GAGA" (77,82) U:G 
H3 102..111 "CUGAAACACG" (101,112) A:G 
H4 123..126 "UACG" (122,127) C:G 
H5 162..165 "GAAA" (161,166) U:G 
H6 202..205 "UUCG" (201,206) U:G 
H7 246..252 "CGAAUAG" (245,253) U:G 
H8 306..309 "AGCC" (305,310) C:G PK{1}
H9 401..405 "CUCAA" (400,406) G:C 
H10 474..479 "GUGAAA" (473,480) G:U 
H11 510..513 "GAAG" (509,514) C:G 
H12 570..578 "AUUAGAUAC" (569,579) G:C 
H13 624..629 "UUUCCG" (623,630) G:C 
H14 647..650 "AAAC" (646,651) C:G PK{2}
H15 682..685 "GCAA" (681,686) C:G 
H16 748..755 "AAGCAACG" (747,756) G:C 
H17 797..800 "GAGA" (796,801) A:U 
H18 813..816 "UUCG" (812,817) C:G 
H19 861..864 "GUGA" (860,865) C:G 
H20 874..879 "UUAAGU" (873,880) G:C 
H21 920..925 "UUCAAU" (919,926) A:U 
H22 951..955 "GACAA" (950,956) U:A 
H23 1051..1054 "GCGA" (1050,1055) C:G 
H24 1100..1107 "UGCAACUC" (1099,1108) C:G 
H25 1143..1149 "UCAGCAU" (1142,1150) A:G 
B1 24..25 "AU" (23,65) C:G (64,26) U:U 
B2 31..32 "AG" (30,56) U:G (55,33) A:U 
B3 75..75 "C" (74,84) A:U (83,76) G:C 
B4 85..87 "GAU" (84,74) U:A (73,88) G:C 
B5 129..129 "G" (128,121) G:C (120,130) U:A 
B6 150..150 "U" (149,175) U:A (174,151) G:C 
B7 288..293 "GGCUAA" (287,324) C:G (323,294) G:U PK{1}
B8 366..366 "A" (365,541) C:G (540,367) U:G 
B9 378..378 "A" (377,428) U:G (427,379) U:A 
B10 424..425 "AA" (423,381) A:U (380,426) G:C 
B11 506..507 "UG" (505,516) G:G (515,508) C:G 
B12 531..531 "A" (530,441) U:G (440,532) U:A 
B13 558..560 "GGA" (557,588) G:C (587,561) U:G 
B14 710..710 "G" (709,1176) G:U (1175,711) U:G 
B15 826..826 "U" (825,783) G:C (782,827) C:G 
B16 833..833 "U" (832,994) G:C (993,834) C:G 
B17 838..839 "CA" (837,990) G:U (989,840) A:U 
B18 891..891 "U" (890,853) G:C (852,892) G:C 
B19 916..917 "AA" (915,928) C:G (927,918) G:C 
B20 981..981 "C" (980,845) C:G (844,982) U:A 
B21 985..987 "CAU" (984,842) U:G (841,988) G:C 
B22 1012..1012 "A" (1011,738) C:G (737,1013) G:C 
B23 1179..1179 "A" (1178,707) U:A (706,1180) G:C 
I1.1 27..27 "A" (26,64) U:U 
I1.2 59..63 "GACGA" (58,28) C:G 
I2.1 154..156 "CAA" (153,172) G:C 
I2.2 171..171 "A" (170,157) G:U 
I3.1 192..197 "GAUGAA" (191,215) U:A 
I3.2 210..214 "GUAAA" (209,198) C:U 
I4.1 221..222 "UU" (220,279) G:G 
I4.2 277..278 "CA" (276,223) C:G 
I5.1 229..231 "GAA" (228,271) G:C 
I5.2 267..270 "GGUA" (266,232) C:G 
I6.1 233..237 "AACAA" (232,266) G:C 
I6.2 261..265 "CUUGA" (260,238) C:G 
I7.1 299..303 "UGCCA" (298,319) G:C 
I7.2 312..318 "GGUAAUA" (311,304) C:G 
I8.1 364..364 "G" (363,544) C:G 
I8.2 542..543 "AG" (541,365) G:C 
I9.1 390..393 "AAAG" (389,415) G:G 
I9.2 413..414 "GG" (412,394) A:C 
I10.1 447..448 "GA" (446,525) A:G 
I10.2 524..524 "G" (523,449) U:G 
I11.1 458..459 "AA" (457,499) G:A 
I11.2 497..498 "GA" (496,460) G:U 
I12.1 468..470 "GUA" (467,489) U:A 
I12.2 483..488 "GUAGAG" (482,471) C:G 
I13.1 563..565 "GAA" (562,586) C:G 
I13.2 583..585 "GUA" (582,566) G:C 
I14.1 672..677 "GAGUAC" (671,694) G:C 
I14.2 690..693 "GAAA" (689,678) U:G 
I15.1 718..718 "C" (717,1169) G:C 
I15.2 1166..1168 "UCC" (1165,719) U:A 
I16.1 911..913 "UUG" (910,934) G:C 
I16.2 930..933 "CACU" (929,914) G:C 
I17.1 1033..1037 "ACAAA" (1032,1075) A:U 
I17.2 1070..1074 "ACAAA" (1069,1038) C:G 
I18.1 1041..1042 "CA" (1040,1067) G:C 
I18.2 1063..1066 "CAAU" (1062,1043) C:G 
I19.1 1045..1047 "GAA" (1044,1061) C:G 
I19.2 1058..1060 "UAA" (1057,1048) C:G 
I20.1 1089..1091 "GGA" (1088,1119) C:G 
I20.2 1116..1118 "GAA" (1115,1092) U:U 
M1.1 353..359 "GUAAAGC" (352,665) C:G (547,360) C:G PK{2}
M1.2 548..551 "GAAA" (547,360) C:G (593,552) C:G 
M1.3 594..603 "CGUAAACGAU" (593,552) C:G (663,604) C:G 
M1.4 664..664 "C" (663,604) C:G (352,665) C:G 
M2.1 370..370 "G" (369,538) C:G (434,371) C:G 
M2.2 435..437 "UUG" (434,371) C:G (534,438) U:A 
M2.3 535..537 "GAC" (534,438) U:A (369,538) C:G 
M3.1 456..455 "" (455,517) U:A (500,456) C:G 
M3.2 501..504 "ACCA" (500,456) C:G (516,505) G:G 
M3.3 517..516 "" (516,505) G:G (455,517) U:A 
M4.1 610..611 "UA" (609,658) C:G (641,612) U:A 
M4.2 642..644 "GCA" (641,612) U:A (652,645) C:G 
M4.3 653..657 "AUUAA" (652,645) C:G (609,658) C:G 
M5.1 721..722 "AA" (720,1164) C:G (1129,723) C:G 
M5.2 1130..1134 "UAGUA" (1129,723) C:G (1157,1135) U:A 
M5.3 1158..1163 "GAAUAC" (1157,1135) U:A (720,1164) C:G 
M6.1 728..727 "" (727,1125) U:A (1022,728) C:G 
M6.2 1023..1025 "AAU" (1022,728) C:G (1081,1026) C:G 
M6.3 1082..1087 "UCAGUU" (1081,1026) C:G (1119,1088) G:C 
M6.4 1120..1124 "CUGGA" (1119,1088) G:C (727,1125) U:A 
M7.1 740..743 "UUAA" (739,1010) U:A (759,744) A:U 
M7.2 760..767 "GAACCUUA" (759,744) A:U (1006,768) G:C 
M7.3 1007..1009 "GCU" (1006,768) G:C (739,1010) U:A 
M8.1 775..779 "UUGAC" (774,1000) C:G (829,780) C:A 
M8.2 830..830 "A" (829,780) C:A (995,831) C:G 
M8.3 996..999 "UUAU" (995,831) C:G (774,1000) C:G 
M9.1 788..789 "AC" (787,821) G:C (807,790) U:A 
M9.2 808..808 "U" (807,790) U:A (820,809) G:C 
M9.3 821..820 "" (820,809) G:C (787,821) G:C 
M10.1 849..851 "UCA" (848,977) G:C (892,852) C:G 
M10.2 893..897 "GCAAC" (892,852) C:G (972,898) G:C 
M10.3 973..976 "AUGA" (972,898) G:C (848,977) G:C 
M11.1 858..857 "" (857,886) U:A (867,858) U:G 
M11.2 868..869 "GU" (867,858) U:G (883,870) G:U 
M11.3 884..885 "CA" (883,870) G:U (857,886) U:A 
M12.1 902..903 "GU" (901,969) U:G (940,904) U:A 
M12.2 941..942 "GA" (940,904) U:A (963,943) G:C 
M12.3 964..968 "AAAGU" (963,943) G:C (901,969) U:G 
X1 68..69 "UA" (67,21) G:C (91,70) C:G 
X2 92..95 "CACA" (91,70) C:G (117,96) G:C 
X4 132..148 "GCAGCAGUAGGGAAUCU" (131,119) G:U (175,149) A:U 
X5 176..187 "GCAACGCGGCGU" (175,149) A:U (218,188) C:G 
X7 280..282 "AAA" (279,220) G:G (328,283) C:G 
X8 329..349 "AAGCGUUGUCCGGAAUUAUUG" (328,283) C:G (667,350) C:G 
X10 697..704 "AAAGGAAU" (696,669) C:G (1181,705) A:U 
E1 1..20 "ACUUACAGAUGGACCCGCGG" 
E2 1182..1213 "CACCGCCCGUCACACCACGAGAGUUUGUAACA" 
PK1 3bp 288..290 307..309 B7 288..293 H8 306..309
PK2 2bp 353..354 649..650 M1.1 353..359 H14 647..650
PK1.1 288 G 309 C
PK1.2 289 G 308 C
PK1.3 290 C 307 G
PK2.1 353 G 650 C
PK2.2 354 U 649 A
NCBP1 220 G 279 G S16
NCBP2 846 U 979 U S64
NCBP3 101 A 112 G S8
NCBP4 389 G 415 G S29
NCBP5 388 U 416 U S29
NCBP6 906 C 938 A S70
NCBP7 791 A 806 C S58
NCBP8 446 A 525 G S32
NCBP9 26 U 64 U S2
NCBP10 394 C 412 A S30
NCBP11 198 U 209 C S15
NCBP12 847 C 978 C S64
NCBP13 780 A 829 C S56
NCBP14 240 A 258 C S19
NCBP15 871 C 882 C S68
NCBP16 1092 U 1115 U S79
NCBP17 190 C 216 C S14
NCBP18 284 C 327 A S20
NCBP19 729 G 1021 A S52
NCBP20 1142 A 1150 G S80
NCBP21 457 G 499 A S34
NCBP22 505 G 516 G S37
NCBP23 945 G 961 A S73
segment1 15bp 21..40 CGCAUUAGCUAGUUGGUGAG 48..67 CUCACCAAGGCGACGAUGCG
segment2 7bp 70..77 GCCGACCU 82..91 GGUGAUCGGC
segment3 6bp 96..101 CUGGGA 112..117 GCCCAG
segment4 4bp 119..122 UUCC 127..131 GGGAG
segment5 9bp 149..161 UUCCGCAAUGGAU 166..175 GUCUGACGGA
segment6 8bp 188..201 GACUGAUGAAUGUU 206..218 GAUCGUAAAACUC
segment7 16bp 220..245 GUUGUUAGGGAAGAACAAGUACCGUU 253..279 GGCGGCACCUUGACGGUACCUAACCAG
segment8 12bp 283..305 GCCACGGCUAAUUACGUGCCAGC 310..328 GCGGUAAUACGUAGGUGAC
segment9 3bp 350..352 GGC 665..667 GCC
segment10 8bp 360..369 GCGCGCAGGC 538..547 GCUGAGGCGC
segment11 25bp 371..400 GUUUCUUAAGUCUGAUGUGAAAGCCCCCGG 406..434 CCGGGGAGGGUCAUUGGAAACUGGGGAAC
segment12 16bp 438..455 AGUGCAGAAGAGGAGAGU 517..534 ACUCUCUGGUCUGUAACU
segment13 13bp 456..473 GGAAUUCCACGUGUAGCG 480..500 UGCGUAGAGAUGUGGAGGAAC
segment14 3bp 505..509 GUGGC 514..516 GCG
segment15 12bp 552..569 GCGUGGGGAGCGAACAGG 579..593 CCUGGUAGUCCACGC
segment16 6bp 604..609 GAGUGC 658..663 GCACUC
segment17 12bp 612..623 AGUGUUAGAGGG 630..641 CCCUUUAGUGCU
segment18 2bp 645..646 GC 651..652 GC
segment19 7bp 669..681 GGGGAGUACGGUC 686..696 GACUGAAACUC
segment20 14bp 705..720 UGACGGGGGCCCGCAC 1164..1181 GUUCCCGGGCCUUGUACA
segment21 5bp 723..727 GCGGU 1125..1129 AUCGC
segment22 12bp 728..739 GGAGCAUGUGGU 1010..1022 ACACACGUGCUAC
segment23 4bp 744..747 UUCG 756..759 CGAA
segment24 7bp 768..774 CCAGGUC 1000..1006 GACCUGG
segment25 8bp 780..787 AUCCUCUG 821..829 CAGAGUGAC
segment26 7bp 790..796 AACCCUA 801..807 UAGGGCU
segment27 4bp 809..812 CCCC 817..820 GGGG
segment28 15bp 831..848 GGUGGUGCAUGGUUGUCG 977..995 CCUCCAAUCAUCAUGCCCC
segment29 6bp 852..857 GCUCGU 886..892 ACGAGUC
segment30 3bp 858..860 GUC 865..867 GAU
segment31 4bp 870..873 UCGG 880..883 CCCG
segment32 4bp 898..901 CCUU 969..972 GGGG
segment33 11bp 904..919 AUCUUAGUUGCCAACA 926..940 UGGGCACUCUAAAGU
segment34 8bp 943..950 CUGCCGGU 956..963 ACCGGAAG
segment35 15bp 1026..1050 GGGCAGAACAAAGGGCAGCGAAGCC 1055..1081 GGCUAAGCCAAUCCCACAAAUCUGUUC
segment36 9bp 1088..1099 CGGAUCGCAGUC 1108..1119 GACUGCGUGAAG
segment37 8bp 1135..1142 AUCGCGGA 1150..1157 GCCGCGGU
