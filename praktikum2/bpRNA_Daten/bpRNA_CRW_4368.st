#Name: bpRNA_CRW_4368
#Length:  1157 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
UCACGGAGAGUUUGAUCCUGGCUCAGGACGAACGCUGGCGGCGUGCUUAACACAUGCAAGUCGAACGAUGAAGCCCUUCGGGGUGGAUUAGUGGCGAACGGGUGAGUAACACGUGGGCAAUCUGCCCUUCACUCUGGGACAAGCCCUGGAAACGGGGUCUAAUACCGGAUACGACCUGCCGAGGCAUCUCGGUGGGUGGAAACCUAUCAGCUNNUUGGUGGGGUAAUGGCCUACCAAGGCGACGACGGGUAGCCGGCCUGAGAGGGNGACCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGAAAGCCUGAUGCAGCGACGCCGCGUGAGGGAUGACGGCCUNCGGGUUGUAAACCUCUUUCAGCAGGGAAGAAGCGAAAGUGACGGUACCUGCAGAAGAAGCGCCGGCCGUCGGAUGUGAAAGCNNNNGGCUUAACCCCGGGUCUGCAUUCGAUACGGGCAGGCUAGAGUGUGGUAGGGGAGAUCGGAAUUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAGGAACACCGGUGGCGAAGGCGGAUCUCUGGGCCAUUACUGACGCUGAGGAGCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGUUGGGAACUAGGUGUUGGCGACAUUCCACGUCGUCGGUGCCGCAGCUAACGCAUUAAGUUCCCCGCCNGGGGAGUACGGCGAAGAACCUUACCAAGGCUUGACAUAUACCGGAAACGGCCAGAGAUGGUCGCCCCCUUGUNGUCGGUAUACAGGUGGUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUUCUNUGUUGCCAGCAUGCUUCGGGGUNAUGGGGACUCACAGGAGACUGCCGGGGUCAACUCGGAGGAAGGUGGGGACGACGUCAAGUCAUCAUGCCCCUUAUGUCUUGGGCUGCACACGUGCUACAAUGGCCGGUACAAUGAGCUGCGAUACCGUGAGGUGGAGCGAAUCUCAAAAAGCCGGUCUCAGUUCGGAUUGGGGUCUGCAACUCGACCCCAUGAAGUUGGAGUNGCUAGUAAUCGCAGAUCAGCAUUGCUGCGG
.......(((((.......))))).............(((((((((....(((.(((..(((..(((((...((((....))))..)).))))))......(((.............................((((....(((((((....))))))).....)))).....((((((((((....)))))))))).....(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).)).))))))..((((......((((....)))).....)))).[.(((((((...(............)....))))))..])..........(((((((((((....(((((((.....)))))))..)))))))))..))..........(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).))).................((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.......))))))))))))...((....)).....))))))............................(((((((.....(((((((((..(((((((....))))))).((.....))))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..(((...........)))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...................(((((((...(((((..((...(((....)))...))....)))))...)))))))......(...((((((((........))))))))...)................(((((((........)))))))
EEEEEEESSSSSHHHHHHHSSSSSXXXXXXXXXXXXXSSSSSSSSSIIIISSSBSSSMMSSSBBSSSSSIIISSSSHHHHSSSSIISSBSSSSSSMMMMMMSSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHSSSSSSSSSSMMMMMSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSBSSSSSSXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHHHHHHSIIIISSSSSSBBBSXXXXXXXXXXSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSXXXXXXXXXXSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSXXXXXXXXXXXXXXXXXSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSXXXXXXXXXXSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSHHHHHSSSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSSHHHHHHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSXXXXXXXXXXXXXXXXXXXSSSSSSSIIISSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSIIISSSSSSSXXXXXXSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISXXXXXXXXXXXXXXXXSSSSSSSHHHHHHHHSSSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 8..12 "GAGUU" 20..24 "GGCUC"
S2 38..43 "GCGGCG" 361..366 "CGCCGC"
S3 44..45 "UG" 358..359 "CG"
S4 46..46 "C" 324..324 "G"
S5 51..53 "CAC" 320..322 "GUG"
S6 55..57 "UGC" 317..319 "GCA"
S7 60..62 "GUC" 93..95 "GGC"
S8 65..67 "ACG" 90..92 "AGU"
S9 68..69 "AU" 87..88 "AU"
S10 73..76 "GCCC" 81..84 "GGGU"
S11 102..104 "GUG" 275..277 "CAC"
S12 134..137 "CUGG" 165..168 "CCGG"
S13 142..148 "AGCCCUG" 153..159 "CGGGGUC"
S14 174..183 "ACCUGCCGAG" 188..197 "CUCGGUGGGU"
S15 203..205 "CCU" 247..249 "GGG"
S16 208..208 "C" 246..246 "C"
S17 210..212 "GCU" 238..240 "GGC"
S18 215..222 "UUGGUGGG" 230..237 "CCUACCAA"
S19 252..255 "GCCG" 271..274 "CGGC"
S20 256..259 "GCCU" 264..267 "GGGN"
S21 279..284 "CUGGGA" 295..300 "GCCCAG"
S22 302..305 "CUCC" 310..313 "GGAG"
S23 330..330 "U" 356..356 "A"
S24 332..334 "GCA" 353..355 "UGC"
S25 338..342 "UGGGC" 347..351 "GCCUG"
S26 369..372 "GAGG" 396..399 "CCUC"
S27 379..382 "GGCC" 387..390 "GGUU"
S28 403..403 "C" 440..440 "G"
S29 404..409 "AGCAGG" 431..436 "CCUGCA"
S30 413..413 "G" 426..426 "C"
S31 451..452 "CG" 498..499 "CG"
S32 453..461 "UCGGAUGUG" 487..495 "UGCAUUCGA"
S33 466..472 "CNNNNGG" 478..484 "CCCCGGG"
S34 510..512 "AGU" 604..606 "ACU"
S35 513..518 "GUGGUA" 597..602 "GGCCAU"
S36 521..527 "GGAGAUC" 589..595 "GAUCUCU"
S37 528..529 "GG" 571..572 "AC"
S38 532..539 "UUCCUGGU" 561..568 "AUCAGGAG"
S39 543..545 "GCG" 552..554 "UGC"
S40 577..577 "G" 588..588 "G"
S41 580..581 "GC" 586..587 "GC"
S42 624..629 "GCGUGG" 660..665 "CCACGC"
S43 633..634 "GC" 658..659 "GU"
S44 638..641 "CAGG" 651..654 "CCUG"
S45 676..681 "GGGAAC" 731..736 "GUUCCC"
S46 684..695 "GGUGUUGGCGAC" 703..714 "GUCGUCGGUGCC"
S47 718..719 "GC" 724..725 "GC"
S48 765..771 "CCAAGGC" 1000..1006 "GUCUUGG"
S49 777..785 "AUAUACCGG" 816..824 "UCGGUAUAC"
S50 788..794 "ACGGCCA" 799..805 "UGGUCGC"
S51 807..808 "CC" 814..815 "NG"
S52 826..827 "GG" 994..995 "CC"
S53 829..832 "GGUG" 990..993 "UGCC"
S54 835..836 "UG" 988..989 "CA"
S55 837..839 "GCU" 982..984 "AGU"
S56 840..843 "GUCG" 977..980 "CGUC"
S57 847..852 "GCUCGU" 881..886 "ACGAGC"
S58 853..855 "GUC" 860..862 "GAU"
S59 865..868 "UGGG" 875..878 "CCCG"
S60 892..895 "CCUU" 969..972 "GGGG"
S61 897..903 "UUCUNUG" 934..940 "CACAGGA"
S62 907..908 "CC" 928..929 "GG"
S63 911..913 "CAU" 925..927 "AUG"
S64 943..950 "CUGCCGGG" 956..963 "CUCGGAGG"
S65 1026..1032 "GGCCGGU" 1075..1081 "GCCGGUC"
S66 1036..1040 "AUGAG" 1067..1071 "CUCAA"
S67 1043..1044 "GC" 1061..1062 "GC"
S68 1048..1050 "ACC" 1055..1057 "GGU"
S69 1088..1088 "C" 1119..1119 "G"
S70 1092..1099 "UUGGGGUC" 1108..1115 "GACCCCAU"
S71 1136..1142 "UCGCAGA" 1151..1157 "GCUGCGG"
H1 13..19 "UGAUCCU" (12,20) U:G 
H2 77..80 "UUCG" (76,81) C:G 
H3 149..152 "GAAA" (148,153) G:C 
H4 184..187 "GCAU" (183,188) G:C 
H5 223..229 "GUAAUGG" (222,230) G:C 
H6 260..263 "GAGA" (259,264) U:G 
H7 285..294 "CUGAGACACG" (284,295) A:G 
H8 306..309 "UACG" (305,310) C:G 
H9 343..346 "GAAA" (342,347) C:G 
H10 383..386 "UNCG" (382,387) C:G 
H11 414..425 "AAGCGAAAGUGA" (413,426) G:C 
H12 473..477 "CUUAA" (472,478) G:C 
H13 546..551 "GUGAAA" (545,552) G:U 
H14 582..585 "GAAG" (581,586) C:G 
H15 642..650 "AUUAGAUAC" (641,651) G:C 
H16 696..702 "AUUCCAC" (695,703) C:G 
H17 720..723 "UAAC" (719,724) C:G 
H18 795..798 "GAGA" (794,799) A:U 
H19 809..813 "CUUGU" (808,814) C:N 
H20 856..859 "GUGA" (855,860) C:G 
H21 869..874 "UUAAGU" (868,875) G:C 
H22 914..924 "GCUUCGGGGUN" (913,925) U:A 
H23 951..955 "GUCAA" (950,956) G:C 
H24 1051..1054 "GUGA" (1050,1055) C:G 
H25 1100..1107 "UGCAACUC" (1099,1108) C:G 
H26 1143..1150 "UCAGCAUU" (1142,1151) A:G 
B1 54..54 "A" (53,320) C:G (319,55) A:U 
B2 63..64 "GA" (62,93) C:G (92,65) U:A 
B3 89..89 "U" (88,68) U:A (67,90) G:A 
B4 206..207 "AU" (205,247) U:G (246,208) C:C 
B5 213..214 "NN" (212,238) U:G (237,215) A:U 
B6 268..270 "GAC" (267,256) N:G (255,271) G:C 
B7 331..331 "U" (330,356) U:A (355,332) C:G 
B8 360..360 "A" (359,44) G:U (43,361) G:C 
B9 437..439 "GAA" (436,404) A:A (403,440) C:G PK{1}
B10 496..497 "UA" (495,453) A:U (452,498) G:C 
B11 578..579 "UG" (577,588) G:G (587,580) C:G 
B12 603..603 "U" (602,513) U:G (512,604) U:A 
B13 630..632 "GGA" (629,660) G:C (659,633) U:G 
B14 828..828 "U" (827,994) G:C (993,829) C:G 
B15 833..834 "CA" (832,990) G:U (989,835) A:U 
B16 909..910 "AG" (908,928) C:G (927,911) G:C 
B17 981..981 "A" (980,840) C:G (839,982) U:A 
B18 985..987 "CAU" (984,837) U:G (836,988) G:C 
I1.1 47..50 "UUAA" (46,324) C:G 
I1.2 323..323 "G" (322,51) G:C 
I2.1 70..72 "GAA" (69,87) U:A 
I2.2 85..86 "GG" (84,73) U:G 
I3.1 138..141 "GACA" (137,165) G:C 
I3.2 160..164 "UAAUA" (159,142) C:A 
I4.1 209..209 "A" (208,246) C:C 
I4.2 241..245 "GACGA" (240,210) C:G 
I5.1 335..337 "CAA" (334,353) A:U 
I5.2 352..352 "A" (351,338) G:U 
I6.1 373..378 "GAUGAC" (372,396) G:C 
I6.2 391..395 "GUAAA" (390,379) U:G 
I7.1 410..412 "GAA" (409,431) G:C 
I7.2 427..430 "GGUA" (426,413) C:G 
I8.1 462..465 "AAAG" (461,487) G:U 
I8.2 485..486 "UC" (484,466) G:C 
I9.1 519..520 "GG" (518,597) A:G 
I9.2 596..596 "G" (595,521) U:G 
I10.1 530..531 "AA" (529,571) G:A 
I10.2 569..570 "GA" (568,532) G:U 
I11.1 540..542 "GUA" (539,561) U:A 
I11.2 555..560 "GCAGAU" (554,543) C:G 
I12.1 635..637 "GAA" (634,658) C:G 
I12.2 655..657 "GUA" (654,638) G:C 
I13.1 904..906 "UUG" (903,934) G:C 
I13.2 930..933 "GACU" (929,907) G:C 
I14.1 1033..1035 "ACA" (1032,1075) U:G 
I14.2 1072..1074 "AAA" (1071,1036) A:A 
I15.1 1041..1042 "CU" (1040,1067) G:C 
I15.2 1063..1066 "GAAU" (1062,1043) C:G 
I16.1 1045..1047 "GAU" (1044,1061) C:G 
I16.2 1058..1060 "GGA" (1057,1048) U:A 
I17.1 1089..1091 "GGA" (1088,1119) C:G 
I17.2 1116..1118 "GAA" (1115,1092) U:U 
M1.1 46..45 "" (45,358) G:C (324,46) G:C 
M1.2 325..329 "GAAUA" (324,46) G:C (356,330) A:U 
M1.3 357..357 "G" (356,330) A:U (45,358) G:C 
M2.1 58..59 "AA" (57,317) C:G (95,60) C:G 
M2.2 96..101 "GAACGG" (95,60) C:G (277,102) C:G 
M2.3 278..278 "A" (277,102) C:G (300,279) G:C 
M2.4 301..301 "A" (300,279) G:C (313,302) G:C 
M2.5 314..316 "GCA" (313,302) G:C (57,317) C:G 
M3.1 105..133 "AGUAACACGUGGGCAAUCUGCCCUUCACU" (104,275) G:C (168,134) G:C 
M3.2 169..173 "AUACG" (168,134) G:C (197,174) U:A 
M3.3 198..202 "GGAAA" (197,174) U:A (249,203) G:C 
M3.4 250..251 "UA" (249,203) G:C (274,252) C:G 
M3.5 275..274 "" (274,252) C:G (104,275) G:C 
M4.1 528..527 "" (527,589) C:G (572,528) C:G 
M4.2 573..576 "ACCG" (572,528) C:G (588,577) G:G 
M4.3 589..588 "" (588,577) G:G (527,589) C:G 
M5.1 682..683 "UA" (681,731) C:G (714,684) C:G 
M5.2 715..717 "GCA" (714,684) C:G (725,718) C:G 
M5.3 726..730 "AUUAA" (725,718) C:G (681,731) C:G 
M6.1 772..776 "UUGAC" (771,1000) C:G (824,777) C:A 
M6.2 825..825 "A" (824,777) C:A (995,826) C:G 
M6.3 996..999 "UUAU" (995,826) C:G (771,1000) C:G 
M7.1 786..787 "AA" (785,816) G:U (805,788) C:A 
M7.2 806..806 "C" (805,788) C:A (815,807) G:C 
M7.3 816..815 "" (815,807) G:C (785,816) G:U 
M8.1 844..846 "UCA" (843,977) G:C (886,847) C:G 
M8.2 887..891 "GCAAC" (886,847) C:G (972,892) G:C 
M8.3 973..976 "ACGA" (972,892) G:C (843,977) G:C 
M9.1 853..852 "" (852,881) U:A (862,853) U:G 
M9.2 863..864 "GU" (862,853) U:G (878,865) G:U 
M9.3 879..880 "CA" (878,865) G:U (852,881) U:A 
M10.1 896..896 "G" (895,969) U:G (940,897) A:U 
M10.2 941..942 "GA" (940,897) A:U (963,943) G:C 
M10.3 964..968 "AAGGU" (963,943) G:C (895,969) U:G 
X1 25..37 "AGGACGAACGCUG" (24,8) C:G (366,38) C:G 
X2 367..368 "GU" (366,38) C:G (399,369) C:G 
X3 400..402 "UUU" (399,369) C:G (440,403) G:C PK{1}
X4 441..450 "AAGCGCCGGC" (440,403) G:C (499,451) G:C 
X5 500..509 "GGCAGGCUAG" (499,451) G:C (606,510) U:A 
X6 607..623 "GACGCUGAGGAGCGAAA" (606,510) U:A (665,624) C:G 
X7 666..675 "CGUAAACGUU" (665,624) C:G (736,676) C:G 
X8 737..764 "CGCCNGGGGAGUACGGCGAAGAACCUUA" (736,676) C:G (1006,765) G:C 
X9 1007..1025 "GCUGCACACGUGCUACAAU" (1006,765) G:C (1081,1026) C:G 
X10 1082..1087 "UCAGUU" (1081,1026) C:G (1119,1088) G:C 
X11 1120..1135 "UUGGAGUNGCUAGUAA" (1119,1088) G:C (1157,1136) G:U 
E1 1..7 "UCACGGA" 
PK1 1bp 401..401 439..439 X3 400..402 B9 437..439
PK1.1 401 U 439 A
NCBP1 901 N 936 C S61
NCBP2 468 N 482 G S33
NCBP3 256 G 267 N S20
NCBP4 142 A 159 C S13
NCBP5 469 N 481 C S33
NCBP6 788 A 805 C S50
NCBP7 208 C 246 C S16
NCBP8 67 G 90 A S8
NCBP9 841 U 979 U S56
NCBP10 467 N 483 G S33
NCBP11 1092 U 1115 U S70
NCBP12 1036 A 1071 A S66
NCBP13 777 A 824 C S49
NCBP14 518 A 597 G S35
NCBP15 529 G 571 A S37
NCBP16 284 A 295 G S21
NCBP17 808 C 814 N S51
NCBP18 1142 A 1151 G S71
NCBP19 404 A 436 A S29
NCBP20 577 G 588 G S40
NCBP21 470 N 480 C S33
NCBP22 945 G 961 A S64
segment1 5bp 8..12 GAGUU 20..24 GGCUC
segment2 8bp 38..45 GCGGCGUG 358..366 CGACGCCGC
segment3 7bp 46..57 CUUAACACAUGC 317..324 GCAGUGGG
segment4 12bp 60..76 GUCGAACGAUGAAGCCC 81..95 GGGUGGAUUAGUGGC
segment5 3bp 102..104 GUG 275..277 CAC
segment6 11bp 134..148 CUGGGACAAGCCCUG 153..168 CGGGGUCUAAUACCGG
segment7 10bp 174..183 ACCUGCCGAG 188..197 CUCGGUGGGU
segment8 15bp 203..222 CCUAUCAGCUNNUUGGUGGG 230..249 CCUACCAAGGCGACGACGGG
segment9 8bp 252..259 GCCGGCCU 264..274 GGGNGACCGGC
segment10 6bp 279..284 CUGGGA 295..300 GCCCAG
segment11 4bp 302..305 CUCC 310..313 GGAG
segment12 9bp 330..342 UUGCACAAUGGGC 347..356 GCCUGAUGCA
segment13 8bp 369..382 GAGGGAUGACGGCC 387..399 GGUUGUAAACCUC
segment14 8bp 403..413 CAGCAGGGAAG 426..440 CGGUACCUGCAGAAG
segment15 18bp 451..472 CGUCGGAUGUGAAAGCNNNNGG 478..499 CCCCGGGUCUGCAUUCGAUACG
segment16 16bp 510..527 AGUGUGGUAGGGGAGAUC 589..606 GAUCUCUGGGCCAUUACU
segment17 13bp 528..545 GGAAUUCCUGGUGUAGCG 552..572 UGCGCAGAUAUCAGGAGGAAC
segment18 3bp 577..581 GUGGC 586..588 GCG
segment19 12bp 624..641 GCGUGGGGAGCGAACAGG 651..665 CCUGGUAGUCCACGC
segment20 6bp 676..681 GGGAAC 731..736 GUUCCC
segment21 12bp 684..695 GGUGUUGGCGAC 703..714 GUCGUCGGUGCC
segment22 2bp 718..719 GC 724..725 GC
segment23 7bp 765..771 CCAAGGC 1000..1006 GUCUUGG
segment24 9bp 777..785 AUAUACCGG 816..824 UCGGUAUAC
segment25 7bp 788..794 ACGGCCA 799..805 UGGUCGC
segment26 2bp 807..808 CC 814..815 NG
segment27 15bp 826..843 GGUGGUGCAUGGCUGUCG 977..995 CGUCAAGUCAUCAUGCCCC
segment28 6bp 847..852 GCUCGU 881..886 ACGAGC
segment29 3bp 853..855 GUC 860..862 GAU
segment30 4bp 865..868 UGGG 875..878 CCCG
segment31 4bp 892..895 CCUU 969..972 GGGG
segment32 12bp 897..913 UUCUNUGUUGCCAGCAU 925..940 AUGGGGACUCACAGGA
segment33 8bp 943..950 CUGCCGGG 956..963 CUCGGAGG
segment34 17bp 1026..1050 GGCCGGUACAAUGAGCUGCGAUACC 1055..1081 GGUGGAGCGAAUCUCAAAAAGCCGGUC
segment35 9bp 1088..1099 CGGAUUGGGGUC 1108..1119 GACCCCAUGAAG
segment36 7bp 1136..1142 UCGCAGA 1151..1157 GCUGCGG
