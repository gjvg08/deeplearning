#Name: bpRNA_CRW_8174
#Length:  1080 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
GCGAACGCUGGCGGCGUGCCUAACACAUGCAAGUCGUGCGCAGGCUUGCUCCCUUUUGGGAGCGGGUGCUGAGCGGCAAACGGGUGAGUAACACGUGGGUAACCUACCCCCAGGAGGGGGAUAACCCCGGGAAACCGGGGCUAAUACCCCAUAAUGCCACCCGCCACUAAGGCGUGGUGGCCAAAGGGGGCCUCUGGCCUUUGGCCAUGCUCCCGCCUGGGGAUGGGCCCGCGGCCCAUCAGGUAGUUGGUGGGGUAACGGCCCACCAAGCCUAUGACGGGUAGCCGGCCUGAGAGGGUGGCCGGCCACAGCGGGACUGAGACACGGCCCGCACCCCUACGGGGGGCAGCAGUGGGGAAUCGUGGGCAAUGGGCGAAAGCCUGACCCCGCGACGCCGCGUGGGGGAAGAAGCCCUGCGGGGUGUAAACCCCUGUCGGGGGGGACGAAGCGGCUAUGGGUUAAUAGCCCAUAGUCGUGACGGUACCCCCAGAGGAAGGGACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUCCCGAGCGUUGCGCGAAGUCACUGGGCGUAAAGCGUCCGCAGCCGGUCGGGUAAGCGGGAUGUCAAAGCCCACGGCUCAACCGUGGAAUGGCAUCCCGAACUGCCCGACUUGAGGCACGCCCGGGCAGGCGGAAUUCCCGGGGUAGCGGUGAAAUGCGUAGAUCUCGGGAGGAACACCGAAGGGGAAGCCAGCCUGCUGGGGCUGUCCUGACGGUCAGGGACGAAAGCCGGGGGAGCAAACCGGAUUAGAUACCCGGGUAGUCCCGGCCGUAAACCAUGGGCGCUAGGGCUUGUCCCUUUGGGGCAGGCUCGCAGCUAACGCGUUAAGCGCCCCGCCUGGGGAGUACGGGCGCAAGCCUGAAACUCAAAGGAAUUGGCGGGGGCCCGCACAACCGGUGGAGCGUCUGGUUCAAUUCGAUGCUAACCGAAGAACCUUACCCGGGCUUGACAUGCUGGGGAGACUCCGCGAAAGCGGAGUUGUGGAAGUCCCUAGGACUUCCCCCCAGCACAGGUGGUGCAUGGCCGUCGUCAGCUCGUGUCGU
((.((((((.(((((((((....(((.(((..(((..((((((((((((((((....)))))))))).)))))))))......(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....((((((((((.....)))).))))))....((((((...(((((...))))).)))))).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).)).))))))..((((......((((....)))).....)))).[.(((((((...(.....(((((((((.....))))))))).....)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).))))))))............((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((.....))))))))))...((..]])).....)))))))))).(((......((((....))))....)))...............................................((((........))))....................(((((((((..(((((((....)))))))...((((((((...))))))))))))))))).................................
SSBSSSSSSMSSSSSSSSSIIIISSSBSSSMMSSSBBSSSSSSSSSSSSSSSSHHHHSSSSSSSSSSBSSSSSSSSSMMMMMMSSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHHSSSSBSSSSSSMMMMSSSSSSIIISSSSSHHHSSSSSISSSSSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSBSSSSSSMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMMMSSSSSSSIIISIIIIISSSSSSSSSHHHHHSSSSSSSSSIIIIISIIIISSSSSSBBBSMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMSSSSSSSSMMMMMMMMMMMMSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSHHHHHSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSMSSSIIIIIISSSSHHHHSSSSIIIISSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSSSHHHHHHHHSSSSMMMMMMMMMMMMMMMMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMMMSSSSSSSSHHHSSSSSSSSSSSSSSSSSEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..2 "GC" 549..550 "GC"
S2 4..9 "AACGCU" 543..548 "AGCGUU"
S3 11..16 "GCGGCG" 393..398 "CGCCGC"
S4 17..18 "UG" 390..391 "CG"
S5 19..19 "C" 356..356 "G"
S6 24..26 "CAC" 352..354 "GUG"
S7 28..30 "UGC" 349..351 "GCA"
S8 33..35 "GUC" 75..77 "GGC"
S9 38..43 "GCGCAG" 69..74 "CUGAGC"
S10 44..53 "GCUUGCUCCC" 58..67 "GGGAGCGGGU"
S11 84..86 "GUG" 307..309 "CAC"
S12 93..100 "ACGUGGGU" 227..234 "GCCCGCGG"
S13 103..104 "CC" 225..226 "GG"
S14 108..114 "CCCCAGG" 216..222 "CCUGGGG"
S15 116..119 "GGGG" 147..150 "CCCC"
S16 124..130 "ACCCCGG" 135..141 "CCGGGGC"
S17 156..161 "GCCACC" 176..181 "GGUGGC"
S18 162..165 "CGCC" 171..174 "GGCG"
S19 186..191 "GGGGGC" 209..214 "GCUCCC"
S20 195..199 "UGGCC" 203..207 "GGCCA"
S21 235..237 "CCC" 279..281 "GGG"
S22 240..240 "C" 278..278 "C"
S23 242..244 "GGU" 270..272 "GCC"
S24 247..254 "UUGGUGGG" 262..269 "CCCACCAA"
S25 284..287 "GCCG" 303..306 "CGGC"
S26 288..291 "GCCU" 296..299 "GGGU"
S27 311..316 "GCGGGA" 327..332 "GCCCGC"
S28 334..337 "CCCC" 342..345 "GGGG"
S29 362..362 "G" 388..388 "C"
S30 364..366 "GGG" 385..387 "CCC"
S31 370..374 "UGGGC" 379..383 "GCCUG"
S32 401..404 "GGGG" 428..431 "CCCC"
S33 411..414 "GCCC" 419..422 "GGGU"
S34 435..435 "C" 493..493 "G"
S35 436..441 "GGGGGG" 484..489 "CCCCCA"
S36 445..445 "G" 479..479 "C"
S37 451..459 "GCUAUGGGU" 465..473 "GCCCAUAGU"
S38 496..500 "GGGAC" 537..541 "GUCCC"
S39 507..511 "CUACG" 532..536 "CGUAG"
S40 517..518 "GC" 523..524 "GC"
S41 563..565 "GGC" 873..875 "GCC"
S42 571..571 "G" 872..872 "C"
S43 573..576 "GUCC" 757..760 "GGAC"
S44 578..578 "C" 754..754 "C"
S45 580..582 "GCC" 751..753 "GGU"
S46 584..590 "GUCGGGU" 641..647 "GCCCGAC"
S47 592..593 "AG" 639..640 "CU"
S48 594..602 "CGGGAUGUC" 628..636 "GGCAUCCCG"
S49 607..613 "CCCACGG" 619..625 "CCGUGGA"
S50 651..653 "AGG" 745..747 "CCU"
S51 654..659 "CACGCC" 738..743 "GGGCUG"
S52 662..668 "GGCAGGC" 730..736 "GCCUGCU"
S53 669..670 "GG" 712..713 "AC"
S54 673..680 "UUCCCGGG" 702..709 "CUCGGGAG"
S55 684..686 "GCG" 693..695 "UGC"
S56 718..718 "A" 729..729 "A"
S57 721..722 "GG" 727..728 "CC"
S58 765..770 "GCCGGG" 801..806 "CCCGGC"
S59 774..775 "GC" 799..800 "GU"
S60 779..782 "CCGG" 792..795 "CCGG"
S61 817..822 "GGGCGC" 866..871 "GCGCCC"
S62 825..834 "GGGCUUGUCC" 840..849 "GGGCAGGCUC"
S63 853..854 "GC" 859..860 "GC"
S64 877..879 "GGG" 902..904 "CUC"
S65 886..889 "GGGC" 894..897 "GCCU"
S66 952..955 "UUCG" 964..967 "CGAA"
S67 988..996 "AUGCUGGGG" 1039..1047 "CCCCAGCAC"
S68 999..1005 "ACUCCGC" 1010..1016 "GCGGAGU"
S69 1020..1027 "GGAAGUCC" 1031..1038 "GGACUUCC"
H1 54..57 "UUUU" (53,58) C:G 
H2 131..134 "GAAA" (130,135) G:C 
H3 166..170 "ACUAA" (165,171) C:G 
H4 200..202 "UUU" (199,203) C:G 
H5 255..261 "GUAACGG" (254,262) G:C 
H6 292..295 "GAGA" (291,296) U:G 
H7 317..326 "CUGAGACACG" (316,327) A:G 
H8 338..341 "UACG" (337,342) C:G 
H9 375..378 "GAAA" (374,379) C:G 
H10 415..418 "UGCG" (414,419) C:G 
H11 460..464 "UAAUA" (459,465) U:G 
H12 519..522 "AGCC" (518,523) C:G PK{2}
H13 614..618 "CUCAA" (613,619) G:C 
H14 687..692 "GUGAAA" (686,693) G:U 
H15 723..726 "GAAG" (722,727) G:C 
H16 783..791 "AUUAGAUAC" (782,792) G:C 
H17 835..839 "CUUUG" (834,840) C:G 
H18 855..858 "UAAC" (854,859) C:G PK{3}
H19 890..893 "GCAA" (889,894) C:G 
H20 956..963 "AUGCUAAC" (955,964) G:C 
H21 1006..1009 "GAAA" (1005,1010) C:G 
H22 1028..1030 "CUA" (1027,1031) C:G 
B1 3..3 "G" (2,549) C:G (548,4) U:A 
B2 27..27 "A" (26,352) C:G (351,28) A:U 
B3 36..37 "GU" (35,75) C:G (74,38) C:G 
B4 68..68 "G" (67,44) U:G (43,69) G:C 
B5 101..102 "AA" (100,227) U:G (226,103) G:C 
B6 175..175 "U" (174,162) G:C (161,176) C:G 
B7 238..239 "AU" (237,279) C:G (278,240) C:C 
B8 245..246 "AG" (244,270) U:G (269,247) A:U 
B9 300..302 "GGC" (299,288) U:G (287,303) G:C 
B10 363..363 "U" (362,388) G:C (387,364) C:G 
B11 392..392 "A" (391,17) G:U (16,393) G:C 
B12 490..492 "GAG" (489,436) A:G (435,493) C:G PK{1}
B13 501..506 "GGCUAA" (500,537) C:G (536,507) G:C PK{2}
B14 566..570 "GUAAA" (565,873) C:G (872,571) C:G PK{3}
B15 579..579 "A" (578,754) C:C (753,580) U:G 
B16 591..591 "A" (590,641) U:G (640,592) U:A 
B17 637..638 "AA" (636,594) G:C (593,639) G:C 
B18 719..720 "AG" (718,729) A:A (728,721) C:G 
B19 744..744 "U" (743,654) G:C (653,745) G:C 
B20 771..773 "GGA" (770,801) G:C (800,774) U:G 
I1.1 20..23 "CUAA" (19,356) C:G 
I1.2 355..355 "G" (354,24) G:C 
I2.1 105..107 "UAC" (104,225) C:G 
I2.2 223..224 "AU" (222,108) G:C 
I3.1 120..123 "GAUA" (119,147) G:C 
I3.2 142..146 "UAAUA" (141,124) C:A 
I4.1 192..194 "CUC" (191,209) C:G 
I4.2 208..208 "U" (207,195) A:U 
I5.1 241..241 "A" (240,278) C:C 
I5.2 273..277 "UAUGA" (272,242) C:G 
I6.1 367..369 "CAA" (366,385) G:C 
I6.2 384..384 "A" (383,370) G:U 
I7.1 405..410 "GAAGAA" (404,428) G:C 
I7.2 423..427 "GUAAA" (422,411) U:G 
I8.1 442..444 "GAC" (441,484) G:C 
I8.2 480..483 "GGUA" (479,445) C:G 
I9.1 446..450 "AAGCG" (445,479) G:C 
I9.2 474..478 "CGUGA" (473,451) U:G 
I10.1 512..516 "UGCCA" (511,532) G:C 
I10.2 525..531 "GGUAAUA" (524,517) C:G 
I11.1 577..577 "G" (576,757) C:G 
I11.2 755..756 "AG" (754,578) C:C 
I12.1 603..606 "AAAG" (602,628) C:G 
I12.2 626..627 "AU" (625,607) A:C 
I13.1 660..661 "CG" (659,738) C:G 
I13.2 737..737 "G" (736,662) U:G 
I14.1 671..672 "AA" (670,712) G:A 
I14.2 710..711 "GA" (709,673) G:U 
I15.1 681..683 "GUA" (680,702) G:C 
I15.2 696..701 "GUAGAU" (695,684) C:G 
I16.1 776..778 "AAA" (775,799) C:G 
I16.2 796..798 "GUA" (795,779) G:C 
I17.1 880..885 "GAGUAC" (879,902) G:C 
I17.2 898..901 "GAAA" (897,886) U:G 
M1.1 10..10 "G" (9,543) U:A (398,11) C:G 
M1.2 399..400 "GU" (398,11) C:G (431,401) C:G 
M1.3 432..434 "UGU" (431,401) C:G (493,435) G:C PK{1}
M1.4 494..495 "AA" (493,435) G:C (541,496) C:G 
M1.5 542..542 "G" (541,496) C:G (9,543) U:A 
M2.1 19..18 "" (18,390) G:C (356,19) G:C 
M2.2 357..361 "GAAUC" (356,19) G:C (388,362) C:G 
M2.3 389..389 "G" (388,362) C:G (18,390) G:C 
M3.1 31..32 "AA" (30,349) C:G (77,33) C:G 
M3.2 78..83 "AAACGG" (77,33) C:G (309,84) C:G 
M3.3 310..310 "A" (309,84) C:G (332,311) C:G 
M3.4 333..333 "A" (332,311) C:G (345,334) G:C 
M3.5 346..348 "GCA" (345,334) G:C (30,349) C:G 
M4.1 87..92 "AGUAAC" (86,307) G:C (234,93) G:A 
M4.2 235..234 "" (234,93) G:A (281,235) G:C 
M4.3 282..283 "UA" (281,235) G:C (306,284) C:G 
M4.4 307..306 "" (306,284) C:G (86,307) G:C 
M5.1 115..115 "A" (114,216) G:C (150,116) C:G 
M5.2 151..155 "AUAAU" (150,116) C:G (181,156) C:G 
M5.3 182..185 "CAAA" (181,156) C:G (214,186) C:G 
M5.4 215..215 "G" (214,186) C:G (114,216) G:C 
M6.1 551..562 "GCGAAGUCACUG" (550,1) C:G (875,563) C:G 
M6.2 876..876 "U" (875,563) C:G (904,877) C:G 
M6.3 905..951 "AAAGGAAUUGGCGGGGGCCCGCACAACCGGUGGAGCGUCUGGUUCAA" (904,877) C:G (967,952) A:U 
M6.4 968..987 "GAACCUUACCCGGGCUUGAC" (967,952) A:U (1047,988) C:A 
M7.1 572..572 "C" (571,872) G:C (760,573) C:G 
M7.2 761..764 "GAAA" (760,573) C:G (806,765) C:G 
M7.3 807..816 "CGUAAACCAU" (806,765) C:G (871,817) C:G 
M7.4 872..871 "" (871,817) C:G (571,872) G:C 
M8.1 583..583 "G" (582,751) C:G (647,584) C:G 
M8.2 648..650 "UUG" (647,584) C:G (747,651) U:A 
M8.3 748..750 "GAC" (747,651) U:A (582,751) C:G 
M9.1 669..668 "" (668,730) C:G (713,669) C:G 
M9.2 714..717 "ACCG" (713,669) C:G (729,718) A:A 
M9.3 730..729 "" (729,718) A:A (668,730) C:G 
M10.1 823..824 "UA" (822,866) C:G (849,825) C:G 
M10.2 850..852 "GCA" (849,825) C:G (860,853) C:G 
M10.3 861..865 "GUUAA" (860,853) C:G (822,866) C:G 
M11.1 997..998 "AG" (996,1039) G:C (1016,999) U:A 
M11.2 1017..1019 "UGU" (1016,999) U:A (1038,1020) C:G 
M11.3 1039..1038 "" (1038,1020) C:G (996,1039) G:C 
E1 1048..1080 "AGGUGGUGCAUGGCCGUCGUCAGCUCGUGUCGU" 
PK1 1bp 433..433 492..492 M1.3 432..434 B12 490..492
PK2 3bp 501..503 520..522 B13 501..506 H12 519..522
PK3 2bp 566..567 857..858 B14 566..570 H18 855..858
PK1.1 433 G 492 G
PK2.1 501 G 522 C
PK2.2 502 G 521 C
PK2.3 503 C 520 G
PK3.1 566 G 858 C
PK3.2 567 U 857 A
NCBP1 578 C 754 C S44
NCBP2 124 A 141 C S16
NCBP3 607 C 625 A S49
NCBP4 436 G 489 A S35
NCBP5 718 A 729 A S56
NCBP6 40 G 72 A S9
NCBP7 316 A 327 G S27
NCBP8 988 A 1047 C S67
NCBP9 93 A 234 G S12
NCBP10 240 C 278 C S22
NCBP11 657 G 740 G S51
NCBP12 656 C 741 C S51
NCBP13 670 G 712 A S53
NCBP14 433 G 492 G PK1.1
segment1 8bp 1..9 GCGAACGCU 543..550 AGCGUUGC
segment2 8bp 11..18 GCGGCGUG 390..398 CGACGCCGC
segment3 7bp 19..30 CCUAACACAUGC 349..356 GCAGUGGG
segment4 19bp 33..53 GUCGUGCGCAGGCUUGCUCCC 58..77 GGGAGCGGGUGCUGAGCGGC
segment5 3bp 84..86 GUG 307..309 CAC
segment6 17bp 93..114 ACGUGGGUAACCUACCCCCAGG 216..234 CCUGGGGAUGGGCCCGCGG
segment7 11bp 116..130 GGGGGAUAACCCCGG 135..150 CCGGGGCUAAUACCCC
segment8 10bp 156..165 GCCACCCGCC 171..181 GGCGUGGUGGC
segment9 11bp 186..199 GGGGGCCUCUGGCC 203..214 GGCCAUGCUCCC
segment10 15bp 235..254 CCCAUCAGGUAGUUGGUGGG 262..281 CCCACCAAGCCUAUGACGGG
segment11 8bp 284..291 GCCGGCCU 296..306 GGGUGGCCGGC
segment12 6bp 311..316 GCGGGA 327..332 GCCCGC
segment13 4bp 334..337 CCCC 342..345 GGGG
segment14 9bp 362..374 GUGGGCAAUGGGC 379..388 GCCUGACCCC
segment15 8bp 401..414 GGGGGAAGAAGCCC 419..431 GGGUGUAAACCCC
segment16 17bp 435..459 CGGGGGGGACGAAGCGGCUAUGGGU 465..493 GCCCAUAGUCGUGACGGUACCCCCAGAGG
segment17 12bp 496..518 GGGACGGCUAACUACGUGCCAGC 523..541 GCGGUAAUACGUAGGUCCC
segment18 4bp 563..571 GGCGUAAAG 872..875 CGCC
segment19 8bp 573..582 GUCCGCAGCC 751..760 GGUCAGGGAC
segment20 25bp 584..613 GUCGGGUAAGCGGGAUGUCAAAGCCCACGG 619..647 CCGUGGAAUGGCAUCCCGAACUGCCCGAC
segment21 16bp 651..668 AGGCACGCCCGGGCAGGC 730..747 GCCUGCUGGGGCUGUCCU
segment22 13bp 669..686 GGAAUUCCCGGGGUAGCG 693..713 UGCGUAGAUCUCGGGAGGAAC
segment23 3bp 718..722 AAGGG 727..729 CCA
segment24 12bp 765..782 GCCGGGGGAGCAAACCGG 792..806 CCGGGUAGUCCCGGC
segment25 6bp 817..822 GGGCGC 866..871 GCGCCC
segment26 10bp 825..834 GGGCUUGUCC 840..849 GGGCAGGCUC
segment27 2bp 853..854 GC 859..860 GC
segment28 7bp 877..889 GGGGAGUACGGGC 894..904 GCCUGAAACUC
segment29 4bp 952..955 UUCG 964..967 CGAA
segment30 9bp 988..996 AUGCUGGGG 1039..1047 CCCCAGCAC
segment31 7bp 999..1005 ACUCCGC 1010..1016 GCGGAGU
segment32 8bp 1020..1027 GGAAGUCC 1031..1038 GGACUUCC
