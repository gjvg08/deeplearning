#Name: bpRNA_CRW_11551
#Length:  1096 
#PageNumber: 2
GAUGCGUANCCGACCUGAGAGGGUGAUCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCCGCAAUGGACGAAAGUCUGACGGAGCAACGCCGCGUGAACGAAGAAGGCCUUCGGGUCGUAAAGUUCUGUUGUUAGGGAAGAACAAGUACCAGAGUAACUGCUGGUACCUUGACGGUACCUAACCAGAAAGCCACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUGGCAAGCGUUGUCCGGAAUUAUUGGGCGUAAAGCGCGCGCAGGUGGUUCCUUAAGUCUGAUGUGAAAGCCCACGGCUCAACCGUGGAGGGUCAUUGGAAACUGGGGAACUUGAGUNCAGAAGAGGAAAGUGGAAUUCCAAGUGUAGCGGUGAAAUGCGUAGAGAUUUGGAGGAACACCAGUGGCGAAGGCGACUUUCUGGUCUGUAACUGACACUGAGGCGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAAGUGUUAGAGGGUUUCCGCCCUUUAGUGCUGCAGCUAACGCAUUAAGCACUCCGCCUGGGGAGUACGGCCGCAAGGCUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCUCUGACAACCCUAGAGAUAGGNUUUCCCCUUCGGGGGACAGAGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGAUCUUAGUUGCCAGCAUUCAGUUGGGCACUCUAAGGUGACUGCCGGUGACAANCCGGAGGAAGGUGGGGAUGACGUCAANUCAUCAUGCCCCUUAUGAUCNGGUGGACAAACGUGCUACAAUGGAUGGUACAAAGGGCUGCAAACCUGCGAAGGUAAGCGAAUCCCAUAAAGCCAUUCUCAGUUCGGAUUGUAGGCUGCAACUCGCCUACAUGAAGCCGGAAUCGCUAGUAAUCGCGGAUCAGCAUGCCGCGGUGAA
........((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))..........................(((((((((((((((((....((((........))))........(((((((.....((((((((...((((((....)))))).((.((....)).))))))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))...
EEEEEEEESSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSMMMSSSSSSHHHHSSSSSSMSSISSHHHHSSISSSSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSXXXXXSSSSSSSSHHHHHHHSSSSSSSSEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 9..12 "NCCG" 28..31 "CGGC"
S2 13..16 "ACCU" 21..24 "GGGU"
S3 36..41 "CUGGGA" 52..57 "GCCCAG"
S4 59..62 "CUCC" 67..70 "GGAG"
S5 87..87 "U" 113..113 "A"
S6 89..91 "CCG" 110..112 "CGG"
S7 95..99 "UGGAC" 104..108 "GUCUG"
S8 126..129 "GAAC" 153..156 "GUUC"
S9 136..139 "GGCC" 144..147 "GGUC"
S10 158..158 "G" 216..216 "G"
S11 161..166 "GUUAGG" 208..213 "CCUAAC"
S12 170..170 "G" 203..203 "C"
S13 176..183 "GUACCAGA" 190..197 "GCUGGUAC"
S14 220..224 "GCCAC" 261..265 "GUGGC"
S15 231..235 "CUACG" 256..260 "CGUAG"
S16 241..242 "GC" 247..248 "GC"
S17 287..289 "GGC" 602..604 "GCC"
S18 297..300 "GCGC" 481..484 "GCGC"
S19 302..302 "C" 478..478 "G"
S20 304..306 "GGU" 475..477 "ACU"
S21 308..314 "GUUCCUU" 365..371 "GGGGAAC"
S22 316..317 "AG" 363..364 "CU"
S23 318..326 "UCUGAUGUG" 352..360 "GUCAUUGGA"
S24 331..337 "CCCACGG" 343..349 "CCGUGGA"
S25 375..377 "AGU" 469..471 "ACU"
S26 378..383 "NCAGAA" 462..467 "GUCUGU"
S27 386..392 "GGAAAGU" 454..460 "ACUUUCU"
S28 393..394 "GG" 436..437 "AC"
S29 397..404 "UUCCAAGU" 426..433 "AUUUGGAG"
S30 408..410 "GCG" 417..419 "UGC"
S31 442..442 "G" 453..453 "G"
S32 445..446 "GC" 451..452 "GC"
S33 489..494 "GCGUGG" 525..530 "CCACGC"
S34 498..499 "GC" 523..524 "GU"
S35 503..506 "CAGG" 516..519 "CCUG"
S36 541..546 "GAGUGC" 595..600 "GCACUC"
S37 549..560 "AGUGUUAGAGGG" 567..578 "CCCUUUAGUGCU"
S38 582..583 "GC" 588..589 "GC"
S39 606..608 "GGG" 631..633 "CUC"
S40 615..618 "GGCC" 623..626 "GGCU"
S41 660..664 "GCGGU" 1061..1065 "AUCGC"
S42 665..674 "GGAGCAUGUG" 949..958 "AACGUGCUAC"
S43 675..676 "GU" 946..947 "AC"
S44 681..684 "UUCG" 693..696 "CGAA"
S45 705..711 "CCAGGUC" 936..942 "GAUCNGG"
S46 717..719 "AUC" 765..767 "GAC"
S47 720..724 "CUCUG" 759..763 "CAGAG"
S48 728..733 "ACCCUA" 738..743 "UAGGNU"
S49 745..746 "UC" 757..758 "GA"
S50 748..749 "CC" 754..755 "GG"
S51 769..770 "GG" 930..931 "CC"
S52 772..775 "GGUG" 926..929 "UGCC"
S53 778..779 "UG" 924..925 "CA"
S54 780..782 "GUU" 918..920 "ANU"
S55 783..786 "GUCG" 913..916 "CGUC"
S56 790..795 "GCUCGU" 824..829 "ACGAGC"
S57 796..798 "GUC" 803..805 "GAU"
S58 808..811 "UGGG" 818..821 "CCCG"
S59 835..838 "CCUU" 905..908 "GGGG"
S60 840..846 "AUCUUAG" 870..876 "CUAAGGU"
S61 850..851 "CC" 864..865 "GG"
S62 854..855 "CA" 862..863 "UG"
S63 879..886 "CUGCCGGU" 892..899 "NCCGGAGG"
S64 962..968 "GGAUGGU" 1011..1017 "GCCAUUC"
S65 974..976 "GGG" 1003..1005 "CCC"
S66 979..980 "GC" 997..998 "GC"
S67 984..986 "CCU" 991..993 "AGG"
S68 1024..1024 "C" 1055..1055 "G"
S69 1028..1035 "UUGUAGGC" 1044..1051 "GCCUACAU"
S70 1071..1078 "AUCGCGGA" 1086..1093 "GCCGCGGU"
H1 17..20 "GAGA" (16,21) U:G 
H2 42..51 "CUGAGACACG" (41,52) A:G 
H3 63..66 "UACG" (62,67) C:G 
H4 100..103 "GAAA" (99,104) C:G 
H5 140..143 "UUCG" (139,144) C:G 
H6 184..189 "GUAACU" (183,190) A:G 
H7 243..246 "AGCC" (242,247) C:G PK{1}
H8 338..342 "CUCAA" (337,343) G:C 
H9 411..416 "GUGAAA" (410,417) G:U 
H10 447..450 "GAAG" (446,451) C:G 
H11 507..515 "AUUAGAUAC" (506,516) G:C 
H12 561..566 "UUUCCG" (560,567) G:C 
H13 584..587 "UAAC" (583,588) C:G PK{2}
H14 619..622 "GCAA" (618,623) C:G 
H15 685..692 "AAGCAACG" (684,693) G:C 
H16 734..737 "GAGA" (733,738) A:U 
H17 750..753 "UUCG" (749,754) C:G 
H18 799..802 "GUGA" (798,803) C:G 
H19 812..817 "UUAAGU" (811,818) G:C 
H20 856..861 "UUCAGU" (855,862) A:U 
H21 887..891 "GACAA" (886,892) U:N 
H22 987..990 "GCGA" (986,991) U:A 
H23 1036..1043 "UGCAACUC" (1035,1044) C:G 
H24 1079..1085 "UCAGCAU" (1078,1086) A:G 
B1 25..27 "GAU" (24,13) U:A (12,28) G:C 
B2 88..88 "U" (87,113) U:A (112,89) G:C 
B3 225..230 "GGCUAA" (224,261) C:G (260,231) G:C PK{1}
B4 303..303 "A" (302,478) C:G (477,304) U:G 
B5 315..315 "A" (314,365) U:G (364,316) U:A 
B6 361..362 "AA" (360,318) A:U (317,363) G:C 
B7 443..444 "UG" (442,453) G:G (452,445) C:G 
B8 468..468 "A" (467,378) U:N (377,469) U:A 
B9 495..497 "GGA" (494,525) G:C (524,498) U:G 
B10 764..764 "U" (763,720) G:C (719,765) C:G 
B11 771..771 "U" (770,930) G:C (929,772) C:G 
B12 776..777 "CA" (775,926) G:U (925,778) A:U 
B13 852..853 "AG" (851,864) C:G (863,854) G:C 
B14 917..917 "A" (916,783) C:G (782,918) U:A 
B15 921..923 "CAU" (920,780) U:G (779,924) G:C 
B16 948..948 "A" (947,675) C:G (674,949) G:A 
I1.1 92..94 "CAA" (91,110) G:C 
I1.2 109..109 "A" (108,95) G:U 
I2.1 130..135 "GAAGAA" (129,153) C:G 
I2.2 148..152 "GUAAA" (147,136) C:G 
I3.1 159..160 "UU" (158,216) G:G 
I3.2 214..215 "CA" (213,161) C:G 
I4.1 167..169 "GAA" (166,208) G:C 
I4.2 204..207 "GGUA" (203,170) C:G 
I5.1 171..175 "AACAA" (170,203) G:C 
I5.2 198..202 "CUUGA" (197,176) C:G 
I6.1 236..240 "UGCCA" (235,256) G:C 
I6.2 249..255 "GGUAAUA" (248,241) C:G 
I7.1 301..301 "G" (300,481) C:G 
I7.2 479..480 "AG" (478,302) G:C 
I8.1 327..330 "AAAG" (326,352) G:G 
I8.2 350..351 "GG" (349,331) A:C 
I9.1 384..385 "GA" (383,462) A:G 
I9.2 461..461 "G" (460,386) U:G 
I10.1 395..396 "AA" (394,436) G:A 
I10.2 434..435 "GA" (433,397) G:U 
I11.1 405..407 "GUA" (404,426) U:A 
I11.2 420..425 "GUAGAG" (419,408) C:G 
I12.1 500..502 "AAA" (499,523) C:G 
I12.2 520..522 "GUA" (519,503) G:C 
I13.1 609..614 "GAGUAC" (608,631) G:C 
I13.2 627..630 "GAAA" (626,615) U:G 
I14.1 747..747 "C" (746,757) C:G 
I14.2 756..756 "G" (755,748) G:C 
I15.1 847..849 "UUG" (846,870) G:C 
I15.2 866..869 "CACU" (865,850) G:C 
I16.1 969..973 "ACAAA" (968,1011) U:G 
I16.2 1006..1010 "AUAAA" (1005,974) C:G 
I17.1 977..978 "CU" (976,1003) G:C 
I17.2 999..1002 "GAAU" (998,979) C:G 
I18.1 981..983 "AAA" (980,997) C:G 
I18.2 994..996 "UAA" (993,984) G:C 
I19.1 1025..1027 "GGA" (1024,1055) C:G 
I19.2 1052..1054 "GAA" (1051,1028) U:U 
M1.1 290..296 "GUAAAGC" (289,602) C:G (484,297) C:G PK{2}
M1.2 485..488 "GAAA" (484,297) C:G (530,489) C:G 
M1.3 531..540 "CGUAAACGAU" (530,489) C:G (600,541) C:G 
M1.4 601..601 "C" (600,541) C:G (289,602) C:G 
M2.1 307..307 "G" (306,475) U:A (371,308) C:G 
M2.2 372..374 "UUG" (371,308) C:G (471,375) U:A 
M2.3 472..474 "GAC" (471,375) U:A (306,475) U:A 
M3.1 393..392 "" (392,454) U:A (437,393) C:G 
M3.2 438..441 "ACCA" (437,393) C:G (453,442) G:G 
M3.3 454..453 "" (453,442) G:G (392,454) U:A 
M4.1 547..548 "UA" (546,595) C:G (578,549) U:A 
M4.2 579..581 "GCA" (578,549) U:A (589,582) C:G 
M4.3 590..594 "AUUAA" (589,582) C:G (546,595) C:G 
M5.1 665..664 "" (664,1061) U:A (958,665) C:G 
M5.2 959..961 "AAU" (958,665) C:G (1017,962) C:G 
M5.3 1018..1023 "UCAGUU" (1017,962) C:G (1055,1024) G:C 
M5.4 1056..1060 "CCGGA" (1055,1024) G:C (664,1061) U:A 
M6.1 677..680 "UUAA" (676,946) U:A (696,681) A:U 
M6.2 697..704 "GAACCUUA" (696,681) A:U (942,705) G:C 
M6.3 943..945 "UGG" (942,705) G:C (676,946) U:A 
M7.1 712..716 "UUGAC" (711,936) C:G (767,717) C:A 
M7.2 768..768 "A" (767,717) C:A (931,769) C:G 
M7.3 932..935 "UUAU" (931,769) C:G (711,936) C:G 
M8.1 725..727 "ACA" (724,759) G:C (743,728) U:A 
M8.2 744..744 "U" (743,728) U:A (758,745) A:U 
M8.3 759..758 "" (758,745) A:U (724,759) G:C 
M9.1 787..789 "UCA" (786,913) G:C (829,790) C:G 
M9.2 830..834 "GCAAC" (829,790) C:G (908,835) G:C 
M9.3 909..912 "AUGA" (908,835) G:C (786,913) G:C 
M10.1 796..795 "" (795,824) U:A (805,796) U:G 
M10.2 806..807 "GU" (805,796) U:G (821,808) G:U 
M10.3 822..823 "CA" (821,808) G:U (795,824) U:A 
M11.1 839..839 "G" (838,905) U:G (876,840) U:A 
M11.2 877..878 "GA" (876,840) U:A (899,879) G:C 
M11.3 900..904 "AAGGU" (899,879) G:C (838,905) U:G 
X1 32..35 "CACA" (31,9) C:N (57,36) G:C 
X3 71..86 "GCAGCAGUAGGGAAUC" (70,59) G:C (113,87) A:U 
X4 114..125 "GCAACGCCGCGU" (113,87) A:U (156,126) C:G 
X6 217..219 "AAA" (216,158) G:G (265,220) C:G 
X7 266..286 "AAGCGUUGUCCGGAAUUAUUG" (265,220) C:G (604,287) C:G 
X9 634..659 "AAAGGAAUUGACGGGGGCCCGCACAA" (633,606) C:G (1065,660) C:G 
X10 1066..1070 "UAGUA" (1065,660) C:G (1093,1071) U:A 
E1 1..8 "GAUGCGUA" 
E2 1094..1096 "GAA" 
PK1 3bp 225..227 244..246 B3 225..230 H7 243..246
PK2 2bp 290..291 586..587 M1.1 290..296 H13 584..587
PK1.1 225 G 246 C
PK1.2 226 G 245 C
PK1.3 227 C 244 G
PK2.1 290 G 587 C
PK2.2 291 U 586 A
NCBP1 331 C 349 A S24
NCBP2 378 N 467 U S26
NCBP3 325 U 353 U S23
NCBP4 881 G 897 A S63
NCBP5 784 U 915 U S55
NCBP6 666 G 957 A S42
NCBP7 717 A 767 C S46
NCBP8 394 G 436 A S28
NCBP9 9 N 31 C S1
NCBP10 781 U 919 N S54
NCBP11 442 G 453 G S31
NCBP12 886 U 892 N S63
NCBP13 1028 U 1051 U S69
NCBP14 674 G 949 A S42
NCBP15 326 G 352 G S23
NCBP16 41 A 52 G S3
NCBP17 1078 A 1086 G S70
NCBP18 158 G 216 G S10
NCBP19 183 A 190 G S13
NCBP20 707 A 940 N S45
NCBP21 729 C 742 N S48
NCBP22 383 A 462 G S26
segment1 8bp 9..16 NCCGACCU 21..31 GGGUGAUCGGC
segment2 6bp 36..41 CUGGGA 52..57 GCCCAG
segment3 4bp 59..62 CUCC 67..70 GGAG
segment4 9bp 87..99 UUCCGCAAUGGAC 104..113 GUCUGACGGA
segment5 8bp 126..139 GAACGAAGAAGGCC 144..156 GGUCGUAAAGUUC
segment6 16bp 158..183 GUUGUUAGGGAAGAACAAGUACCAGA 190..216 GCUGGUACCUUGACGGUACCUAACCAG
segment7 12bp 220..242 GCCACGGCUAACUACGUGCCAGC 247..265 GCGGUAAUACGUAGGUGGC
segment8 3bp 287..289 GGC 602..604 GCC
segment9 8bp 297..306 GCGCGCAGGU 475..484 ACUGAGGCGC
segment10 25bp 308..337 GUUCCUUAAGUCUGAUGUGAAAGCCCACGG 343..371 CCGUGGAGGGUCAUUGGAAACUGGGGAAC
segment11 16bp 375..392 AGUNCAGAAGAGGAAAGU 454..471 ACUUUCUGGUCUGUAACU
segment12 13bp 393..410 GGAAUUCCAAGUGUAGCG 417..437 UGCGUAGAGAUUUGGAGGAAC
segment13 3bp 442..446 GUGGC 451..453 GCG
segment14 12bp 489..506 GCGUGGGGAGCAAACAGG 516..530 CCUGGUAGUCCACGC
segment15 6bp 541..546 GAGUGC 595..600 GCACUC
segment16 12bp 549..560 AGUGUUAGAGGG 567..578 CCCUUUAGUGCU
segment17 2bp 582..583 GC 588..589 GC
segment18 7bp 606..618 GGGGAGUACGGCC 623..633 GGCUGAAACUC
segment19 5bp 660..664 GCGGU 1061..1065 AUCGC
segment20 12bp 665..676 GGAGCAUGUGGU 946..958 ACAAACGUGCUAC
segment21 4bp 681..684 UUCG 693..696 CGAA
segment22 7bp 705..711 CCAGGUC 936..942 GAUCNGG
segment23 8bp 717..724 AUCCUCUG 759..767 CAGAGUGAC
segment24 6bp 728..733 ACCCUA 738..743 UAGGNU
segment25 4bp 745..749 UCCCC 754..758 GGGGA
segment26 15bp 769..786 GGUGGUGCAUGGUUGUCG 913..931 CGUCAANUCAUCAUGCCCC
segment27 6bp 790..795 GCUCGU 824..829 ACGAGC
segment28 3bp 796..798 GUC 803..805 GAU
segment29 4bp 808..811 UGGG 818..821 CCCG
segment30 4bp 835..838 CCUU 905..908 GGGG
segment31 11bp 840..855 AUCUUAGUUGCCAGCA 862..876 UGGGCACUCUAAGGU
segment32 8bp 879..886 CUGCCGGU 892..899 NCCGGAGG
segment33 15bp 962..986 GGAUGGUACAAAGGGCUGCAAACCU 991..1017 AGGUAAGCGAAUCCCAUAAAGCCAUUC
segment34 9bp 1024..1035 CGGAUUGUAGGC 1044..1055 GCCUACAUGAAG
segment35 8bp 1071..1078 AUCGCGGA 1086..1093 GCCGCGGU
