#Name: bpRNA_CRW_12830
#Length:  1258 
#PageNumber: 2
AUUGCUUCACUAUGAGAUGGACCUGCGUUGUAUUAGCUAGUUGGUGAGGUAACGGCUCACCAAGGCUUCGAUACAUAGCCGACCUGAGAGGGUGAUCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCGGCAAUGGGGGCAACCCUGACCGAGCAACGCCGCGUGAGUGAAGAAGGUUUUCGGAUCGUAAAGCUCUGUUGUAAGAGAAGAACUGUGAGAAGAGUGGAAAGUUUCUCACUUGACGGUAUCUUACCAGAAAGGGACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUCCCGAGCGUUGUCCGGAUUUAUUGGGCGUAAAGCGAGCGCAGGCGGUUUGAUAAGUCUGAAGUAAAAGGCUGUGGCUUAACCAUAGUACGCUUUGGAAACUGUCAAACUUGAGUGCAGAAGGGGAGAGUGGAAUUCCAUGUGUAGCGGUGAAAUGCGUAGAUAUAUGGAGGAACACCGGUGGCGAAAGCGGCUCUCUGGUCUGUAACUGACGCUGAGGCUCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAGGUGUUGGGUCCUUUCCGGGACUCAGUGCCGCAGCUAACGCAUUAAGCACUCCGCCUGGGGAGUACGACCGCAAGGUUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCCGAUGACCGCCCUAGAGAUAGGGUUUCUCUUCGGAGCAUCGGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCCUAUUGUUAGUUGCCAUCAUUCAGUUGGGCACUCUAGCGAGACUGCCGGUAAUAAACCGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGACCUGGGCUACACACGUGCUACAAUGGCUGGUACAACGAGUCGCAAGUCGGUGACGGCAAGCUAAUCUCUUAAAGCCAGUCUCAGUUCGGAUUGUAGGCUGCAACUCGCCUACAUGAAGUCGGAAUCGCUAGUAAUCGCGGAUCAGCACGCCGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACACCACGAGAGUUUGUAACACCCGAAGUCGGUGAGGUAACCUUUUAGGAGCCAGCCGCCUA
............................(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((.......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.((((((((((.....(((((((.....)))))))..))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....((((((.((..(((((((....))))))).(((....))))).))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).))......................................((((((.((((..((.....)).))))))))))..
EEEEEEEEEEEEEEEEEEEEEEEEEEEESSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSIIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSISSMMSSSSSSSHHHHSSSSSSSMSSSHHHHSSSSSISSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSBSSSSIISSHHHHHSSISSSSSSSSSSEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 29..31 "UGU" 73..75 "ACA"
S2 34..34 "U" 72..72 "U"
S3 36..38 "GCU" 64..66 "GGC"
S4 41..48 "UUGGUGAG" 56..63 "CUCACCAA"
S5 78..81 "GCCG" 97..100 "CGGC"
S6 82..85 "ACCU" 90..93 "GGGU"
S7 105..110 "CUGGGA" 121..126 "GCCCAG"
S8 128..131 "CUCC" 136..139 "GGAG"
S9 156..156 "U" 182..182 "A"
S10 158..160 "CGG" 179..181 "CCG"
S11 164..168 "UGGGG" 173..177 "CCCUG"
S12 195..198 "GAGU" 222..225 "GCUC"
S13 205..208 "GGUU" 213..216 "GAUC"
S14 227..227 "G" 286..286 "G"
S15 230..235 "GUAAGA" 278..283 "UCUUAC"
S16 239..239 "G" 273..273 "C"
S17 245..252 "UGAGAAGA" 260..267 "GUUUCUCA"
S18 290..294 "GGGAC" 331..335 "GUCCC"
S19 301..305 "CUACG" 326..330 "CGUAG"
S20 311..312 "GC" 317..318 "GC"
S21 357..359 "GGC" 671..673 "GCC"
S22 367..370 "GAGC" 550..553 "GCUC"
S23 372..372 "C" 547..547 "G"
S24 374..376 "GGC" 544..546 "GCU"
S25 378..384 "GUUUGAU" 434..440 "GUCAAAC"
S26 386..387 "AG" 432..433 "CU"
S27 388..395 "UCUGAAGU" 422..429 "GCUUUGGA"
S28 401..407 "GCUGUGG" 413..419 "CCAUAGU"
S29 444..446 "AGU" 538..540 "ACU"
S30 447..452 "GCAGAA" 531..536 "GUCUGU"
S31 455..461 "GGAGAGU" 523..529 "GCUCUCU"
S32 462..463 "GG" 505..506 "AC"
S33 466..473 "UUCCAUGU" 495..502 "AUAUGGAG"
S34 477..479 "GCG" 486..488 "UGC"
S35 511..511 "G" 522..522 "G"
S36 514..515 "GC" 520..521 "GC"
S37 558..563 "GCGUGG" 594..599 "CCACGC"
S38 567..568 "GC" 592..593 "GU"
S39 572..575 "CAGG" 585..588 "CCUG"
S40 610..615 "GAGUGC" 664..669 "GCACUC"
S41 618..629 "GGUGUUGGGUCC" 636..647 "GGACUCAGUGCC"
S42 651..652 "GC" 657..658 "GC"
S43 675..677 "GGG" 700..702 "CUC"
S44 684..687 "GACC" 692..695 "GGUU"
S45 711..712 "UG" 1184..1185 "CA"
S46 713..715 "ACG" 1180..1182 "UGU"
S47 717..723 "GGGCCCG" 1173..1179 "CGGGCCU"
S48 725..726 "AC" 1168..1169 "GU"
S49 729..733 "GCGGU" 1129..1133 "AUCGC"
S50 734..743 "GGAGCAUGUG" 1017..1026 "CACGUGCUAC"
S51 744..745 "GU" 1014..1015 "AC"
S52 750..753 "UUCG" 762..765 "CGAA"
S53 774..780 "CCAGGUC" 1004..1010 "GACCUGG"
S54 786..788 "AUC" 833..835 "GAC"
S55 789..791 "CCG" 829..831 "CGG"
S56 793..794 "UG" 826..827 "CA"
S57 797..803 "CGCCCUA" 808..814 "UAGGGUU"
S58 816..818 "CUC" 823..825 "GAG"
S59 837..838 "GG" 998..999 "CC"
S60 840..843 "GGUG" 994..997 "UGCC"
S61 846..847 "UG" 992..993 "CA"
S62 848..850 "GUU" 986..988 "AAU"
S63 851..854 "GUCG" 981..984 "CGUC"
S64 858..863 "GCUCGU" 892..897 "ACGAGC"
S65 864..866 "GUC" 871..873 "GAU"
S66 876..879 "UGGG" 886..889 "CCCG"
S67 903..906 "CCCU" 973..976 "GGGG"
S68 908..914 "UUGUUAG" 938..944 "CUAGCGA"
S69 918..919 "CC" 932..933 "GG"
S70 922..923 "CA" 930..931 "UG"
S71 947..954 "CUGCCGGU" 960..967 "ACCGGAGG"
S72 1030..1036 "GGCUGGU" 1079..1085 "GCCAGUC"
S73 1042..1044 "GAG" 1071..1073 "CUC"
S74 1047..1048 "GC" 1065..1066 "GC"
S75 1052..1054 "UCG" 1059..1061 "CGG"
S76 1092..1092 "C" 1123..1123 "G"
S77 1096..1103 "UUGUAGGC" 1112..1119 "GCCUACAU"
S78 1139..1146 "AUCGCGGA" 1154..1161 "GCCGCGGU"
S79 1224..1229 "GUCGGU" 1251..1256 "GCCGCC"
S80 1231..1234 "AGGU" 1247..1250 "GCCA"
S81 1237..1238 "CC" 1244..1245 "GG"
H1 49..55 "GUAACGG" (48,56) G:C 
H2 86..89 "GAGA" (85,90) U:G 
H3 111..120 "CUGAGACACG" (110,121) A:G 
H4 132..135 "UACG" (131,136) C:G 
H5 169..172 "GCAA" (168,173) G:C 
H6 209..212 "UUCG" (208,213) U:G 
H7 253..259 "GUGGAAA" (252,260) A:G 
H8 313..316 "AGCC" (312,317) C:G PK{1}
H9 408..412 "CUUAA" (407,413) G:C 
H10 480..485 "GUGAAA" (479,486) G:U 
H11 516..519 "GAAA" (515,520) C:G 
H12 576..584 "AUUAGAUAC" (575,585) G:C 
H13 630..635 "UUUCCG" (629,636) C:G 
H14 653..656 "UAAC" (652,657) C:G PK{2}
H15 688..691 "GCAA" (687,692) C:G 
H16 754..761 "AAGCAACG" (753,762) G:C 
H17 804..807 "GAGA" (803,808) A:U 
H18 819..822 "UUCG" (818,823) C:G 
H19 867..870 "GUGA" (866,871) C:G 
H20 880..885 "UUAAGU" (879,886) G:C 
H21 924..929 "UUCAGU" (923,930) A:U 
H22 955..959 "AAUAA" (954,960) U:A 
H23 1055..1058 "GUGA" (1054,1059) G:C 
H24 1104..1111 "UGCAACUC" (1103,1112) C:G 
H25 1147..1153 "UCAGCAC" (1146,1154) A:G 
H26 1239..1243 "UUUUA" (1238,1244) C:G 
B1 32..33 "AU" (31,73) U:A (72,34) U:U 
B2 39..40 "AG" (38,64) U:G (63,41) A:U 
B3 94..96 "GAU" (93,82) U:A (81,97) G:C 
B4 157..157 "U" (156,182) U:A (181,158) G:C 
B5 295..300 "GGCUAA" (294,331) C:G (330,301) G:C PK{1}
B6 373..373 "A" (372,547) C:G (546,374) U:G 
B7 385..385 "A" (384,434) U:G (433,386) U:A 
B8 430..431 "AA" (429,388) A:U (387,432) G:C 
B9 512..513 "UG" (511,522) G:G (521,514) C:G 
B10 537..537 "A" (536,447) U:G (446,538) U:A 
B11 564..566 "GGA" (563,594) G:C (593,567) U:G 
B12 716..716 "G" (715,1180) G:U (1179,717) U:G 
B13 832..832 "U" (831,789) G:C (788,833) C:G 
B14 839..839 "U" (838,998) G:C (997,840) C:G 
B15 844..845 "CA" (843,994) G:U (993,846) A:U 
B16 920..921 "AU" (919,932) C:G (931,922) G:C 
B17 985..985 "A" (984,851) C:G (850,986) U:A 
B18 989..991 "CAU" (988,848) U:G (847,992) G:C 
B19 1016..1016 "A" (1015,744) C:G (743,1017) G:C 
B20 1183..1183 "A" (1182,713) U:A (712,1184) G:C 
B21 1230..1230 "G" (1229,1251) U:G (1250,1231) A:A 
I1.1 35..35 "A" (34,72) U:U 
I1.2 67..71 "UUCGA" (66,36) C:G 
I2.1 161..163 "CAA" (160,179) G:C 
I2.2 178..178 "A" (177,164) G:U 
I3.1 199..204 "GAAGAA" (198,222) U:G 
I3.2 217..221 "GUAAA" (216,205) C:G 
I4.1 228..229 "UU" (227,286) G:G 
I4.2 284..285 "CA" (283,230) C:G 
I5.1 236..238 "GAA" (235,278) A:U 
I5.2 274..277 "GGUA" (273,239) C:G 
I6.1 240..244 "AACUG" (239,273) G:C 
I6.2 268..272 "CUUGA" (267,245) A:U 
I7.1 306..310 "UGCCA" (305,326) G:C 
I7.2 319..325 "GGUAAUA" (318,311) C:G 
I8.1 371..371 "G" (370,550) C:G 
I8.2 548..549 "AG" (547,372) G:C 
I9.1 396..400 "AAAAG" (395,422) U:G 
I9.2 420..421 "AC" (419,401) U:G 
I10.1 453..454 "GG" (452,531) A:G 
I10.2 530..530 "G" (529,455) U:G 
I11.1 464..465 "AA" (463,505) G:A 
I11.2 503..504 "GA" (502,466) G:U 
I12.1 474..476 "GUA" (473,495) U:A 
I12.2 489..494 "GUAGAU" (488,477) C:G 
I13.1 569..571 "GAA" (568,592) C:G 
I13.2 589..591 "GUA" (588,572) G:C 
I14.1 678..683 "GAGUAC" (677,700) G:C 
I14.2 696..699 "GAAA" (695,684) U:G 
I15.1 724..724 "C" (723,1173) G:C 
I15.2 1170..1172 "UCC" (1169,725) U:A 
I16.1 792..792 "A" (791,829) G:C 
I16.2 828..828 "U" (827,793) A:U 
I17.1 915..917 "UUG" (914,938) G:C 
I17.2 934..937 "CACU" (933,918) G:C 
I18.1 1037..1041 "ACAAC" (1036,1079) U:G 
I18.2 1074..1078 "UUAAA" (1073,1042) C:G 
I19.1 1045..1046 "UC" (1044,1071) G:C 
I19.2 1067..1070 "UAAU" (1066,1047) C:G 
I20.1 1049..1051 "AAG" (1048,1065) C:G 
I20.2 1062..1064 "CAA" (1061,1052) G:U 
I21.1 1093..1095 "GGA" (1092,1123) C:G 
I21.2 1120..1122 "GAA" (1119,1096) U:U 
I22.1 1235..1236 "AA" (1234,1247) U:G 
I22.2 1246..1246 "A" (1245,1237) G:C 
M1.1 360..366 "GUAAAGC" (359,671) C:G (553,367) C:G PK{2}
M1.2 554..557 "GAAA" (553,367) C:G (599,558) C:G 
M1.3 600..609 "CGUAAACGAU" (599,558) C:G (669,610) C:G 
M1.4 670..670 "C" (669,610) C:G (359,671) C:G 
M2.1 377..377 "G" (376,544) C:G (440,378) C:G 
M2.2 441..443 "UUG" (440,378) C:G (540,444) U:A 
M2.3 541..543 "GAC" (540,444) U:A (376,544) C:G 
M3.1 462..461 "" (461,523) U:G (506,462) C:G 
M3.2 507..510 "ACCG" (506,462) C:G (522,511) G:G 
M3.3 523..522 "" (522,511) G:G (461,523) U:G 
M4.1 616..617 "UA" (615,664) C:G (647,618) C:G 
M4.2 648..650 "GCA" (647,618) C:G (658,651) C:G 
M4.3 659..663 "AUUAA" (658,651) C:G (615,664) C:G 
M5.1 727..728 "AA" (726,1168) C:G (1133,729) C:G 
M5.2 1134..1138 "UAGUA" (1133,729) C:G (1161,1139) U:A 
M5.3 1162..1167 "GAAUAC" (1161,1139) U:A (726,1168) C:G 
M6.1 734..733 "" (733,1129) U:A (1026,734) C:G 
M6.2 1027..1029 "AAU" (1026,734) C:G (1085,1030) C:G 
M6.3 1086..1091 "UCAGUU" (1085,1030) C:G (1123,1092) G:C 
M6.4 1124..1128 "UCGGA" (1123,1092) G:C (733,1129) U:A 
M7.1 746..749 "UUAA" (745,1014) U:A (765,750) A:U 
M7.2 766..773 "GAACCUUA" (765,750) A:U (1010,774) G:C 
M7.3 1011..1013 "GCU" (1010,774) G:C (745,1014) U:A 
M8.1 781..785 "UUGAC" (780,1004) C:G (835,786) C:A 
M8.2 836..836 "A" (835,786) C:A (999,837) C:G 
M8.3 1000..1003 "UUAU" (999,837) C:G (780,1004) C:G 
M9.1 795..796 "AC" (794,826) G:C (814,797) U:C 
M9.2 815..815 "U" (814,797) U:C (825,816) G:C 
M9.3 826..825 "" (825,816) G:C (794,826) G:C 
M10.1 855..857 "UCA" (854,981) G:C (897,858) C:G 
M10.2 898..902 "GCAAC" (897,858) C:G (976,903) G:C 
M10.3 977..980 "AUGA" (976,903) G:C (854,981) G:C 
M11.1 864..863 "" (863,892) U:A (873,864) U:G 
M11.2 874..875 "GU" (873,864) U:G (889,876) G:U 
M11.3 890..891 "CA" (889,876) G:U (863,892) U:A 
M12.1 907..907 "A" (906,973) U:G (944,908) A:U 
M12.2 945..946 "GA" (944,908) A:U (967,947) G:C 
M12.3 968..972 "AAGGU" (967,947) G:C (906,973) U:G 
X1 76..77 "UA" (75,29) A:U (100,78) C:G 
X2 101..104 "CACA" (100,78) C:G (126,105) G:C 
X4 140..155 "GCAGCAGUAGGGAAUC" (139,128) G:C (182,156) A:U 
X5 183..194 "GCAACGCCGCGU" (182,156) A:U (225,195) C:G 
X7 287..289 "AAA" (286,227) G:G (335,290) C:G 
X8 336..356 "GAGCGUUGUCCGGAUUUAUUG" (335,290) C:G (673,357) C:G 
X10 703..710 "AAAGGAAU" (702,675) C:G (1185,711) A:U 
X11 1186..1223 "CACCGCCCGUCACACCACGAGAGUUUGUAACACCCGAA" (1185,711) A:U (1256,1224) C:G 
E1 1..28 "AUUGCUUCACUAUGAGAUGGACCUGCGU" 
E2 1257..1258 "UA" 
PK1 3bp 295..297 314..316 B5 295..300 H8 313..316
PK2 2bp 360..361 655..656 M1.1 360..366 H14 653..656
PK1.1 295 G 316 C
PK1.2 296 G 315 C
PK1.3 297 C 314 G
PK2.1 360 G 656 C
PK2.2 361 U 655 A
NCBP1 463 G 505 A S32
NCBP2 452 A 531 G S30
NCBP3 110 A 121 G S7
NCBP4 227 G 286 G S14
NCBP5 797 C 814 U S57
NCBP6 34 U 72 U S2
NCBP7 1146 A 1154 G S78
NCBP8 511 G 522 G S35
NCBP9 786 A 835 C S54
NCBP10 1096 U 1119 U S77
NCBP11 1231 A 1250 A S80
NCBP12 735 G 1025 A S50
NCBP13 852 U 983 U S63
NCBP14 1225 U 1255 C S79
NCBP15 949 G 965 A S71
NCBP16 252 A 260 G S17
segment1 15bp 29..48 UGUAUUAGCUAGUUGGUGAG 56..75 CUCACCAAGGCUUCGAUACA
segment2 8bp 78..85 GCCGACCU 90..100 GGGUGAUCGGC
segment3 6bp 105..110 CUGGGA 121..126 GCCCAG
segment4 4bp 128..131 CUCC 136..139 GGAG
segment5 9bp 156..168 UUCGGCAAUGGGG 173..182 CCCUGACCGA
segment6 8bp 195..208 GAGUGAAGAAGGUU 213..225 GAUCGUAAAGCUC
segment7 16bp 227..252 GUUGUAAGAGAAGAACUGUGAGAAGA 260..286 GUUUCUCACUUGACGGUAUCUUACCAG
segment8 12bp 290..312 GGGACGGCUAACUACGUGCCAGC 317..335 GCGGUAAUACGUAGGUCCC
segment9 3bp 357..359 GGC 671..673 GCC
segment10 8bp 367..376 GAGCGCAGGC 544..553 GCUGAGGCUC
segment11 24bp 378..407 GUUUGAUAAGUCUGAAGUAAAAGGCUGUGG 413..440 CCAUAGUACGCUUUGGAAACUGUCAAAC
segment12 16bp 444..461 AGUGCAGAAGGGGAGAGU 523..540 GCUCUCUGGUCUGUAACU
segment13 13bp 462..479 GGAAUUCCAUGUGUAGCG 486..506 UGCGUAGAUAUAUGGAGGAAC
segment14 3bp 511..515 GUGGC 520..522 GCG
segment15 12bp 558..575 GCGUGGGGAGCGAACAGG 585..599 CCUGGUAGUCCACGC
segment16 6bp 610..615 GAGUGC 664..669 GCACUC
segment17 12bp 618..629 GGUGUUGGGUCC 636..647 GGACUCAGUGCC
segment18 2bp 651..652 GC 657..658 GC
segment19 7bp 675..687 GGGGAGUACGACC 692..702 GGUUGAAACUC
segment20 14bp 711..726 UGACGGGGGCCCGCAC 1168..1185 GUUCCCGGGCCUUGUACA
segment21 5bp 729..733 GCGGU 1129..1133 AUCGC
segment22 12bp 734..745 GGAGCAUGUGGU 1014..1026 ACACACGUGCUAC
segment23 4bp 750..753 UUCG 762..765 CGAA
segment24 7bp 774..780 CCAGGUC 1004..1010 GACCUGG
segment25 8bp 786..794 AUCCCGAUG 826..835 CAUCGGUGAC
segment26 7bp 797..803 CGCCCUA 808..814 UAGGGUU
segment27 3bp 816..818 CUC 823..825 GAG
segment28 15bp 837..854 GGUGGUGCAUGGUUGUCG 981..999 CGUCAAAUCAUCAUGCCCC
segment29 6bp 858..863 GCUCGU 892..897 ACGAGC
segment30 3bp 864..866 GUC 871..873 GAU
segment31 4bp 876..879 UGGG 886..889 CCCG
segment32 4bp 903..906 CCCU 973..976 GGGG
segment33 11bp 908..923 UUGUUAGUUGCCAUCA 930..944 UGGGCACUCUAGCGA
segment34 8bp 947..954 CUGCCGGU 960..967 ACCGGAGG
segment35 15bp 1030..1054 GGCUGGUACAACGAGUCGCAAGUCG 1059..1085 CGGCAAGCUAAUCUCUUAAAGCCAGUC
segment36 9bp 1092..1103 CGGAUUGUAGGC 1112..1123 GCCUACAUGAAG
segment37 8bp 1139..1146 AUCGCGGA 1154..1161 GCCGCGGU
segment38 12bp 1224..1238 GUCGGUGAGGUAACC 1244..1256 GGAGCCAGCCGCC
