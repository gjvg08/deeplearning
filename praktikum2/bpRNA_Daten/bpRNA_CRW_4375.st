#Name: bpRNA_CRW_4375
#Length:  1181 
#PageNumber: 1
UCACGGAGAGUUUGAUCCUGGCUCAGGACGAACGCUGGCGGCGUGCUUAACACAUGCAAGUCGAACGAUGAAGCCCUUCGGGGUGGAUUAGUGGCAACGGGUGAGUAACACGUGGGCAAUCUGCCCUUCACUCUGGGACAAGCCCUGGAAACGGGGUCUAAUACCGGAUACGACCUGCCGAGGCAUCUCGGUGGGUGGAAAGCUCCGGCGGUGAAGGAUGAGCCCGCGGCCUAUCAGCUUGUUGGUGGGGUAAUGGCCUACCAAGGCGACGACGGGUAGCCGGCCUAGAGGGCGACCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGAAAGCCUGAUGCAGCGCGCCGCGUGAGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCAGCAGGGAAGAAGCGAGNGUGACGGUACCUGCAGAGAAGCGCCGGCUACGUAGGCGGCUUGUNNGUCGGAUGUGAAAGCCNGGGGCCUAACCCCGGGUCUGCAUUCGAUACGGGCAGGCUAGAGUGUGGUAGGGGAGAUCGGAAUUCCUGGUGUAGCGGUGAAUGCGCAGAUAUCAGGAGGAACACCGGUGGCGAAGGCGGAUCUCUGGGCCAUGACUGACGCUGAGGAGCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCUGGUAGUCCACGCCGUAAACGUUGGGAACUAGGUGUUGGCGACAUUCCACGUNGUCGGUGCCGCAGCUAACGCAUUAAGUUCCCCGCCUGGGGAGUACCCGCAAGGCCGAAGAACCUUACCAAGGCUUGAAUAUACCGGAAACGGCCAGAGAUGGUCGCCCCCUUGUGGUCGGUAUACAGGUGGUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGCCCGCAACGAGCGCAACCCUUGUUCUGUGUUGCCAGCAUGCUUCGGGGUNAUGGGGACUCACAGGAGACUGCCGGGGUCAACUCGGAGGAAGGAGUCAUCAUGCCCCUUAUGUCUUGGGCUGCACACGUGCUACAAUGGCCGGUACAAUGAGCUGCGAUGCCGUGAGGUGGAGCGAAUCUCAAAAAGCCGGUUCAGUUCGGAUUGGGGUCUGCAACUCGACCCCAUGAAGUUGGAGUUGCUAGUAAUCGCAGAUCAGCAUUGC
.......(((((.......))))).............(((((((((....(((.(((..(((..(((((...((((....))))..)).)))))).....(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....((((((((((....)))))))))).....((....)).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((...))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).))))))))..((((......((((....)))).....))))...(((((((...(............)....))))))..)............(.(.(((.(((((((.((((((((((....(((((((.....)))))))..)))))))))..).)))))))...(((((((((..(((((((((..((((((((...(((.....)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..).......((((((...((...(((..........)))...))))))))..........((((((..((((((((((((.......))))))))))))...((....)).....)))))).................(....)............(((((((....(((((((((..(((((((....))))))).((.....))))))))))).((.((((..(((((.......(((((((((....)))..((((.....))))..))))))..........(((((((...((..(((...........)))))....)))))))..((((((((.....))))))))....)))...))))))))....)))))))....................((((((...(((((..((...(((....)))...))....)))))...))))))......(...((((((((........))))))))...).....................((........))
EEEEEEESSSSSHHHHHHHSSSSSXXXXXXXXXXXXXSSSSSSSSSIIIISSSBSSSMMSSSBBSSSSSIIISSSSHHHHSSSSIISSBSSSSSSMMMMMSSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHSSSSSSSSSSMMMMMSSHHHHSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSSSSSSSXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHHHHHHSIIIISSSSSSBBSXXXXXXXXXXXXSISBSSSMSSSSSSSISSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSISSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISXXXXXXXSSSSSSBBBSSIIISSSHHHHHHHHHHSSSIIISSSSSSSSXXXXXXXXXXSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSXXXXXXXXXXXXXXXXXSHHHHSXXXXXXXXXXXXSSSSSSSMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSHHHHHSSSSSSSSSSSMSSBSSSSBBSSSSSMMMMMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHSSSSMMSSSSSSMMMMMMMMMMSSSSSSSIIISSBBSSSHHHHHHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMSSSBBBSSSSSSSSMMMMSSSSSSSXXXXXXXXXXXXXXXXXXXXSSSSSSIIISSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSIIISSSSSSXXXXXXSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISXXXXXXXXXXXXXXXXXXXXXSSHHHHHHHHSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 8..12 "GAGUU" 20..24 "GGCUC"
S2 38..45 "GCGGCGUG" 384..391 "CGCGCCGC"
S3 46..46 "C" 350..350 "G"
S4 51..53 "CAC" 346..348 "GUG"
S5 55..57 "UGC" 343..345 "GCA"
S6 60..62 "GUC" 93..95 "GGC"
S7 65..67 "ACG" 90..92 "AGU"
S8 68..69 "AU" 87..88 "AU"
S9 73..76 "GCCC" 81..84 "GGGU"
S10 101..103 "GUG" 301..303 "CAC"
S11 110..117 "ACGUGGGC" 222..229 "GCCCGCGG"
S12 120..121 "UC" 220..221 "GA"
S13 125..131 "CCUUCAC" 211..217 "GUGAAGG"
S14 133..136 "CUGG" 164..167 "CCGG"
S15 141..147 "AGCCCUG" 152..158 "CGGGGUC"
S16 173..182 "ACCUGCCGAG" 187..196 "CUCGGUGGGU"
S17 202..203 "GC" 208..209 "GC"
S18 230..232 "CCU" 274..276 "GGG"
S19 235..235 "C" 273..273 "C"
S20 237..239 "GCU" 265..267 "GGC"
S21 242..249 "UUGGUGGG" 257..264 "CCUACCAA"
S22 279..282 "GCCG" 297..300 "CGGC"
S23 283..286 "GCCU" 290..293 "GGGC"
S24 305..310 "CUGGGA" 321..326 "GCCCAG"
S25 328..331 "CUCC" 336..339 "GGAG"
S26 356..356 "U" 382..382 "A"
S27 358..360 "GCA" 379..381 "UGC"
S28 364..368 "UGGGC" 373..377 "GCCUG"
S29 394..397 "GAGG" 421..424 "CCUC"
S30 404..407 "GGCC" 412..415 "GGUU"
S31 428..428 "C" 464..464 "G"
S32 429..434 "AGCAGG" 456..461 "CCUGCA"
S33 438..438 "G" 451..451 "C"
S34 477..477 "C" 656..656 "G"
S35 479..479 "U" 653..653 "G"
S36 481..483 "GGC" 650..652 "GCU"
S37 485..491 "GCUUGUN" 541..547 "GGCAGGC"
S38 493..493 "G" 539..539 "C"
S39 494..502 "UCGGAUGUG" 528..536 "UGCAUUCGA"
S40 507..513 "CCNGGGG" 519..525 "CCCCGGG"
S41 551..553 "AGU" 644..646 "ACU"
S42 554..559 "GUGGUA" 637..642 "GGCCAU"
S43 562..568 "GGAGAUC" 629..635 "GAUCUCU"
S44 569..570 "GG" 611..612 "AC"
S45 573..580 "UUCCUGGU" 601..608 "AUCAGGAG"
S46 584..586 "GCG" 592..594 "UGC"
S47 617..617 "G" 628..628 "G"
S48 620..621 "GC" 626..627 "GC"
S49 664..669 "GCGUGG" 699..704 "CCACGC"
S50 673..674 "GC" 697..698 "GU"
S51 678..680 "CAG" 691..693 "CUG"
S52 715..720 "GGGAAC" 770..775 "GUUCCC"
S53 723..734 "GGUGUUGGCGAC" 742..753 "GUNGUCGGUGCC"
S54 757..758 "GC" 763..764 "GC"
S55 793..793 "C" 798..798 "C"
S56 811..817 "CCAAGGC" 1030..1036 "GUCUUGG"
S57 822..830 "AUAUACCGG" 861..869 "UCGGUAUAC"
S58 833..839 "ACGGCCA" 844..850 "UGGUCGC"
S59 852..853 "CC" 859..860 "GG"
S60 871..872 "GG" 1024..1025 "CC"
S61 874..877 "GGUG" 1020..1023 "UGCC"
S62 880..881 "UG" 1018..1019 "CA"
S63 882..884 "GCU" 1012..1014 "AGU"
S64 892..897 "GCUCGU" 925..930 "ACGAGC"
S65 898..900 "GUC" 905..907 "GAU"
S66 910..913 "UGGG" 919..922 "CCCG"
S67 941..947 "UUCUGUG" 978..984 "CACAGGA"
S68 951..952 "CC" 972..973 "GG"
S69 955..957 "CAU" 969..971 "AUG"
S70 987..994 "CUGCCGGG" 1000..1007 "CUCGGAGG"
S71 1057..1062 "GCCGGU" 1105..1110 "GCCGGU"
S72 1066..1070 "AUGAG" 1097..1101 "CUCAA"
S73 1073..1074 "GC" 1091..1092 "GC"
S74 1078..1080 "GCC" 1085..1087 "GGU"
S75 1117..1117 "C" 1148..1148 "G"
S76 1121..1128 "UUGGGGUC" 1137..1144 "GACCCCAU"
S77 1170..1171 "GA" 1180..1181 "GC"
H1 13..19 "UGAUCCU" (12,20) U:G 
H2 77..80 "UUCG" (76,81) C:G 
H3 148..151 "GAAA" (147,152) G:C 
H4 183..186 "GCAU" (182,187) G:C 
H5 204..207 "UCCG" (203,208) C:G 
H6 250..256 "GUAAUGG" (249,257) G:C 
H7 287..289 "AGA" (286,290) U:G 
H8 311..320 "CUGAGACACG" (310,321) A:G 
H9 332..335 "UACG" (331,336) C:G 
H10 369..372 "GAAA" (368,373) C:G 
H11 408..411 "UUCG" (407,412) C:G 
H12 439..450 "AAGCGAGNGUGA" (438,451) G:C 
H13 514..518 "CCUAA" (513,519) G:C 
H14 587..591 "GUGAA" (586,592) G:U 
H15 622..625 "GAAG" (621,626) C:G 
H16 681..690 "GAUUAGAUAC" (680,691) G:C 
H17 735..741 "AUUCCAC" (734,742) C:G 
H18 759..762 "UAAC" (758,763) C:G 
H19 794..797 "AAGG" (793,798) C:C 
H20 840..843 "GAGA" (839,844) A:U 
H21 854..858 "CUUGU" (853,859) C:G 
H22 901..904 "GUGA" (900,905) C:G 
H23 914..918 "UUAAG" (913,919) G:C 
H24 958..968 "GCUUCGGGGUN" (957,969) U:A 
H25 995..999 "GUCAA" (994,1000) G:C 
H26 1081..1084 "GUGA" (1080,1085) C:G 
H27 1129..1136 "UGCAACUC" (1128,1137) C:G 
H28 1172..1179 "UCAGCAUU" (1171,1180) A:G 
B1 54..54 "A" (53,346) C:G (345,55) A:U 
B2 63..64 "GA" (62,93) C:G (92,65) U:A 
B3 89..89 "U" (88,68) U:A (67,90) G:A 
B4 118..119 "AA" (117,222) C:G (221,120) A:U 
B5 233..234 "AU" (232,274) U:G (273,235) C:C 
B6 240..241 "UG" (239,265) U:G (264,242) A:U 
B7 294..296 "GAC" (293,283) C:G (282,297) G:C 
B8 357..357 "U" (356,382) U:A (381,358) C:G 
B9 462..463 "GA" (461,429) A:A (428,464) C:G 
B10 480..480 "A" (479,653) U:G (652,481) U:G 
B11 537..538 "UA" (536,494) A:U (493,539) G:C 
B12 618..619 "UG" (617,628) G:G (627,620) C:G 
B13 643..643 "G" (642,554) U:G (553,644) U:A 
B14 670..672 "GGA" (669,699) G:C (698,673) U:G 
B15 873..873 "U" (872,1024) G:C (1023,874) C:G 
B16 878..879 "CA" (877,1020) G:U (1019,880) A:U 
B17 953..954 "AG" (952,972) C:G (971,955) G:C 
B18 1015..1017 "CAU" (1014,882) U:G (881,1018) G:C 
I1.1 47..50 "UUAA" (46,350) C:G 
I1.2 349..349 "G" (348,51) G:C 
I2.1 70..72 "GAA" (69,87) U:A 
I2.2 85..86 "GG" (84,73) U:G 
I3.1 122..124 "UGC" (121,220) C:G 
I3.2 218..219 "AU" (217,125) G:C 
I4.1 137..140 "GACA" (136,164) G:C 
I4.2 159..163 "UAAUA" (158,141) C:A 
I5.1 236..236 "A" (235,273) C:C 
I5.2 268..272 "GACGA" (267,237) C:G 
I6.1 361..363 "CAA" (360,379) A:U 
I6.2 378..378 "A" (377,364) G:U 
I7.1 398..403 "GAUGAC" (397,421) G:C 
I7.2 416..420 "GUAAA" (415,404) U:G 
I8.1 435..437 "GAA" (434,456) G:C 
I8.2 452..455 "GGUA" (451,438) C:G 
I9.1 478..478 "G" (477,656) C:G 
I9.2 654..655 "AG" (653,479) G:U 
I10.1 492..492 "N" (491,541) N:G 
I10.2 540..540 "G" (539,493) C:G 
I11.1 503..506 "AAAG" (502,528) G:U 
I11.2 526..527 "UC" (525,507) G:C 
I12.1 560..561 "GG" (559,637) A:G 
I12.2 636..636 "G" (635,562) U:G 
I13.1 571..572 "AA" (570,611) G:A 
I13.2 609..610 "GA" (608,573) G:U 
I14.1 581..583 "GUA" (580,601) U:A 
I14.2 595..600 "GCAGAU" (594,584) C:G 
I15.1 675..677 "GAA" (674,697) C:G 
I15.2 694..696 "GUA" (693,678) G:C 
I16.1 948..950 "UUG" (947,978) G:C 
I16.2 974..977 "GACU" (973,951) G:C 
I17.1 1063..1065 "ACA" (1062,1105) U:G 
I17.2 1102..1104 "AAA" (1101,1066) A:A 
I18.1 1071..1072 "CU" (1070,1097) G:C 
I18.2 1093..1096 "GAAU" (1092,1073) C:G 
I19.1 1075..1077 "GAU" (1074,1091) C:G 
I19.2 1088..1090 "GGA" (1087,1078) U:G 
I20.1 1118..1120 "GGA" (1117,1148) C:G 
I20.2 1145..1147 "GAA" (1144,1121) U:U 
M1.1 46..45 "" (45,384) G:C (350,46) G:C 
M1.2 351..355 "GAAUA" (350,46) G:C (382,356) A:U 
M1.3 383..383 "G" (382,356) A:U (45,384) G:C 
M2.1 58..59 "AA" (57,343) C:G (95,60) C:G 
M2.2 96..100 "AACGG" (95,60) C:G (303,101) C:G 
M2.3 304..304 "A" (303,101) C:G (326,305) G:C 
M2.4 327..327 "A" (326,305) G:C (339,328) G:C 
M2.5 340..342 "GCA" (339,328) G:C (57,343) C:G 
M3.1 104..109 "AGUAAC" (103,301) G:C (229,110) G:A 
M3.2 230..229 "" (229,110) G:A (276,230) G:C 
M3.3 277..278 "UA" (276,230) G:C (300,279) C:G 
M3.4 301..300 "" (300,279) C:G (103,301) G:C 
M4.1 132..132 "U" (131,211) C:G (167,133) G:C 
M4.2 168..172 "AUACG" (167,133) G:C (196,173) U:A 
M4.3 197..201 "GGAAA" (196,173) U:A (209,202) C:G 
M4.4 210..210 "G" (209,202) C:G (131,211) C:G 
M5.1 484..484 "G" (483,650) C:G (547,485) C:G 
M5.2 548..550 "UAG" (547,485) C:G (646,551) U:A 
M5.3 647..649 "GAC" (646,551) U:A (483,650) C:G 
M6.1 569..568 "" (568,629) C:G (612,569) C:G 
M6.2 613..616 "ACCG" (612,569) C:G (628,617) G:G 
M6.3 629..628 "" (628,617) G:G (568,629) C:G 
M7.1 721..722 "UA" (720,770) C:G (753,723) C:G 
M7.2 754..756 "GCA" (753,723) C:G (764,757) C:G 
M7.3 765..769 "AUUAA" (764,757) C:G (720,770) C:G 
M8.1 818..821 "UUGA" (817,1030) C:G (869,822) C:A 
M8.2 870..870 "A" (869,822) C:A (1025,871) C:G 
M8.3 1026..1029 "UUAU" (1025,871) C:G (817,1030) C:G 
M9.1 831..832 "AA" (830,861) G:U (850,833) C:A 
M9.2 851..851 "C" (850,833) C:A (860,852) G:C 
M9.3 861..860 "" (860,852) G:C (830,861) G:U 
M10.1 885..891 "GUCGUCA" (884,1012) U:A (930,892) C:G 
M10.2 931..940 "GCAACCCUUG" (930,892) C:G (984,941) A:U 
M10.3 985..986 "GA" (984,941) A:U (1007,987) G:C 
M10.4 1008..1011 "AAGG" (1007,987) G:C (884,1012) U:A 
M11.1 898..897 "" (897,925) U:A (907,898) U:G 
M11.2 908..909 "GU" (907,898) U:G (922,910) G:U 
M11.3 923..924 "CA" (922,910) G:U (897,925) U:A 
X1 25..37 "AGGACGAACGCUG" (24,8) C:G (391,38) C:G 
X2 392..393 "GU" (391,38) C:G (424,394) C:G 
X3 425..427 "UUU" (424,394) C:G (464,428) G:C 
X4 465..476 "AAGCGCCGGCUA" (464,428) G:C (656,477) G:C 
X5 657..663 "AGCGAAA" (656,477) G:C (704,664) C:G 
X6 705..714 "CGUAAACGUU" (704,664) C:G (775,715) C:G 
X7 776..792 "CGCCUGGGGAGUACCCG" (775,715) C:G (798,793) C:C 
X8 799..810 "CGAAGAACCUUA" (798,793) C:C (1036,811) G:C 
X9 1037..1056 "GCUGCACACGUGCUACAAUG" (1036,811) G:C (1110,1057) U:G 
X10 1111..1116 "UCAGUU" (1110,1057) U:G (1148,1117) G:C 
X11 1149..1169 "UUGGAGUUGCUAGUAAUCGCA" (1148,1117) G:C (1181,1170) C:G 
E1 1..7 "UCACGGA" 
NCBP1 559 A 637 G S42
NCBP2 833 A 850 C S58
NCBP3 1171 A 1180 G S77
NCBP4 509 N 523 G S40
NCBP5 235 C 273 C S19
NCBP6 110 A 229 G S11
NCBP7 617 G 628 G S47
NCBP8 310 A 321 G S24
NCBP9 429 A 461 A S32
NCBP10 67 G 90 A S7
NCBP11 732 G 744 N S53
NCBP12 1121 U 1144 U S76
NCBP13 822 A 869 C S57
NCBP14 1066 A 1101 A S72
NCBP15 989 G 1005 A S70
NCBP16 793 C 798 C S55
NCBP17 141 A 158 C S15
NCBP18 570 G 611 A S44
NCBP19 491 N 541 G S37
segment1 5bp 8..12 GAGUU 20..24 GGCUC
segment2 8bp 38..45 GCGGCGUG 384..391 CGCGCCGC
segment3 7bp 46..57 CUUAACACAUGC 343..350 GCAGUGGG
segment4 12bp 60..76 GUCGAACGAUGAAGCCC 81..95 GGGUGGAUUAGUGGC
segment5 3bp 101..103 GUG 301..303 CAC
segment6 17bp 110..131 ACGUGGGCAAUCUGCCCUUCAC 211..229 GUGAAGGAUGAGCCCGCGG
segment7 11bp 133..147 CUGGGACAAGCCCUG 152..167 CGGGGUCUAAUACCGG
segment8 10bp 173..182 ACCUGCCGAG 187..196 CUCGGUGGGU
segment9 2bp 202..203 GC 208..209 GC
segment10 15bp 230..249 CCUAUCAGCUUGUUGGUGGG 257..276 CCUACCAAGGCGACGACGGG
segment11 8bp 279..286 GCCGGCCU 290..300 GGGCGACCGGC
segment12 6bp 305..310 CUGGGA 321..326 GCCCAG
segment13 4bp 328..331 CUCC 336..339 GGAG
segment14 9bp 356..368 UUGCACAAUGGGC 373..382 GCCUGAUGCA
segment15 8bp 394..407 GAGGGAUGACGGCC 412..424 GGUUGUAAACCUC
segment16 8bp 428..438 CAGCAGGGAAG 451..464 CGGUACCUGCAGAG
segment17 5bp 477..483 CGUAGGC 650..656 GCUGAGG
segment18 24bp 485..513 GCUUGUNNGUCGGAUGUGAAAGCCNGGGG 519..547 CCCCGGGUCUGCAUUCGAUACGGGCAGGC
segment19 16bp 551..568 AGUGUGGUAGGGGAGAUC 629..646 GAUCUCUGGGCCAUGACU
segment20 13bp 569..586 GGAAUUCCUGGUGUAGCG 592..612 UGCGCAGAUAUCAGGAGGAAC
segment21 3bp 617..621 GUGGC 626..628 GCG
segment22 11bp 664..680 GCGUGGGGAGCGAACAG 691..704 CUGGUAGUCCACGC
segment23 6bp 715..720 GGGAAC 770..775 GUUCCC
segment24 12bp 723..734 GGUGUUGGCGAC 742..753 GUNGUCGGUGCC
segment25 2bp 757..758 GC 763..764 GC
segment26 1bp 793..793 C 798..798 C
segment27 7bp 811..817 CCAAGGC 1030..1036 GUCUUGG
segment28 9bp 822..830 AUAUACCGG 861..869 UCGGUAUAC
segment29 7bp 833..839 ACGGCCA 844..850 UGGUCGC
segment30 2bp 852..853 CC 859..860 GG
segment31 11bp 871..884 GGUGGUGCAUGGCU 1012..1025 AGUCAUCAUGCCCC
segment32 6bp 892..897 GCUCGU 925..930 ACGAGC
segment33 3bp 898..900 GUC 905..907 GAU
segment34 4bp 910..913 UGGG 919..922 CCCG
segment35 12bp 941..957 UUCUGUGUUGCCAGCAU 969..984 AUGGGGACUCACAGGA
segment36 8bp 987..994 CUGCCGGG 1000..1007 CUCGGAGG
segment37 16bp 1057..1080 GCCGGUACAAUGAGCUGCGAUGCC 1085..1110 GGUGGAGCGAAUCUCAAAAAGCCGGU
segment38 9bp 1117..1128 CGGAUUGGGGUC 1137..1148 GACCCCAUGAAG
segment39 2bp 1170..1171 GA 1180..1181 GC
