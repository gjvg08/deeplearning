#Name: bpRNA_CRW_8191
#Length:  1009 
#PageNumber: 2
GCAGCCGCGGUAAUACGUAGGUCCCGAGCGUUGCGCGAUGUUACUGGGCGUAAAGCGUCCGUAGCCGGUUUCGUAAGCGGAUUGUCAAAGCCCGAAGCUCAACUUCGGCAGGGCAUUCCGAACUGCGUUACUUGAGUCCUGUUCAGGUAGGCGGAAUUCCCGGUGUAGCGGUGAAAUGCGUAGAUAUCGGGAGGAACACCAGUGGCGAAGGCGGCCUACUGGGACGGUACUGACGGUCAUGGACGAAAGCUGGGGGAGCAAACCGGAUUAGAUACCCGGGUAGUCCCAGCCGUAAACGAUGGAUGUUAGGUGUAGUGAUAACUUCGCUGCACCCCAGCUAACGCGUUAAACAUCCCGCCUGGGGAGUACGGGCGCAAGCCUGAAACUCAAAGGAAUUGGCGGGGGCCCGCACAACCGGUGGAGCGUCUGGUUUAAUUCGAUGCUAACCGAAAAACCUUACCAGGGUUUGAAAUGGUAGGAAAGCUGUCGAAAGAUAGCUGUGUCUCCUUUUGGGAGAAAUCCUACCACAGGUGGUGCAUGCCGUCGUCAGCUCGUGUUGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUCCUUAUUUGCUUCCUCAUAUGAGGUGCCCUAUAAGGAGACUGCCGGCGACAAGCCGGAGGAAGGAGGGGACGACGUCAGGUCAGUAUGCCCUUUAUGCCCUGGGCUACACAGGCGCUACAGUGGCAAGGACAAUGAGUAGCGAGAGCGCAAGCUCGAGCUAAUCUCACAAACCUUGUCGUGGUGCGAAUUGAAGGUUGAAACUCACCUUCAUGAAGCCGGAAUCGGUAGUAAUGGCGUAUCAGCUAUGUCGCCGUGAAUACGUUCCCGGGCCUUGCACACACCGCCCGUCACGCCACGGAAGCUGGUGUUUUCGGAAGUCCUCAAGCUAACCCGCAAGGGAGGCAGAGGCCGAUGAAAACAUCGGUAACUGGGGCGAAGUCGUAACAAGGUAGCCGUAGGGGAACCUGC
((....))......................................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((.....))))))))))...((..]])).....)))))))))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....(((((((((..((((((....))))).)...(((((.....)))..))))))))))).((.((((..((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((.((((....))))))....)))))))..((((((((.....)))))))).....))))....)))).)))....)))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((........))))))))......))...)))))))))).))..(.((.((.(.((((((((..((((((((((((....((((((.((((..(((....))).))))))))))...))))))))))))..))))))))..).))..))..)......(((((....)))))
SSHHHHSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSHHHHHSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSHHHHSSSSSBSMMMSSSSSHHHHHSSSBBSSSSSSSSSSSMSSBSSSSBBSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBSSSSHHHHSSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBBSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXSISSISSISISSSSSSSSIISSSSSSSSSSSSIIIISSSSSSBSSSSIISSSHHHHSSSISSSSSSSSSSIIISSSSSSSSSSSSIISSSSSSSSIISISSIISSIISXXXXXXSSSSSHHHHSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..2 "GC" 7..8 "GC"
S2 47..49 "GGC" 357..359 "GCC"
S3 55..55 "G" 356..356 "C"
S4 57..60 "GUCC" 241..244 "GGAC"
S5 62..62 "U" 238..238 "C"
S6 64..66 "GCC" 235..237 "GGU"
S7 68..74 "GUUUCGU" 125..131 "GCGUUAC"
S8 76..77 "AG" 123..124 "CU"
S9 78..86 "CGGAUUGUC" 112..120 "GGCAUUCCG"
S10 91..97 "CCCGAAG" 103..109 "CUUCGGC"
S11 135..137 "AGU" 229..231 "ACU"
S12 138..143 "CCUGUU" 222..227 "GGACGG"
S13 146..152 "GGUAGGC" 214..220 "GCCUACU"
S14 153..154 "GG" 196..197 "AC"
S15 157..164 "UUCCCGGU" 186..193 "AUCGGGAG"
S16 168..170 "GCG" 177..179 "UGC"
S17 202..202 "G" 213..213 "G"
S18 205..206 "GC" 211..212 "GC"
S19 249..254 "GCUGGG" 285..290 "CCCAGC"
S20 258..259 "GC" 283..284 "GU"
S21 263..266 "CCGG" 276..279 "CCGG"
S22 301..306 "GGAUGU" 350..355 "ACAUCC"
S23 309..318 "GGUGUAGUGA" 324..333 "UCGCUGCACC"
S24 337..338 "GC" 343..344 "GC"
S25 361..363 "GGG" 386..388 "CUC"
S26 370..373 "GGGC" 378..381 "GCCU"
S27 397..398 "UG" 878..879 "CA"
S28 399..401 "GCG" 874..876 "UGC"
S29 403..409 "GGGCCCG" 867..873 "CGGGCCU"
S30 411..412 "AC" 862..863 "GU"
S31 415..419 "CCGGU" 822..826 "AUCGG"
S32 420..429 "GGAGCGUCUG" 710..719 "CAGGCGCUAC"
S33 430..431 "GU" 707..708 "AC"
S34 436..439 "UUCG" 448..451 "CGAA"
S35 460..466 "CCAGGGU" 697..703 "GCCCUGG"
S36 472..480 "AUGGUAGGA" 520..528 "UCCUACCAC"
S37 483..483 "G" 499..499 "U"
S38 484..488 "CUGUC" 493..497 "GAUAG"
S39 503..504 "UC" 518..519 "AA"
S40 505..507 "UCC" 513..515 "GGA"
S41 530..531 "GG" 691..692 "CU"
S42 533..536 "GGUG" 687..690 "UGCC"
S43 539..539 "U" 686..686 "A"
S44 540..542 "GCC" 679..681 "GGU"
S45 543..546 "GUCG" 674..677 "CGUC"
S46 550..555 "GCUCGU" 584..589 "ACGAGC"
S47 556..558 "GUU" 563..565 "GAU"
S48 568..571 "UGGG" 578..581 "CCCG"
S49 595..598 "CCUU" 666..669 "GGGG"
S50 600..606 "UCCUUAU" 631..637 "AUAAGGA"
S51 610..611 "CU" 625..626 "UG"
S52 613..616 "CCUC" 621..624 "GAGG"
S53 640..647 "CUGCCGGC" 653..660 "GCCGGAGG"
S54 723..729 "GGCAAGG" 772..778 "CCUUGUC"
S55 735..737 "GAG" 764..766 "CUC"
S56 740..741 "GC" 758..759 "GC"
S57 745..747 "AGC" 752..754 "GCU"
S58 785..785 "C" 816..816 "G"
S59 789..796 "UUGAAGGU" 805..812 "ACCUUCAU"
S60 832..839 "AUGGCGUA" 848..855 "GUCGCCGU"
S61 882..882 "C" 989..989 "G"
S62 884..885 "GC" 985..986 "AC"
S63 887..888 "CG" 981..982 "CG"
S64 890..890 "C" 979..979 "G"
S65 892..899 "CGCCACGG" 969..976 "CUGGGGCG"
S66 902..913 "GCUGGUGUUUUC" 955..966 "GAAAACAUCGGU"
S67 918..923 "GUCCUC" 946..951 "GAGGCC"
S68 925..928 "AGCU" 942..945 "GGCA"
S69 931..933 "CCC" 938..940 "GGG"
S70 996..1000 "GUAGG" 1005..1009 "CCUGC"
H1 3..6 "AGCC" (2,7) C:G 
H2 98..102 "CUCAA" (97,103) G:C 
H3 171..176 "GUGAAA" (170,177) G:U 
H4 207..210 "GAAG" (206,211) C:G 
H5 267..275 "AUUAGAUAC" (266,276) G:C 
H6 319..323 "UAACU" (318,324) A:U 
H7 339..342 "UAAC" (338,343) C:G PK{1}
H8 374..377 "GCAA" (373,378) C:G 
H9 440..447 "AUGCUAAC" (439,448) G:C 
H10 489..492 "GAAA" (488,493) C:G 
H11 508..512 "UUUUG" (507,513) C:G 
H12 559..562 "GUGA" (558,563) U:G 
H13 572..577 "UUAAGU" (571,578) G:C 
H14 617..620 "AUAU" (616,621) C:G 
H15 648..652 "GACAA" (647,653) C:G 
H16 748..751 "GCAA" (747,752) C:G 
H17 797..804 "UGAAACUC" (796,805) U:A 
H18 840..847 "UCAGCUAU" (839,848) A:G 
H19 934..937 "GCAA" (933,938) C:G 
H20 1001..1004 "GGAA" (1000,1005) G:C 
B1 50..54 "GUAAA" (49,357) C:G (356,55) C:G PK{1}
B2 63..63 "A" (62,238) U:C (237,64) U:G 
B3 75..75 "A" (74,125) U:G (124,76) U:A 
B4 121..122 "AA" (120,78) G:C (77,123) G:C 
B5 203..204 "UG" (202,213) G:G (212,205) C:G 
B6 228..228 "U" (227,138) G:C (137,229) U:A 
B7 255..257 "GGA" (254,285) G:C (284,258) U:G 
B8 402..402 "G" (401,874) G:U (873,403) U:G 
B9 498..498 "C" (497,484) G:C (483,499) G:U 
B10 516..517 "GA" (515,505) A:U (504,518) C:A 
B11 532..532 "U" (531,691) G:C (690,533) C:G 
B12 537..538 "CA" (536,687) G:U (686,539) A:U 
B13 612..612 "U" (611,625) U:U (624,613) G:C 
B14 678..678 "A" (677,543) C:G (542,679) C:G 
B15 682..685 "CAGU" (681,540) U:G (539,686) U:A 
B16 709..709 "A" (708,430) C:G (429,710) G:C 
B17 877..877 "A" (876,399) C:G (398,878) G:C 
B18 924..924 "A" (923,946) C:G (945,925) A:A 
I1.1 61..61 "G" (60,241) C:G 
I1.2 239..240 "AU" (238,62) C:U 
I2.1 87..90 "AAAG" (86,112) C:G 
I2.2 110..111 "AG" (109,91) C:C 
I3.1 144..145 "CA" (143,222) U:G 
I3.2 221..221 "G" (220,146) U:G 
I4.1 155..156 "AA" (154,196) G:A 
I4.2 194..195 "GA" (193,157) G:U 
I5.1 165..167 "GUA" (164,186) U:A 
I5.2 180..185 "GUAGAU" (179,168) C:G 
I6.1 260..262 "AAA" (259,283) C:G 
I6.2 280..282 "GUA" (279,263) G:C 
I7.1 364..369 "GAGUAC" (363,386) G:C 
I7.2 382..385 "GAAA" (381,370) U:G 
I8.1 410..410 "C" (409,867) G:C 
I8.2 864..866 "UCC" (863,411) U:A 
I9.1 607..609 "UUG" (606,631) U:A 
I9.2 627..630 "CCCU" (626,610) G:C 
I10.1 730..734 "ACAAU" (729,772) G:C 
I10.2 767..771 "ACAAA" (766,735) C:G 
I11.1 738..739 "UA" (737,764) G:C 
I11.2 760..763 "UAAU" (759,740) C:G 
I12.1 742..744 "GAG" (741,758) C:G 
I12.2 755..757 "CGA" (754,745) U:A 
I13.1 786..788 "GAA" (785,816) C:G 
I13.2 813..815 "GAA" (812,789) U:U 
I14.1 883..883 "C" (882,989) C:G 
I14.2 987..988 "AA" (986,884) C:G 
I15.1 886..886 "C" (885,985) C:A 
I15.2 983..984 "UA" (982,887) G:C 
I16.1 889..889 "U" (888,981) G:C 
I16.2 980..980 "U" (979,890) G:C 
I17.1 891..891 "A" (890,979) C:G 
I17.2 977..978 "AA" (976,892) G:C 
I18.1 900..901 "AA" (899,969) G:C 
I18.2 967..968 "AA" (966,902) U:G 
I19.1 914..917 "GGAA" (913,955) C:G 
I19.2 952..954 "GAU" (951,918) C:G 
I20.1 929..930 "AA" (928,942) U:G 
I20.2 941..941 "A" (940,931) G:C 
M1.1 56..56 "C" (55,356) G:C (244,57) C:G 
M1.2 245..248 "GAAA" (244,57) C:G (290,249) C:G 
M1.3 291..300 "CGUAAACGAU" (290,249) C:G (355,301) C:G 
M1.4 356..355 "" (355,301) C:G (55,356) G:C 
M2.1 67..67 "G" (66,235) C:G (131,68) C:G 
M2.2 132..134 "UUG" (131,68) C:G (231,135) U:A 
M2.3 232..234 "GAC" (231,135) U:A (66,235) C:G 
M3.1 153..152 "" (152,214) C:G (197,153) C:G 
M3.2 198..201 "ACCA" (197,153) C:G (213,202) G:G 
M3.3 214..213 "" (213,202) G:G (152,214) C:G 
M4.1 307..308 "UA" (306,350) U:A (333,309) C:G 
M4.2 334..336 "CCA" (333,309) C:G (344,337) C:G 
M4.3 345..349 "GUUAA" (344,337) C:G (306,350) U:A 
M5.1 413..414 "AA" (412,862) C:G (826,415) G:C 
M5.2 827..831 "UAGUA" (826,415) G:C (855,832) U:A 
M5.3 856..861 "GAAUAC" (855,832) U:A (412,862) C:G 
M6.1 420..419 "" (419,822) U:A (719,420) C:G 
M6.2 720..722 "AGU" (719,420) C:G (778,723) C:G 
M6.3 779..784 "GUGGUG" (778,723) C:G (816,785) G:C 
M6.4 817..821 "CCGGA" (816,785) G:C (419,822) U:A 
M7.1 432..435 "UUAA" (431,707) U:A (451,436) A:U 
M7.2 452..459 "AAACCUUA" (451,436) A:U (703,460) G:C 
M7.3 704..706 "GCU" (703,460) G:C (431,707) U:A 
M8.1 467..471 "UUGAA" (466,697) U:G (528,472) C:A 
M8.2 529..529 "A" (528,472) C:A (692,530) U:G 
M8.3 693..696 "UUAU" (692,530) U:G (466,697) U:G 
M9.1 481..482 "AA" (480,520) A:U (499,483) U:G 
M9.2 500..502 "GUG" (499,483) U:G (519,503) A:U 
M9.3 520..519 "" (519,503) A:U (480,520) A:U 
M10.1 547..549 "UCA" (546,674) G:C (589,550) C:G 
M10.2 590..594 "GCAAC" (589,550) C:G (669,595) G:C 
M10.3 670..673 "ACGA" (669,595) G:C (546,674) G:C 
M11.1 556..555 "" (555,584) U:A (565,556) U:G 
M11.2 566..567 "GU" (565,556) U:G (581,568) G:U 
M11.3 582..583 "CA" (581,568) G:U (555,584) U:A 
M12.1 599..599 "G" (598,666) U:G (637,600) A:U 
M12.2 638..639 "GA" (637,600) A:U (660,640) G:C 
M12.3 661..665 "AAGGA" (660,640) G:C (598,666) U:G 
X1 9..46 "GGUAAUACGUAGGUCCCGAGCGUUGCGCGAUGUUACUG" (8,1) C:G (359,47) C:G 
X3 389..396 "AAAGGAAU" (388,361) C:G (879,397) A:U 
X4 880..881 "CA" (879,397) A:U (989,882) G:C 
X5 990..995 "GUAGCC" (989,882) G:C (1009,996) C:G 
PK1 2bp 50..51 341..342 B1 50..54 H7 339..342
PK1.1 50 G 342 C
PK1.2 51 U 341 A
NCBP1 71 U 128 U S7
NCBP2 62 U 238 C S5
NCBP3 789 U 812 U S59
NCBP4 919 U 950 C S67
NCBP5 82 U 116 U S9
NCBP6 611 U 625 U S51
NCBP7 642 G 658 A S53
NCBP8 202 G 213 G S17
NCBP9 140 U 225 C S12
NCBP10 154 G 196 A S14
NCBP11 91 C 109 C S10
NCBP12 838 U 849 U S60
NCBP13 896 A 972 G S65
NCBP14 544 U 676 U S45
NCBP15 472 A 528 C S36
NCBP16 504 C 518 A S39
NCBP17 885 C 985 A S62
NCBP18 70 U 129 U S7
NCBP19 839 A 848 G S60
NCBP20 141 G 224 A S12
NCBP21 421 G 718 A S32
NCBP22 925 A 945 A S68
segment1 2bp 1..2 GC 7..8 GC
segment2 4bp 47..55 GGCGUAAAG 356..359 CGCC
segment3 8bp 57..66 GUCCGUAGCC 235..244 GGUCAUGGAC
segment4 25bp 68..97 GUUUCGUAAGCGGAUUGUCAAAGCCCGAAG 103..131 CUUCGGCAGGGCAUUCCGAACUGCGUUAC
segment5 16bp 135..152 AGUCCUGUUCAGGUAGGC 214..231 GCCUACUGGGACGGUACU
segment6 13bp 153..170 GGAAUUCCCGGUGUAGCG 177..197 UGCGUAGAUAUCGGGAGGAAC
segment7 3bp 202..206 GUGGC 211..213 GCG
segment8 12bp 249..266 GCUGGGGGAGCAAACCGG 276..290 CCGGGUAGUCCCAGC
segment9 6bp 301..306 GGAUGU 350..355 ACAUCC
segment10 10bp 309..318 GGUGUAGUGA 324..333 UCGCUGCACC
segment11 2bp 337..338 GC 343..344 GC
segment12 7bp 361..373 GGGGAGUACGGGC 378..388 GCCUGAAACUC
segment13 14bp 397..412 UGGCGGGGGCCCGCAC 862..879 GUUCCCGGGCCUUGCACA
segment14 5bp 415..419 CCGGU 822..826 AUCGG
segment15 12bp 420..431 GGAGCGUCUGGU 707..719 ACACAGGCGCUAC
segment16 4bp 436..439 UUCG 448..451 CGAA
segment17 7bp 460..466 CCAGGGU 697..703 GCCCUGG
segment18 9bp 472..480 AUGGUAGGA 520..528 UCCUACCAC
segment19 6bp 483..488 GCUGUC 493..499 GAUAGCU
segment20 5bp 503..507 UCUCC 513..519 GGAGAAA
segment21 14bp 530..546 GGUGGUGCAUGCCGUCG 674..692 CGUCAGGUCAGUAUGCCCU
segment22 6bp 550..555 GCUCGU 584..589 ACGAGC
segment23 3bp 556..558 GUU 563..565 GAU
segment24 4bp 568..571 UGGG 578..581 CCCG
segment25 4bp 595..598 CCUU 666..669 GGGG
segment26 13bp 600..616 UCCUUAUUUGCUUCCUC 621..637 GAGGUGCCCUAUAAGGA
segment27 8bp 640..647 CUGCCGGC 653..660 GCCGGAGG
segment28 15bp 723..747 GGCAAGGACAAUGAGUAGCGAGAGC 752..778 GCUCGAGCUAAUCUCACAAACCUUGUC
segment29 9bp 785..796 CGAAUUGAAGGU 805..816 ACCUUCAUGAAG
segment30 8bp 832..839 AUGGCGUA 848..855 GUCGCCGU
segment31 39bp 882..933 CCGCCCGUCACGCCACGGAAGCUGGUGUUUUCGGAAGUCCUCAAGCUAACCC 938..989 GGGAGGCAGAGGCCGAUGAAAACAUCGGUAACUGGGGCGAAGUCGUAACAAG
segment32 5bp 996..1000 GUAGG 1005..1009 CCUGC
