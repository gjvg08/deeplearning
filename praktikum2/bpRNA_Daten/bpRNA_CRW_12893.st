#Name: bpRNA_CRW_12893
#Length:  1234 
#PageNumber: 2
CUUCGGCUGUCACUUAUGGAUGGACCCGCGUCGCAUUAGCUAGUUGGUGAGGUAACGGCUCACCAAGGCAGCGAUGCGUAGCCGACCUGAGAGGGUGAUCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCCGCAAUGGACGAAAGUCUGACGGAGCAACGCCGCGUGAGUGAUGAAGGCUUUCGGGUCGUAAAACUCUGUUGUUAGGGAAGAACAAGUGCUAGUUGAAUAAGCUGGCACCUUGACGGUACCUAACCAGAAAGCCACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUGGCAAGCGUUAUCCGGAAUUAUUGGGCGUAAAGCGCGCGCAGGUGGUUUCUUAAGUCUGAUGUGAAAGCCCACGGCUCAACCGUGGAGGGUCAUUGGAAACUGGGAGACUUGAGUGCAGAAGAGGAAAGUGGAAUUCCAUGUGUAGCGGUGAAAUGCGUAGAGAUAUGGAGGAACACCAGUGGCGAAGGCGACUUUCUGGUCUGUAACUGACACUGAGGCGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAAGUGUUAGAGGGUUUCCGCCCUUUAGUGCUGAAGUUAACGCAUUAAGCACUCCGCCUGGGGAGUACGGCCGCAAGGCUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCUCUGAAAACCCUAGAGAUAGGGCUUCUCCUUCGGGAGCAGAGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGAUCUUAGUUGCCAUCAUUAAGUUGGGCACUCUAAGGUGACUGCCGGUGACAAACCGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGACCUGGGCUACACACGUGCUACAAUGGACGGUACAAAGAGCUGCAAGACCGCGAGGUGGAGCUAAUCUCAUAAAACCGUUCUCAGUUCGGAUUGUAGGCUGCAACUCGCCUACAUGAAGCUGGAAUCGCUAGUAAUCGCGGAUCAGCAUGCCGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACACCACGAGAGUUUGUAACACCCGAAGUCGGUG
(....).........................(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((.......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((.(((...((...((((.........))))...))))).))..........((((((..((((((((((.((....)).))))))))))...((..]])).....)))))).))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....((((((((..(((((((....))))))).((.(....).))))))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).)).............................................
SHHHHSXXXXXXXXXXXXXXXXXXXXXXXXXSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSISSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSISSMMMMMMMMMMSSSSSSMMSSSSSSSSSSISSHHHHSSISSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSISHHHHSISSSSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..1 "C" 6..6 "G"
S2 32..34 "CGC" 76..78 "GCG"
S3 37..37 "U" 75..75 "U"
S4 39..41 "GCU" 67..69 "GGC"
S5 44..51 "UUGGUGAG" 59..66 "CUCACCAA"
S6 81..84 "GCCG" 100..103 "CGGC"
S7 85..88 "ACCU" 93..96 "GGGU"
S8 108..113 "CUGGGA" 124..129 "GCCCAG"
S9 131..134 "CUCC" 139..142 "GGAG"
S10 159..159 "U" 185..185 "A"
S11 161..163 "CCG" 182..184 "CGG"
S12 167..171 "UGGAC" 176..180 "GUCUG"
S13 198..201 "GAGU" 225..228 "ACUC"
S14 208..211 "GGCU" 216..219 "GGUC"
S15 230..230 "G" 289..289 "G"
S16 233..238 "GUUAGG" 281..286 "CCUAAC"
S17 242..242 "G" 276..276 "C"
S18 248..255 "GUGCUAGU" 263..270 "GCUGGCAC"
S19 293..297 "GCCAC" 334..338 "GUGGC"
S20 304..308 "CUACG" 329..333 "CGUAG"
S21 314..315 "GC" 320..321 "GC"
S22 360..362 "GGC" 675..677 "GCC"
S23 370..373 "GCGC" 554..557 "GCGC"
S24 375..375 "C" 551..551 "G"
S25 377..379 "GGU" 548..550 "ACU"
S26 381..387 "GUUUCUU" 438..444 "GGGAGAC"
S27 389..390 "AG" 436..437 "CU"
S28 391..399 "UCUGAUGUG" 425..433 "GUCAUUGGA"
S29 404..410 "CCCACGG" 416..422 "CCGUGGA"
S30 448..450 "AGU" 542..544 "ACU"
S31 451..456 "GCAGAA" 535..540 "GUCUGU"
S32 459..465 "GGAAAGU" 527..533 "ACUUUCU"
S33 466..467 "GG" 509..510 "AC"
S34 470..477 "UUCCAUGU" 499..506 "AUAUGGAG"
S35 481..483 "GCG" 490..492 "UGC"
S36 515..515 "G" 526..526 "G"
S37 518..519 "GC" 524..525 "GC"
S38 562..563 "GC" 602..603 "GC"
S39 565..567 "UGG" 598..600 "CCA"
S40 571..572 "GC" 596..597 "GU"
S41 576..579 "CAGG" 589..592 "CCUG"
S42 614..619 "GAGUGC" 668..673 "GCACUC"
S43 622..631 "AGUGUUAGAG" 642..651 "CUUUAGUGCU"
S44 633..634 "GU" 639..640 "GC"
S45 655..656 "GU" 661..662 "GC"
S46 679..681 "GGG" 704..706 "CUC"
S47 688..691 "GGCC" 696..699 "GGCU"
S48 715..716 "UG" 1188..1189 "CA"
S49 717..719 "ACG" 1184..1186 "UGU"
S50 721..727 "GGGCCCG" 1177..1183 "CGGGCCU"
S51 729..730 "AC" 1172..1173 "GU"
S52 733..737 "GCGGU" 1133..1137 "AUCGC"
S53 738..747 "GGAGCAUGUG" 1021..1030 "CACGUGCUAC"
S54 748..749 "GU" 1018..1019 "AC"
S55 754..757 "UUCG" 766..769 "CGAA"
S56 778..784 "CCAGGUC" 1008..1014 "GACCUGG"
S57 790..792 "AUC" 837..839 "GAC"
S58 793..797 "CUCUG" 831..835 "CAGAG"
S59 800..806 "AACCCUA" 811..817 "UAGGGCU"
S60 819..820 "CU" 829..830 "AG"
S61 822..822 "C" 827..827 "G"
S62 841..842 "GG" 1002..1003 "CC"
S63 844..847 "GGUG" 998..1001 "UGCC"
S64 850..851 "UG" 996..997 "CA"
S65 852..854 "GUU" 990..992 "AAU"
S66 855..858 "GUCG" 985..988 "CGUC"
S67 862..867 "GCUCGU" 896..901 "ACGAGC"
S68 868..870 "GUC" 875..877 "GAU"
S69 880..883 "UGGG" 890..893 "CCCG"
S70 907..910 "CCUU" 977..980 "GGGG"
S71 912..918 "AUCUUAG" 942..948 "CUAAGGU"
S72 922..923 "CC" 936..937 "GG"
S73 926..927 "CA" 934..935 "UG"
S74 951..958 "CUGCCGGU" 964..971 "ACCGGAGG"
S75 1034..1040 "GGACGGU" 1083..1089 "ACCGUUC"
S76 1046..1048 "GAG" 1075..1077 "CUC"
S77 1051..1052 "GC" 1069..1070 "GC"
S78 1056..1058 "ACC" 1063..1065 "GGU"
S79 1096..1096 "C" 1127..1127 "G"
S80 1100..1107 "UUGUAGGC" 1116..1123 "GCCUACAU"
S81 1143..1150 "AUCGCGGA" 1158..1165 "GCCGCGGU"
H1 2..5 "UUCG" (1,6) C:G 
H2 52..58 "GUAACGG" (51,59) G:C 
H3 89..92 "GAGA" (88,93) U:G 
H4 114..123 "CUGAGACACG" (113,124) A:G 
H5 135..138 "UACG" (134,139) C:G 
H6 172..175 "GAAA" (171,176) C:G 
H7 212..215 "UUCG" (211,216) U:G 
H8 256..262 "UGAAUAA" (255,263) U:G 
H9 316..319 "AGCC" (315,320) C:G PK{1}
H10 411..415 "CUCAA" (410,416) G:C 
H11 484..489 "GUGAAA" (483,490) G:U 
H12 520..523 "GAAG" (519,524) C:G 
H13 580..588 "AUUAGAUAC" (579,589) G:C 
H14 635..638 "UUCC" (634,639) U:G 
H15 657..660 "UAAC" (656,661) U:G PK{2}
H16 692..695 "GCAA" (691,696) C:G 
H17 758..765 "AAGCAACG" (757,766) G:C 
H18 807..810 "GAGA" (806,811) A:U 
H19 823..826 "UUCG" (822,827) C:G 
H20 871..874 "GUGA" (870,875) C:G 
H21 884..889 "UUAAGU" (883,890) G:C 
H22 928..933 "UUAAGU" (927,934) A:U 
H23 959..963 "GACAA" (958,964) U:A 
H24 1059..1062 "GCGA" (1058,1063) C:G 
H25 1108..1115 "UGCAACUC" (1107,1116) C:G 
H26 1151..1157 "UCAGCAU" (1150,1158) A:G 
B1 35..36 "AU" (34,76) C:G (75,37) U:U 
B2 42..43 "AG" (41,67) U:G (66,44) A:U 
B3 97..99 "GAU" (96,85) U:A (84,100) G:C 
B4 160..160 "U" (159,185) U:A (184,161) G:C 
B5 298..303 "GGCUAA" (297,334) C:G (333,304) G:C PK{1}
B6 376..376 "A" (375,551) C:G (550,377) U:G 
B7 388..388 "A" (387,438) U:G (437,389) U:A 
B8 434..435 "AA" (433,391) A:U (390,436) G:C 
B9 516..517 "UG" (515,526) G:G (525,518) C:G 
B10 541..541 "A" (540,451) U:G (450,542) U:A 
B11 568..570 "GGA" (567,598) G:C (597,571) U:G 
B12 720..720 "G" (719,1184) G:U (1183,721) U:G 
B13 836..836 "U" (835,793) G:C (792,837) C:G 
B14 843..843 "U" (842,1002) G:C (1001,844) C:G 
B15 848..849 "CA" (847,998) G:U (997,850) A:U 
B16 924..925 "AU" (923,936) C:G (935,926) G:C 
B17 989..989 "A" (988,855) C:G (854,990) U:A 
B18 993..995 "CAU" (992,852) U:G (851,996) G:C 
B19 1020..1020 "A" (1019,748) C:G (747,1021) G:C 
B20 1187..1187 "A" (1186,717) U:A (716,1188) G:C 
I1.1 38..38 "A" (37,75) U:U 
I1.2 70..74 "AGCGA" (69,39) C:G 
I2.1 164..166 "CAA" (163,182) G:C 
I2.2 181..181 "A" (180,167) G:U 
I3.1 202..207 "GAUGAA" (201,225) U:A 
I3.2 220..224 "GUAAA" (219,208) C:G 
I4.1 231..232 "UU" (230,289) G:G 
I4.2 287..288 "CA" (286,233) C:G 
I5.1 239..241 "GAA" (238,281) G:C 
I5.2 277..280 "GGUA" (276,242) C:G 
I6.1 243..247 "AACAA" (242,276) G:C 
I6.2 271..275 "CUUGA" (270,248) C:G 
I7.1 309..313 "UGCCA" (308,329) G:C 
I7.2 322..328 "GGUAAUA" (321,314) C:G 
I8.1 374..374 "G" (373,554) C:G 
I8.2 552..553 "AG" (551,375) G:C 
I9.1 400..403 "AAAG" (399,425) G:G 
I9.2 423..424 "GG" (422,404) A:C 
I10.1 457..458 "GA" (456,535) A:G 
I10.2 534..534 "G" (533,459) U:G 
I11.1 468..469 "AA" (467,509) G:A 
I11.2 507..508 "GA" (506,470) G:U 
I12.1 478..480 "GUA" (477,499) U:A 
I12.2 493..498 "GUAGAG" (492,481) C:G 
I13.1 564..564 "G" (563,602) C:G 
I13.2 601..601 "C" (600,565) A:U 
I14.1 573..575 "AAA" (572,596) C:G 
I14.2 593..595 "GUA" (592,576) G:C 
I15.1 632..632 "G" (631,642) G:C 
I15.2 641..641 "C" (640,633) C:G 
I16.1 682..687 "GAGUAC" (681,704) G:C 
I16.2 700..703 "GAAA" (699,688) U:G 
I17.1 728..728 "C" (727,1177) G:C 
I17.2 1174..1176 "UCC" (1173,729) U:A 
I18.1 821..821 "C" (820,829) U:A 
I18.2 828..828 "G" (827,822) G:C 
I19.1 919..921 "UUG" (918,942) G:C 
I19.2 938..941 "CACU" (937,922) G:C 
I20.1 1041..1045 "ACAAA" (1040,1083) U:A 
I20.2 1078..1082 "AUAAA" (1077,1046) C:G 
I21.1 1049..1050 "CU" (1048,1075) G:C 
I21.2 1071..1074 "UAAU" (1070,1051) C:G 
I22.1 1053..1055 "AAG" (1052,1069) C:G 
I22.2 1066..1068 "GGA" (1065,1056) U:A 
I23.1 1097..1099 "GGA" (1096,1127) C:G 
I23.2 1124..1126 "GAA" (1123,1100) U:U 
M1.1 363..369 "GUAAAGC" (362,675) C:G (557,370) C:G PK{2}
M1.2 558..561 "GAAA" (557,370) C:G (603,562) C:G 
M1.3 604..613 "CGUAAACGAU" (603,562) C:G (673,614) C:G 
M1.4 674..674 "C" (673,614) C:G (362,675) C:G 
M2.1 380..380 "G" (379,548) U:A (444,381) C:G 
M2.2 445..447 "UUG" (444,381) C:G (544,448) U:A 
M2.3 545..547 "GAC" (544,448) U:A (379,548) U:A 
M3.1 466..465 "" (465,527) U:A (510,466) C:G 
M3.2 511..514 "ACCA" (510,466) C:G (526,515) G:G 
M3.3 527..526 "" (526,515) G:G (465,527) U:A 
M4.1 620..621 "UA" (619,668) C:G (651,622) U:A 
M4.2 652..654 "GAA" (651,622) U:A (662,655) C:G 
M4.3 663..667 "AUUAA" (662,655) C:G (619,668) C:G 
M5.1 731..732 "AA" (730,1172) C:G (1137,733) C:G 
M5.2 1138..1142 "UAGUA" (1137,733) C:G (1165,1143) U:A 
M5.3 1166..1171 "GAAUAC" (1165,1143) U:A (730,1172) C:G 
M6.1 738..737 "" (737,1133) U:A (1030,738) C:G 
M6.2 1031..1033 "AAU" (1030,738) C:G (1089,1034) C:G 
M6.3 1090..1095 "UCAGUU" (1089,1034) C:G (1127,1096) G:C 
M6.4 1128..1132 "CUGGA" (1127,1096) G:C (737,1133) U:A 
M7.1 750..753 "UUAA" (749,1018) U:A (769,754) A:U 
M7.2 770..777 "GAACCUUA" (769,754) A:U (1014,778) G:C 
M7.3 1015..1017 "GCU" (1014,778) G:C (749,1018) U:A 
M8.1 785..789 "UUGAC" (784,1008) C:G (839,790) C:A 
M8.2 840..840 "A" (839,790) C:A (1003,841) C:G 
M8.3 1004..1007 "UUAU" (1003,841) C:G (784,1008) C:G 
M9.1 798..799 "AA" (797,831) G:C (817,800) U:A 
M9.2 818..818 "U" (817,800) U:A (830,819) G:C 
M9.3 831..830 "" (830,819) G:C (797,831) G:C 
M10.1 859..861 "UCA" (858,985) G:C (901,862) C:G 
M10.2 902..906 "GCAAC" (901,862) C:G (980,907) G:C 
M10.3 981..984 "AUGA" (980,907) G:C (858,985) G:C 
M11.1 868..867 "" (867,896) U:A (877,868) U:G 
M11.2 878..879 "GU" (877,868) U:G (893,880) G:U 
M11.3 894..895 "CA" (893,880) G:U (867,896) U:A 
M12.1 911..911 "G" (910,977) U:G (948,912) U:A 
M12.2 949..950 "GA" (948,912) U:A (971,951) G:C 
M12.3 972..976 "AAGGU" (971,951) G:C (910,977) U:G 
X1 7..31 "CUGUCACUUAUGGAUGGACCCGCGU" (6,1) G:C (78,32) G:C 
X2 79..80 "UA" (78,32) G:C (103,81) C:G 
X3 104..107 "CACA" (103,81) C:G (129,108) G:C 
X5 143..158 "GCAGCAGUAGGGAAUC" (142,131) G:C (185,159) A:U 
X6 186..197 "GCAACGCCGCGU" (185,159) A:U (228,198) C:G 
X8 290..292 "AAA" (289,230) G:G (338,293) C:G 
X9 339..359 "AAGCGUUAUCCGGAAUUAUUG" (338,293) C:G (677,360) C:G 
X11 707..714 "AAAGGAAU" (706,679) C:G (1189,715) A:U 
E1 1190..1234 "CACCGCCCGUCACACCACGAGAGUUUGUAACACCCGAAGUCGGUG" 
PK1 3bp 298..300 317..319 B5 298..303 H9 316..319
PK2 2bp 363..364 659..660 M1.1 363..369 H15 657..660
PK1.1 298 G 319 C
PK1.2 299 G 318 C
PK1.3 300 C 317 G
PK2.1 363 G 660 C
PK2.2 364 U 659 A
NCBP1 739 G 1029 A S53
NCBP2 113 A 124 G S8
NCBP3 953 G 969 A S74
NCBP4 399 G 425 G S28
NCBP5 456 A 535 G S31
NCBP6 515 G 526 G S36
NCBP7 856 U 987 U S66
NCBP8 790 A 839 C S57
NCBP9 801 A 816 C S59
NCBP10 37 U 75 U S3
NCBP11 230 G 289 G S15
NCBP12 467 G 509 A S33
NCBP13 398 U 426 U S28
NCBP14 1150 A 1158 G S81
NCBP15 404 C 422 A S29
NCBP16 1100 U 1123 U S80
segment1 1bp 1..1 C 6..6 G
segment2 15bp 32..51 CGCAUUAGCUAGUUGGUGAG 59..78 CUCACCAAGGCAGCGAUGCG
segment3 8bp 81..88 GCCGACCU 93..103 GGGUGAUCGGC
segment4 6bp 108..113 CUGGGA 124..129 GCCCAG
segment5 4bp 131..134 CUCC 139..142 GGAG
segment6 9bp 159..171 UUCCGCAAUGGAC 176..185 GUCUGACGGA
segment7 8bp 198..211 GAGUGAUGAAGGCU 216..228 GGUCGUAAAACUC
segment8 16bp 230..255 GUUGUUAGGGAAGAACAAGUGCUAGU 263..289 GCUGGCACCUUGACGGUACCUAACCAG
segment9 12bp 293..315 GCCACGGCUAACUACGUGCCAGC 320..338 GCGGUAAUACGUAGGUGGC
segment10 3bp 360..362 GGC 675..677 GCC
segment11 8bp 370..379 GCGCGCAGGU 548..557 ACUGAGGCGC
segment12 25bp 381..410 GUUUCUUAAGUCUGAUGUGAAAGCCCACGG 416..444 CCGUGGAGGGUCAUUGGAAACUGGGAGAC
segment13 16bp 448..465 AGUGCAGAAGAGGAAAGU 527..544 ACUUUCUGGUCUGUAACU
segment14 13bp 466..483 GGAAUUCCAUGUGUAGCG 490..510 UGCGUAGAGAUAUGGAGGAAC
segment15 3bp 515..519 GUGGC 524..526 GCG
segment16 11bp 562..579 GCGUGGGGAGCAAACAGG 589..603 CCUGGUAGUCCACGC
segment17 6bp 614..619 GAGUGC 668..673 GCACUC
segment18 12bp 622..634 AGUGUUAGAGGGU 639..651 GCCCUUUAGUGCU
segment19 2bp 655..656 GU 661..662 GC
segment20 7bp 679..691 GGGGAGUACGGCC 696..706 GGCUGAAACUC
segment21 14bp 715..730 UGACGGGGGCCCGCAC 1172..1189 GUUCCCGGGCCUUGUACA
segment22 5bp 733..737 GCGGU 1133..1137 AUCGC
segment23 12bp 738..749 GGAGCAUGUGGU 1018..1030 ACACACGUGCUAC
segment24 4bp 754..757 UUCG 766..769 CGAA
segment25 7bp 778..784 CCAGGUC 1008..1014 GACCUGG
segment26 8bp 790..797 AUCCUCUG 831..839 CAGAGUGAC
segment27 7bp 800..806 AACCCUA 811..817 UAGGGCU
segment28 3bp 819..822 CUCC 827..830 GGAG
segment29 15bp 841..858 GGUGGUGCAUGGUUGUCG 985..1003 CGUCAAAUCAUCAUGCCCC
segment30 6bp 862..867 GCUCGU 896..901 ACGAGC
segment31 3bp 868..870 GUC 875..877 GAU
segment32 4bp 880..883 UGGG 890..893 CCCG
segment33 4bp 907..910 CCUU 977..980 GGGG
segment34 11bp 912..927 AUCUUAGUUGCCAUCA 934..948 UGGGCACUCUAAGGU
segment35 8bp 951..958 CUGCCGGU 964..971 ACCGGAGG
segment36 15bp 1034..1058 GGACGGUACAAAGAGCUGCAAGACC 1063..1089 GGUGGAGCUAAUCUCAUAAAACCGUUC
segment37 9bp 1096..1107 CGGAUUGUAGGC 1116..1127 GCCUACAUGAAG
segment38 8bp 1143..1150 AUCGCGGA 1158..1165 GCCGCGGU
