#Name: bpRNA_CRW_8175
#Length:  1100 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
UUGAUCCUGGCUCAGCGCGAACGCUGGCGGCGUGCCUAACACAUGCAAGUCGUGCGCAGGCUCGCUCCCUUUUGGGAGCGGGUGCUGAGCGGCAAACGGGUGAGUAACACGUGGGUAACCUACCCCCAGGAGGGGGAUAACCCCGGGAAACCGGGGCUAAUACCCCAUAAUGCCACCCGCCACUAAGGCGUGGUGGCCAAAGGGGGCCUCUGGCCUUUGGCCAUGCUCCCGCCUGGGGAUGGGCCCGCGGCCCAUCAGGUAGUUGGUGGGGUAACGGCCCACCAAGCCUAUGACGGGUAGCCGGCCUGAGAGGGUGGCCGGCCACAGCGGGACUGAGACACGGCCCGCACCCCUACGGGGGGCAGCAGUGGGGAAUCGUGGGCAAUGGGCGAAAGCCUGACCCCGCGACGCCGCGUGGGGGAAGAAGCCCUGCGGGGUGUAAACCCCUGUCGGGGGGGACGAAGCGGCUAUGGGUUAAUAGCCCAUAGUCGUGACGGUACCCCCAGAGGAAGGGACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUCCCGAGCGUUGCGCGAAGUCACUGGGCGUAAAGCGUCCGCAGCCGGUCGGGUAAGCGGGAUGUCAAAGCCCACGGCUCAACCGUGGAAUGGCAUCCCGAACUGCCCGACUUGAGGCACGCCCGGGCAGGCGGAAUUCCCGGGGUAGCGGUGAAAUGCGUAGAUCUCGGGAGGAACACCGAAGGGGAAGCCAGCCUGCUGGGGCUGUCCUGACGGUCAGGGACGAAAGCCGGGGGAGCAAACCGGAUUAGAUACCCGGGUAGUCCCGGCCGUAAACCAUGGGCGCUAGGGCUUGUCCCUUUGGGGCAGGCUCGCAGCUAACGCGUUAAGCGCCCCGCCUGGGGAGUACGGGCGCAAGCCUGAAACUCAAAGGAAUUGGCGGGGGCCCGCACAACCGGUGGAGCGUCUGGUUCAAUUCGAUGCUAACCGAAGAACCUUACCCGGGCUUGACAUGCUGGGGAGACUCCGCGAAAGCGGAGUUGUGGAAGUCCCUAGGACUUCCCCCCAGCACAGGUGGUGCAUGGCCGUCGUCAGCUCGUGUCGUGAGA
[...(((.].....((((.((((((.(((((((((....(((.(((..(((..((((((((((((((((....)))))))))).)))))))))......(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....((((((((((.....)))).))))))....((((((...(((((...))))).)))))).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).)).))))))..((((......((((....)))).....)))).[.(((((((...(.....(((((((((.....))))))))).....)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).))))))))))..........((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((.....))))))))))...((..]])).....)))))))))).(((......((((....))))....)))...))).........................................((((........))))....................(((((((((..(((((((....)))))))...((((((((...))))))))))))))))).............................((....))
EEEESSSMMMMMMMSSSSBSSSSSSMSSSSSSSSSIIIISSSBSSSMMSSSBBSSSSSSSSSSSSSSSSHHHHSSSSSSSSSSBSSSSSSSSSMMMMMMSSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSSSSHHHHHSSSSBSSSSSSMMMMSSSSSSIIISSSSSHHHSSSSSISSSSSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSBSSSSSSMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMMMSSSSSSSIIISIIIIISSSSSSSSSHHHHHSSSSSSSSSIIIIISIIIISSSSSSBBBSMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMSSSSSSSSSSMMMMMMMMMMSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSHHHHHSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSMSSSIIIIIISSSSHHHHSSSSIIIISSSMMMSSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSSSHHHHHHHHSSSSMMMMMMMMMMMMMMMMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMMMSSSSSSSSHHHSSSSSSSSSSSSSSSSSMMMMMMMMMMMMMMMMMMMMMMMMMMMMMSSHHHHSS
KNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 5..7 "UCC" 924..926 "GGA"
S2 15..18 "GCGC" 565..568 "GCGC"
S3 20..25 "AACGCU" 559..564 "AGCGUU"
S4 27..32 "GCGGCG" 409..414 "CGCCGC"
S5 33..34 "UG" 406..407 "CG"
S6 35..35 "C" 372..372 "G"
S7 40..42 "CAC" 368..370 "GUG"
S8 44..46 "UGC" 365..367 "GCA"
S9 49..51 "GUC" 91..93 "GGC"
S10 54..59 "GCGCAG" 85..90 "CUGAGC"
S11 60..69 "GCUCGCUCCC" 74..83 "GGGAGCGGGU"
S12 100..102 "GUG" 323..325 "CAC"
S13 109..116 "ACGUGGGU" 243..250 "GCCCGCGG"
S14 119..120 "CC" 241..242 "GG"
S15 124..130 "CCCCAGG" 232..238 "CCUGGGG"
S16 132..135 "GGGG" 163..166 "CCCC"
S17 140..146 "ACCCCGG" 151..157 "CCGGGGC"
S18 172..177 "GCCACC" 192..197 "GGUGGC"
S19 178..181 "CGCC" 187..190 "GGCG"
S20 202..207 "GGGGGC" 225..230 "GCUCCC"
S21 211..215 "UGGCC" 219..223 "GGCCA"
S22 251..253 "CCC" 295..297 "GGG"
S23 256..256 "C" 294..294 "C"
S24 258..260 "GGU" 286..288 "GCC"
S25 263..270 "UUGGUGGG" 278..285 "CCCACCAA"
S26 300..303 "GCCG" 319..322 "CGGC"
S27 304..307 "GCCU" 312..315 "GGGU"
S28 327..332 "GCGGGA" 343..348 "GCCCGC"
S29 350..353 "CCCC" 358..361 "GGGG"
S30 378..378 "G" 404..404 "C"
S31 380..382 "GGG" 401..403 "CCC"
S32 386..390 "UGGGC" 395..399 "GCCUG"
S33 417..420 "GGGG" 444..447 "CCCC"
S34 427..430 "GCCC" 435..438 "GGGU"
S35 451..451 "C" 509..509 "G"
S36 452..457 "GGGGGG" 500..505 "CCCCCA"
S37 461..461 "G" 495..495 "C"
S38 467..475 "GCUAUGGGU" 481..489 "GCCCAUAGU"
S39 512..516 "GGGAC" 553..557 "GUCCC"
S40 523..527 "CUACG" 548..552 "CGUAG"
S41 533..534 "GC" 539..540 "GC"
S42 579..581 "GGC" 889..891 "GCC"
S43 587..587 "G" 888..888 "C"
S44 589..592 "GUCC" 773..776 "GGAC"
S45 594..594 "C" 770..770 "C"
S46 596..598 "GCC" 767..769 "GGU"
S47 600..606 "GUCGGGU" 657..663 "GCCCGAC"
S48 608..609 "AG" 655..656 "CU"
S49 610..618 "CGGGAUGUC" 644..652 "GGCAUCCCG"
S50 623..629 "CCCACGG" 635..641 "CCGUGGA"
S51 667..669 "AGG" 761..763 "CCU"
S52 670..675 "CACGCC" 754..759 "GGGCUG"
S53 678..684 "GGCAGGC" 746..752 "GCCUGCU"
S54 685..686 "GG" 728..729 "AC"
S55 689..696 "UUCCCGGG" 718..725 "CUCGGGAG"
S56 700..702 "GCG" 709..711 "UGC"
S57 734..734 "A" 745..745 "A"
S58 737..738 "GG" 743..744 "CC"
S59 781..786 "GCCGGG" 817..822 "CCCGGC"
S60 790..791 "GC" 815..816 "GU"
S61 795..798 "CCGG" 808..811 "CCGG"
S62 833..838 "GGGCGC" 882..887 "GCGCCC"
S63 841..850 "GGGCUUGUCC" 856..865 "GGGCAGGCUC"
S64 869..870 "GC" 875..876 "GC"
S65 893..895 "GGG" 918..920 "CUC"
S66 902..905 "GGGC" 910..913 "GCCU"
S67 968..971 "UUCG" 980..983 "CGAA"
S68 1004..1012 "AUGCUGGGG" 1055..1063 "CCCCAGCAC"
S69 1015..1021 "ACUCCGC" 1026..1032 "GCGGAGU"
S70 1036..1043 "GGAAGUCC" 1047..1054 "GGACUUCC"
S71 1093..1094 "UC" 1099..1100 "GA"
H1 70..73 "UUUU" (69,74) C:G 
H2 147..150 "GAAA" (146,151) G:C 
H3 182..186 "ACUAA" (181,187) C:G 
H4 216..218 "UUU" (215,219) C:G 
H5 271..277 "GUAACGG" (270,278) G:C 
H6 308..311 "GAGA" (307,312) U:G 
H7 333..342 "CUGAGACACG" (332,343) A:G 
H8 354..357 "UACG" (353,358) C:G 
H9 391..394 "GAAA" (390,395) C:G 
H10 431..434 "UGCG" (430,435) C:G 
H11 476..480 "UAAUA" (475,481) U:G 
H12 535..538 "AGCC" (534,539) C:G PK{3}
H13 630..634 "CUCAA" (629,635) G:C 
H14 703..708 "GUGAAA" (702,709) G:U 
H15 739..742 "GAAG" (738,743) G:C 
H16 799..807 "AUUAGAUAC" (798,808) G:C 
H17 851..855 "CUUUG" (850,856) C:G 
H18 871..874 "UAAC" (870,875) C:G PK{4}
H19 906..909 "GCAA" (905,910) C:G 
H20 972..979 "AUGCUAAC" (971,980) G:C 
H21 1022..1025 "GAAA" (1021,1026) C:G 
H22 1044..1046 "CUA" (1043,1047) C:G 
H23 1095..1098 "GUGA" (1094,1099) C:G 
B1 19..19 "G" (18,565) C:G (564,20) U:A 
B2 43..43 "A" (42,368) C:G (367,44) A:U 
B3 52..53 "GU" (51,91) C:G (90,54) C:G 
B4 84..84 "G" (83,60) U:G (59,85) G:C 
B5 117..118 "AA" (116,243) U:G (242,119) G:C 
B6 191..191 "U" (190,178) G:C (177,192) C:G 
B7 254..255 "AU" (253,295) C:G (294,256) C:C 
B8 261..262 "AG" (260,286) U:G (285,263) A:U 
B9 316..318 "GGC" (315,304) U:G (303,319) G:C 
B10 379..379 "U" (378,404) G:C (403,380) C:G 
B11 408..408 "A" (407,33) G:U (32,409) G:C 
B12 506..508 "GAG" (505,452) A:G (451,509) C:G PK{2}
B13 517..522 "GGCUAA" (516,553) C:G (552,523) G:C PK{3}
B14 582..586 "GUAAA" (581,889) C:G (888,587) C:G PK{4}
B15 595..595 "A" (594,770) C:C (769,596) U:G 
B16 607..607 "A" (606,657) U:G (656,608) U:A 
B17 653..654 "AA" (652,610) G:C (609,655) G:C 
B18 735..736 "AG" (734,745) A:A (744,737) C:G 
B19 760..760 "U" (759,670) G:C (669,761) G:C 
B20 787..789 "GGA" (786,817) G:C (816,790) U:G 
I1.1 36..39 "CUAA" (35,372) C:G 
I1.2 371..371 "G" (370,40) G:C 
I2.1 121..123 "UAC" (120,241) C:G 
I2.2 239..240 "AU" (238,124) G:C 
I3.1 136..139 "GAUA" (135,163) G:C 
I3.2 158..162 "UAAUA" (157,140) C:A 
I4.1 208..210 "CUC" (207,225) C:G 
I4.2 224..224 "U" (223,211) A:U 
I5.1 257..257 "A" (256,294) C:C 
I5.2 289..293 "UAUGA" (288,258) C:G 
I6.1 383..385 "CAA" (382,401) G:C 
I6.2 400..400 "A" (399,386) G:U 
I7.1 421..426 "GAAGAA" (420,444) G:C 
I7.2 439..443 "GUAAA" (438,427) U:G 
I8.1 458..460 "GAC" (457,500) G:C 
I8.2 496..499 "GGUA" (495,461) C:G 
I9.1 462..466 "AAGCG" (461,495) G:C 
I9.2 490..494 "CGUGA" (489,467) U:G 
I10.1 528..532 "UGCCA" (527,548) G:C 
I10.2 541..547 "GGUAAUA" (540,533) C:G 
I11.1 593..593 "G" (592,773) C:G 
I11.2 771..772 "AG" (770,594) C:C 
I12.1 619..622 "AAAG" (618,644) C:G 
I12.2 642..643 "AU" (641,623) A:C 
I13.1 676..677 "CG" (675,754) C:G 
I13.2 753..753 "G" (752,678) U:G 
I14.1 687..688 "AA" (686,728) G:A 
I14.2 726..727 "GA" (725,689) G:U 
I15.1 697..699 "GUA" (696,718) G:C 
I15.2 712..717 "GUAGAU" (711,700) C:G 
I16.1 792..794 "AAA" (791,815) C:G 
I16.2 812..814 "GUA" (811,795) G:C 
I17.1 896..901 "GAGUAC" (895,918) G:C 
I17.2 914..917 "GAAA" (913,902) U:G 
M1.1 8..14 "UGGCUCA" (7,924) C:G (568,15) C:G PK{1}
M1.2 569..578 "GAAGUCACUG" (568,15) C:G (891,579) C:G 
M1.3 892..892 "U" (891,579) C:G (920,893) C:G 
M1.4 921..923 "AAA" (920,893) C:G (7,924) C:G 
M2.1 26..26 "G" (25,559) U:A (414,27) C:G 
M2.2 415..416 "GU" (414,27) C:G (447,417) C:G 
M2.3 448..450 "UGU" (447,417) C:G (509,451) G:C PK{2}
M2.4 510..511 "AA" (509,451) G:C (557,512) C:G 
M2.5 558..558 "G" (557,512) C:G (25,559) U:A 
M3.1 35..34 "" (34,406) G:C (372,35) G:C 
M3.2 373..377 "GAAUC" (372,35) G:C (404,378) C:G 
M3.3 405..405 "G" (404,378) C:G (34,406) G:C 
M4.1 47..48 "AA" (46,365) C:G (93,49) C:G 
M4.2 94..99 "AAACGG" (93,49) C:G (325,100) C:G 
M4.3 326..326 "A" (325,100) C:G (348,327) C:G 
M4.4 349..349 "A" (348,327) C:G (361,350) G:C 
M4.5 362..364 "GCA" (361,350) G:C (46,365) C:G 
M5.1 103..108 "AGUAAC" (102,323) G:C (250,109) G:A 
M5.2 251..250 "" (250,109) G:A (297,251) G:C 
M5.3 298..299 "UA" (297,251) G:C (322,300) C:G 
M5.4 323..322 "" (322,300) C:G (102,323) G:C 
M6.1 131..131 "A" (130,232) G:C (166,132) C:G 
M6.2 167..171 "AUAAU" (166,132) C:G (197,172) C:G 
M6.3 198..201 "CAAA" (197,172) C:G (230,202) C:G 
M6.4 231..231 "G" (230,202) C:G (130,232) G:C 
M7.1 588..588 "C" (587,888) G:C (776,589) C:G 
M7.2 777..780 "GAAA" (776,589) C:G (822,781) C:G 
M7.3 823..832 "CGUAAACCAU" (822,781) C:G (887,833) C:G 
M7.4 888..887 "" (887,833) C:G (587,888) G:C 
M8.1 599..599 "G" (598,767) C:G (663,600) C:G 
M8.2 664..666 "UUG" (663,600) C:G (763,667) U:A 
M8.3 764..766 "GAC" (763,667) U:A (598,767) C:G 
M9.1 685..684 "" (684,746) C:G (729,685) C:G 
M9.2 730..733 "ACCG" (729,685) C:G (745,734) A:A 
M9.3 746..745 "" (745,734) A:A (684,746) C:G 
M10.1 839..840 "UA" (838,882) C:G (865,841) C:G 
M10.2 866..868 "GCA" (865,841) C:G (876,869) C:G 
M10.3 877..881 "GUUAA" (876,869) C:G (838,882) C:G 
M11.1 927..967 "AUUGGCGGGGGCCCGCACAACCGGUGGAGCGUCUGGUUCAA" (926,5) A:U (983,968) A:U 
M11.2 984..1003 "GAACCUUACCCGGGCUUGAC" (983,968) A:U (1063,1004) C:A 
M11.3 1064..1092 "AGGUGGUGCAUGGCCGUCGUCAGCUCGUG" (1063,1004) C:A (1100,1093) A:U 
M12.1 1013..1014 "AG" (1012,1055) G:C (1032,1015) U:A 
M12.2 1033..1035 "UGU" (1032,1015) U:A (1054,1036) C:G 
M12.3 1055..1054 "" (1054,1036) C:G (1012,1055) G:C 
E1 1..4 "UUGA" PK{1}
PK1 1bp 1..1 9..9 E1 1..4 M1.1 8..14
PK2 1bp 449..449 508..508 M2.3 448..450 B12 506..508
PK3 3bp 517..519 536..538 B13 517..522 H12 535..538
PK4 2bp 582..583 873..874 B14 582..586 H18 871..874
PK1.1 1 U 9 G
PK2.1 449 G 508 G
PK3.1 517 G 538 C
PK3.2 518 G 537 C
PK3.3 519 C 536 G
PK4.1 582 G 874 C
PK4.2 583 U 873 A
NCBP1 594 C 770 C S45
NCBP2 686 G 728 A S54
NCBP3 452 G 505 A S36
NCBP4 734 A 745 A S57
NCBP5 109 A 250 G S13
NCBP6 672 C 757 C S52
NCBP7 256 C 294 C S23
NCBP8 332 A 343 G S28
NCBP9 1004 A 1063 C S68
NCBP10 140 A 157 C S17
NCBP11 673 G 756 G S52
NCBP12 623 C 641 A S50
NCBP13 56 G 88 A S10
NCBP14 449 G 508 G PK2.1
segment1 3bp 5..7 UCC 924..926 GGA
segment2 10bp 15..25 GCGCGAACGCU 559..568 AGCGUUGCGC
segment3 8bp 27..34 GCGGCGUG 406..414 CGACGCCGC
segment4 7bp 35..46 CCUAACACAUGC 365..372 GCAGUGGG
segment5 19bp 49..69 GUCGUGCGCAGGCUCGCUCCC 74..93 GGGAGCGGGUGCUGAGCGGC
segment6 3bp 100..102 GUG 323..325 CAC
segment7 17bp 109..130 ACGUGGGUAACCUACCCCCAGG 232..250 CCUGGGGAUGGGCCCGCGG
segment8 11bp 132..146 GGGGGAUAACCCCGG 151..166 CCGGGGCUAAUACCCC
segment9 10bp 172..181 GCCACCCGCC 187..197 GGCGUGGUGGC
segment10 11bp 202..215 GGGGGCCUCUGGCC 219..230 GGCCAUGCUCCC
segment11 15bp 251..270 CCCAUCAGGUAGUUGGUGGG 278..297 CCCACCAAGCCUAUGACGGG
segment12 8bp 300..307 GCCGGCCU 312..322 GGGUGGCCGGC
segment13 6bp 327..332 GCGGGA 343..348 GCCCGC
segment14 4bp 350..353 CCCC 358..361 GGGG
segment15 9bp 378..390 GUGGGCAAUGGGC 395..404 GCCUGACCCC
segment16 8bp 417..430 GGGGGAAGAAGCCC 435..447 GGGUGUAAACCCC
segment17 17bp 451..475 CGGGGGGGACGAAGCGGCUAUGGGU 481..509 GCCCAUAGUCGUGACGGUACCCCCAGAGG
segment18 12bp 512..534 GGGACGGCUAACUACGUGCCAGC 539..557 GCGGUAAUACGUAGGUCCC
segment19 4bp 579..587 GGCGUAAAG 888..891 CGCC
segment20 8bp 589..598 GUCCGCAGCC 767..776 GGUCAGGGAC
segment21 25bp 600..629 GUCGGGUAAGCGGGAUGUCAAAGCCCACGG 635..663 CCGUGGAAUGGCAUCCCGAACUGCCCGAC
segment22 16bp 667..684 AGGCACGCCCGGGCAGGC 746..763 GCCUGCUGGGGCUGUCCU
segment23 13bp 685..702 GGAAUUCCCGGGGUAGCG 709..729 UGCGUAGAUCUCGGGAGGAAC
segment24 3bp 734..738 AAGGG 743..745 CCA
segment25 12bp 781..798 GCCGGGGGAGCAAACCGG 808..822 CCGGGUAGUCCCGGC
segment26 6bp 833..838 GGGCGC 882..887 GCGCCC
segment27 10bp 841..850 GGGCUUGUCC 856..865 GGGCAGGCUC
segment28 2bp 869..870 GC 875..876 GC
segment29 7bp 893..905 GGGGAGUACGGGC 910..920 GCCUGAAACUC
segment30 4bp 968..971 UUCG 980..983 CGAA
segment31 9bp 1004..1012 AUGCUGGGG 1055..1063 CCCCAGCAC
segment32 7bp 1015..1021 ACUCCGC 1026..1032 GCGGAGU
segment33 8bp 1036..1043 GGAAGUCC 1047..1054 GGACUUCC
segment34 2bp 1093..1094 UC 1099..1100 GA
