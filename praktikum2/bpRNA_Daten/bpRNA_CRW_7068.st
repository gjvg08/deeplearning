#Name: bpRNA_CRW_7068
#Length:  1097 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
GGCUUAGAGUUUGAUCAUGGCUCAGAGCGAACGUUGGCGGCAGGCUUAACACAUGCAAGUCGAGCGGGCAUAGCAAUAUGUCAGCGGCAGACGGGUGAGUAACGCGUGGGAACGUACCUUUUGGUUCGGAACAACACAGGGAAACUUGUGCUAAUACCGGAUAAGCCCUUACGGGGAAAGAUUUAUCGCCGAAAGAUCGGCCCGCGUCUGAUUAGCUAGUUGGUGAGGUAAUGGCUCACCAAGGCGACGAUCAGUAGCUGGUCUGAGAGGAUGAUCAGCCACAUUGGGACUGAGACACGGCCCAAACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGGACAAUGGGCGCAAGCUGAUCCAGCCAUGCCGCGUGAGUGAUGAAGGCCCUAGGGUUGUAAAGCUYYUUUGUGCGGGAAGAUAAUGACGGUACCGCAAGAAUAAGCCCCGGCUAACUUCGUGCCAGCAGCCGCGGUAAUACGAAGGGGGCUAGCGUUGCUCGGAAUCACUGGGCGUAAAGGGUGCGUAGGCGGGUCUUUAAGUCAGGGGUGAAAUCCUGGAGCUCAACUCCAGAACUGCCUUUGAUACUGAAGAUCUUGAGUUCGGGAGAGGUGAGUGGAACUGCGAGUGUAGAGGUGAAAUUCGUAGAUAUUCGCAAGAACACCAGUGGCGAAGGCGGCUCACUGGCCCGAUACUGACGCUGAGGCACGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAAUGCCAGCCGUUAGUGGGUUUACUCACUAGUGGCGCAGCUAACGCUUUAAGCAUUCCGCCUGGGGAGUACGGUCGCAAGAUUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGKKUAAUUCGACGCAACGCGCAGAACCUUACCAGCCCUUGACAUCCCGGUCGCGGACUCCAGAGACGGAGUUCUUCAGUUCGGCUGGACCGGAGACAGGUGCUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUUAGUCCGCAACGAGCGCAACCCCGUCCCUUAGUUGCUACCAUUUAGUUGAGCACUCUAAGG
......(((((...[[[.))))).((((.((((((.(((((((((....(((.(((..(((..(((((((((....))))))))))))......(((......((((((((.((...(((((((.((((....(((((((....))))))).....)))).....(((....)))....((....)).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((.(....)))).)))).)).))))))..((((......((((....)))).....)))).[.(((((((...(.......)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).))))))))))..........((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((....))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))...]]].........................................((((........))))....................(((((((.....(((((((....)))).)))..(((.....)))..)))).)))......................(((((((((....)))..(.((......)))..))))))...........((((((...((..((......))))....))))))
EEEEEESSSSSHHHHHHHSSSSSXSSSSBSSSSSSMSSSSSSSSSIIIISSSBSSSMMSSSBBSSSSSSSSSHHHHSSSSSSSSSSSSMMMMMMSSSMMMMMMSSSSSSSSBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSHHHHSSSMMMMSSHHHHSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSBSHHHHSSSSISSSSMSSBSSSSSSMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMMMSSSSSSSIIISHHHHHHHSIIIISSSSSSBBBSMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMSSSSSSSSSSXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSHHHHHHHHSSSSXXXXXXXXXXXXXXXXXXXXSSSSSSSMMMMMSSSSSSSHHHHSSSSBSSSMMSSSHHHHHSSSMMSSSSBSSSXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSHHHHSSSMMSBSSHHHHHHSSSMMSSSSSSXXXXXXXXXXXSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSS
NNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 7..11 "GAGUU" 19..23 "GGCUC"
S2 25..28 "GAGC" 495..498 "GCUC"
S3 30..35 "AACGUU" 489..494 "AGCGUU"
S4 37..42 "GCGGCA" 365..370 "UGCCGC"
S5 43..44 "GG" 362..363 "CC"
S6 45..45 "C" 329..329 "G"
S7 50..52 "CAC" 325..327 "GUG"
S8 54..56 "UGC" 322..324 "GCA"
S9 59..61 "GUC" 86..88 "GGC"
S10 64..72 "GCGGGCAUA" 77..85 "UAUGUCAGC"
S11 95..97 "GUG" 280..282 "CAC"
S12 104..111 "GCGUGGGA" 200..207 "GCCCGCGU"
S13 113..114 "CG" 198..199 "CG"
S14 118..124 "CUUUUGG" 189..195 "CCGAAAG"
S15 126..129 "UCGG" 157..160 "CCGG"
S16 134..140 "ACACAGG" 145..151 "CUUGUGC"
S17 166..168 "CCC" 173..175 "GGG"
S18 180..181 "GA" 186..187 "UC"
S19 208..210 "CUG" 252..254 "CAG"
S20 213..213 "U" 251..251 "U"
S21 215..217 "GCU" 243..245 "GGC"
S22 220..227 "UUGGUGAG" 235..242 "CUCACCAA"
S23 257..260 "GCUG" 276..279 "CAGC"
S24 261..264 "GUCU" 269..272 "GGAU"
S25 284..289 "UUGGGA" 300..305 "GCCCAA"
S26 307..310 "CUCC" 315..318 "GGAG"
S27 335..335 "U" 360..360 "A"
S28 337..339 "GGA" 357..359 "UCC"
S29 343..345 "UGG" 353..355 "CUG"
S30 347..347 "C" 352..352 "G"
S31 373..376 "GAGU" 400..403 "GCUY"
S32 383..386 "GGCC" 391..394 "GGUU"
S33 407..407 "U" 439..439 "U"
S34 408..413 "GUGCGG" 430..435 "CCGCAA"
S35 417..417 "G" 425..425 "C"
S36 442..446 "GCCCC" 483..487 "GGGGC"
S37 453..457 "CUUCG" 478..482 "CGAAG"
S38 463..464 "GC" 469..470 "GC"
S39 509..511 "GGC" 822..824 "GCC"
S40 517..517 "G" 821..821 "C"
S41 519..522 "GUGC" 703..706 "GCAC"
S42 524..524 "U" 700..700 "G"
S43 526..528 "GGC" 697..699 "GCU"
S44 530..536 "GGUCUUU" 587..593 "GAAGAUC"
S45 538..539 "AG" 585..586 "CU"
S46 540..548 "UCAGGGGUG" 574..582 "UGCCUUUGA"
S47 553..559 "CCUGGAG" 565..571 "CUCCAGA"
S48 597..599 "AGU" 691..693 "ACU"
S49 600..605 "UCGGGA" 684..689 "GCCCGA"
S50 608..614 "GGUGAGU" 676..682 "GCUCACU"
S51 615..616 "GG" 658..659 "AC"
S52 619..626 "CUGCGAGU" 648..655 "AUUCGCAA"
S53 630..632 "GAG" 639..641 "UUC"
S54 664..664 "G" 675..675 "G"
S55 667..668 "GC" 673..674 "GC"
S56 711..716 "GCGUGG" 747..752 "CCACGC"
S57 720..721 "GC" 745..746 "GU"
S58 725..728 "CAGG" 738..741 "CCUG"
S59 763..768 "GAAUGC" 815..820 "GCAUUC"
S60 771..782 "GCCGUUAGUGGG" 787..798 "CUCACUAGUGGC"
S61 802..803 "GC" 808..809 "GC"
S62 826..828 "GGG" 851..853 "CUC"
S63 835..838 "GGUC" 843..846 "GAUU"
S64 901..904 "UUCG" 913..916 "CGCA"
S65 937..939 "AUC" 988..990 "GAC"
S66 940..943 "CCGG" 983..986 "CCGG"
S67 949..951 "GAC" 965..967 "UUC"
S68 952..955 "UCCA" 960..963 "CGGA"
S69 970..972 "CAG" 978..980 "CUG"
S70 1013..1018 "GCUCGU" 1046..1051 "ACGAGC"
S71 1019..1021 "GUC" 1026..1028 "GAU"
S72 1031..1031 "U" 1043..1043 "G"
S73 1033..1034 "GG" 1041..1042 "CC"
S74 1063..1068 "CCUUAG" 1092..1097 "CUAAGG"
S75 1072..1073 "CU" 1086..1087 "AG"
S76 1076..1077 "CA" 1084..1085 "UG"
H1 12..18 "UGAUCAU" (11,19) U:G PK{1}
H2 73..76 "GCAA" (72,77) A:U 
H3 141..144 "GAAA" (140,145) G:C 
H4 169..172 "UUAC" (168,173) C:G 
H5 182..185 "UUUA" (181,186) A:U 
H6 228..234 "GUAAUGG" (227,235) G:C 
H7 265..268 "GAGA" (264,269) U:G 
H8 290..299 "CUGAGACACG" (289,300) A:G 
H9 311..314 "UACG" (310,315) C:G 
H10 348..351 "GCAA" (347,352) C:G 
H11 387..390 "CUAG" (386,391) C:G 
H12 418..424 "AUAAUGA" (417,425) G:C 
H13 465..468 "AGCC" (464,469) C:G PK{3}
H14 560..564 "CUCAA" (559,565) G:C 
H15 633..638 "GUGAAA" (632,639) G:U 
H16 669..672 "GAAG" (668,673) C:G 
H17 729..737 "AUUAGAUAC" (728,738) G:C 
H18 783..786 "UUUA" (782,787) G:C 
H19 804..807 "UAAC" (803,808) C:G PK{4}
H20 839..842 "GCAA" (838,843) C:G 
H21 905..912 "ACGCAACG" (904,913) G:C 
H22 956..959 "GAGA" (955,960) A:C 
H23 973..977 "UUCGG" (972,978) G:C 
H24 1022..1025 "GUGA" (1021,1026) C:G 
H25 1035..1040 "UUUAGU" (1034,1041) G:C 
H26 1078..1083 "UUUAGU" (1077,1084) A:U 
B1 29..29 "G" (28,495) C:G (494,30) U:A 
B2 53..53 "A" (52,325) C:G (324,54) A:U 
B3 62..63 "GA" (61,86) C:G (85,64) C:G 
B4 112..112 "A" (111,200) A:G (199,113) G:C 
B5 211..212 "AU" (210,252) G:C (251,213) U:U 
B6 218..219 "AG" (217,243) U:G (242,220) A:U 
B7 273..275 "GAU" (272,261) U:G (260,276) G:C 
B8 336..336 "U" (335,360) U:A (359,337) C:G 
B9 346..346 "G" (345,353) G:C (352,347) G:C 
B10 364..364 "A" (363,43) C:G (42,365) A:U 
B11 436..438 "GAA" (435,408) A:G (407,439) U:U PK{2}
B12 447..452 "GGCUAA" (446,483) C:G (482,453) G:C PK{3}
B13 512..516 "GUAAA" (511,822) C:G (821,517) C:G PK{4}
B14 525..525 "A" (524,700) U:G (699,526) U:G 
B15 537..537 "A" (536,587) U:G (586,538) U:A 
B16 583..584 "UA" (582,540) A:U (539,585) G:C 
B17 665..666 "UG" (664,675) G:G (674,667) C:G 
B18 690..690 "U" (689,600) A:U (599,691) U:A 
B19 717..719 "GGA" (716,747) G:C (746,720) U:G 
B20 964..964 "G" (963,952) A:U (951,965) C:U 
B21 987..987 "A" (986,940) G:C (939,988) C:G 
B22 1032..1032 "G" (1031,1043) U:G (1042,1033) C:G 
B23 1074..1075 "AC" (1073,1086) U:A (1085,1076) G:C 
I1.1 46..49 "UUAA" (45,329) C:G 
I1.2 328..328 "G" (327,50) G:C 
I2.1 115..117 "UAC" (114,198) G:C 
I2.2 196..197 "AU" (195,118) G:C 
I3.1 130..133 "AACA" (129,157) G:C 
I3.2 152..156 "UAAUA" (151,134) C:A 
I4.1 214..214 "A" (213,251) U:U 
I4.2 246..250 "GACGA" (245,215) C:G 
I5.1 340..342 "CAA" (339,357) A:U 
I5.2 356..356 "A" (355,343) G:U 
I6.1 377..382 "GAUGAA" (376,400) U:G 
I6.2 395..399 "GUAAA" (394,383) U:G 
I7.1 414..416 "GAA" (413,430) G:C 
I7.2 426..429 "GGUA" (425,417) C:G 
I8.1 458..462 "UGCCA" (457,478) G:C 
I8.2 471..477 "GGUAAUA" (470,463) C:G 
I9.1 523..523 "G" (522,703) C:G 
I9.2 701..702 "AG" (700,524) G:U 
I10.1 549..552 "AAAU" (548,574) G:U 
I10.2 572..573 "AC" (571,553) A:C 
I11.1 606..607 "GA" (605,684) A:G 
I11.2 683..683 "G" (682,608) U:G 
I12.1 617..618 "AA" (616,658) G:A 
I12.2 656..657 "GA" (655,619) A:C 
I13.1 627..629 "GUA" (626,648) U:A 
I13.2 642..647 "GUAGAU" (641,630) C:G 
I14.1 722..724 "AAA" (721,745) C:G 
I14.2 742..744 "GUA" (741,725) G:C 
I15.1 829..834 "GAGUAC" (828,851) G:C 
I15.2 847..850 "AAAA" (846,835) U:G 
I16.1 1069..1071 "UUG" (1068,1092) G:C 
I16.2 1088..1091 "CACU" (1087,1072) G:C 
M1.1 36..36 "G" (35,489) U:A (370,37) C:G 
M1.2 371..372 "GU" (370,37) C:G (403,373) Y:G 
M1.3 404..406 "YUU" (403,373) Y:G (439,407) U:U PK{2}
M1.4 440..441 "AA" (439,407) U:U (487,442) C:G 
M1.5 488..488 "U" (487,442) C:G (35,489) U:A 
M2.1 45..44 "" (44,362) G:C (329,45) G:C 
M2.2 330..334 "GAAUA" (329,45) G:C (360,335) A:U 
M2.3 361..361 "G" (360,335) A:U (44,362) G:C 
M3.1 57..58 "AA" (56,322) C:G (88,59) C:G 
M3.2 89..94 "AGACGG" (88,59) C:G (282,95) C:G 
M3.3 283..283 "A" (282,95) C:G (305,284) A:U 
M3.4 306..306 "A" (305,284) A:U (318,307) G:C 
M3.5 319..321 "GCA" (318,307) G:C (56,322) C:G 
M4.1 98..103 "AGUAAC" (97,280) G:C (207,104) U:G 
M4.2 208..207 "" (207,104) U:G (254,208) G:C 
M4.3 255..256 "UA" (254,208) G:C (279,257) C:G 
M4.4 280..279 "" (279,257) C:G (97,280) G:C 
M5.1 125..125 "U" (124,189) G:C (160,126) G:U 
M5.2 161..165 "AUAAG" (160,126) G:U (175,166) G:C 
M5.3 176..179 "GAAA" (175,166) G:C (187,180) C:G 
M5.4 188..188 "G" (187,180) C:G (124,189) G:C 
M6.1 518..518 "G" (517,821) G:C (706,519) C:G 
M6.2 707..710 "GAAA" (706,519) C:G (752,711) C:G 
M6.3 753..762 "CGUAAACGAU" (752,711) C:G (820,763) C:G 
M6.4 821..820 "" (820,763) C:G (517,821) G:C 
M7.1 529..529 "G" (528,697) C:G (593,530) C:G 
M7.2 594..596 "UUG" (593,530) C:G (693,597) U:A 
M7.3 694..696 "GAC" (693,597) U:A (528,697) C:G 
M8.1 615..614 "" (614,676) U:G (659,615) C:G 
M8.2 660..663 "ACCA" (659,615) C:G (675,664) G:G 
M8.3 676..675 "" (675,664) G:G (614,676) U:G 
M9.1 769..770 "CA" (768,815) C:G (798,771) C:G 
M9.2 799..801 "GCA" (798,771) C:G (809,802) C:G 
M9.3 810..814 "UUUAA" (809,802) C:G (768,815) C:G 
M10.1 944..948 "UCGCG" (943,983) G:C (967,949) C:G 
M10.2 968..969 "UU" (967,949) C:G (980,970) G:C 
M10.3 981..982 "GA" (980,970) G:C (943,983) G:C 
M11.1 1019..1018 "" (1018,1046) U:A (1028,1019) U:G 
M11.2 1029..1030 "GU" (1028,1019) U:G (1043,1031) G:U 
M11.3 1044..1045 "CA" (1043,1031) G:U (1018,1046) U:A 
X2 499..508 "GGAAUCACUG" (498,25) C:G (824,509) C:G 
X4 854..900 "AAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGKKUAA" (853,826) C:G (916,901) A:U PK{1}
X5 917..936 "GAACCUUACCAGCCCUUGAC" (916,901) A:U (990,937) C:A 
X6 991..1012 "AGGUGCUGCAUGGCUGUCGUCA" (990,937) C:A (1051,1013) C:G 
X7 1052..1062 "GCAACCCCGUC" (1051,1013) C:G (1097,1063) G:C 
E1 1..6 "GGCUUA" 
PK1 3bp 15..17 857..859 H1 12..18 X4 854..900
PK2 1bp 405..405 438..438 M1.3 404..406 B11 436..438
PK3 3bp 447..449 466..468 B12 447..452 H13 465..468
PK4 2bp 512..513 806..807 B13 512..516 H19 804..807
PK1.1 15 U 859 A
PK1.2 16 C 858 G
PK1.3 17 A 857 G
PK2.1 405 U 438 A
PK3.1 447 G 468 C
PK3.2 448 G 467 C
PK3.3 449 C 466 G
PK4.1 512 G 807 C
PK4.2 513 U 806 A
NCBP1 373 G 403 Y S31
NCBP2 605 A 684 G S49
NCBP3 213 U 251 U S20
NCBP4 664 G 675 G S54
NCBP5 951 C 965 U S67
NCBP6 937 A 990 C S65
NCBP7 553 C 571 A S47
NCBP8 619 C 655 A S52
NCBP9 902 U 915 C S64
NCBP10 616 G 658 A S51
NCBP11 955 A 960 C S68
NCBP12 407 U 439 U S33
NCBP13 289 A 300 G S25
NCBP14 66 G 83 A S10
NCBP15 134 A 151 C S16
NCBP16 111 A 200 G S12
NCBP17 408 G 435 A S34
NCBP18 17 A 857 G PK1.3
segment1 5bp 7..11 GAGUU 19..23 GGCUC
segment2 10bp 25..35 GAGCGAACGUU 489..498 AGCGUUGCUC
segment3 8bp 37..44 GCGGCAGG 362..370 CCAUGCCGC
segment4 7bp 45..56 CUUAACACAUGC 322..329 GCAGUGGG
segment5 12bp 59..72 GUCGAGCGGGCAUA 77..88 UAUGUCAGCGGC
segment6 3bp 95..97 GUG 280..282 CAC
segment7 17bp 104..124 GCGUGGGAACGUACCUUUUGG 189..207 CCGAAAGAUCGGCCCGCGU
segment8 11bp 126..140 UCGGAACAACACAGG 145..160 CUUGUGCUAAUACCGG
segment9 3bp 166..168 CCC 173..175 GGG
segment10 2bp 180..181 GA 186..187 UC
segment11 15bp 208..227 CUGAUUAGCUAGUUGGUGAG 235..254 CUCACCAAGGCGACGAUCAG
segment12 8bp 257..264 GCUGGUCU 269..279 GGAUGAUCAGC
segment13 6bp 284..289 UUGGGA 300..305 GCCCAA
segment14 4bp 307..310 CUCC 315..318 GGAG
segment15 8bp 335..347 UUGGACAAUGGGC 352..360 GCUGAUCCA
segment16 8bp 373..386 GAGUGAUGAAGGCC 391..403 GGUUGUAAAGCUY
segment17 8bp 407..417 UGUGCGGGAAG 425..439 CGGUACCGCAAGAAU
segment18 12bp 442..464 GCCCCGGCUAACUUCGUGCCAGC 469..487 GCGGUAAUACGAAGGGGGC
segment19 4bp 509..517 GGCGUAAAG 821..824 CGCC
segment20 8bp 519..528 GUGCGUAGGC 697..706 GCUGAGGCAC
segment21 25bp 530..559 GGUCUUUAAGUCAGGGGUGAAAUCCUGGAG 565..593 CUCCAGAACUGCCUUUGAUACUGAAGAUC
segment22 16bp 597..614 AGUUCGGGAGAGGUGAGU 676..693 GCUCACUGGCCCGAUACU
segment23 13bp 615..632 GGAACUGCGAGUGUAGAG 639..659 UUCGUAGAUAUUCGCAAGAAC
segment24 3bp 664..668 GUGGC 673..675 GCG
segment25 12bp 711..728 GCGUGGGGAGCAAACAGG 738..752 CCUGGUAGUCCACGC
segment26 6bp 763..768 GAAUGC 815..820 GCAUUC
segment27 12bp 771..782 GCCGUUAGUGGG 787..798 CUCACUAGUGGC
segment28 2bp 802..803 GC 808..809 GC
segment29 7bp 826..838 GGGGAGUACGGUC 843..853 GAUUAAAACUC
segment30 4bp 901..904 UUCG 913..916 CGCA
segment31 7bp 937..943 AUCCCGG 983..990 CCGGAGAC
segment32 7bp 949..955 GACUCCA 960..967 CGGAGUUC
segment33 3bp 970..972 CAG 978..980 CUG
segment34 6bp 1013..1018 GCUCGU 1046..1051 ACGAGC
segment35 3bp 1019..1021 GUC 1026..1028 GAU
segment36 3bp 1031..1034 UGGG 1041..1043 CCG
segment37 10bp 1063..1077 CCUUAGUUGCUACCA 1084..1097 UGAGCACUCUAAGG
