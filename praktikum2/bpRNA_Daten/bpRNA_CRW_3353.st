#Name: bpRNA_CRW_3353
#Length:  1202 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
UGGCCUACCAAGGCGACGACGGGUAGCCGGCCUGAGAGGGUGACCGGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGCAAGCCUGAUGCAGCGACGCCGCGUGAGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCAGCACGGAAGAAGCGAAAGUGACGGUACGUGCAGAAGAAGCGCCGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGCGCAAGCGUUGUCCGGAAUUAUUGGGCGUAAAGAGCUCGUAAGCGGUUUGUCGCGUCUGCUGUGAAAGCCCGGGGCUUAACCCCGGGUGUGCAGUGGGUACGGGCAGACUUGAGUGCAGUAGGGGAGAACUGGAACUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAAGAACACCGAUGGCGAAGGCAGGUCUCUGGGCUGUUACUGACGCUGAGGAGCGAAAGCAUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCAUGCCGUAAACGUUGGGCACUAGGUGUGGGGGACAUUCCACGUUUUCCGCGCCGUAGCUAACGCAUUAAGUGCCCCGCCUGGGGAGUACGGCCGCAAGGCUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGCGGAGCAUGCGGAUUAAUUCGAUGCAACGCGAAGAACCUUACCAAGGCUUGACAUACACCGGAUCGGCUCAGAGAUGGGUUUUCCUCCUUGUGGGGCUGGUGUACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUCGUUCUAUGUUGCCAGCACGUGAUGGUGGGGACUCAUAGGAGACUGCCGGGGUCAACUCGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGUCUUGGGCUUCACGCAUGCUACAAUGGCCAGUACAAUGGGUUGCGAUACCGUGAGGUGGAGCUAAUCCCAAAAAGCUGGUCUCAGUUCGGAUCGUGGUCUGCAACUCGACCACGUGAAGUCGGAGUCGCUAGUAAUCGCAGAUCAGCAACGCUGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCAAGUCACGAAAGUUGGUAACACCCGAAGCCGGUGGCCUAACCCUUGUGGUGGCGAUUGGGACUAAGUCGUA
.........................((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(..((....))..)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((.((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.......))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....(((((((((..(((((((....)))))))...((.....))..))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..(((......)))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((...(((((..((...(((....)))...))....)))))...)))))))......(...((((((((........))))))))...).....))))).....((((((((........))))))))......))...)))))))))).))..........................((((((((....((((((.((((..((.....)).))))))))))...))))))))
EEEEEEEEEEEEEEEEEEEEEEEEESSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISIISSHHHHSSIISIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSBSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMMMSSHHHHHSSMMSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSSHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIISSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSIIIISSSSSSBSSSSIISSHHHHHSSISSSSSSSSSSIIISSSSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 26..29 "GCCG" 45..48 "CGGC"
S2 30..33 "GCCU" 38..41 "GGGU"
S3 53..58 "CUGGGA" 69..74 "GCCCAG"
S4 76..79 "CUCC" 84..87 "GGAG"
S5 104..104 "U" 130..130 "A"
S6 106..108 "GCA" 127..129 "UGC"
S7 112..116 "UGGGC" 121..125 "GCCUG"
S8 143..146 "GAGG" 170..173 "CCUC"
S9 153..156 "GGCC" 161..164 "GGUU"
S10 177..177 "C" 214..214 "G"
S11 178..183 "AGCACG" 205..210 "CGUGCA"
S12 187..187 "G" 200..200 "C"
S13 190..191 "GC" 196..197 "GU"
S14 217..221 "GCGCC" 258..262 "GGCGC"
S15 228..232 "CUACG" 253..257 "CGUAG"
S16 238..239 "GC" 244..245 "GC"
S17 284..286 "GGC" 601..603 "GCC"
S18 292..292 "G" 600..600 "C"
S19 294..297 "GCUC" 479..482 "GAGC"
S20 299..299 "U" 476..476 "G"
S21 301..303 "AGC" 473..475 "GCU"
S22 305..311 "GUUUGUC" 362..368 "GGCAGAC"
S23 313..314 "CG" 360..361 "CG"
S24 315..323 "UCUGCUGUG" 349..357 "UGCAGUGGG"
S25 328..334 "CCCGGGG" 340..346 "CCCCGGG"
S26 372..374 "AGU" 467..469 "ACU"
S27 375..380 "GCAGUA" 460..465 "GGCUGU"
S28 383..387 "GGAGA" 454..458 "UCUCU"
S29 389..390 "CU" 452..453 "GG"
S30 391..392 "GG" 434..435 "AC"
S31 395..402 "CUCCUGGU" 424..431 "AUCAGGAA"
S32 406..408 "GCG" 415..417 "UGC"
S33 440..440 "A" 451..451 "A"
S34 443..444 "GC" 449..450 "GC"
S35 487..492 "GCAUGG" 523..528 "CCAUGC"
S36 496..497 "GC" 521..522 "GU"
S37 501..504 "CAGG" 514..517 "CCUG"
S38 539..544 "GGGCAC" 594..599 "GUGCCC"
S39 547..558 "GGUGUGGGGGAC" 566..577 "GUUUUCCGCGCC"
S40 581..582 "GC" 587..588 "GC"
S41 605..607 "GGG" 630..632 "CUC"
S42 614..617 "GGCC" 622..625 "GGCU"
S43 641..642 "UG" 1119..1120 "CA"
S44 643..645 "ACG" 1115..1117 "UGU"
S45 647..653 "GGGCCCG" 1108..1114 "CGGGCCU"
S46 655..656 "AC" 1103..1104 "GU"
S47 659..663 "GCGGC" 1063..1067 "GUCGC"
S48 664..673 "GGAGCAUGCG" 951..960 "CGCAUGCUAC"
S49 674..675 "GA" 948..949 "UC"
S50 680..683 "UUCG" 692..695 "CGAA"
S51 704..710 "CCAAGGC" 938..944 "GUCUUGG"
S52 716..724 "AUACACCGG" 759..767 "CUGGUGUAC"
S53 727..733 "CGGCUCA" 738..744 "UGGGUUU"
S54 748..749 "UC" 755..756 "GG"
S55 769..770 "GG" 932..933 "CC"
S56 772..775 "GGUG" 928..931 "UGCC"
S57 778..779 "UG" 926..927 "CA"
S58 780..782 "GUU" 920..922 "AAU"
S59 783..786 "GUCG" 915..918 "CGUC"
S60 790..795 "GCUCGU" 824..829 "ACGAGC"
S61 796..798 "GUC" 803..805 "GAU"
S62 808..811 "UGGG" 818..821 "CCCG"
S63 835..838 "CCUC" 907..910 "GGGG"
S64 840..846 "UUCUAUG" 872..878 "CAUAGGA"
S65 850..851 "CC" 866..867 "GG"
S66 854..856 "CAC" 863..865 "GUG"
S67 881..888 "CUGCCGGG" 894..901 "CUCGGAGG"
S68 964..970 "GGCCAGU" 1013..1019 "GCUGGUC"
S69 974..978 "AUGGG" 1005..1009 "CCCAA"
S70 981..982 "GC" 999..1000 "GC"
S71 986..988 "ACC" 993..995 "GGU"
S72 1026..1026 "C" 1057..1057 "G"
S73 1030..1037 "UCGUGGUC" 1046..1053 "GACCACGU"
S74 1073..1080 "AUCGCAGA" 1089..1096 "GCUGCGGU"
S75 1147..1154 "GUAACACC" 1195..1202 "AAGUCGUA"
S76 1159..1164 "GCCGGU" 1186..1191 "AUUGGG"
S77 1166..1169 "GCCU" 1182..1185 "GGCG"
S78 1172..1173 "CC" 1179..1180 "GG"
H1 34..37 "GAGA" (33,38) U:G 
H2 59..68 "CUGAGACACG" (58,69) A:G 
H3 80..83 "UACG" (79,84) C:G 
H4 117..120 "GCAA" (116,121) C:G 
H5 157..160 "UUCG" (156,161) C:G 
H6 192..195 "GAAA" (191,196) C:G 
H7 240..243 "AGCC" (239,244) C:G PK{2}
H8 335..339 "CUUAA" (334,340) G:C 
H9 409..414 "GUGAAA" (408,415) G:U 
H10 445..448 "GAAG" (444,449) C:G 
H11 505..513 "AUUAGAUAC" (504,514) G:C 
H12 559..565 "AUUCCAC" (558,566) C:G 
H13 583..586 "UAAC" (582,587) C:G PK{3}
H14 618..621 "GCAA" (617,622) C:G 
H15 684..691 "AUGCAACG" (683,692) G:C 
H16 734..737 "GAGA" (733,738) A:U 
H17 750..754 "CUUGU" (749,755) C:G 
H18 799..802 "GUGA" (798,803) C:G 
H19 812..817 "UUAAGU" (811,818) G:C 
H20 857..862 "GUGAUG" (856,863) C:G 
H21 889..893 "GUCAA" (888,894) G:C 
H22 989..992 "GUGA" (988,993) C:G 
H23 1038..1045 "UGCAACUC" (1037,1046) C:G 
H24 1081..1088 "UCAGCAAC" (1080,1089) A:G 
H25 1174..1178 "CUUGU" (1173,1179) C:G 
B1 42..44 "GAC" (41,30) U:G (29,45) G:C 
B2 105..105 "U" (104,130) U:A (129,106) C:G 
B3 211..213 "GAA" (210,178) A:A (177,214) C:G PK{1}
B4 222..227 "GGCUAA" (221,258) C:G (257,228) G:C PK{2}
B5 287..291 "GUAAA" (286,601) C:G (600,292) C:G PK{3}
B6 300..300 "A" (299,476) U:G (475,301) U:A 
B7 312..312 "G" (311,362) C:G (361,313) G:C 
B8 358..359 "UA" (357,315) G:U (314,360) G:C 
B9 388..388 "A" (387,454) A:U (453,389) G:C 
B10 441..442 "UG" (440,451) A:A (450,443) C:G 
B11 466..466 "U" (465,375) U:G (374,467) U:A 
B12 493..495 "GGA" (492,523) G:C (522,496) U:G 
B13 646..646 "G" (645,1115) G:U (1114,647) U:G 
B14 771..771 "U" (770,932) G:C (931,772) C:G 
B15 776..777 "CA" (775,928) G:U (927,778) A:U 
B16 852..853 "AG" (851,866) C:G (865,854) G:C 
B17 919..919 "A" (918,783) C:G (782,920) U:A 
B18 923..925 "CAU" (922,780) U:G (779,926) G:C 
B19 950..950 "A" (949,674) C:G (673,951) G:C 
B20 1118..1118 "A" (1117,643) U:A (642,1119) G:C 
B21 1165..1165 "G" (1164,1186) U:A (1185,1166) G:G 
I1.1 109..111 "CAA" (108,127) A:U 
I1.2 126..126 "A" (125,112) G:U 
I2.1 147..152 "GAUGAC" (146,170) G:C 
I2.2 165..169 "GUAAA" (164,153) U:G 
I3.1 184..186 "GAA" (183,205) G:C 
I3.2 201..204 "GGUA" (200,187) C:G 
I4.1 188..189 "AA" (187,200) G:C 
I4.2 198..199 "GA" (197,190) U:G 
I5.1 233..237 "UGCCA" (232,253) G:C 
I5.2 246..252 "GGUAAUA" (245,238) C:G 
I6.1 298..298 "G" (297,479) C:G 
I6.2 477..478 "AG" (476,299) G:U 
I7.1 324..327 "AAAG" (323,349) G:U 
I7.2 347..348 "UG" (346,328) G:C 
I8.1 381..382 "GG" (380,460) A:G 
I8.2 459..459 "G" (458,383) U:G 
I9.1 393..394 "AA" (392,434) G:A 
I9.2 432..433 "GA" (431,395) A:C 
I10.1 403..405 "GUA" (402,424) U:A 
I10.2 418..423 "GCAGAU" (417,406) C:G 
I11.1 498..500 "GAA" (497,521) C:G 
I11.2 518..520 "GUA" (517,501) G:C 
I12.1 608..613 "GAGUAC" (607,630) G:C 
I12.2 626..629 "AAAA" (625,614) U:G 
I13.1 654..654 "C" (653,1108) G:C 
I13.2 1105..1107 "UCC" (1104,655) U:A 
I14.1 847..849 "UUG" (846,872) G:C 
I14.2 868..871 "GACU" (867,850) G:C 
I15.1 971..973 "ACA" (970,1013) U:G 
I15.2 1010..1012 "AAA" (1009,974) A:A 
I16.1 979..980 "UU" (978,1005) G:C 
I16.2 1001..1004 "UAAU" (1000,981) C:G 
I17.1 983..985 "GAU" (982,999) C:G 
I17.2 996..998 "GGA" (995,986) U:A 
I18.1 1027..1029 "GGA" (1026,1057) C:G 
I18.2 1054..1056 "GAA" (1053,1030) U:U 
I19.1 1155..1158 "CGAA" (1154,1195) C:A 
I19.2 1192..1194 "ACU" (1191,1159) G:G 
I20.1 1170..1171 "AA" (1169,1182) U:G 
I20.2 1181..1181 "U" (1180,1172) G:C 
M1.1 293..293 "A" (292,600) G:C (482,294) C:G 
M1.2 483..486 "GAAA" (482,294) C:G (528,487) C:G 
M1.3 529..538 "CGUAAACGUU" (528,487) C:G (599,539) C:G 
M1.4 600..599 "" (599,539) C:G (292,600) G:C 
M2.1 304..304 "G" (303,473) C:G (368,305) C:G 
M2.2 369..371 "UUG" (368,305) C:G (469,372) U:A 
M2.3 470..472 "GAC" (469,372) U:A (303,473) C:G 
M3.1 391..390 "" (390,452) U:G (435,391) C:G 
M3.2 436..439 "ACCG" (435,391) C:G (451,440) A:A 
M3.3 452..451 "" (451,440) A:A (390,452) U:G 
M4.1 545..546 "UA" (544,594) C:G (577,547) C:G 
M4.2 578..580 "GUA" (577,547) C:G (588,581) C:G 
M4.3 589..593 "AUUAA" (588,581) C:G (544,594) C:G 
M5.1 657..658 "AA" (656,1103) C:G (1067,659) C:G 
M5.2 1068..1072 "UAGUA" (1067,659) C:G (1096,1073) U:A 
M5.3 1097..1102 "GAAUAC" (1096,1073) U:A (656,1103) C:G 
M6.1 664..663 "" (663,1063) C:G (960,664) C:G 
M6.2 961..963 "AAU" (960,664) C:G (1019,964) C:G 
M6.3 1020..1025 "UCAGUU" (1019,964) C:G (1057,1026) G:C 
M6.4 1058..1062 "UCGGA" (1057,1026) G:C (663,1063) C:G 
M7.1 676..679 "UUAA" (675,948) A:U (695,680) A:U 
M7.2 696..703 "GAACCUUA" (695,680) A:U (944,704) G:C 
M7.3 945..947 "GCU" (944,704) G:C (675,948) A:U 
M8.1 711..715 "UUGAC" (710,938) C:G (767,716) C:A 
M8.2 768..768 "A" (767,716) C:A (933,769) C:G 
M8.3 934..937 "UUAU" (933,769) C:G (710,938) C:G 
M9.1 725..726 "AU" (724,759) G:C (744,727) U:C 
M9.2 745..747 "UCC" (744,727) U:C (756,748) G:U 
M9.3 757..758 "GG" (756,748) G:U (724,759) G:C 
M10.1 787..789 "UCA" (786,915) G:C (829,790) C:G 
M10.2 830..834 "GCAAC" (829,790) C:G (910,835) G:C 
M10.3 911..914 "AUGA" (910,835) G:C (786,915) G:C 
M11.1 796..795 "" (795,824) U:A (805,796) U:G 
M11.2 806..807 "GU" (805,796) U:G (821,808) G:U 
M11.3 822..823 "CA" (821,808) G:U (795,824) U:A 
M12.1 839..839 "G" (838,907) C:G (878,840) A:U 
M12.2 879..880 "GA" (878,840) A:U (901,881) G:C 
M12.3 902..906 "AAGGU" (901,881) G:C (838,907) C:G 
X1 49..52 "CACA" (48,26) C:G (74,53) G:C 
X3 88..103 "GCAGCAGUGGGGAAUA" (87,76) G:C (130,104) A:U 
X4 131..142 "GCGACGCCGCGU" (130,104) A:U (173,143) C:G 
X5 174..176 "UUU" (173,143) C:G (214,177) G:C PK{1}
X6 215..216 "AA" (214,177) G:C (262,217) C:G 
X7 263..283 "AAGCGUUGUCCGGAAUUAUUG" (262,217) C:G (603,284) C:G 
X9 633..640 "AAAGGAAU" (632,605) C:G (1120,641) A:U 
X10 1121..1146 "CACCGCCCGUCAAGUCACGAAAGUUG" (1120,641) A:U (1202,1147) A:G 
E1 1..25 "UGGCCUACCAAGGCGACGACGGGUA" 
PK1 1bp 175..175 213..213 X5 174..176 B3 211..213
PK2 3bp 222..224 241..243 B4 222..227 H7 240..243
PK3 2bp 287..288 585..586 B5 287..291 H13 583..586
PK1.1 175 U 213 A
PK2.1 222 G 243 C
PK2.2 223 G 242 C
PK2.3 224 C 241 G
PK3.1 287 G 586 C
PK3.2 288 U 585 A
NCBP1 58 A 69 G S3
NCBP2 1152 A 1197 G S75
NCBP3 178 A 210 A S11
NCBP4 716 A 767 C S52
NCBP5 1153 C 1196 A S75
NCBP6 784 U 917 U S59
NCBP7 380 A 460 G S27
NCBP8 1148 U 1201 U S75
NCBP9 392 G 434 A S30
NCBP10 1154 C 1195 A S75
NCBP11 1149 A 1200 G S75
NCBP12 395 C 431 A S31
NCBP13 665 G 959 A S48
NCBP14 727 C 744 U S53
NCBP15 1080 A 1089 G S74
NCBP16 1166 G 1185 G S77
NCBP17 1147 G 1202 A S75
NCBP18 440 A 451 A S33
NCBP19 1167 C 1184 C S77
NCBP20 1150 A 1199 C S75
NCBP21 974 A 1009 A S69
NCBP22 1151 C 1198 U S75
NCBP23 1030 U 1053 U S73
NCBP24 883 G 899 A S67
NCBP25 1159 G 1191 G S76
segment1 8bp 26..33 GCCGGCCU 38..48 GGGUGACCGGC
segment2 6bp 53..58 CUGGGA 69..74 GCCCAG
segment3 4bp 76..79 CUCC 84..87 GGAG
segment4 9bp 104..116 UUGCACAAUGGGC 121..130 GCCUGAUGCA
segment5 8bp 143..156 GAGGGAUGACGGCC 161..173 GGUUGUAAACCUC
segment6 10bp 177..191 CAGCACGGAAGAAGC 196..214 GUGACGGUACGUGCAGAAG
segment7 12bp 217..239 GCGCCGGCUAACUACGUGCCAGC 244..262 GCGGUAAUACGUAGGGCGC
segment8 4bp 284..292 GGCGUAAAG 600..603 CGCC
segment9 8bp 294..303 GCUCGUAAGC 473..482 GCUGAGGAGC
segment10 25bp 305..334 GUUUGUCGCGUCUGCUGUGAAAGCCCGGGG 340..368 CCCCGGGUGUGCAGUGGGUACGGGCAGAC
segment11 16bp 372..390 AGUGCAGUAGGGGAGAACU 452..469 GGUCUCUGGGCUGUUACU
segment12 13bp 391..408 GGAACUCCUGGUGUAGCG 415..435 UGCGCAGAUAUCAGGAAGAAC
segment13 3bp 440..444 AUGGC 449..451 GCA
segment14 12bp 487..504 GCAUGGGGAGCGAACAGG 514..528 CCUGGUAGUCCAUGC
segment15 6bp 539..544 GGGCAC 594..599 GUGCCC
segment16 12bp 547..558 GGUGUGGGGGAC 566..577 GUUUUCCGCGCC
segment17 2bp 581..582 GC 587..588 GC
segment18 7bp 605..617 GGGGAGUACGGCC 622..632 GGCUAAAACUC
segment19 14bp 641..656 UGACGGGGGCCCGCAC 1103..1120 GUUCCCGGGCCUUGUACA
segment20 5bp 659..663 GCGGC 1063..1067 GUCGC
segment21 12bp 664..675 GGAGCAUGCGGA 948..960 UCACGCAUGCUAC
segment22 4bp 680..683 UUCG 692..695 CGAA
segment23 7bp 704..710 CCAAGGC 938..944 GUCUUGG
segment24 9bp 716..724 AUACACCGG 759..767 CUGGUGUAC
segment25 7bp 727..733 CGGCUCA 738..744 UGGGUUU
segment26 2bp 748..749 UC 755..756 GG
segment27 15bp 769..786 GGUGGUGCAUGGUUGUCG 915..933 CGUCAAAUCAUCAUGCCCC
segment28 6bp 790..795 GCUCGU 824..829 ACGAGC
segment29 3bp 796..798 GUC 803..805 GAU
segment30 4bp 808..811 UGGG 818..821 CCCG
segment31 4bp 835..838 CCUC 907..910 GGGG
segment32 12bp 840..856 UUCUAUGUUGCCAGCAC 863..878 GUGGGGACUCAUAGGA
segment33 8bp 881..888 CUGCCGGG 894..901 CUCGGAGG
segment34 17bp 964..988 GGCCAGUACAAUGGGUUGCGAUACC 993..1019 GGUGGAGCUAAUCCCAAAAAGCUGGUC
segment35 9bp 1026..1037 CGGAUCGUGGUC 1046..1057 GACCACGUGAAG
segment36 8bp 1073..1080 AUCGCAGA 1089..1096 GCUGCGGU
segment37 20bp 1147..1173 GUAACACCCGAAGCCGGUGGCCUAACC 1179..1202 GGUGGCGAUUGGGACUAAGUCGUA
