#Name: bpRNA_CRW_5677
#Length:  1236 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
AGGGAACGUGCCUUUAUGUUCGGAAUAACUCAGGGAAACUUGUCCUAAUACCGAAUGUGCCCUUCGGGGGAAAGAUUUAUCGCCUUUAGAGCGGCCCGCGUCUGAUUAGCUAGUUGGUGAGGUAAAGGCUCACCAAGGCGACAAUCAGUAGCUGGUCUGAGAGGAUGAUCAGCCACAUUGGGACUGAGACACGGCCCAAACUCCUACGGGAGGCAGCAGUGGGGAAUCUUGCGCAAUGGGCGAAAGUCUGACGCAGCCAUGCCGCGUGAAUGAUGAAGGUCUUACGAUUGUAAAAUUCUUUCACCGGGGACGAUAAUGACGGUACCCGGAGAAUAAGCCCCGGCUAACUUCGUGCCAGCAGCCGCGGUAAUACGAAGGGGGCUAGCGUUGCUCGGAAUUACUGGGCGUAAAGGGAGCGUAGGCGGACAUUUAAGUCAGGGGUGAAAUCCCGGGGCUCAACCUCGGAAUUGCCUUUGAUACUGGGUGUCUUGAGUGUGAGAGAGGUAUGUGGAACUCCGAGUGUAGAGGUGAAAUUCGUAGAUAUUCGGGAAGAACACCAGUGGCGAAGGCGACAUACUGGCUCAUUACUGACGCUGAGGCUCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAUGCUAGUUGUCGGGAUGCAUGCAUUUCGGUGACGCAGCUAACGCAUUAAGCAAUCCGCCUGGGGAGUACGGUCGCAAGAUUAAAACUCAAAGGAAUUGUCGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUGGAAGCAACGCGCAGAACCUUACCACCUUUUGACAUGCCCGGACCGCCACAGAGAUGUGGCUUUCUCUUCGGAGCCUGGGACACAGGUGCUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCACCCCUCGCCAUUAGUUGCCAUCAUUCAGUUGGGAACUCUAAUGGGACUGCCGGUGCUAAGCCGGAGGAAGGUGGGGAUGACGUCAAGUCCUCAUGGCCCUUACAGGGUGGGCUACACACGUGCUACAAUGGCGACUACAGAGGGUUAAUCCUUAAAAGUCGUCUCAGUUCGGAUUGUCCUCUGCAACUCGAGGGCAUGAAGUUGGAAUCGCUAGUAAUCGCGGAUCAGCAUGCCGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACACCAUGGGAGUGGU
(((((.((...(((((((.((((....(((((((....))))))).....)))).....(((....)))....((....)).)))))))..)))))))...(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(.......)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))).)))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........(((((..((((((((((((....))))))))))))...((..]])).....))).)))))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....(((((((...(((((((....)))))))..(((....)))..)))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((.....)))....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).))...........................
SSSSSBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSHHHHSSSMMMMSSHHHHSSMSSSSSSSIISSSSSSSXXXSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHSIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSBSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSMMSSSSSSSSSSSSHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSBSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSMMMSSSSSSSHHHHSSSSSSSMMSSSHHHHSSSMMSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSHHHHHSSSIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSEEEEEEEEEEEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..5 "AGGGA" 94..98 "GCCCG"
S2 7..8 "CG" 92..93 "CG"
S3 12..18 "CUUUAUG" 83..89 "CCUUUAG"
S4 20..23 "UCGG" 51..54 "CCGA"
S5 28..34 "ACUCAGG" 39..45 "CUUGUCC"
S6 60..62 "CCC" 67..69 "GGG"
S7 74..75 "GA" 80..81 "UC"
S8 102..104 "CUG" 146..148 "CAG"
S9 107..107 "U" 145..145 "U"
S10 109..111 "GCU" 137..139 "GGC"
S11 114..121 "UUGGUGAG" 129..136 "CUCACCAA"
S12 151..154 "GCUG" 170..173 "CAGC"
S13 155..158 "GUCU" 163..166 "GGAU"
S14 178..183 "UUGGGA" 194..199 "GCCCAA"
S15 201..204 "CUCC" 209..212 "GGAG"
S16 229..229 "U" 255..255 "A"
S17 231..233 "GCG" 252..254 "CGC"
S18 237..241 "UGGGC" 246..250 "GUCUG"
S19 268..271 "GAAU" 295..298 "AUUC"
S20 278..281 "GGUC" 286..289 "GAUU"
S21 302..302 "C" 334..334 "U"
S22 303..308 "ACCGGG" 325..330 "CCCGGA"
S23 312..312 "G" 320..320 "C"
S24 337..341 "GCCCC" 378..382 "GGGGC"
S25 348..352 "CUUCG" 373..377 "CGAAG"
S26 358..359 "GC" 364..365 "GC"
S27 404..406 "GGC" 717..719 "GCC"
S28 412..412 "G" 716..716 "C"
S29 414..417 "GAGC" 599..602 "GCUC"
S30 419..419 "U" 596..596 "G"
S31 421..423 "GGC" 593..595 "GCU"
S32 425..431 "GACAUUU" 482..488 "GGGUGUC"
S33 433..434 "AG" 480..481 "CU"
S34 435..443 "UCAGGGGUG" 469..477 "UGCCUUUGA"
S35 448..454 "CCCGGGG" 460..466 "CCUCGGA"
S36 492..494 "AGU" 587..589 "ACU"
S37 495..500 "GUGAGA" 580..585 "GCUCAU"
S38 503..509 "GGUAUGU" 572..578 "ACAUACU"
S39 510..511 "GG" 554..555 "AC"
S40 514..516 "CUC" 549..551 "GAA"
S41 517..521 "CGAGU" 543..547 "AUUCG"
S42 525..527 "GAG" 534..536 "UUC"
S43 560..560 "G" 571..571 "G"
S44 563..564 "GC" 569..570 "GC"
S45 607..612 "GCGUGG" 643..648 "CCACGC"
S46 616..617 "GC" 641..642 "GU"
S47 621..624 "CAGG" 634..637 "CCUG"
S48 659..660 "GA" 714..715 "UC"
S49 661..663 "UGC" 710..712 "GCA"
S50 666..677 "GUUGUCGGGAUG" 682..693 "CAUUUCGGUGAC"
S51 697..698 "GC" 703..704 "GC"
S52 721..723 "GGG" 746..748 "CUC"
S53 730..733 "GGUC" 738..741 "GAUU"
S54 757..758 "UG" 1208..1209 "CA"
S55 759..761 "UCG" 1204..1206 "UGU"
S56 763..769 "GGGCCCG" 1197..1203 "CGGGCCU"
S57 771..772 "AC" 1192..1193 "GU"
S58 775..779 "GCGGU" 1153..1157 "AUCGC"
S59 780..789 "GGAGCAUGUG" 1063..1072 "CACGUGCUAC"
S60 790..791 "GU" 1060..1061 "AC"
S61 796..799 "UUGG" 808..811 "CGCA"
S62 820..826 "CCACCUU" 1050..1056 "AGGGUGG"
S63 832..834 "AUG" 879..881 "CAC"
S64 835..838 "CCCG" 874..877 "UGGG"
S65 842..848 "CGCCACA" 853..859 "UGUGGCU"
S66 862..864 "CUC" 869..871 "GAG"
S67 883..884 "GG" 1044..1045 "CC"
S68 886..889 "GCUG" 1040..1043 "UGGC"
S69 892..893 "UG" 1038..1039 "CA"
S70 894..896 "GCU" 1032..1034 "AGU"
S71 897..900 "GUCG" 1027..1030 "CGUC"
S72 904..909 "GCUCGU" 938..943 "ACGAGC"
S73 910..912 "GUC" 917..919 "GAU"
S74 922..925 "UGGG" 932..935 "CCCG"
S75 949..952 "CCUC" 1019..1022 "GGGG"
S76 954..960 "CCAUUAG" 984..990 "CUAAUGG"
S77 964..965 "CC" 978..979 "GG"
S78 968..969 "CA" 976..977 "UG"
S79 993..1000 "CUGCCGGU" 1006..1013 "GCCGGAGG"
S80 1076..1082 "GGCGACU" 1103..1109 "AGUCGUC"
S81 1088..1090 "GGG" 1096..1098 "CCU"
S82 1116..1116 "C" 1147..1147 "G"
S83 1120..1127 "UUGUCCUC" 1136..1143 "GAGGGCAU"
S84 1163..1170 "AUCGCGGA" 1178..1185 "GCCGCGGU"
H1 35..38 "GAAA" (34,39) G:C 
H2 63..66 "UUCG" (62,67) C:G 
H3 76..79 "UUUA" (75,80) A:U 
H4 122..128 "GUAAAGG" (121,129) G:C 
H5 159..162 "GAGA" (158,163) U:G 
H6 184..193 "CUGAGACACG" (183,194) A:G 
H7 205..208 "UACG" (204,209) C:G 
H8 242..245 "GAAA" (241,246) C:G 
H9 282..285 "UUAC" (281,286) C:G 
H10 313..319 "AUAAUGA" (312,320) G:C 
H11 360..363 "AGCC" (359,364) C:G PK{2}
H12 455..459 "CUCAA" (454,460) G:C 
H13 528..533 "GUGAAA" (527,534) G:U 
H14 565..568 "GAAG" (564,569) C:G 
H15 625..633 "AUUAGAUAC" (624,634) G:C 
H16 678..681 "CAUG" (677,682) G:C 
H17 699..702 "UAAC" (698,703) C:G PK{3}
H18 734..737 "GCAA" (733,738) C:G 
H19 800..807 "AAGCAACG" (799,808) G:C 
H20 849..852 "GAGA" (848,853) A:U 
H21 865..868 "UUCG" (864,869) C:G 
H22 913..916 "GUGA" (912,917) C:G 
H23 926..931 "UUAAGU" (925,932) G:C 
H24 970..975 "UUCAGU" (969,976) A:U 
H25 1001..1005 "GCUAA" (1000,1006) U:G 
H26 1091..1095 "UUAAU" (1090,1096) G:C 
H27 1128..1135 "UGCAACUC" (1127,1136) C:G 
H28 1171..1177 "UCAGCAU" (1170,1178) A:G 
B1 6..6 "A" (5,94) A:G (93,7) G:C 
B2 105..106 "AU" (104,146) G:C (145,107) U:U 
B3 112..113 "AG" (111,137) U:G (136,114) A:U 
B4 167..169 "GAU" (166,155) U:G (154,170) G:C 
B5 230..230 "U" (229,255) U:A (254,231) C:G 
B6 331..333 "GAA" (330,303) A:A (302,334) C:U PK{1}
B7 342..347 "GGCUAA" (341,378) C:G (377,348) G:C PK{2}
B8 407..411 "GUAAA" (406,717) C:G (716,412) C:G PK{3}
B9 420..420 "A" (419,596) U:G (595,421) U:G 
B10 432..432 "A" (431,482) U:G (481,433) U:A 
B11 478..479 "UA" (477,435) A:U (434,480) G:C 
B12 548..548 "G" (547,517) G:C (516,549) C:G 
B13 561..562 "UG" (560,571) G:G (570,563) C:G 
B14 586..586 "U" (585,495) U:G (494,587) U:A 
B15 613..615 "GGA" (612,643) G:C (642,616) U:G 
B16 713..713 "A" (712,661) A:U (660,714) A:U 
B17 762..762 "G" (761,1204) G:U (1203,763) U:G 
B18 878..878 "A" (877,835) G:C (834,879) G:C 
B19 885..885 "U" (884,1044) G:C (1043,886) C:G 
B20 890..891 "CA" (889,1040) G:U (1039,892) A:U 
B21 966..967 "AU" (965,978) C:G (977,968) G:C 
B22 1031..1031 "A" (1030,897) C:G (896,1032) U:A 
B23 1035..1037 "CCU" (1034,894) U:G (893,1038) G:C 
B24 1062..1062 "A" (1061,790) C:G (789,1063) G:C 
B25 1207..1207 "A" (1206,759) U:U (758,1208) G:C 
I1.1 9..11 "UGC" (8,92) G:C 
I1.2 90..91 "AG" (89,12) G:C 
I2.1 24..27 "AAUA" (23,51) G:C 
I2.2 46..50 "UAAUA" (45,28) C:A 
I3.1 108..108 "A" (107,145) U:U 
I3.2 140..144 "GACAA" (139,109) C:G 
I4.1 234..236 "CAA" (233,252) G:C 
I4.2 251..251 "A" (250,237) G:U 
I5.1 272..277 "GAUGAA" (271,295) U:A 
I5.2 290..294 "GUAAA" (289,278) U:G 
I6.1 309..311 "GAC" (308,325) G:C 
I6.2 321..324 "GGUA" (320,312) C:G 
I7.1 353..357 "UGCCA" (352,373) G:C 
I7.2 366..372 "GGUAAUA" (365,358) C:G 
I8.1 418..418 "G" (417,599) C:G 
I8.2 597..598 "AG" (596,419) G:U 
I9.1 444..447 "AAAU" (443,469) G:U 
I9.2 467..468 "AU" (466,448) A:C 
I10.1 501..502 "GA" (500,580) A:G 
I10.2 579..579 "G" (578,503) U:G 
I11.1 512..513 "AA" (511,554) G:A 
I11.2 552..553 "GA" (551,514) A:C 
I12.1 522..524 "GUA" (521,543) U:A 
I12.2 537..542 "GUAGAU" (536,525) C:G 
I13.1 618..620 "AAA" (617,641) C:G 
I13.2 638..640 "GUA" (637,621) G:C 
I14.1 724..729 "GAGUAC" (723,746) G:C 
I14.2 742..745 "AAAA" (741,730) U:G 
I15.1 770..770 "C" (769,1197) G:C 
I15.2 1194..1196 "UCC" (1193,771) U:A 
I16.1 961..963 "UUG" (960,984) G:C 
I16.2 980..983 "AACU" (979,964) G:C 
I17.1 1083..1087 "ACAGA" (1082,1103) U:A 
I17.2 1099..1102 "UAAA" (1098,1088) U:G 
I18.1 1117..1119 "GGA" (1116,1147) C:G 
I18.2 1144..1146 "GAA" (1143,1120) U:U 
M1.1 19..19 "U" (18,83) G:C (54,20) A:U 
M1.2 55..59 "AUGUG" (54,20) A:U (69,60) G:C 
M1.3 70..73 "GAAA" (69,60) G:C (81,74) C:G 
M1.4 82..82 "G" (81,74) C:G (18,83) G:C 
M2.1 413..413 "G" (412,716) G:C (602,414) C:G 
M2.2 603..606 "GAAA" (602,414) C:G (648,607) C:G 
M2.3 649..658 "CGUAAACGAU" (648,607) C:G (715,659) C:G 
M2.4 716..715 "" (715,659) C:G (412,716) G:C 
M3.1 424..424 "G" (423,593) C:G (488,425) C:G 
M3.2 489..491 "UUG" (488,425) C:G (589,492) U:A 
M3.3 590..592 "GAC" (589,492) U:A (423,593) C:G 
M4.1 510..509 "" (509,572) U:A (555,510) C:G 
M4.2 556..559 "ACCA" (555,510) C:G (571,560) G:G 
M4.3 572..571 "" (571,560) G:G (509,572) U:A 
M5.1 664..665 "UA" (663,710) C:G (693,666) C:G 
M5.2 694..696 "GCA" (693,666) C:G (704,697) C:G 
M5.3 705..709 "AUUAA" (704,697) C:G (663,710) C:G 
M6.1 773..774 "AA" (772,1192) C:G (1157,775) C:G 
M6.2 1158..1162 "UAGUA" (1157,775) C:G (1185,1163) U:A 
M6.3 1186..1191 "GAAUAC" (1185,1163) U:A (772,1192) C:G 
M7.1 780..779 "" (779,1153) U:A (1072,780) C:G 
M7.2 1073..1075 "AAU" (1072,780) C:G (1109,1076) C:G 
M7.3 1110..1115 "UCAGUU" (1109,1076) C:G (1147,1116) G:C 
M7.4 1148..1152 "UUGGA" (1147,1116) G:C (779,1153) U:A 
M8.1 792..795 "UUAA" (791,1060) U:A (811,796) A:U 
M8.2 812..819 "GAACCUUA" (811,796) A:U (1056,820) G:C 
M8.3 1057..1059 "GCU" (1056,820) G:C (791,1060) U:A 
M9.1 827..831 "UUGAC" (826,1050) U:A (881,832) C:A 
M9.2 882..882 "A" (881,832) C:A (1045,883) C:G 
M9.3 1046..1049 "UUAC" (1045,883) C:G (826,1050) U:A 
M10.1 839..841 "GAC" (838,874) G:U (859,842) U:C 
M10.2 860..861 "UU" (859,842) U:C (871,862) G:C 
M10.3 872..873 "CC" (871,862) G:C (838,874) G:U 
M11.1 901..903 "UCA" (900,1027) G:C (943,904) C:G 
M11.2 944..948 "GCACC" (943,904) C:G (1022,949) G:C 
M11.3 1023..1026 "AUGA" (1022,949) G:C (900,1027) G:C 
M12.1 910..909 "" (909,938) U:A (919,910) U:G 
M12.2 920..921 "GU" (919,910) U:G (935,922) G:U 
M12.3 936..937 "CA" (935,922) G:U (909,938) U:A 
M13.1 953..953 "G" (952,1019) C:G (990,954) G:C 
M13.2 991..992 "GA" (990,954) G:C (1013,993) G:C 
M13.3 1014..1018 "AAGGU" (1013,993) G:C (952,1019) C:G 
X1 99..101 "CGU" (98,1) G:A (148,102) G:C 
X2 149..150 "UA" (148,102) G:C (173,151) C:G 
X3 174..177 "CACA" (173,151) C:G (199,178) A:U 
X5 213..228 "GCAGCAGUGGGGAAUC" (212,201) G:C (255,229) A:U 
X6 256..267 "GCCAUGCCGCGU" (255,229) A:U (298,268) C:G 
X7 299..301 "UUU" (298,268) C:G (334,302) U:C PK{1}
X8 335..336 "AA" (334,302) U:C (382,337) C:G 
X9 383..403 "UAGCGUUGCUCGGAAUUACUG" (382,337) C:G (719,404) C:G 
X11 749..756 "AAAGGAAU" (748,721) C:G (1209,757) A:U 
E1 1210..1236 "CACCGCCCGUCACACCAUGGGAGUGGU" 
PK1 1bp 300..300 333..333 X7 299..301 B6 331..333
PK2 3bp 342..344 361..363 B7 342..347 H11 360..363
PK3 2bp 407..408 701..702 B8 407..411 H17 699..702
PK1.1 300 U 333 A
PK2.1 342 G 363 C
PK2.2 343 G 362 C
PK2.3 344 C 361 G
PK3.1 407 G 702 C
PK3.2 408 U 701 A
NCBP1 560 G 571 G S43
NCBP2 798 G 809 G S61
NCBP3 898 U 1029 U S71
NCBP4 29 C 44 C S5
NCBP5 15 U 86 U S3
NCBP6 14 U 87 U S3
NCBP7 107 U 145 U S9
NCBP8 1 A 98 G S1
NCBP9 759 U 1206 U S55
NCBP10 17 U 84 C S3
NCBP11 500 A 580 G S37
NCBP12 514 C 551 A S40
NCBP13 797 U 810 C S61
NCBP14 28 A 45 C S5
NCBP15 303 A 330 A S22
NCBP16 832 A 881 C S63
NCBP17 781 G 1071 A S59
NCBP18 511 G 554 A S39
NCBP19 842 C 859 U S65
NCBP20 5 A 94 G S1
NCBP21 302 C 334 U S21
NCBP22 183 A 194 G S14
NCBP23 448 C 466 A S35
NCBP24 1120 U 1143 U S83
NCBP25 995 G 1011 A S79
NCBP26 30 U 43 U S5
NCBP27 1170 A 1178 G S84
segment1 14bp 1..18 AGGGAACGUGCCUUUAUG 83..98 CCUUUAGAGCGGCCCG
segment2 11bp 20..34 UCGGAAUAACUCAGG 39..54 CUUGUCCUAAUACCGA
segment3 3bp 60..62 CCC 67..69 GGG
segment4 2bp 74..75 GA 80..81 UC
segment5 15bp 102..121 CUGAUUAGCUAGUUGGUGAG 129..148 CUCACCAAGGCGACAAUCAG
segment6 8bp 151..158 GCUGGUCU 163..173 GGAUGAUCAGC
segment7 6bp 178..183 UUGGGA 194..199 GCCCAA
segment8 4bp 201..204 CUCC 209..212 GGAG
segment9 9bp 229..241 UUGCGCAAUGGGC 246..255 GUCUGACGCA
segment10 8bp 268..281 GAAUGAUGAAGGUC 286..298 GAUUGUAAAAUUC
segment11 8bp 302..312 CACCGGGGACG 320..334 CGGUACCCGGAGAAU
segment12 12bp 337..359 GCCCCGGCUAACUUCGUGCCAGC 364..382 GCGGUAAUACGAAGGGGGC
segment13 4bp 404..412 GGCGUAAAG 716..719 CGCC
segment14 8bp 414..423 GAGCGUAGGC 593..602 GCUGAGGCUC
segment15 25bp 425..454 GACAUUUAAGUCAGGGGUGAAAUCCCGGGG 460..488 CCUCGGAAUUGCCUUUGAUACUGGGUGUC
segment16 16bp 492..509 AGUGUGAGAGAGGUAUGU 572..589 ACAUACUGGCUCAUUACU
segment17 13bp 510..527 GGAACUCCGAGUGUAGAG 534..555 UUCGUAGAUAUUCGGGAAGAAC
segment18 3bp 560..564 GUGGC 569..571 GCG
segment19 12bp 607..624 GCGUGGGGAGCAAACAGG 634..648 CCUGGUAGUCCACGC
segment20 5bp 659..663 GAUGC 710..715 GCAAUC
segment21 12bp 666..677 GUUGUCGGGAUG 682..693 CAUUUCGGUGAC
segment22 2bp 697..698 GC 703..704 GC
segment23 7bp 721..733 GGGGAGUACGGUC 738..748 GAUUAAAACUC
segment24 14bp 757..772 UGUCGGGGGCCCGCAC 1192..1209 GUUCCCGGGCCUUGUACA
segment25 5bp 775..779 GCGGU 1153..1157 AUCGC
segment26 12bp 780..791 GGAGCAUGUGGU 1060..1072 ACACACGUGCUAC
segment27 4bp 796..799 UUGG 808..811 CGCA
segment28 7bp 820..826 CCACCUU 1050..1056 AGGGUGG
segment29 7bp 832..838 AUGCCCG 874..881 UGGGACAC
segment30 7bp 842..848 CGCCACA 853..859 UGUGGCU
segment31 3bp 862..864 CUC 869..871 GAG
segment32 15bp 883..900 GGUGCUGCAUGGCUGUCG 1027..1045 CGUCAAGUCCUCAUGGCCC
segment33 6bp 904..909 GCUCGU 938..943 ACGAGC
segment34 3bp 910..912 GUC 917..919 GAU
segment35 4bp 922..925 UGGG 932..935 CCCG
segment36 4bp 949..952 CCUC 1019..1022 GGGG
segment37 11bp 954..969 CCAUUAGUUGCCAUCA 976..990 UGGGAACUCUAAUGG
segment38 8bp 993..1000 CUGCCGGU 1006..1013 GCCGGAGG
segment39 10bp 1076..1090 GGCGACUACAGAGGG 1096..1109 CCUUAAAAGUCGUC
segment40 9bp 1116..1127 CGGAUUGUCCUC 1136..1147 GAGGGCAUGAAG
segment41 8bp 1163..1170 AUCGCGGA 1178..1185 GCCGCGGU
