#Name: bpRNA_CRW_2635
#Length:  1192 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
CACGGCCCAGACUCCUACGGGAGGCAGCAGUGGGGAAUAUUGCACAAUGGGCGAAAGCCUGAUGCAGCGACGCCGCGUGAGGGAUGACGGCCUUCGGGUUGUAAACCUCUUUCAGCAGGGAAGAAGCGAAAGUGACGGUACCUGCAGAAGAAGCGCCGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGGCGCAAGCGUUGUCCGGAAUUAUUGGGCGUAAAGAGCUCGUAGGCGGCUUGUCACGUCGGUUGUGAAAGCCCGGGGCUUAACCCCGGGUCUGCAGUCAAUACGGGCAGGCUAGAGUUCGGUAGGGGAGAUCGGAAUUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAGGAACACCGGUGGCGAAGGCGGAUCUCUGGGCCGAUACUGACGCUGAGGAGCGAAAGCGUGGGGAGCGAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGGUGGGCACUAGGUGUGGGCAACAUUCCACGUUGUCCGUGCCGCAGCUAACGCAUUAAGUGCCCCGCCUGGGGAGUACGGCCGCAAGGCUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGCGGAGCAUGUGGCUUAAUUCGACGCAACGCGAAGAACCUUACCAAGGCUUGACAUACACCGGAAAGCAUUAGAGAUAGUGCCCCCCUUGUGGUCGGUGUACAGGUGGUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUCCCGUGUUGCCAGCAGGCCCUUGUGGUGCUGGGGACUCACGGGAGACCGCCGGGGUCAACUCGGAGGAAGGUGGGGACGACGUCAAGUCAUCAUGCCCCUUAUGUCUUGGGCUGCACACGUGCUACAAUGGCCGGUACAAUGAGCUGCGAUACCGCGAGGUGGAGCGAAUCUCAAAAAGCCGGUCUCAGUUCGGAUUGGGGUCUGCAACUCGACCCCAUGAAGUCGGAGUCGCUAGUAAUCGCAGAUCAGCAUUGCUGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACGUCACGAAAGUCGGUAACACCCGAAGCCGGUGGCCCAACCCCUUGUGGGAGGGAGCUGUCGAAGGUGGGACUGGCGAUUGGGACGAAGUCGUAACAAGGUAGCCGUACCGGAAGGUGCGGCU
...........((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(............)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((.......))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....(((((((((..(((((((....))))))).((.....))))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..(((............)))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((...(((((..((...(((....)))...))....)))))...)))))))......(...((((((((........))))))))...).....))))).....((((((((........))))))))......))...)))))))))).))..(.((.((.(.((((((((..((((((((((((....((((((.((((..((.......)).))))))))))...))))))))))))..))))))))..).))..))..)..(((((((((....)))))))))
EEEEEEEEEEESSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISHHHHHHHHHHHHSIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSHHHHHSSSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSSHHHHHHHHHHHHSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIISSSSSIISSIIISSSHHHHSSSIIISSIIIISSSSSIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXSISSISSISISSSSSSSSIISSSSSSSSSSSSIIIISSSSSSBSSSSIISSHHHHHHHSSISSSSSSSSSSIIISSSSSSSSSSSSIISSSSSSSSIISISSIISSIISXXSSSSSSSSSHHHHSSSSSSSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 12..15 "CUCC" 20..23 "GGAG"
S2 40..40 "U" 66..66 "A"
S3 42..44 "GCA" 63..65 "UGC"
S4 48..52 "UGGGC" 57..61 "GCCUG"
S5 79..82 "GAGG" 106..109 "CCUC"
S6 89..92 "GGCC" 97..100 "GGUU"
S7 113..113 "C" 150..150 "G"
S8 114..119 "AGCAGG" 141..146 "CCUGCA"
S9 123..123 "G" 136..136 "C"
S10 153..157 "GCGCC" 194..198 "GGCGC"
S11 164..168 "CUACG" 189..193 "CGUAG"
S12 174..175 "GC" 180..181 "GC"
S13 220..222 "GGC" 536..538 "GCC"
S14 228..228 "G" 535..535 "C"
S15 230..233 "GCUC" 414..417 "GAGC"
S16 235..235 "U" 411..411 "G"
S17 237..239 "GGC" 408..410 "GCU"
S18 241..247 "GCUUGUC" 298..304 "GGCAGGC"
S19 249..250 "CG" 296..297 "CG"
S20 251..259 "UCGGUUGUG" 285..293 "UGCAGUCAA"
S21 264..270 "CCCGGGG" 276..282 "CCCCGGG"
S22 308..310 "AGU" 402..404 "ACU"
S23 311..316 "UCGGUA" 395..400 "GGCCGA"
S24 319..325 "GGAGAUC" 387..393 "GAUCUCU"
S25 326..327 "GG" 369..370 "AC"
S26 330..337 "UUCCUGGU" 359..366 "AUCAGGAG"
S27 341..343 "GCG" 350..352 "UGC"
S28 375..375 "G" 386..386 "G"
S29 378..379 "GC" 384..385 "GC"
S30 422..427 "GCGUGG" 458..463 "CCACGC"
S31 431..432 "GC" 456..457 "GU"
S32 436..439 "CAGG" 449..452 "CCUG"
S33 474..479 "GGGCAC" 529..534 "GUGCCC"
S34 482..493 "GGUGUGGGCAAC" 501..512 "GUUGUCCGUGCC"
S35 516..517 "GC" 522..523 "GC"
S36 540..542 "GGG" 565..567 "CUC"
S37 549..552 "GGCC" 557..560 "GGCU"
S38 576..577 "UG" 1056..1057 "CA"
S39 578..580 "ACG" 1052..1054 "UGU"
S40 582..588 "GGGCCCG" 1045..1051 "CGGGCCU"
S41 590..591 "AC" 1040..1041 "GU"
S42 594..598 "GCGGC" 1000..1004 "GUCGC"
S43 599..608 "GGAGCAUGUG" 888..897 "CACGUGCUAC"
S44 609..610 "GC" 885..886 "GC"
S45 615..618 "UUCG" 627..630 "CGAA"
S46 639..645 "CCAAGGC" 875..881 "GUCUUGG"
S47 651..659 "AUACACCGG" 690..698 "UCGGUGUAC"
S48 662..668 "AGCAUUA" 673..679 "UAGUGCC"
S49 681..682 "CC" 688..689 "GG"
S50 700..701 "GG" 869..870 "CC"
S51 703..706 "GGUG" 865..868 "UGCC"
S52 709..710 "UG" 863..864 "CA"
S53 711..713 "GCU" 857..859 "AGU"
S54 714..717 "GUCG" 852..855 "CGUC"
S55 721..726 "GCUCGU" 755..760 "ACGAGC"
S56 727..729 "GUC" 734..736 "GAU"
S57 739..742 "UGGG" 749..752 "CCCG"
S58 766..769 "CCUU" 844..847 "GGGG"
S59 771..777 "UCCCGUG" 809..815 "CACGGGA"
S60 781..782 "CC" 803..804 "GG"
S61 785..787 "CAG" 800..802 "CUG"
S62 818..825 "CCGCCGGG" 831..838 "CUCGGAGG"
S63 901..907 "GGCCGGU" 950..956 "GCCGGUC"
S64 911..915 "AUGAG" 942..946 "CUCAA"
S65 918..919 "GC" 936..937 "GC"
S66 923..925 "ACC" 930..932 "GGU"
S67 963..963 "C" 994..994 "G"
S68 967..974 "UUGGGGUC" 983..990 "GACCCCAU"
S69 1010..1017 "AUCGCAGA" 1026..1033 "GCUGCGGU"
S70 1060..1060 "C" 1168..1168 "G"
S71 1062..1063 "GC" 1164..1165 "AC"
S72 1065..1066 "CG" 1160..1161 "CG"
S73 1068..1068 "C" 1158..1158 "G"
S74 1070..1077 "CGUCACGA" 1148..1155 "UUGGGACG"
S75 1080..1091 "GUCGGUAACACC" 1134..1145 "GGUGGGACUGGC"
S76 1096..1101 "GCCGGU" 1125..1130 "GCUGUC"
S77 1103..1106 "GCCC" 1121..1124 "GGGA"
S78 1109..1110 "CC" 1118..1119 "GG"
S79 1171..1179 "AGCCGUACC" 1184..1192 "GGUGCGGCU"
H1 16..19 "UACG" (15,20) C:G 
H2 53..56 "GAAA" (52,57) C:G 
H3 93..96 "UUCG" (92,97) C:G 
H4 124..135 "AAGCGAAAGUGA" (123,136) G:C 
H5 176..179 "AGCC" (175,180) C:G PK{2}
H6 271..275 "CUUAA" (270,276) G:C 
H7 344..349 "GUGAAA" (343,350) G:U 
H8 380..383 "GAAG" (379,384) C:G 
H9 440..448 "AUUAGAUAC" (439,449) G:C 
H10 494..500 "AUUCCAC" (493,501) C:G 
H11 518..521 "UAAC" (517,522) C:G PK{3}
H12 553..556 "GCAA" (552,557) C:G 
H13 619..626 "ACGCAACG" (618,627) G:C 
H14 669..672 "GAGA" (668,673) A:U 
H15 683..687 "CUUGU" (682,688) C:G 
H16 730..733 "GUGA" (729,734) C:G 
H17 743..748 "UUAAGU" (742,749) G:C 
H18 788..799 "GCCCUUGUGGUG" (787,800) G:C 
H19 826..830 "GUCAA" (825,831) G:C 
H20 926..929 "GCGA" (925,930) C:G 
H21 975..982 "UGCAACUC" (974,983) C:G 
H22 1018..1025 "UCAGCAUU" (1017,1026) A:G 
H23 1111..1117 "CCUUGUG" (1110,1118) C:G 
H24 1180..1183 "GGAA" (1179,1184) C:G 
B1 41..41 "U" (40,66) U:A (65,42) C:G 
B2 147..149 "GAA" (146,114) A:A (113,150) C:G PK{1}
B3 158..163 "GGCUAA" (157,194) C:G (193,164) G:C PK{2}
B4 223..227 "GUAAA" (222,536) C:G (535,228) C:G PK{3}
B5 236..236 "A" (235,411) U:G (410,237) U:G 
B6 248..248 "A" (247,298) C:G (297,249) G:C 
B7 294..295 "UA" (293,251) A:U (250,296) G:C 
B8 376..377 "UG" (375,386) G:G (385,378) C:G 
B9 401..401 "U" (400,311) A:U (310,402) U:A 
B10 428..430 "GGA" (427,458) G:C (457,431) U:G 
B11 581..581 "G" (580,1052) G:U (1051,582) U:G 
B12 702..702 "U" (701,869) G:C (868,703) C:G 
B13 707..708 "CA" (706,865) G:U (864,709) A:U 
B14 783..784 "AG" (782,803) C:G (802,785) G:C 
B15 856..856 "A" (855,714) C:G (713,857) U:A 
B16 860..862 "CAU" (859,711) U:G (710,863) G:C 
B17 887..887 "A" (886,609) C:G (608,888) G:C 
B18 1055..1055 "A" (1054,578) U:A (577,1056) G:C 
B19 1102..1102 "G" (1101,1125) U:G (1124,1103) A:G 
I1.1 45..47 "CAA" (44,63) A:U 
I1.2 62..62 "A" (61,48) G:U 
I2.1 83..88 "GAUGAC" (82,106) G:C 
I2.2 101..105 "GUAAA" (100,89) U:G 
I3.1 120..122 "GAA" (119,141) G:C 
I3.2 137..140 "GGUA" (136,123) C:G 
I4.1 169..173 "UGCCA" (168,189) G:C 
I4.2 182..188 "GGUAAUA" (181,174) C:G 
I5.1 234..234 "G" (233,414) C:G 
I5.2 412..413 "AG" (411,235) G:U 
I6.1 260..263 "AAAG" (259,285) G:U 
I6.2 283..284 "UC" (282,264) G:C 
I7.1 317..318 "GG" (316,395) A:G 
I7.2 394..394 "G" (393,319) U:G 
I8.1 328..329 "AA" (327,369) G:A 
I8.2 367..368 "GA" (366,330) G:U 
I9.1 338..340 "GUA" (337,359) U:A 
I9.2 353..358 "GCAGAU" (352,341) C:G 
I10.1 433..435 "GAA" (432,456) C:G 
I10.2 453..455 "GUA" (452,436) G:C 
I11.1 543..548 "GAGUAC" (542,565) G:C 
I11.2 561..564 "AAAA" (560,549) U:G 
I12.1 589..589 "C" (588,1045) G:C 
I12.2 1042..1044 "UCC" (1041,590) U:A 
I13.1 778..780 "UUG" (777,809) G:C 
I13.2 805..808 "GACU" (804,781) G:C 
I14.1 908..910 "ACA" (907,950) U:G 
I14.2 947..949 "AAA" (946,911) A:A 
I15.1 916..917 "CU" (915,942) G:C 
I15.2 938..941 "GAAU" (937,918) C:G 
I16.1 920..922 "GAU" (919,936) C:G 
I16.2 933..935 "GGA" (932,923) U:A 
I17.1 964..966 "GGA" (963,994) C:G 
I17.2 991..993 "GAA" (990,967) U:U 
I18.1 1061..1061 "C" (1060,1168) C:G 
I18.2 1166..1167 "AA" (1165,1062) C:G 
I19.1 1064..1064 "C" (1063,1164) C:A 
I19.2 1162..1163 "UA" (1161,1065) G:C 
I20.1 1067..1067 "U" (1066,1160) G:C 
I20.2 1159..1159 "U" (1158,1068) G:C 
I21.1 1069..1069 "A" (1068,1158) C:G 
I21.2 1156..1157 "AA" (1155,1070) G:C 
I22.1 1078..1079 "AA" (1077,1148) A:U 
I22.2 1146..1147 "GA" (1145,1080) C:G 
I23.1 1092..1095 "CGAA" (1091,1134) C:G 
I23.2 1131..1133 "GAA" (1130,1096) C:G 
I24.1 1107..1108 "AA" (1106,1121) C:G 
I24.2 1120..1120 "A" (1119,1109) G:C 
M1.1 229..229 "A" (228,535) G:C (417,230) C:G 
M1.2 418..421 "GAAA" (417,230) C:G (463,422) C:G 
M1.3 464..473 "CGUAAACGGU" (463,422) C:G (534,474) C:G 
M1.4 535..534 "" (534,474) C:G (228,535) G:C 
M2.1 240..240 "G" (239,408) C:G (304,241) C:G 
M2.2 305..307 "UAG" (304,241) C:G (404,308) U:A 
M2.3 405..407 "GAC" (404,308) U:A (239,408) C:G 
M3.1 326..325 "" (325,387) C:G (370,326) C:G 
M3.2 371..374 "ACCG" (370,326) C:G (386,375) G:G 
M3.3 387..386 "" (386,375) G:G (325,387) C:G 
M4.1 480..481 "UA" (479,529) C:G (512,482) C:G 
M4.2 513..515 "GCA" (512,482) C:G (523,516) C:G 
M4.3 524..528 "AUUAA" (523,516) C:G (479,529) C:G 
M5.1 592..593 "AA" (591,1040) C:G (1004,594) C:G 
M5.2 1005..1009 "UAGUA" (1004,594) C:G (1033,1010) U:A 
M5.3 1034..1039 "GAAUAC" (1033,1010) U:A (591,1040) C:G 
M6.1 599..598 "" (598,1000) C:G (897,599) C:G 
M6.2 898..900 "AAU" (897,599) C:G (956,901) C:G 
M6.3 957..962 "UCAGUU" (956,901) C:G (994,963) G:C 
M6.4 995..999 "UCGGA" (994,963) G:C (598,1000) C:G 
M7.1 611..614 "UUAA" (610,885) C:G (630,615) A:U 
M7.2 631..638 "GAACCUUA" (630,615) A:U (881,639) G:C 
M7.3 882..884 "GCU" (881,639) G:C (610,885) C:G 
M8.1 646..650 "UUGAC" (645,875) C:G (698,651) C:A 
M8.2 699..699 "A" (698,651) C:A (870,700) C:G 
M8.3 871..874 "UUAU" (870,700) C:G (645,875) C:G 
M9.1 660..661 "AA" (659,690) G:U (679,662) C:A 
M9.2 680..680 "C" (679,662) C:A (689,681) G:C 
M9.3 690..689 "" (689,681) G:C (659,690) G:U 
M10.1 718..720 "UCA" (717,852) G:C (760,721) C:G 
M10.2 761..765 "GCAAC" (760,721) C:G (847,766) G:C 
M10.3 848..851 "ACGA" (847,766) G:C (717,852) G:C 
M11.1 727..726 "" (726,755) U:A (736,727) U:G 
M11.2 737..738 "GU" (736,727) U:G (752,739) G:U 
M11.3 753..754 "CA" (752,739) G:U (726,755) U:A 
M12.1 770..770 "G" (769,844) U:G (815,771) A:U 
M12.2 816..817 "GA" (815,771) A:U (838,818) G:C 
M12.3 839..843 "AAGGU" (838,818) G:C (769,844) U:G 
X1 24..39 "GCAGCAGUGGGGAAUA" (23,12) G:C (66,40) A:U 
X2 67..78 "GCGACGCCGCGU" (66,40) A:U (109,79) C:G 
X3 110..112 "UUU" (109,79) C:G (150,113) G:C PK{1}
X4 151..152 "AA" (150,113) G:C (198,153) C:G 
X5 199..219 "AAGCGUUGUCCGGAAUUAUUG" (198,153) C:G (538,220) C:G 
X7 568..575 "AAAGGAAU" (567,540) C:G (1057,576) A:U 
X8 1058..1059 "CA" (1057,576) A:U (1168,1060) G:C 
X9 1169..1170 "GU" (1168,1060) G:C (1192,1171) U:A 
E1 1..11 "CACGGCCCAGA" 
PK1 1bp 111..111 149..149 X3 110..112 B2 147..149
PK2 3bp 158..160 177..179 B3 158..163 H5 176..179
PK3 2bp 223..224 520..521 B4 223..227 H11 518..521
PK1.1 111 U 149 A
PK2.1 158 G 179 C
PK2.2 159 G 178 C
PK2.3 160 C 177 G
PK3.1 223 G 521 C
PK3.2 224 U 520 A
NCBP1 662 A 679 C S48
NCBP2 715 U 854 U S54
NCBP3 1103 G 1124 A S77
NCBP4 1074 A 1151 G S74
NCBP5 1063 C 1164 A S71
NCBP6 911 A 946 A S64
NCBP7 651 A 698 C S47
NCBP8 1087 A 1138 G S75
NCBP9 967 U 990 U S68
NCBP10 375 G 386 G S28
NCBP11 316 A 395 G S23
NCBP12 114 A 146 A S8
NCBP13 327 G 369 A S25
NCBP14 1017 A 1026 G S69
NCBP15 1086 A 1139 G S75
NCBP16 820 G 836 A S62
NCBP17 1097 C 1129 U S76
NCBP18 600 G 896 A S43
NCBP19 252 C 292 A S20
segment1 4bp 12..15 CUCC 20..23 GGAG
segment2 9bp 40..52 UUGCACAAUGGGC 57..66 GCCUGAUGCA
segment3 8bp 79..92 GAGGGAUGACGGCC 97..109 GGUUGUAAACCUC
segment4 8bp 113..123 CAGCAGGGAAG 136..150 CGGUACCUGCAGAAG
segment5 12bp 153..175 GCGCCGGCUAACUACGUGCCAGC 180..198 GCGGUAAUACGUAGGGCGC
segment6 4bp 220..228 GGCGUAAAG 535..538 CGCC
segment7 8bp 230..239 GCUCGUAGGC 408..417 GCUGAGGAGC
segment8 25bp 241..270 GCUUGUCACGUCGGUUGUGAAAGCCCGGGG 276..304 CCCCGGGUCUGCAGUCAAUACGGGCAGGC
segment9 16bp 308..325 AGUUCGGUAGGGGAGAUC 387..404 GAUCUCUGGGCCGAUACU
segment10 13bp 326..343 GGAAUUCCUGGUGUAGCG 350..370 UGCGCAGAUAUCAGGAGGAAC
segment11 3bp 375..379 GUGGC 384..386 GCG
segment12 12bp 422..439 GCGUGGGGAGCGAACAGG 449..463 CCUGGUAGUCCACGC
segment13 6bp 474..479 GGGCAC 529..534 GUGCCC
segment14 12bp 482..493 GGUGUGGGCAAC 501..512 GUUGUCCGUGCC
segment15 2bp 516..517 GC 522..523 GC
segment16 7bp 540..552 GGGGAGUACGGCC 557..567 GGCUAAAACUC
segment17 14bp 576..591 UGACGGGGGCCCGCAC 1040..1057 GUUCCCGGGCCUUGUACA
segment18 5bp 594..598 GCGGC 1000..1004 GUCGC
segment19 12bp 599..610 GGAGCAUGUGGC 885..897 GCACACGUGCUAC
segment20 4bp 615..618 UUCG 627..630 CGAA
segment21 7bp 639..645 CCAAGGC 875..881 GUCUUGG
segment22 9bp 651..659 AUACACCGG 690..698 UCGGUGUAC
segment23 7bp 662..668 AGCAUUA 673..679 UAGUGCC
segment24 2bp 681..682 CC 688..689 GG
segment25 15bp 700..717 GGUGGUGCAUGGCUGUCG 852..870 CGUCAAGUCAUCAUGCCCC
segment26 6bp 721..726 GCUCGU 755..760 ACGAGC
segment27 3bp 727..729 GUC 734..736 GAU
segment28 4bp 739..742 UGGG 749..752 CCCG
segment29 4bp 766..769 CCUU 844..847 GGGG
segment30 12bp 771..787 UCCCGUGUUGCCAGCAG 800..815 CUGGGGACUCACGGGA
segment31 8bp 818..825 CCGCCGGG 831..838 CUCGGAGG
segment32 17bp 901..925 GGCCGGUACAAUGAGCUGCGAUACC 930..956 GGUGGAGCGAAUCUCAAAAAGCCGGUC
segment33 9bp 963..974 CGGAUUGGGGUC 983..994 GACCCCAUGAAG
segment34 8bp 1010..1017 AUCGCAGA 1026..1033 GCUGCGGU
segment35 38bp 1060..1110 CCGCCCGUCACGUCACGAAAGUCGGUAACACCCGAAGCCGGUGGCCCAACC 1118..1168 GGAGGGAGCUGUCGAAGGUGGGACUGGCGAUUGGGACGAAGUCGUAACAAG
segment36 9bp 1171..1179 AGCCGUACC 1184..1192 GGUGCGGCU
