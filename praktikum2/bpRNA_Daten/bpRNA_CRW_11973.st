#Name: bpRNA_CRW_11973
#Length:  1188 
#PageNumber: 2
ACUCCUACGGGAGGCAGCAGUAGGGAAUCUUCCGCAAUGGACGAAAGUCUGACGGAGCAACGCCGCGUGAGUGAUGAAGGCUUUCGGGUCGUAAAACUCUGUUGUUAGGGAAGAACAAGUGCUAGUUGAAUAAGCUGGCACCUUGACGGUACCUAACCAGAAAGCCACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUAGGUGGCAAGCGUUAUCCGGAAUUAUUGGGCGUAAAGCGCGCGCAGGUGGUUUCUUAAGUCUGAUGUGAAAGCCCACGGCUCAACCGUGGAGGGUCAUUGGAAACUGGGAGACUUGAGUGCAGAAGAGGAAAGUGGAAUUCCAUGUGUAGCGGUGAAAUGCGUAGAGAUAUGGAGGAACACCAGUGGCGAAGGCGACUUUCUGGUCUGUAACUGACACUGAGGCGCGAAAGCGUGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCCGUAAACGAUGAGUGCUAAGUGUUAGAGGGUUUCCGCCCUUUAGUGCUGAAGUUAACGCAUUAAGCACUCCGCCUGGGGAGUACGGCCGCAAGGCUGAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUUAAUUCGAAGCAACGCGAAGAACCUUACCAGGUCUUGACAUCCUCUGACAACCCUAGAGAUAGGGCUUCUCCUUCGGGAGCAGAGUGACAGGUGGUGCAUGGUUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGAUCUUAGUUGCCAUCAUUCAGUUGGGCACUCUAAGGUGACUGCCGGUGACAAACCGGAGGAAGGUGGGGAUGACGUCAAAUCAUCAUGCCCCUUAUGACCUGGGCUACACACGUGCUACAAUGGACGGUACAAAGAGCUGCAAGACCGCGAGGUGGAGCUAAUCUCAUAAAACCGUUCUCAGUUCGGAUUGUAGGCUGCAACUCGCCUACAUGAAGCUGGAAUCGCUAGUAAUCGCGGAUCAGCAUGCCGCGGUGAAUACGUUCCCGGGCCUUGUACACACCGCCCGUCACACCACGAGAGUUUGUAACACCCGAAGUCGGUGGGGUAACCUUUUGGAGCCAGCCGCCUAAGGUGGGACAGAUGAUUGGGGUGAAGUCGUAACAGUAGCCUAUCGGAAGNUCAGGG
.((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).(..((((((...(.....((((((((.......)))))))).....)....))))))..)...((((([[[...(((((.....((.]]])).......)))))))))).....................((([[.....((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((......))))))))))))...((..]])).....)))))).))).(((......((((....))))....)))........(((((.(((((((.((..(((((((((((((((((....((((........))))........(((((((.....((((((((..(((((((....))))))).((((....))))))))).))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((..((......))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))......(...((((((((........))))))))...).....))))).....((((((((.......))))))))......))...)))))))))).))....((.((...((((((((..((((((((((((....((((((.((((..((....)).))))))))))...))))))))))))..))))))))....))..))....(((((((....)))).)))
ESSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXSIISSSSSSIIISIIIIISSSSSSSSHHHHHHHSSSSSSSSIIIIISIIIISSSSSSIISXXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSMMMMMMMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSMSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSISSMMSSSSSSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSMMSSSSSSSHHHHSSSSSSSMSSSSHHHHSSSSSSSSSBSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSMMMSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSMMMMMMSIIISSSSSSSSHHHHHHHHSSSSSSSSIIISMMMMMSSSSSMMMMMSSSSSSSSHHHHHHHSSSSSSSSMMMMMMSSIIISSSSSSSSSSBSSXXXXSSISSIIISSSSSSSSIISSSSSSSSSSSSIIIISSSSSSBSSSSIISSHHHHSSISSSSSSSSSSIIISSSSSSSSSSSSIISSSSSSSSIIIISSIISSXXXXSSSSSSSHHHHSSSSBSSS
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 2..5 "CUCC" 10..13 "GGAG"
S2 30..30 "U" 56..56 "A"
S3 32..34 "CCG" 53..55 "CGG"
S4 38..42 "UGGAC" 47..51 "GUCUG"
S5 69..72 "GAGU" 96..99 "ACUC"
S6 79..82 "GGCU" 87..90 "GGUC"
S7 101..101 "G" 160..160 "G"
S8 104..109 "GUUAGG" 152..157 "CCUAAC"
S9 113..113 "G" 147..147 "C"
S10 119..126 "GUGCUAGU" 134..141 "GCUGGCAC"
S11 164..168 "GCCAC" 205..209 "GUGGC"
S12 175..179 "CUACG" 200..204 "CGUAG"
S13 185..186 "GC" 191..192 "GC"
S14 231..233 "GGC" 546..548 "GCC"
S15 241..244 "GCGC" 425..428 "GCGC"
S16 246..246 "C" 422..422 "G"
S17 248..250 "GGU" 419..421 "ACU"
S18 252..258 "GUUUCUU" 309..315 "GGGAGAC"
S19 260..261 "AG" 307..308 "CU"
S20 262..270 "UCUGAUGUG" 296..304 "GUCAUUGGA"
S21 275..281 "CCCACGG" 287..293 "CCGUGGA"
S22 319..321 "AGU" 413..415 "ACU"
S23 322..327 "GCAGAA" 406..411 "GUCUGU"
S24 330..336 "GGAAAGU" 398..404 "ACUUUCU"
S25 337..338 "GG" 380..381 "AC"
S26 341..348 "UUCCAUGU" 370..377 "AUAUGGAG"
S27 352..354 "GCG" 361..363 "UGC"
S28 386..386 "G" 397..397 "G"
S29 389..390 "GC" 395..396 "GC"
S30 433..438 "GCGUGG" 469..474 "CCACGC"
S31 442..443 "GC" 467..468 "GU"
S32 447..450 "CAGG" 460..463 "CCUG"
S33 485..490 "GAGUGC" 539..544 "GCACUC"
S34 493..504 "AGUGUUAGAGGG" 511..522 "CCCUUUAGUGCU"
S35 526..527 "GU" 532..533 "GC"
S36 550..552 "GGG" 575..577 "CUC"
S37 559..562 "GGCC" 567..570 "GGCU"
S38 586..587 "UG" 1059..1060 "CA"
S39 588..590 "ACG" 1055..1057 "UGU"
S40 592..598 "GGGCCCG" 1048..1054 "CGGGCCU"
S41 600..601 "AC" 1043..1044 "GU"
S42 604..608 "GCGGU" 1004..1008 "AUCGC"
S43 609..618 "GGAGCAUGUG" 892..901 "CACGUGCUAC"
S44 619..620 "GU" 889..890 "AC"
S45 625..628 "UUCG" 637..640 "CGAA"
S46 649..655 "CCAGGUC" 879..885 "GACCUGG"
S47 661..663 "AUC" 708..710 "GAC"
S48 664..668 "CUCUG" 702..706 "CAGAG"
S49 671..677 "AACCCUA" 682..688 "UAGGGCU"
S50 690..693 "CUCC" 698..701 "GGAG"
S51 712..713 "GG" 873..874 "CC"
S52 715..718 "GGUG" 869..872 "UGCC"
S53 721..722 "UG" 867..868 "CA"
S54 723..725 "GUU" 861..863 "AAU"
S55 726..729 "GUCG" 856..859 "CGUC"
S56 733..738 "GCUCGU" 767..772 "ACGAGC"
S57 739..741 "GUC" 746..748 "GAU"
S58 751..754 "UGGG" 761..764 "CCCG"
S59 778..781 "CCUU" 848..851 "GGGG"
S60 783..789 "AUCUUAG" 813..819 "CUAAGGU"
S61 793..794 "CC" 807..808 "GG"
S62 797..798 "CA" 805..806 "UG"
S63 822..829 "CUGCCGGU" 835..842 "ACCGGAGG"
S64 905..911 "GGACGGU" 954..960 "ACCGUUC"
S65 917..919 "GAG" 946..948 "CUC"
S66 922..923 "GC" 940..941 "GC"
S67 927..929 "ACC" 934..936 "GGU"
S68 967..967 "C" 998..998 "G"
S69 971..978 "UUGUAGGC" 987..994 "GCCUACAU"
S70 1014..1021 "AUCGCGGA" 1029..1036 "GCCGCGGU"
S71 1065..1066 "GC" 1164..1165 "AC"
S72 1068..1069 "CG" 1160..1161 "CG"
S73 1073..1080 "CACCACGA" 1148..1155 "UUGGGGUG"
S74 1083..1094 "GUUUGUAACACC" 1134..1145 "GGUGGGACAGAU"
S75 1099..1104 "GUCGGU" 1125..1130 "GCCGCC"
S76 1106..1109 "GGGU" 1121..1124 "GCCA"
S77 1112..1113 "CC" 1118..1119 "GG"
S78 1170..1172 "GCC" 1186..1188 "GGG"
S79 1173..1176 "UAUC" 1181..1184 "GNUC"
H1 6..9 "UACG" (5,10) C:G 
H2 43..46 "GAAA" (42,47) C:G 
H3 83..86 "UUCG" (82,87) U:G 
H4 127..133 "UGAAUAA" (126,134) U:G 
H5 187..190 "AGCC" (186,191) C:G PK{1}
H6 282..286 "CUCAA" (281,287) G:C 
H7 355..360 "GUGAAA" (354,361) G:U 
H8 391..394 "GAAG" (390,395) C:G 
H9 451..459 "AUUAGAUAC" (450,460) G:C 
H10 505..510 "UUUCCG" (504,511) G:C 
H11 528..531 "UAAC" (527,532) U:G PK{2}
H12 563..566 "GCAA" (562,567) C:G 
H13 629..636 "AAGCAACG" (628,637) G:C 
H14 678..681 "GAGA" (677,682) A:U 
H15 694..697 "UUCG" (693,698) C:G 
H16 742..745 "GUGA" (741,746) C:G 
H17 755..760 "UUAAGU" (754,761) G:C 
H18 799..804 "UUCAGU" (798,805) A:U 
H19 830..834 "GACAA" (829,835) U:A 
H20 930..933 "GCGA" (929,934) C:G 
H21 979..986 "UGCAACUC" (978,987) C:G 
H22 1022..1028 "UCAGCAU" (1021,1029) A:G 
H23 1114..1117 "UUUU" (1113,1118) C:G 
H24 1177..1180 "GGAA" (1176,1181) C:G 
B1 31..31 "U" (30,56) U:A (55,32) G:C 
B2 169..174 "GGCUAA" (168,205) C:G (204,175) G:C PK{1}
B3 247..247 "A" (246,422) C:G (421,248) U:G 
B4 259..259 "A" (258,309) U:G (308,260) U:A 
B5 305..306 "AA" (304,262) A:U (261,307) G:C 
B6 387..388 "UG" (386,397) G:G (396,389) C:G 
B7 412..412 "A" (411,322) U:G (321,413) U:A 
B8 439..441 "GGA" (438,469) G:C (468,442) U:G 
B9 591..591 "G" (590,1055) G:U (1054,592) U:G 
B10 707..707 "U" (706,664) G:C (663,708) C:G 
B11 714..714 "U" (713,873) G:C (872,715) C:G 
B12 719..720 "CA" (718,869) G:U (868,721) A:U 
B13 795..796 "AU" (794,807) C:G (806,797) G:C 
B14 860..860 "A" (859,726) C:G (725,861) U:A 
B15 864..866 "CAU" (863,723) U:G (722,867) G:C 
B16 891..891 "A" (890,619) C:G (618,892) G:C 
B17 1058..1058 "A" (1057,588) U:A (587,1059) G:C 
B18 1105..1105 "G" (1104,1125) U:G (1124,1106) A:G 
B19 1185..1185 "A" (1184,1173) C:U (1172,1186) C:G 
I1.1 35..37 "CAA" (34,53) G:C 
I1.2 52..52 "A" (51,38) G:U 
I2.1 73..78 "GAUGAA" (72,96) U:A 
I2.2 91..95 "GUAAA" (90,79) C:G 
I3.1 102..103 "UU" (101,160) G:G 
I3.2 158..159 "CA" (157,104) C:G 
I4.1 110..112 "GAA" (109,152) G:C 
I4.2 148..151 "GGUA" (147,113) C:G 
I5.1 114..118 "AACAA" (113,147) G:C 
I5.2 142..146 "CUUGA" (141,119) C:G 
I6.1 180..184 "UGCCA" (179,200) G:C 
I6.2 193..199 "GGUAAUA" (192,185) C:G 
I7.1 245..245 "G" (244,425) C:G 
I7.2 423..424 "AG" (422,246) G:C 
I8.1 271..274 "AAAG" (270,296) G:G 
I8.2 294..295 "GG" (293,275) A:C 
I9.1 328..329 "GA" (327,406) A:G 
I9.2 405..405 "G" (404,330) U:G 
I10.1 339..340 "AA" (338,380) G:A 
I10.2 378..379 "GA" (377,341) G:U 
I11.1 349..351 "GUA" (348,370) U:A 
I11.2 364..369 "GUAGAG" (363,352) C:G 
I12.1 444..446 "AAA" (443,467) C:G 
I12.2 464..466 "GUA" (463,447) G:C 
I13.1 553..558 "GAGUAC" (552,575) G:C 
I13.2 571..574 "GAAA" (570,559) U:G 
I14.1 599..599 "C" (598,1048) G:C 
I14.2 1045..1047 "UCC" (1044,600) U:A 
I15.1 790..792 "UUG" (789,813) G:C 
I15.2 809..812 "CACU" (808,793) G:C 
I16.1 912..916 "ACAAA" (911,954) U:A 
I16.2 949..953 "AUAAA" (948,917) C:G 
I17.1 920..921 "CU" (919,946) G:C 
I17.2 942..945 "UAAU" (941,922) C:G 
I18.1 924..926 "AAG" (923,940) C:G 
I18.2 937..939 "GGA" (936,927) U:A 
I19.1 968..970 "GGA" (967,998) C:G 
I19.2 995..997 "GAA" (994,971) U:U 
I20.1 1067..1067 "C" (1066,1164) C:A 
I20.2 1162..1163 "UA" (1161,1068) G:C 
I21.1 1070..1072 "UCA" (1069,1160) G:C 
I21.2 1156..1159 "AAGU" (1155,1073) G:C 
I22.1 1081..1082 "GA" (1080,1148) A:U 
I22.2 1146..1147 "GA" (1145,1083) U:G 
I23.1 1095..1098 "CGAA" (1094,1134) C:G 
I23.2 1131..1133 "UAA" (1130,1099) C:G 
I24.1 1110..1111 "AA" (1109,1121) U:G 
I24.2 1120..1120 "A" (1119,1112) G:C 
M1.1 234..240 "GUAAAGC" (233,546) C:G (428,241) C:G PK{2}
M1.2 429..432 "GAAA" (428,241) C:G (474,433) C:G 
M1.3 475..484 "CGUAAACGAU" (474,433) C:G (544,485) C:G 
M1.4 545..545 "C" (544,485) C:G (233,546) C:G 
M2.1 251..251 "G" (250,419) U:A (315,252) C:G 
M2.2 316..318 "UUG" (315,252) C:G (415,319) U:A 
M2.3 416..418 "GAC" (415,319) U:A (250,419) U:A 
M3.1 337..336 "" (336,398) U:A (381,337) C:G 
M3.2 382..385 "ACCA" (381,337) C:G (397,386) G:G 
M3.3 398..397 "" (397,386) G:G (336,398) U:A 
M4.1 491..492 "UA" (490,539) C:G (522,493) U:A 
M4.2 523..525 "GAA" (522,493) U:A (533,526) C:G 
M4.3 534..538 "AUUAA" (533,526) C:G (490,539) C:G 
M5.1 602..603 "AA" (601,1043) C:G (1008,604) C:G 
M5.2 1009..1013 "UAGUA" (1008,604) C:G (1036,1014) U:A 
M5.3 1037..1042 "GAAUAC" (1036,1014) U:A (601,1043) C:G 
M6.1 609..608 "" (608,1004) U:A (901,609) C:G 
M6.2 902..904 "AAU" (901,609) C:G (960,905) C:G 
M6.3 961..966 "UCAGUU" (960,905) C:G (998,967) G:C 
M6.4 999..1003 "CUGGA" (998,967) G:C (608,1004) U:A 
M7.1 621..624 "UUAA" (620,889) U:A (640,625) A:U 
M7.2 641..648 "GAACCUUA" (640,625) A:U (885,649) G:C 
M7.3 886..888 "GCU" (885,649) G:C (620,889) U:A 
M8.1 656..660 "UUGAC" (655,879) C:G (710,661) C:A 
M8.2 711..711 "A" (710,661) C:A (874,712) C:G 
M8.3 875..878 "UUAU" (874,712) C:G (655,879) C:G 
M9.1 669..670 "AC" (668,702) G:C (688,671) U:A 
M9.2 689..689 "U" (688,671) U:A (701,690) G:C 
M9.3 702..701 "" (701,690) G:C (668,702) G:C 
M10.1 730..732 "UCA" (729,856) G:C (772,733) C:G 
M10.2 773..777 "GCAAC" (772,733) C:G (851,778) G:C 
M10.3 852..855 "AUGA" (851,778) G:C (729,856) G:C 
M11.1 739..738 "" (738,767) U:A (748,739) U:G 
M11.2 749..750 "GU" (748,739) U:G (764,751) G:U 
M11.3 765..766 "CA" (764,751) G:U (738,767) U:A 
M12.1 782..782 "G" (781,848) U:G (819,783) U:A 
M12.2 820..821 "GA" (819,783) U:A (842,822) G:C 
M12.3 843..847 "AAGGU" (842,822) G:C (781,848) U:G 
X1 14..29 "GCAGCAGUAGGGAAUC" (13,2) G:C (56,30) A:U 
X2 57..68 "GCAACGCCGCGU" (56,30) A:U (99,69) C:G 
X4 161..163 "AAA" (160,101) G:G (209,164) C:G 
X5 210..230 "AAGCGUUAUCCGGAAUUAUUG" (209,164) C:G (548,231) C:G 
X7 578..585 "AAAGGAAU" (577,550) C:G (1060,586) A:U 
X8 1061..1064 "CACC" (1060,586) A:U (1165,1065) C:G 
X9 1166..1169 "AGUA" (1165,1065) C:G (1188,1170) G:G 
E1 1..1 "A" 
PK1 3bp 169..171 188..190 B2 169..174 H5 187..190
PK2 2bp 234..235 530..531 M1.1 234..240 H11 528..531
PK1.1 169 G 190 C
PK1.2 170 G 189 C
PK1.3 171 C 188 G
PK2.1 234 G 531 C
PK2.2 235 U 530 A
NCBP1 1021 A 1029 G S70
NCBP2 101 G 160 G S7
NCBP3 1090 A 1138 G S74
NCBP4 1089 A 1139 G S74
NCBP5 1173 U 1184 C S79
NCBP6 824 G 840 A S63
NCBP7 661 A 710 C S47
NCBP8 386 G 397 G S28
NCBP9 672 A 687 C S49
NCBP10 275 C 293 A S21
NCBP11 610 G 900 A S43
NCBP12 1077 A 1151 G S73
NCBP13 1175 U 1182 N S79
NCBP14 1106 G 1124 A S76
NCBP15 327 A 406 G S23
NCBP16 727 U 858 U S55
NCBP17 971 U 994 U S69
NCBP18 270 G 296 G S20
NCBP19 338 G 380 A S25
NCBP20 269 U 297 U S20
NCBP21 1066 C 1164 A S71
NCBP22 1170 G 1188 G S78
NCBP23 1100 U 1129 C S75
segment1 4bp 2..5 CUCC 10..13 GGAG
segment2 9bp 30..42 UUCCGCAAUGGAC 47..56 GUCUGACGGA
segment3 8bp 69..82 GAGUGAUGAAGGCU 87..99 GGUCGUAAAACUC
segment4 16bp 101..126 GUUGUUAGGGAAGAACAAGUGCUAGU 134..160 GCUGGCACCUUGACGGUACCUAACCAG
segment5 12bp 164..186 GCCACGGCUAACUACGUGCCAGC 191..209 GCGGUAAUACGUAGGUGGC
segment6 3bp 231..233 GGC 546..548 GCC
segment7 8bp 241..250 GCGCGCAGGU 419..428 ACUGAGGCGC
segment8 25bp 252..281 GUUUCUUAAGUCUGAUGUGAAAGCCCACGG 287..315 CCGUGGAGGGUCAUUGGAAACUGGGAGAC
segment9 16bp 319..336 AGUGCAGAAGAGGAAAGU 398..415 ACUUUCUGGUCUGUAACU
segment10 13bp 337..354 GGAAUUCCAUGUGUAGCG 361..381 UGCGUAGAGAUAUGGAGGAAC
segment11 3bp 386..390 GUGGC 395..397 GCG
segment12 12bp 433..450 GCGUGGGGAGCAAACAGG 460..474 CCUGGUAGUCCACGC
segment13 6bp 485..490 GAGUGC 539..544 GCACUC
segment14 12bp 493..504 AGUGUUAGAGGG 511..522 CCCUUUAGUGCU
segment15 2bp 526..527 GU 532..533 GC
segment16 7bp 550..562 GGGGAGUACGGCC 567..577 GGCUGAAACUC
segment17 14bp 586..601 UGACGGGGGCCCGCAC 1043..1060 GUUCCCGGGCCUUGUACA
segment18 5bp 604..608 GCGGU 1004..1008 AUCGC
segment19 12bp 609..620 GGAGCAUGUGGU 889..901 ACACACGUGCUAC
segment20 4bp 625..628 UUCG 637..640 CGAA
segment21 7bp 649..655 CCAGGUC 879..885 GACCUGG
segment22 8bp 661..668 AUCCUCUG 702..710 CAGAGUGAC
segment23 7bp 671..677 AACCCUA 682..688 UAGGGCU
segment24 4bp 690..693 CUCC 698..701 GGAG
segment25 15bp 712..729 GGUGGUGCAUGGUUGUCG 856..874 CGUCAAAUCAUCAUGCCCC
segment26 6bp 733..738 GCUCGU 767..772 ACGAGC
segment27 3bp 739..741 GUC 746..748 GAU
segment28 4bp 751..754 UGGG 761..764 CCCG
segment29 4bp 778..781 CCUU 848..851 GGGG
segment30 11bp 783..798 AUCUUAGUUGCCAUCA 805..819 UGGGCACUCUAAGGU
segment31 8bp 822..829 CUGCCGGU 835..842 ACCGGAGG
segment32 15bp 905..929 GGACGGUACAAAGAGCUGCAAGACC 934..960 GGUGGAGCUAAUCUCAUAAAACCGUUC
segment33 9bp 967..978 CGGAUUGUAGGC 987..998 GCCUACAUGAAG
segment34 8bp 1014..1021 AUCGCGGA 1029..1036 GCCGCGGU
segment35 36bp 1065..1113 GCCCGUCACACCACGAGAGUUUGUAACACCCGAAGUCGGUGGGGUAACC 1118..1165 GGAGCCAGCCGCCUAAGGUGGGACAGAUGAUUGGGGUGAAGUCGUAAC
segment36 7bp 1170..1176 GCCUAUC 1181..1188 GNUCAGGG
