#Name: bpRNA_CRW_8410
#Length:  1187 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
AAACUGUCCGAAAGGAUAGCUAAUACCCUAUAUUGCCUUUUGUCACUAAGGCAAAAGGUGAAAGCGAGGCUGUCAAAGGCUUCGGCUCAAAGAGGGGUUCGCGUCCUAUCAGGUAGUUGGUGGGGUAAUGGCCUACCAAGCCAAUGACGGGUAGCUGGUCUUAGAGGACGAUCAGCCACAGCGGGACUGAGACACGGCCCGCACCCCUACGGGGGGCAGCAGUGGGGAAUCGUGGGCAAUGGGCGCAAGCCUGACCCCGCAAUGCCGCGUGAGGGAAGAAGCCCUUCGGGGUGUAAACCUCUGUCAGAUGGGAAGAUGGGACUAGGGGCUAAUACCUCCUAGUCUUGACGGUACCAUCAGAGGAAGGGACGGCUAACUACGUGCCAGCAGCCGCGGUAAUACGUUGGUCCCGAGCGUUGCGCGAUGUUACUGGGCGUAAAGCGUCCGUAGCUGGUUUCGUAAGCGGAUUGUCAAAGCCCGAAGCUCAACUUCGGCAAGGCAUUCCGAACUGCGUUACUUGAGUCCUGUUCAGGUAGGCGGAAUUCCCGGUGUAGCGGUGAAAUGCGUAGAUAUCGGGAGGAACACCAGUGGCGAAGGCGGCCUACUGGGACGGUACUGACGGUCAUGGACGAAAGCUGGGGGAGCAAACCGGAUUAGAUACCCGGGUAGUCCCAGCCGUAAACGAUGGAUGUUAGGUGUAGUGAUAACUUCGCUGCACCCCAGCUAACGCGUUAAACAUCCCGCCUGGGGAGUACGGGCGCAAGCCUGAAACUCAAAGGAAUUGGCGGGGGCCCGCACAACCGGUGGAGCGUCUGGUUUAAUUCGAUGCUAACCGAAAAACCUUACCAGGGUUUGAAAUGGUAGGAAAGGCUGUCGAAAGAUAGCCGUGUUUCCCUUUUGGGAAAAAUCCUACCACAGGUGGUGCAUGGCCGUCGUCAGCUCGUGUUGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUUGUCCUUAUUUGCUUCCUCGUAUGAGGUGCUCUAUAAGGAGACUGCCGGCGACAAGCCGGAGGAAGGAGGGGACGACGUCAGGUCAGUAUGCCCUUUAUGCCCUGGGCUACACAGGCGCUACAGUGGCAAGGACAAUGAGCAGCGAGAGCGCGAGCUCAAGCUAAUCUCACAAACCUUGUCGUUAAUUUGACUCAAUAUAG
..(((((((....)))))))..............((((((((((.....)))))))))).....(((((((.....).))))))....................(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))....((((((..........)))))).((((....))))................(.(((...(((((....))))).))))............((((......((((....)))).....)))).[.(((((((...(.....(((((((((.....))))))))).....)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).....................((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))))))...(((((((((..(((((((((..((((((((...(((......)))......))))))))..))....(..((....)))))))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((.....))))))))))...((..]])).....)))))))))).(((......((((....))))....)))...............................((((((((((((....((((........))))........(((((((.....(((((((((..(((((((....)))))))...((((((....))))..))))))))))).((.((((..(((((((((...(((((((((....)))..((((......))))..)))))).....((((.(((((((...((.((((....))))))....)))))))..((((((((.....)))))))).....))))....)))).)))...))))))))....)))))))...)).))))))))))...(((((((.....(((..((...(((....)))...))....))).....)))))))....................
EESSSSSSSHHHHSSSSSSSXXXXXXXXXXXXXXSSSSSSSSSSHHHHHSSSSSSSSSSXXXXXSSSSSSSHHHHHSBSSSSSSXXXXXXXXXXXXXXXXXXXXSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSXXSSSSSSSSHHHHSSSSBBBSSSSXXXXSSSSSSHHHHHHHHHHSSSSSSXSSSSHHHHSSSSXXXXXXXXXXXXXXXXSBSSSIIISSSSSHHHHSSSSSISSSSXXXXXXXXXXXXSSSSIIIIIISSSSHHHHSSSSIIIIISSSSXXXSSSSSSSIIISIIIIISSSSSSSSSHHHHHSSSSSSSSSIIIIISIIIISSSSSSBBBSXXSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSSSSMMMSSSSSSSSSIISSSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSSSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSHHHHHSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSMMMMMSSSSSSSSSMMSSSSSSSHHHHSSSSSSSMMMSSSSSSHHHHSSSSBBSSSSSSSSSSSMSSBSSSSBBSSSSSSSSSMMMSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSMMMMMSSSSMSSSSSSSIIISSBSSSSHHHHSSSSSSIIIISSSSSSSMMSSSSSSSSHHHHHSSSSSSSSMMMMMSSSSMMMMSSSSBSSSBBBSSSSSSSSMMMMSSSSSSSMMMSSBSSSSSSSSSSXXXSSSSSSSIIIIISSSIISSIIISSSHHHHSSSIIISSIIIISSSIIIIISSSSSSSEEEEEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 3..9 "ACUGUCC" 14..20 "GGAUAGC"
S2 35..44 "GCCUUUUGUC" 50..59 "GGCAAAAGGU"
S3 65..70 "CGAGGC" 79..84 "GCUUCG"
S4 71..71 "U" 77..77 "A"
S5 105..107 "CCU" 149..151 "GGG"
S6 110..110 "C" 148..148 "C"
S7 112..114 "GGU" 140..142 "GCC"
S8 117..124 "UUGGUGGG" 132..139 "CCUACCAA"
S9 154..157 "GCUG" 173..176 "CAGC"
S10 158..161 "GUCU" 166..169 "GGAC"
S11 181..186 "GCGGGA" 197..202 "GCCCGC"
S12 204..207 "CCCC" 212..215 "GGGG"
S13 232..232 "G" 258..258 "C"
S14 234..236 "GGG" 255..257 "CCC"
S15 240..244 "UGGGC" 249..253 "GCCUG"
S16 271..274 "GAGG" 298..301 "CCUC"
S17 281..284 "GCCC" 289..292 "GGGU"
S18 305..305 "C" 363..363 "G"
S19 306..311 "AGAUGG" 354..359 "CCAUCA"
S20 315..315 "G" 349..349 "C"
S21 321..329 "ACUAGGGGC" 335..343 "CCUCCUAGU"
S22 366..370 "GGGAC" 407..411 "GUCCC"
S23 377..381 "CUACG" 402..406 "CGUUG"
S24 387..388 "GC" 393..394 "GC"
S25 433..435 "GGC" 743..745 "GCC"
S26 441..441 "G" 742..742 "C"
S27 443..446 "GUCC" 627..630 "GGAC"
S28 448..448 "U" 624..624 "C"
S29 450..452 "GCU" 621..623 "GGU"
S30 454..460 "GUUUCGU" 511..517 "GCGUUAC"
S31 462..463 "AG" 509..510 "CU"
S32 464..472 "CGGAUUGUC" 498..506 "GGCAUUCCG"
S33 477..483 "CCCGAAG" 489..495 "CUUCGGC"
S34 521..523 "AGU" 615..617 "ACU"
S35 524..529 "CCUGUU" 608..613 "GGACGG"
S36 532..538 "GGUAGGC" 600..606 "GCCUACU"
S37 539..540 "GG" 582..583 "AC"
S38 543..550 "UUCCCGGU" 572..579 "AUCGGGAG"
S39 554..556 "GCG" 563..565 "UGC"
S40 588..588 "G" 599..599 "G"
S41 591..592 "GC" 597..598 "GC"
S42 635..640 "GCUGGG" 671..676 "CCCAGC"
S43 644..645 "GC" 669..670 "GU"
S44 649..652 "CCGG" 662..665 "CCGG"
S45 687..692 "GGAUGU" 736..741 "ACAUCC"
S46 695..704 "GGUGUAGUGA" 710..719 "UCGCUGCACC"
S47 723..724 "GC" 729..730 "GC"
S48 747..749 "GGG" 772..774 "CUC"
S49 756..759 "GGGC" 764..767 "GCCU"
S50 806..815 "GGAGCGUCUG" 1099..1108 "CAGGCGCUAC"
S51 816..817 "GU" 1096..1097 "AC"
S52 822..825 "UUCG" 834..837 "CGAA"
S53 846..852 "CCAGGGU" 1086..1092 "GCCCUGG"
S54 858..866 "AUGGUAGGA" 908..916 "UCCUACCAC"
S55 869..875 "GGCUGUC" 880..886 "GAUAGCC"
S56 890..891 "UU" 906..907 "AA"
S57 892..895 "UCCC" 900..903 "GGGA"
S58 918..919 "GG" 1080..1081 "CU"
S59 921..924 "GGUG" 1076..1079 "UGCC"
S60 927..928 "UG" 1074..1075 "UA"
S61 929..931 "GCC" 1068..1070 "GGU"
S62 932..935 "GUCG" 1063..1066 "CGUC"
S63 939..944 "GCUCGU" 973..978 "ACGAGC"
S64 945..947 "GUU" 952..954 "GAU"
S65 957..960 "UGGG" 967..970 "CCCG"
S66 984..987 "CCUU" 1055..1058 "GGGG"
S67 989..995 "UCCUUAU" 1020..1026 "AUAAGGA"
S68 999..1000 "CU" 1014..1015 "UG"
S69 1002..1005 "CCUC" 1010..1013 "GAGG"
S70 1029..1036 "CUGCCGGC" 1042..1049 "GCCGGAGG"
S71 1112..1118 "GGCAAGG" 1161..1167 "CCUUGUC"
S72 1124..1126 "GAG" 1153..1155 "CUC"
S73 1129..1130 "GC" 1147..1148 "GC"
S74 1134..1136 "AGC" 1141..1143 "GCU"
H1 10..13 "GAAA" (9,14) C:G 
H2 45..49 "ACUAA" (44,50) C:G 
H3 72..76 "GUCAA" (71,77) U:A 
H4 125..131 "GUAAUGG" (124,132) G:C 
H5 162..165 "UAGA" (161,166) U:G 
H6 187..196 "CUGAGACACG" (186,197) A:G 
H7 208..211 "UACG" (207,212) C:G 
H8 245..248 "GCAA" (244,249) C:G 
H9 285..288 "UUCG" (284,289) C:G 
H10 330..334 "UAAUA" (329,335) C:C 
H11 389..392 "AGCC" (388,393) C:G PK{2}
H12 484..488 "CUCAA" (483,489) G:C 
H13 557..562 "GUGAAA" (556,563) G:U 
H14 593..596 "GAAG" (592,597) C:G 
H15 653..661 "AUUAGAUAC" (652,662) G:C 
H16 705..709 "UAACU" (704,710) A:U 
H17 725..728 "UAAC" (724,729) C:G PK{3}
H18 760..763 "GCAA" (759,764) C:G 
H19 826..833 "AUGCUAAC" (825,834) G:C 
H20 876..879 "GAAA" (875,880) C:G 
H21 896..899 "UUUU" (895,900) C:G 
H22 948..951 "GUGA" (947,952) U:G 
H23 961..966 "UUAAGU" (960,967) G:C 
H24 1006..1009 "GUAU" (1005,1010) C:G 
H25 1037..1041 "GACAA" (1036,1042) C:G 
H26 1137..1140 "GCGA" (1136,1141) C:G 
B1 78..78 "G" (77,71) A:U (70,79) C:G 
B2 108..109 "AU" (107,149) U:G (148,110) C:C 
B3 115..116 "AG" (114,140) U:G (139,117) A:U 
B4 170..172 "GAU" (169,158) C:G (157,173) G:C 
B5 233..233 "U" (232,258) G:C (257,234) C:G 
B6 360..362 "GAG" (359,306) A:A (305,363) C:G PK{1}
B7 371..376 "GGCUAA" (370,407) C:G (406,377) G:C PK{2}
B8 436..440 "GUAAA" (435,743) C:G (742,441) C:G PK{3}
B9 449..449 "A" (448,624) U:C (623,450) U:G 
B10 461..461 "A" (460,511) U:G (510,462) U:A 
B11 507..508 "AA" (506,464) G:C (463,509) G:C 
B12 589..590 "UG" (588,599) G:G (598,591) C:G 
B13 614..614 "U" (613,524) G:C (523,615) U:A 
B14 641..643 "GGA" (640,671) G:C (670,644) U:G 
B15 904..905 "AA" (903,892) A:U (891,906) U:A 
B16 920..920 "U" (919,1080) G:C (1079,921) C:G 
B17 925..926 "CA" (924,1076) G:U (1075,927) A:U 
B18 1001..1001 "U" (1000,1014) U:U (1013,1002) G:C 
B19 1067..1067 "A" (1066,932) C:G (931,1068) C:G 
B20 1071..1073 "CAG" (1070,929) U:G (928,1074) G:U 
B21 1098..1098 "A" (1097,816) C:G (815,1099) G:C 
I1.1 111..111 "A" (110,148) C:C 
I1.2 143..147 "AAUGA" (142,112) C:G 
I2.1 237..239 "CAA" (236,255) G:C 
I2.2 254..254 "A" (253,240) G:U 
I3.1 275..280 "GAAGAA" (274,298) G:C 
I3.2 293..297 "GUAAA" (292,281) U:G 
I4.1 312..314 "GAA" (311,354) G:C 
I4.2 350..353 "GGUA" (349,315) C:G 
I5.1 316..320 "AUGGG" (315,349) G:C 
I5.2 344..348 "CUUGA" (343,321) U:A 
I6.1 382..386 "UGCCA" (381,402) G:C 
I6.2 395..401 "GGUAAUA" (394,387) C:G 
I7.1 447..447 "G" (446,627) C:G 
I7.2 625..626 "AU" (624,448) C:U 
I8.1 473..476 "AAAG" (472,498) C:G 
I8.2 496..497 "AA" (495,477) C:C 
I9.1 530..531 "CA" (529,608) U:G 
I9.2 607..607 "G" (606,532) U:G 
I10.1 541..542 "AA" (540,582) G:A 
I10.2 580..581 "GA" (579,543) G:U 
I11.1 551..553 "GUA" (550,572) U:A 
I11.2 566..571 "GUAGAU" (565,554) C:G 
I12.1 646..648 "AAA" (645,669) C:G 
I12.2 666..668 "GUA" (665,649) G:C 
I13.1 750..755 "GAGUAC" (749,772) G:C 
I13.2 768..771 "GAAA" (767,756) U:G 
I14.1 996..998 "UUG" (995,1020) U:A 
I14.2 1016..1019 "CUCU" (1015,999) G:C 
I15.1 1119..1123 "ACAAU" (1118,1161) G:C 
I15.2 1156..1160 "ACAAA" (1155,1124) C:G 
I16.1 1127..1128 "CA" (1126,1153) G:C 
I16.2 1149..1152 "UAAU" (1148,1129) C:G 
I17.1 1131..1133 "GAG" (1130,1147) C:G 
I17.2 1144..1146 "CAA" (1143,1134) U:A 
M1.1 442..442 "C" (441,742) G:C (630,443) C:G 
M1.2 631..634 "GAAA" (630,443) C:G (676,635) C:G 
M1.3 677..686 "CGUAAACGAU" (676,635) C:G (741,687) C:G 
M1.4 742..741 "" (741,687) C:G (441,742) G:C 
M2.1 453..453 "G" (452,621) U:G (517,454) C:G 
M2.2 518..520 "UUG" (517,454) C:G (617,521) U:A 
M2.3 618..620 "GAC" (617,521) U:A (452,621) U:G 
M3.1 539..538 "" (538,600) C:G (583,539) C:G 
M3.2 584..587 "ACCA" (583,539) C:G (599,588) G:G 
M3.3 600..599 "" (599,588) G:G (538,600) C:G 
M4.1 693..694 "UA" (692,736) U:A (719,695) C:G 
M4.2 720..722 "CCA" (719,695) C:G (730,723) C:G 
M4.3 731..735 "GUUAA" (730,723) C:G (692,736) U:A 
M5.1 818..821 "UUAA" (817,1096) U:A (837,822) A:U 
M5.2 838..845 "AAACCUUA" (837,822) A:U (1092,846) G:C 
M5.3 1093..1095 "GCU" (1092,846) G:C (817,1096) U:A 
M6.1 853..857 "UUGAA" (852,1086) U:G (916,858) C:A 
M6.2 917..917 "A" (916,858) C:A (1081,918) U:G 
M6.3 1082..1085 "UUAU" (1081,918) U:G (852,1086) U:G 
M7.1 867..868 "AA" (866,908) A:U (886,869) C:G 
M7.2 887..889 "GUG" (886,869) C:G (907,890) A:U 
M7.3 908..907 "" (907,890) A:U (866,908) A:U 
M8.1 936..938 "UCA" (935,1063) G:C (978,939) C:G 
M8.2 979..983 "GCAAC" (978,939) C:G (1058,984) G:C 
M8.3 1059..1062 "ACGA" (1058,984) G:C (935,1063) G:C 
M9.1 945..944 "" (944,973) U:A (954,945) U:G 
M9.2 955..956 "GU" (954,945) U:G (970,957) G:U 
M9.3 971..972 "CA" (970,957) G:U (944,973) U:A 
M10.1 988..988 "G" (987,1055) U:G (1026,989) A:U 
M10.2 1027..1028 "GA" (1026,989) A:U (1049,1029) G:C 
M10.3 1050..1054 "AAGGA" (1049,1029) G:C (987,1055) U:G 
X1 21..34 "UAAUACCCUAUAUU" (20,3) C:A (59,35) U:G 
X2 60..64 "GAAAG" (59,35) U:G (84,65) G:C 
X3 85..104 "GCUCAAAGAGGGGUUCGCGU" (84,65) G:C (151,105) G:C 
X4 152..153 "UA" (151,105) G:C (176,154) C:G 
X5 177..180 "CACA" (176,154) C:G (202,181) C:G 
X7 216..231 "GCAGCAGUGGGGAAUC" (215,204) G:C (258,232) C:G 
X8 259..270 "GCAAUGCCGCGU" (258,232) C:G (301,271) C:G 
X9 302..304 "UGU" (301,271) C:G (363,305) G:C PK{1}
X10 364..365 "AA" (363,305) G:C (411,366) C:G 
X11 412..432 "GAGCGUUGCGCGAUGUUACUG" (411,366) C:G (745,433) C:G 
X13 775..805 "AAAGGAAUUGGCGGGGGCCCGCACAACCGGU" (774,747) C:G (1108,806) C:G 
X14 1109..1111 "AGU" (1108,806) C:G (1167,1112) C:G 
E1 1..2 "AA" 
E2 1168..1187 "GUUAAUUUGACUCAAUAUAG" 
PK1 1bp 303..303 362..362 X9 302..304 B6 360..362
PK2 3bp 371..373 390..392 B7 371..376 H11 389..392
PK3 2bp 436..437 727..728 B8 436..440 H17 725..728
PK1.1 303 G 362 G
PK2.1 371 G 392 C
PK2.2 372 G 391 C
PK2.3 373 C 390 G
PK3.1 436 G 728 C
PK3.2 437 U 727 A
NCBP1 1031 G 1047 A S70
NCBP2 378 U 405 U S23
NCBP3 540 G 582 A S37
NCBP4 527 G 610 A S35
NCBP5 933 U 1065 U S62
NCBP6 1000 U 1014 U S68
NCBP7 456 U 515 U S30
NCBP8 329 C 335 C S21
NCBP9 468 U 502 U S32
NCBP10 110 C 148 C S6
NCBP11 588 G 599 G S40
NCBP12 526 U 611 C S35
NCBP13 477 C 495 C S33
NCBP14 858 A 916 C S54
NCBP15 186 A 197 G S11
NCBP16 448 U 624 C S28
NCBP17 457 U 514 U S30
NCBP18 306 A 359 A S19
NCBP19 3 A 20 C S1
NCBP20 807 G 1107 A S50
NCBP21 303 G 362 G PK1.1
segment1 7bp 3..9 ACUGUCC 14..20 GGAUAGC
segment2 10bp 35..44 GCCUUUUGUC 50..59 GGCAAAAGGU
segment3 7bp 65..71 CGAGGCU 77..84 AGGCUUCG
segment4 15bp 105..124 CCUAUCAGGUAGUUGGUGGG 132..151 CCUACCAAGCCAAUGACGGG
segment5 8bp 154..161 GCUGGUCU 166..176 GGACGAUCAGC
segment6 6bp 181..186 GCGGGA 197..202 GCCCGC
segment7 4bp 204..207 CCCC 212..215 GGGG
segment8 9bp 232..244 GUGGGCAAUGGGC 249..258 GCCUGACCCC
segment9 8bp 271..284 GAGGGAAGAAGCCC 289..301 GGGUGUAAACCUC
segment10 17bp 305..329 CAGAUGGGAAGAUGGGACUAGGGGC 335..363 CCUCCUAGUCUUGACGGUACCAUCAGAGG
segment11 12bp 366..388 GGGACGGCUAACUACGUGCCAGC 393..411 GCGGUAAUACGUUGGUCCC
segment12 4bp 433..441 GGCGUAAAG 742..745 CGCC
segment13 8bp 443..452 GUCCGUAGCU 621..630 GGUCAUGGAC
segment14 25bp 454..483 GUUUCGUAAGCGGAUUGUCAAAGCCCGAAG 489..517 CUUCGGCAAGGCAUUCCGAACUGCGUUAC
segment15 16bp 521..538 AGUCCUGUUCAGGUAGGC 600..617 GCCUACUGGGACGGUACU
segment16 13bp 539..556 GGAAUUCCCGGUGUAGCG 563..583 UGCGUAGAUAUCGGGAGGAAC
segment17 3bp 588..592 GUGGC 597..599 GCG
segment18 12bp 635..652 GCUGGGGGAGCAAACCGG 662..676 CCGGGUAGUCCCAGC
segment19 6bp 687..692 GGAUGU 736..741 ACAUCC
segment20 10bp 695..704 GGUGUAGUGA 710..719 UCGCUGCACC
segment21 2bp 723..724 GC 729..730 GC
segment22 7bp 747..759 GGGGAGUACGGGC 764..774 GCCUGAAACUC
segment23 12bp 806..817 GGAGCGUCUGGU 1096..1108 ACACAGGCGCUAC
segment24 4bp 822..825 UUCG 834..837 CGAA
segment25 7bp 846..852 CCAGGGU 1086..1092 GCCCUGG
segment26 9bp 858..866 AUGGUAGGA 908..916 UCCUACCAC
segment27 7bp 869..875 GGCUGUC 880..886 GAUAGCC
segment28 6bp 890..895 UUUCCC 900..907 GGGAAAAA
segment29 15bp 918..935 GGUGGUGCAUGGCCGUCG 1063..1081 CGUCAGGUCAGUAUGCCCU
segment30 6bp 939..944 GCUCGU 973..978 ACGAGC
segment31 3bp 945..947 GUU 952..954 GAU
segment32 4bp 957..960 UGGG 967..970 CCCG
segment33 4bp 984..987 CCUU 1055..1058 GGGG
segment34 13bp 989..1005 UCCUUAUUUGCUUCCUC 1010..1026 GAGGUGCUCUAUAAGGA
segment35 8bp 1029..1036 CUGCCGGC 1042..1049 GCCGGAGG
segment36 15bp 1112..1136 GGCAAGGACAAUGAGCAGCGAGAGC 1141..1167 GCUCAAGCUAAUCUCACAAACCUUGUC
