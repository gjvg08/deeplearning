#Name: bpRNA_CRW_54706
#Length:  1200 
#PageNumber: 2
AAAUUAGAGUUUGGUGCUGGCUCAGCUUUCAUGCGCCGAGCGGUGGUUUAUACAUGCGAGCUUAAAAGAUUAAGGCCACGAGUGCGUAAUACGCGAGUUUAAAUUUAUUAUACAAAAGUUGCUAGGUGACCCAAUACGGGCAUCGGGCAAAACUCGCGUAGGAUUAGCGAGCUGGGUCGCGACGCCAAUACAGCGGCCCAUGGCUUAUCCUUAGCCUGUCUUAACGGUACUAGGCCACGGUGGCACUGAAAAGGGGCCACGGUUCUUAUGAACCCAGCAGUGUUGAAUUUUGGACAAUCGCUUGAACGGCGGAUCCAGAUGCUGCUUACAAAAUUGUUUGUGAACACCCCCCAAGCACGUGCCAGAAGGGUCGGUAAAACGUGCGGUGUCAGUAUAAAGCGUCUUGACUAGGCAGGCAGCGCGUCUGAGCGUUAAUGGUGAACACUUUAAACGUUGGGUAAUAUUCGGAGGAUCGGUCAAAUGAGAAUAUUCCGGAUGGAAAGCCGAAGGCGAAAGCACCAACAUCAGAGUCACUAAAGCUUCAACGCGUAAGUUUGGGUAGCGAACCGGAUUAGAGACCCGCAGUAGUCCAAACCGUCAACACAUUAUAGUUUUCAAAUCUAUAACGCCUGGUGAUACGGUGGCAACACUAUAAAUCAAAGCAAUUGGCAGCGAUAGAGAUGCGCGGUGGAAUAUGCUGUUUAAAUCGAAUUUACGCGCAAAAUCUUACCACUUUUUAUUAAAAUAGCUCUUGCAUUGCUGAAAAGUUCGAGCGGGACCUCGCCCUUAGUGCCGGUCUUUGCCACGUUUUCUGAACGGUCGAGCAAGUCAUCAUGGGGCUUAUAGAGUGGGCUACAGGCGUAUUACAUUGGACACCCACAAGUUGGAUACCAAAACUGUCCGAAUAUACGGAUUGGAGUAGCAAAACGAGAGAACUAUACAGUUCGUUUGCAUCGUUAUGCCUUGCUGAAACUAGCCUCCAUGAAGAAGGAAUCGCGAGUAAUCGUAGAUCAUUAGCGCUACGGUGAAGGUAACCUCUAUUGUGCACACAUUGCCCGUCACCUCCGAUAAUAGUAUUGUACAGGAAGAACUAUGGCUACACUUAAGUCGCGGCCUAGGAACGUAUGCGUGAUAUUAGAGUUGGAGUAAGUCGUAACAGGUUGGGGUAGGGGAACCUGCUCCAGAGUCAAAUUUAUAUAA
......(((((...[[[.))))).((((..(((((..((((((.......(((.(((..((((....................................................))))......(((.....(((((((............)))))))(((..(.(((..((((((((..............)))))))))))...))))..((((((((....))))..))))))).((((((.........)))))).((((....))))...)))))).......(.(((...(((((.....))))).))))....))))))................(((((......(((((....................)))))))))).))))).))))..........(((.......((((..((((.............(((((..(((((((((((((((((((...(((......)))......))))))))))))....(..((....)))))))))).))))).......))))..))))....(((((((...(...((((.........))))....))))))))..........((((((.........)))))).))).(((.....((((....))))....)))...]]]..(((((.(((((((....((((((.(((((((((((....((((........))))........(((((((...........((.((((..(((((....((((((((......)))............)))))......(((((.......)))))....)))...))))))))....)))))))...)).)))))))))..(((((((......................))))))).....((((((((((..............((((((((((.....))))))))))...................)))))))))).....))))))....((((((((........))))))))..........)))))))))).))....((.((.(.((((((((((((((((((((((.......(((.((((.............))))))).......))))))))))))))))))))))..).))..))...((((((((((....))))))))))................
EEEEEESSSSSHHHHHHHSSSSSXSSSSIISSSSSMMSSSSSSMMMMMMMSSSBSSSMMSSSSHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHSSSSMMMMMMSSSMMMMMSSSSSSSHHHHHHHHHHHHSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHHHHHHHHSSSSSSSSSSSIIISSSSMMSSSSSSSSHHHHSSSSBBSSSSSSSMSSSSSSHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSMMMMMMMSBSSSIIISSSSSHHHHHSSSSSISSSSMMMMSSSSSSMMMMMMMMMMMMMMMMSSSSSBBBBBBSSSSSHHHHHHHHHHHHHHHHHHHHSSSSSSSSSSMSSSSSISSSSXXXXXXXXXXSSSMMMMMMMSSSSIISSSSIIIIIIIIIIIIISSSSSIISSSSSSSSSSSSSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSSSSSMMMMSBBSSHHHHSSSSSSSSSSISSSSSIIIIIIISSSSIISSSSMMMMSSSSSSSBBBSIIISSSSHHHHHHHHHSSSSIIIISSSSSSSSMMMMMMMMMMSSSSSSHHHHHHHHHSSSSSSMSSSXSSSIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXSSSSSBSSSSSSSMMMMSSSSSSMSSSSSSSSSSSMMMMSSSSHHHHHHHHSSSSMMMMMMMMSSSSSSSIIIIIIIIIIISSBSSSSBBSSSSSMMMMSSSSSSSSHHHHHHSSSBBBBBBBBBBBBSSSSSMMMMMMSSSSSHHHHHHHSSSSSMMMMSSSBBBSSSSSSSSIIIISSSSSSSMMMSSBSSSSSSSSSMMSSSSSSSHHHHHHHHHHHHHHHHHHHHHHSSSSSSSMMMMMSSSSSSSSSSIIIIIIIIIIIIIISSSSSSSSSSHHHHHSSSSSSSSSSIIIIIIIIIIIIIIIIIIISSSSSSSSSSMMMMMSSSSSSMMMMSSSSSSSSHHHHHHHHSSSSSSSSMMMMMMMMMMSSSSSSSSSSBSSXXXXSSISSISISSSSSSSSSSSSSSSSSSSSSSIIIIIIISSSBSSSSHHHHHHHHHHHHHSSSSSSSIIIIIIISSSSSSSSSSSSSSSSSSSSSSIISISSIISSXXXSSSSSSSSSSHHHHSSSSSSSSSSEEEEEEEEEEEEEEEE
NNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 7..11 "GAGUU" 19..23 "GGCUC"
S2 25..28 "GCUU" 397..400 "AAGC"
S3 31..35 "AUGCG" 391..395 "AGUAU"
S4 38..43 "GAGCGG" 322..327 "CUGCUU"
S5 51..53 "UAC" 280..282 "GUG"
S6 55..57 "UGC" 277..279 "GCA"
S7 60..63 "GCUU" 116..119 "AAGU"
S8 126..128 "GUG" 236..238 "CAC"
S9 134..140 "AUACGGG" 153..159 "CUCGCGU"
S10 160..162 "AGG" 209..211 "CCU"
S11 165..165 "U" 208..208 "U"
S12 167..169 "GCG" 202..204 "GGC"
S13 172..179 "CUGGGUCG" 194..201 "CGGCCCAU"
S14 214..217 "GCCU" 232..235 "AGGC"
S15 218..221 "GUCU" 226..229 "GGUA"
S16 240..245 "GUGGCA" 255..260 "GGCCAC"
S17 262..265 "GUUC" 270..273 "GAAC"
S18 290..290 "U" 317..317 "A"
S19 292..294 "GGA" 314..316 "UCC"
S20 298..302 "UCGCU" 308..312 "GGCGG"
S21 344..348 "ACACC" 385..389 "GGUGU"
S22 355..359 "GCACG" 380..384 "CGUGC"
S23 411..413 "GGC" 628..630 "GCC"
S24 421..424 "GCGU" 545..548 "ACGC"
S25 427..430 "GAGC" 539..542 "GCUU"
S26 444..448 "ACUUU" 527..531 "AGAGU"
S27 451..457 "ACGUUGG" 519..525 "CCAACAU"
S28 458..469 "GUAAUAUUCGGA" 491..502 "UCCGGAUGGAAA"
S29 473..475 "UCG" 482..484 "UGA"
S30 507..507 "A" 518..518 "A"
S31 510..511 "GC" 516..517 "GC"
S32 553..559 "GUUUGGG" 589..595 "UCCAAAC"
S33 563..563 "C" 588..588 "G"
S34 567..570 "CCGG" 580..583 "CCGC"
S35 606..611 "UUAUAG" 621..626 "CUAUAA"
S36 632..634 "GGU" 656..658 "AUC"
S37 640..643 "GGUG" 648..651 "CACU"
S38 667..668 "UG" 1048..1049 "CA"
S39 669..671 "GCA" 1044..1046 "UGC"
S40 673..679 "CGAUAGA" 1037..1043 "UCUAUUG"
S41 684..689 "CGCGGU" 993..998 "AUCGCG"
S42 691..699 "GAAUAUGCU" 858..866 "GGCGUAUUA"
S43 700..701 "GU" 855..856 "AC"
S44 706..709 "AUCG" 718..721 "CGCA"
S45 730..736 "CCACUUU" 845..851 "AGAGUGG"
S46 748..749 "GC" 839..840 "GC"
S47 751..754 "CUUG" 835..838 "UGGG"
S48 757..758 "UU" 833..834 "CA"
S49 759..761 "GCU" 827..829 "AGU"
S50 766..770 "AGUUC" 795..799 "GGUCU"
S51 771..773 "GAG" 780..782 "CUC"
S52 806..810 "CGUUU" 818..822 "GGUCG"
S53 869..875 "UUGGACA" 898..904 "UGUCCGA"
S54 910..919 "CGGAUUGGAG" 978..987 "CUCCAUGAAG"
S55 934..943 "AACUAUACAG" 949..958 "UUGCAUCGUU"
S56 1003..1010 "AUCGUAGA" 1019..1026 "GCUACGGU"
S57 1054..1055 "GC" 1156..1157 "AC"
S58 1057..1058 "CG" 1152..1153 "CG"
S59 1060..1060 "C" 1150..1150 "G"
S60 1062..1083 "CCUCCGAUAAUAGUAUUGUACA" 1126..1147 "UGCGUGAUAUUAGAGUUGGAGU"
S61 1091..1093 "CUA" 1116..1118 "UAG"
S62 1095..1098 "GGCU" 1112..1115 "GGCC"
S63 1161..1170 "UUGGGGUAGG" 1175..1184 "CCUGCUCCAG"
H1 12..18 "UGGUGCU" (11,19) U:G PK{1}
H2 64..115 "AAAAGAUUAAGGCCACGAGUGCGUAAUACGCGAGUUUAAAUUUAUUAUACAA" (63,116) U:A 
H3 141..152 "CAUCGGGCAAAA" (140,153) G:C 
H4 180..193 "CGACGCCAAUACAG" (179,194) G:C 
H5 222..225 "UAAC" (221,226) U:G 
H6 246..254 "CUGAAAAGG" (245,255) A:G 
H7 266..269 "UUAU" (265,270) C:G 
H8 303..307 "UGAAC" (302,308) U:G 
H9 360..379 "UGCCAGAAGGGUCGGUAAAA" (359,380) G:C 
H10 476..481 "GUCAAA" (475,482) G:U 
H11 512..515 "GAAA" (511,516) C:G 
H12 571..579 "AUUAGAGAC" (570,580) G:C 
H13 612..620 "UUUUCAAAU" (611,621) G:C 
H14 644..647 "GCAA" (643,648) G:C 
H15 710..717 "AAUUUACG" (709,718) G:C 
H16 774..779 "CGGGAC" (773,780) G:C 
H17 811..817 "UCUGAAC" (810,818) U:G 
H18 876..897 "CCCACAAGUUGGAUACCAAAAC" (875,898) A:U 
H19 944..948 "UUCGU" (943,949) G:U 
H20 1011..1018 "UCAUUAGC" (1010,1019) A:G 
H21 1099..1111 "ACACUUAAGUCGC" (1098,1112) U:G 
H22 1171..1174 "GGAA" (1170,1175) G:C 
B1 54..54 "A" (53,280) C:G (279,55) A:U 
B2 163..164 "AU" (162,209) G:C (208,165) U:U 
B3 170..171 "AG" (169,202) G:G (201,172) U:C 
B4 230..231 "CU" (229,218) A:G (217,232) U:A 
B5 291..291 "U" (290,317) U:A (316,292) C:G 
B6 349..354 "CCCCAA" (348,385) C:G (384,355) C:G 
B7 508..509 "AG" (507,518) A:A (517,510) C:G 
B8 560..562 "UAG" (559,589) G:U (588,563) G:C 
B9 672..672 "G" (671,1044) A:U (1043,673) G:C 
B10 750..750 "U" (749,839) C:G (838,751) G:C 
B11 755..756 "CA" (754,835) G:U (834,757) A:U 
B12 783..794 "GCCCUUAGUGCC" (782,771) C:G (770,795) C:G 
B13 830..832 "CAU" (829,759) U:G (758,833) U:C 
B14 857..857 "A" (856,700) C:G (699,858) U:G 
B15 1047..1047 "A" (1046,669) C:G (668,1048) G:C 
B16 1094..1094 "U" (1093,1116) A:U (1115,1095) C:G 
I1.1 29..30 "UC" (28,397) U:A 
I1.2 396..396 "A" (395,31) U:A 
I2.1 166..166 "A" (165,208) U:U 
I2.2 205..207 "UUA" (204,167) C:G 
I3.1 295..297 "CAA" (294,314) A:U 
I3.2 313..313 "A" (312,298) G:U 
I4.1 425..426 "CU" (424,545) U:A 
I4.2 543..544 "CA" (542,427) U:G 
I5.1 431..443 "GUUAAUGGUGAAC" (430,539) C:G 
I5.2 532..538 "CACUAAA" (531,444) U:A 
I6.1 449..450 "AA" (448,527) U:A 
I6.2 526..526 "C" (525,451) U:A 
I7.1 470..472 "GGA" (469,491) A:U 
I7.2 485..490 "GAAUAU" (484,473) A:U 
I8.1 564..566 "GAA" (563,588) C:G 
I8.2 584..587 "AGUA" (583,567) C:C 
I9.1 635..639 "GAUAC" (634,656) U:A 
I9.2 652..655 "AUAA" (651,640) U:G 
I10.1 737..747 "UUAUUAAAAUA" (736,845) U:A 
I10.2 841..844 "UUAU" (840,748) C:G 
I11.1 920..933 "UAGCAAAACGAGAG" (919,978) G:C 
I11.2 959..977 "AUGCCUUGCUGAAACUAGC" (958,934) U:A 
I12.1 1056..1056 "C" (1055,1156) C:A 
I12.2 1154..1155 "UA" (1153,1057) G:C 
I13.1 1059..1059 "U" (1058,1152) G:C 
I13.2 1151..1151 "U" (1150,1060) G:C 
I14.1 1061..1061 "A" (1060,1150) C:G 
I14.2 1148..1149 "AA" (1147,1062) U:C 
I15.1 1084..1090 "GGAAGAA" (1083,1126) A:U 
I15.2 1119..1125 "GAACGUA" (1118,1091) G:C 
M1.1 36..37 "CC" (35,391) G:A (327,38) U:G 
M1.2 328..343 "ACAAAAUUGUUUGUGA" (327,38) U:G (389,344) U:A 
M1.3 390..390 "C" (389,344) U:A (35,391) G:A 
M2.1 44..50 "UGGUUUA" (43,322) G:C (282,51) G:U 
M2.2 283..289 "UUGAAUU" (282,51) G:U (317,290) A:U 
M2.3 318..321 "GAUG" (317,290) A:U (43,322) G:C 
M3.1 58..59 "GA" (57,277) C:G (119,60) U:G 
M3.2 120..125 "UGCUAG" (119,60) U:G (238,126) C:G 
M3.3 239..239 "G" (238,126) C:G (260,240) C:G 
M3.4 261..261 "G" (260,240) C:G (273,262) C:G 
M3.5 274..276 "CCA" (273,262) C:G (57,277) C:G 
M4.1 129..133 "ACCCA" (128,236) G:C (159,134) U:A 
M4.2 160..159 "" (159,134) U:A (211,160) U:A 
M4.3 212..213 "UA" (211,160) U:A (235,214) C:G 
M4.4 236..235 "" (235,214) C:G (128,236) G:C 
M5.1 414..420 "AGGCAGC" (413,628) C:G (548,421) C:G 
M5.2 549..552 "GUAA" (548,421) C:G (595,553) C:G 
M5.3 596..605 "CGUCAACACA" (595,553) C:G (626,606) A:U 
M5.4 627..627 "C" (626,606) A:U (413,628) C:G 
M6.1 458..457 "" (457,519) G:C (502,458) A:G 
M6.2 503..506 "GCCG" (502,458) A:G (518,507) A:A 
M6.3 519..518 "" (518,507) A:A (457,519) G:C 
M7.1 680..683 "GAUG" (679,1037) A:U (998,684) G:C 
M7.2 999..1002 "AGUA" (998,684) G:C (1026,1003) U:A 
M7.3 1027..1036 "GAAGGUAACC" (1026,1003) U:A (679,1037) A:U 
M8.1 690..690 "G" (689,993) U:A (866,691) A:G 
M8.2 867..868 "CA" (866,691) A:G (904,869) A:U 
M8.3 905..909 "AUAUA" (904,869) A:U (987,910) G:C 
M8.4 988..992 "AAGGA" (987,910) G:C (689,993) U:A 
M9.1 702..705 "UUAA" (701,855) U:A (721,706) A:A 
M9.2 722..729 "AAAUCUUA" (721,706) A:A (851,730) G:C 
M9.3 852..854 "GCU" (851,730) G:C (701,855) U:A 
M10.1 762..765 "GAAA" (761,827) U:A (799,766) U:A 
M10.2 800..805 "UUGCCA" (799,766) U:A (822,806) G:C 
M10.3 823..826 "AGCA" (822,806) G:C (761,827) U:A 
X2 401..410 "GUCUUGACUA" (400,25) C:G (630,411) C:G 
X4 659..666 "AAAGCAAU" (658,632) C:G (1049,667) A:U PK{1}
X5 1050..1053 "CAUU" (1049,667) A:U (1157,1054) C:G 
X6 1158..1160 "AGG" (1157,1054) C:G (1184,1161) G:U 
E1 1..6 "AAAUUA" 
E2 1185..1200 "AGUCAAAUUUAUAUAA" 
PK1 3bp 15..17 662..664 H1 12..18 X4 659..666
PK1.1 15 U 664 A
PK1.2 16 G 663 C
PK1.3 17 C 662 G
NCBP1 458 G 502 A S28
NCBP2 758 U 833 C S48
NCBP3 706 A 721 A S44
NCBP4 218 G 229 A S15
NCBP5 940 A 952 C S55
NCBP6 460 A 500 A S28
NCBP7 452 C 524 A S27
NCBP8 507 A 518 A S30
NCBP9 567 C 583 C S34
NCBP10 136 A 157 C S9
NCBP11 914 U 983 U S54
NCBP12 1062 C 1147 U S60
NCBP13 691 G 866 A S42
NCBP14 911 G 986 A S54
NCBP15 245 A 255 G S16
NCBP16 461 A 499 G S28
NCBP17 937 U 955 C S55
NCBP18 35 G 391 A S3
NCBP19 913 A 984 G S54
NCBP20 768 U 797 U S50
NCBP21 169 G 202 G S12
NCBP22 912 G 985 A S54
NCBP23 707 U 720 C S44
NCBP24 1010 A 1019 G S56
NCBP25 165 U 208 U S11
NCBP26 808 U 820 U S52
NCBP27 172 C 201 U S13
NCBP28 1071 A 1138 G S60
NCBP29 1081 A 1128 C S60
NCBP30 1055 C 1156 A S57
NCBP31 1070 A 1139 A S60
NCBP32 219 U 228 U S15
segment1 5bp 7..11 GAGUU 19..23 GGCUC
segment2 9bp 25..35 GCUUUCAUGCG 391..400 AGUAUAAAGC
segment3 6bp 38..43 GAGCGG 322..327 CUGCUU
segment4 6bp 51..57 UACAUGC 277..282 GCAGUG
segment5 4bp 60..63 GCUU 116..119 AAGU
segment6 3bp 126..128 GUG 236..238 CAC
segment7 7bp 134..140 AUACGGG 153..159 CUCGCGU
segment8 15bp 160..179 AGGAUUAGCGAGCUGGGUCG 194..211 CGGCCCAUGGCUUAUCCU
segment9 8bp 214..221 GCCUGUCU 226..235 GGUACUAGGC
segment10 6bp 240..245 GUGGCA 255..260 GGCCAC
segment11 4bp 262..265 GUUC 270..273 GAAC
segment12 9bp 290..302 UUGGACAAUCGCU 308..317 GGCGGAUCCA
segment13 10bp 344..359 ACACCCCCCAAGCACG 380..389 CGUGCGGUGU
segment14 3bp 411..413 GGC 628..630 GCC
segment15 20bp 421..457 GCGUCUGAGCGUUAAUGGUGAACACUUUAAACGUUGG 519..548 CCAACAUCAGAGUCACUAAAGCUUCAACGC
segment16 15bp 458..475 GUAAUAUUCGGAGGAUCG 482..502 UGAGAAUAUUCCGGAUGGAAA
segment17 3bp 507..511 AAGGC 516..518 GCA
segment18 12bp 553..570 GUUUGGGUAGCGAACCGG 580..595 CCGCAGUAGUCCAAAC
segment19 6bp 606..611 UUAUAG 621..626 CUAUAA
segment20 7bp 632..643 GGUGAUACGGUG 648..658 CACUAUAAAUC
segment21 12bp 667..679 UGGCAGCGAUAGA 1037..1049 UCUAUUGUGCACA
segment22 6bp 684..689 CGCGGU 993..998 AUCGCG
segment23 11bp 691..701 GAAUAUGCUGU 855..866 ACAGGCGUAUUA
segment24 4bp 706..709 AUCG 718..721 CGCA
segment25 18bp 730..761 CCACUUUUUAUUAAAAUAGCUCUUGCAUUGCU 827..851 AGUCAUCAUGGGGCUUAUAGAGUGG
segment26 8bp 766..773 AGUUCGAG 780..799 CUCGCCCUUAGUGCCGGUCU
segment27 5bp 806..810 CGUUU 818..822 GGUCG
segment28 7bp 869..875 UUGGACA 898..904 UGUCCGA
segment29 20bp 910..943 CGGAUUGGAGUAGCAAAACGAGAGAACUAUACAG 949..987 UUGCAUCGUUAUGCCUUGCUGAAACUAGCCUCCAUGAAG
segment30 8bp 1003..1010 AUCGUAGA 1019..1026 GCUACGGU
segment31 34bp 1054..1098 GCCCGUCACCUCCGAUAAUAGUAUUGUACAGGAAGAACUAUGGCU 1112..1157 GGCCUAGGAACGUAUGCGUGAUAUUAGAGUUGGAGUAAGUCGUAAC
segment32 10bp 1161..1170 UUGGGGUAGG 1175..1184 CCUGCUCCAG
