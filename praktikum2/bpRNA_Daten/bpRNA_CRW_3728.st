#Name: bpRNA_CRW_3728
#Length:  1130 
#PageNumber: 2
#Warning: Structure contains linked PK-segments of same sizes 1 and 1. Using PK brackets for the more 5' segment
GAGUUUGAUCCUGGCUCAGGACGAACGCUGGCGGCGCGCCUAAAACAUGCAAGUCGAACGAGAAGCCCGGGCUUGCCCGUGGACAGUGGCGAACGGGUGAGUAACACGUGAGGAACCUGCCCCGAAGACCGGGACAACACCGGGAAACCGGUGCUAAUACCGGAUACCUUCACCGGAUCGCAUGGUCUGGUGAAGAAAUGGAUUCCGCUUCGGGAUGGCCUCGCGGCCUAUCAGCUUGUUGGUGAGGUAACGGCUCACCAAGGCAACGACGGGUAGCUGGUCUGAGAGGAUGUCCAGCCACACUGGGACUGAGACACGGCCCAGACUCCUACGGGAGGCAGCAGUUAGGAAUCUUGCGCAAUGGACGAAAGUCUGACGCAGCGACGCCGCGUGAGGGAUGAAGGCCCUCGGGUUGUAAACCUCUUUCGACAGGGACGAUAGUGACGGUACCUGCAGAAGAAGCUCCGGCCAACUACGUGCCAGCAGCCGCGGUGAUACGUAGGGAGCGAGCGUUGUCCGGAUUCAUUGGGCGUAAAGAGCUCGUAGGCGGCUCAGUAAGUCAGGUGUGAAAUCCUCAGGCUCAACCUGGGGUCGCCAUCUGAUACUGCUGUGGCUAGAGCCCGGUAGGGCCCACGGAAUUCCUGGUGUAGCGGUGAAAUGCGCAGAUAUCAGGAGGAACACCAGUGGCGAAGGCGGCUCACUGGCCCGGUACUGACGCUCAGGUGCGAAANNNNGGGGAGCAAACAGGAUUAGAUACCCUGGUAGUCCACGCUGUAAACGAUGUAUGCUAGCCGUUGGAGGGUUUACCCUUCAGUGGCGCANNUAACGCAUUAAGCAUCCCGCCUGGGGAGUACGGUCGCAAGAUUAAAACUCAAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUCAAUUCGAAGCAACGCGAAGAACCUUACCAGCCCUUGACAUCCCGGUCGCGGGAACGAGAGAUCGAUCCCUUCAGUUCGGCUGGACCGGAGACAGGUGCUGCAUGGCUGUCGUCAGCUCGUGUCGUGAGAUGUUGGGUUAAGUCCCGCAACGAGCGCAACCCUCGCCCUUAGUUGCCAUCAUUUAGUUGGGCACUCUAAGGGGACUGCCGGUG
(((((...[[[.))))).((((.((((((.(((((((((....(((.(((..(((..(((.....((((((....)))).))..))))))......(((......((((((((..((...(((((((.((((....(((((((....))))))).....)))).....(((((((.(((....))).)))))))....((....)).)))))))..))))))))))(((..(.(((..((((((((.......))))))))))).....))))..((((((((....))))...))))))).((((((..........)))))).((((....))))...)))))).).....(.(((...(((((....))))).)))).)).))))))..((((......((((....)))).....)))).[.(((((((...(.......)....))))))..])..((((([[[...(((((.....((.]]])).......)))))))))).))))))))))..........((([[...(.((((.(.(((.(((((((.(((((((((((....(((((((.....)))))))..)))))))))..)))))).)))...(((((((((..((((((((..((((((((...(((......)))......))))))))..))....(..((....)))).))))).)))))).)))...))))..))))....((((((...((...((((.........))))...))))))))..........((((((..((((((((((((....))))))))))))...((..]])).....)))))))))).(((......((((....))))....)))...]]].........................................((((........))))....................(((((((((..(((.((((....)))))))....((.....))..)))))))))......................(((((((((....)))..((((......))))..))))))..........(((((((...((..((......))))....)))))))...........
SSSSSHHHHHHHSSSSSXSSSSBSSSSSSMSSSSSSSSSIIIISSSBSSSMMSSSBBSSSIIIIISSSSSSHHHHSSSSBSSIISSSSSSMMMMMMSSSMMMMMMSSSSSSSSBBSSIIISSSSSSSMSSSSIIIISSSSSSSHHHHSSSSSSSIIIIISSSSMMMMMSSSSSSSISSSHHHHSSSISSSSSSSMMMMSSHHHHSSMSSSSSSSIISSSSSSSSSSSSSBBSISSSBBSSSSSSSSHHHHHHHSSSSSSSSSSSIIIIISSSSMMSSSSSSSSHHHHSSSSBBBSSSSSSSMSSSSSSHHHHHHHHHHSSSSSSMSSSSHHHHSSSSMMMSSSSSSISMMMMMSBSSSIIISSSSSHHHHSSSSSISSSSMSSBSSSSSSMMSSSSIIIIIISSSSHHHHSSSSIIIIISSSSMMMSSSSSSSIIISHHHHHHHSIIIISSSSSSBBBSMMSSSSSBBBBBBSSSSSIIIIISSHHHHSSIIIIIIISSSSSSSSSSMSSSSSSSSSSXXXXXXXXXXSSSBBBBBSMSSSSISBSSSMSSSSSSSBSSSSSSSSSSSIIIISSSSSSSHHHHHSSSSSSSIISSSSSSSSSBBSSSSSSBSSSMMMSSSSSSSSSIISSSSSSSSIISSSSSSSSIIISSSHHHHHHSSSIIIIIISSSSSSSSIISSMMMMSBBSSHHHHSSSSBSSSSSISSSSSSBSSSMMMSSSSIISSSSMMMMSSSSSSBBBSSIIISSSSHHHHHHHHHSSSSIIISSSSSSSSMMMMMMMMMMSSSSSSMMSSSSSSSSSSSSHHHHSSSSSSSSSSSSMMMSSHHHHSSMMMMMSSSSSSSSSSXSSSIIIIIISSSSHHHHSSSSIIIISSSXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXSSSSHHHHHHHHSSSSXXXXXXXXXXXXXXXXXXXXSSSSSSSSSMMSSSBSSSSHHHHSSSSSSSMMMMSSHHHHHSSMMSSSSSSSSSXXXXXXXXXXXXXXXXXXXXXXSSSSSSSSSHHHHSSSMMSSSSHHHHHHSSSSMMSSSSSSXXXXXXXXXXSSSSSSSIIISSBBSSHHHHHHSSSSIIIISSSSSSSEEEEEEEEEEE
NNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKNNNNNNNNKKKNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNKKKNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
S1 1..5 "GAGUU" 13..17 "GGCUC"
S2 19..22 "GGAC" 515..518 "GUCC"
S3 24..29 "AACGCU" 509..514 "AGCGUU"
S4 31..36 "GCGGCG" 385..390 "CGCCGC"
S5 37..38 "CG" 382..383 "CG"
S6 39..39 "C" 348..348 "G"
S7 44..46 "AAC" 344..346 "GUU"
S8 48..50 "UGC" 341..343 "GCA"
S9 53..55 "GUC" 88..90 "GGC"
S10 58..60 "ACG" 85..87 "AGU"
S11 66..67 "CC" 81..82 "GG"
S12 68..71 "CGGG" 76..79 "CCCG"
S13 97..99 "GUG" 299..301 "CAC"
S14 106..113 "ACGUGAGG" 219..226 "CCUCGCGG"
S15 116..117 "CC" 217..218 "GG"
S16 121..127 "CCCGAAG" 208..214 "CUUCGGG"
S17 129..132 "CCGG" 160..163 "CCGG"
S18 137..143 "ACACCGG" 148..154 "CCGGUGC"
S19 169..175 "UUCACCG" 188..194 "UGGUGAA"
S20 177..179 "AUC" 184..186 "GGU"
S21 199..200 "UG" 205..206 "CC"
S22 227..229 "CCU" 271..273 "GGG"
S23 232..232 "C" 270..270 "C"
S24 234..236 "GCU" 262..264 "GGC"
S25 239..246 "UUGGUGAG" 254..261 "CUCACCAA"
S26 276..279 "GCUG" 295..298 "CAGC"
S27 280..283 "GUCU" 288..291 "GGAU"
S28 303..308 "CUGGGA" 319..324 "GCCCAG"
S29 326..329 "CUCC" 334..337 "GGAG"
S30 354..354 "U" 380..380 "A"
S31 356..358 "GCG" 377..379 "CGC"
S32 362..366 "UGGAC" 371..375 "GUCUG"
S33 393..396 "GAGG" 420..423 "CCUC"
S34 403..406 "GGCC" 411..414 "GGUU"
S35 427..427 "C" 459..459 "G"
S36 428..433 "GACAGG" 450..455 "CCUGCA"
S37 437..437 "G" 445..445 "C"
S38 462..466 "GCUCC" 503..507 "GGAGC"
S39 473..477 "CUACG" 498..502 "CGUAG"
S40 483..484 "GC" 489..490 "GC"
S41 529..531 "GGC" 842..844 "GCC"
S42 537..537 "G" 841..841 "C"
S43 539..542 "GCUC" 723..726 "GUGC"
S44 544..544 "U" 720..720 "C"
S45 546..548 "GGC" 717..719 "GCU"
S46 550..552 "GCU" 612..614 "GGC"
S47 553..556 "CAGU" 607..610 "GCUG"
S48 558..559 "AG" 605..606 "CU"
S49 560..568 "UCAGGUGUG" 594..602 "GCCAUCUGA"
S50 573..579 "CCUCAGG" 585..591 "CCUGGGG"
S51 618..620 "AGC" 711..713 "ACU"
S52 621..626 "CCGGUA" 704..709 "GCCCGG"
S53 629..633 "GCCCA" 698..702 "UCACU"
S54 634..634 "C" 696..696 "G"
S55 635..636 "GG" 678..679 "AC"
S56 639..646 "UUCCUGGU" 668..675 "AUCAGGAG"
S57 650..652 "GCG" 659..661 "UGC"
S58 684..684 "G" 695..695 "G"
S59 687..688 "GC" 693..694 "GC"
S60 731..736 "NNNNGG" 767..772 "CCACGC"
S61 740..741 "GC" 765..766 "GU"
S62 745..748 "CAGG" 758..761 "CCUG"
S63 783..788 "GUAUGC" 835..840 "GCAUCC"
S64 791..802 "GCCGUUGGAGGG" 807..818 "CCCUUCAGUGGC"
S65 822..823 "NN" 828..829 "GC"
S66 846..848 "GGG" 871..873 "CUC"
S67 855..858 "GGUC" 863..866 "GAUU"
S68 921..924 "UUCG" 933..936 "CGAA"
S69 957..965 "AUCCCGGUC" 1002..1010 "ACCGGAGAC"
S70 968..970 "GGG" 984..986 "UCC"
S71 972..975 "ACGA" 980..983 "UCGA"
S72 991..992 "AG" 998..999 "CU"
S73 1033..1038 "GCUCGU" 1067..1072 "ACGAGC"
S74 1039..1041 "GUC" 1046..1048 "GAU"
S75 1051..1054 "UGGG" 1061..1064 "CCCG"
S76 1083..1089 "CCCUUAG" 1113..1119 "CUAAGGG"
S77 1093..1094 "CC" 1107..1108 "GG"
S78 1097..1098 "CA" 1105..1106 "UG"
H1 6..12 "UGAUCCU" (5,13) U:G PK{1}
H2 72..75 "CUUG" (71,76) G:C 
H3 144..147 "GAAA" (143,148) G:C 
H4 180..183 "GCAU" (179,184) C:G 
H5 201..204 "GAUU" (200,205) G:C 
H6 247..253 "GUAACGG" (246,254) G:C 
H7 284..287 "GAGA" (283,288) U:G 
H8 309..318 "CUGAGACACG" (308,319) A:G 
H9 330..333 "UACG" (329,334) C:G 
H10 367..370 "GAAA" (366,371) C:G 
H11 407..410 "CUCG" (406,411) C:G 
H12 438..444 "AUAGUGA" (437,445) G:C 
H13 485..488 "AGCC" (484,489) C:G PK{3}
H14 580..584 "CUCAA" (579,585) G:C 
H15 653..658 "GUGAAA" (652,659) G:U 
H16 689..692 "GAAG" (688,693) C:G 
H17 749..757 "AUUAGAUAC" (748,758) G:C 
H18 803..806 "UUUA" (802,807) G:C 
H19 824..827 "UAAC" (823,828) N:G PK{4}
H20 859..862 "GCAA" (858,863) C:G 
H21 925..932 "AAGCAACG" (924,933) G:C 
H22 976..979 "GAGA" (975,980) A:U 
H23 993..997 "UUCGG" (992,998) G:C 
H24 1042..1045 "GUGA" (1041,1046) C:G 
H25 1055..1060 "UUAAGU" (1054,1061) G:C 
H26 1099..1104 "UUUAGU" (1098,1105) A:U 
B1 23..23 "G" (22,515) C:G (514,24) U:A 
B2 47..47 "A" (46,344) C:G (343,48) A:U 
B3 56..57 "GA" (55,88) C:G (87,58) U:A 
B4 80..80 "U" (79,68) G:C (67,81) C:G 
B5 114..115 "AA" (113,219) G:C (218,116) G:C 
B6 230..231 "AU" (229,271) U:G (270,232) C:C 
B7 237..238 "UG" (236,262) U:G (261,239) A:U 
B8 292..294 "GUC" (291,280) U:G (279,295) G:C 
B9 355..355 "U" (354,380) U:A (379,356) C:G 
B10 384..384 "A" (383,37) G:C (36,385) G:C 
B11 456..458 "GAA" (455,428) A:G (427,459) C:G PK{2}
B12 467..472 "GGCCAA" (466,503) C:G (502,473) G:C PK{3}
B13 532..536 "GUAAA" (531,842) C:G (841,537) C:G PK{4}
B14 545..545 "A" (544,720) U:C (719,546) U:G 
B15 557..557 "A" (556,607) U:G (606,558) U:A 
B16 603..604 "UA" (602,560) A:U (559,605) G:C 
B17 611..611 "U" (610,553) G:C (552,612) U:G 
B18 685..686 "UG" (684,695) G:G (694,687) C:G 
B19 697..697 "C" (696,634) G:C (633,698) A:U 
B20 710..710 "U" (709,621) G:C (620,711) C:A 
B21 737..739 "GGA" (736,767) G:C (766,740) U:G 
B22 971..971 "A" (970,984) G:U (983,972) A:A 
B23 1095..1096 "AU" (1094,1107) C:G (1106,1097) G:C 
I1.1 40..43 "CUAA" (39,348) C:G 
I1.2 347..347 "A" (346,44) U:A 
I2.1 61..65 "AGAAG" (60,85) G:A 
I2.2 83..84 "AC" (82,66) G:C 
I3.1 118..120 "UGC" (117,217) C:G 
I3.2 215..216 "AU" (214,121) G:C 
I4.1 133..136 "GACA" (132,160) G:C 
I4.2 155..159 "UAAUA" (154,137) C:A 
I5.1 176..176 "G" (175,188) G:U 
I5.2 187..187 "C" (186,177) U:A 
I6.1 233..233 "A" (232,270) C:C 
I6.2 265..269 "AACGA" (264,234) C:G 
I7.1 359..361 "CAA" (358,377) G:C 
I7.2 376..376 "A" (375,362) G:U 
I8.1 397..402 "GAUGAA" (396,420) G:C 
I8.2 415..419 "GUAAA" (414,403) U:G 
I9.1 434..436 "GAC" (433,450) G:C 
I9.2 446..449 "GGUA" (445,437) C:G 
I10.1 478..482 "UGCCA" (477,498) G:C 
I10.2 491..497 "GGUGAUA" (490,483) C:G 
I11.1 543..543 "G" (542,723) C:G 
I11.2 721..722 "AG" (720,544) C:U 
I12.1 569..572 "AAAU" (568,594) G:G 
I12.2 592..593 "UC" (591,573) G:C 
I13.1 627..628 "GG" (626,704) A:G 
I13.2 703..703 "G" (702,629) U:G 
I14.1 637..638 "AA" (636,678) G:A 
I14.2 676..677 "GA" (675,639) G:U 
I15.1 647..649 "GUA" (646,668) U:A 
I15.2 662..667 "GCAGAU" (661,650) C:G 
I16.1 742..744 "AAA" (741,765) C:G 
I16.2 762..764 "GUA" (761,745) G:C 
I17.1 849..854 "GAGUAC" (848,871) G:C 
I17.2 867..870 "AAAA" (866,855) U:G 
I18.1 1090..1092 "UUG" (1089,1113) G:C 
I18.2 1109..1112 "CACU" (1108,1093) G:C 
M1.1 30..30 "G" (29,509) U:A (390,31) C:G 
M1.2 391..392 "GU" (390,31) C:G (423,393) C:G 
M1.3 424..426 "UUU" (423,393) C:G (459,427) G:C PK{2}
M1.4 460..461 "AA" (459,427) G:C (507,462) C:G 
M1.5 508..508 "G" (507,462) C:G (29,509) U:A 
M2.1 39..38 "" (38,382) G:C (348,39) G:C 
M2.2 349..353 "GAAUC" (348,39) G:C (380,354) A:U 
M2.3 381..381 "G" (380,354) A:U (38,382) G:C 
M3.1 51..52 "AA" (50,341) C:G (90,53) C:G 
M3.2 91..96 "GAACGG" (90,53) C:G (301,97) C:G 
M3.3 302..302 "A" (301,97) C:G (324,303) G:C 
M3.4 325..325 "A" (324,303) G:C (337,326) G:C 
M3.5 338..340 "GCA" (337,326) G:C (50,341) C:G 
M4.1 100..105 "AGUAAC" (99,299) G:C (226,106) G:A 
M4.2 227..226 "" (226,106) G:A (273,227) G:C 
M4.3 274..275 "UA" (273,227) G:C (298,276) C:G 
M4.4 299..298 "" (298,276) C:G (99,299) G:C 
M5.1 128..128 "A" (127,208) G:C (163,129) G:C 
M5.2 164..168 "AUACC" (163,129) G:C (194,169) A:U 
M5.3 195..198 "GAAA" (194,169) A:U (206,199) C:U 
M5.4 207..207 "G" (206,199) C:U (127,208) G:C 
M6.1 538..538 "A" (537,841) G:C (726,539) C:G 
M6.2 727..730 "GAAA" (726,539) C:G (772,731) C:N 
M6.3 773..782 "UGUAAACGAU" (772,731) C:N (840,783) C:G 
M6.4 841..840 "" (840,783) C:G (537,841) G:C 
M7.1 549..549 "G" (548,717) C:G (614,550) C:G 
M7.2 615..617 "UAG" (614,550) C:G (713,618) U:A 
M7.3 714..716 "GAC" (713,618) U:A (548,717) C:G 
M8.1 635..634 "" (634,696) C:G (679,635) C:G 
M8.2 680..683 "ACCA" (679,635) C:G (695,684) G:G 
M8.3 696..695 "" (695,684) G:G (634,696) C:G 
M9.1 789..790 "UA" (788,835) C:G (818,791) C:G 
M9.2 819..821 "GCA" (818,791) C:G (829,822) C:N 
M9.3 830..834 "AUUAA" (829,822) C:N (788,835) C:G 
M10.1 966..967 "GC" (965,1002) C:A (986,968) C:G 
M10.2 987..990 "CUUC" (986,968) C:G (999,991) U:A 
M10.3 1000..1001 "GG" (999,991) U:A (965,1002) C:A 
M11.1 1039..1038 "" (1038,1067) U:A (1048,1039) U:G 
M11.2 1049..1050 "GU" (1048,1039) U:G (1064,1051) G:U 
M11.3 1065..1066 "CA" (1064,1051) G:U (1038,1067) U:A 
X2 519..528 "GGAUUCAUUG" (518,19) C:G (844,529) C:G 
X4 874..920 "AAAGGAAUUGACGGGGGCCCGCACAAGCGGUGGAGCAUGUGGUUCAA" (873,846) C:G (936,921) A:U PK{1}
X5 937..956 "GAACCUUACCAGCCCUUGAC" (936,921) A:U (1010,957) C:A 
X6 1011..1032 "AGGUGCUGCAUGGCUGUCGUCA" (1010,957) C:A (1072,1033) C:G 
X7 1073..1082 "GCAACCCUCG" (1072,1033) C:G (1119,1083) G:C 
E1 1120..1130 "GACUGCCGGUG" 
PK1 3bp 9..11 877..879 H1 6..12 X4 874..920
PK2 1bp 425..425 458..458 M1.3 424..426 B11 456..458
PK3 3bp 467..469 486..488 B12 467..472 H13 485..488
PK4 2bp 532..533 826..827 B13 532..536 H19 824..827
PK1.1 9 U 879 A
PK1.2 10 C 878 G
PK1.3 11 C 877 G
PK2.1 425 U 458 A
PK3.1 467 G 488 C
PK3.2 468 G 487 C
PK3.3 469 C 486 G
PK4.1 532 G 827 C
PK4.2 533 U 826 A
NCBP1 684 G 695 G S58
NCBP2 541 U 724 U S43
NCBP3 784 U 839 C S63
NCBP4 734 N 769 A S60
NCBP5 567 U 595 C S49
NCBP6 965 C 1002 A S69
NCBP7 308 A 319 G S28
NCBP8 568 G 594 G S49
NCBP9 137 A 154 C S18
NCBP10 60 G 85 A S10
NCBP11 428 G 455 A S36
NCBP12 232 C 270 C S23
NCBP13 632 C 699 C S53
NCBP14 960 C 1007 A S69
NCBP15 626 A 704 G S52
NCBP16 620 C 711 A S51
NCBP17 199 U 206 C S21
NCBP18 429 A 454 C S36
NCBP19 732 N 771 G S60
NCBP20 957 A 1010 C S69
NCBP21 731 N 772 C S60
NCBP22 544 U 720 C S44
NCBP23 962 G 1005 G S69
NCBP24 733 N 770 C S60
NCBP25 630 C 701 C S53
NCBP26 822 N 829 C S65
NCBP27 106 A 226 G S14
NCBP28 964 U 1003 C S69
NCBP29 823 N 828 G S65
NCBP30 636 G 678 A S55
NCBP31 625 U 705 C S52
NCBP32 631 C 700 A S53
NCBP33 972 A 983 A S71
segment1 5bp 1..5 GAGUU 13..17 GGCUC
segment2 10bp 19..29 GGACGAACGCU 509..518 AGCGUUGUCC
segment3 8bp 31..38 GCGGCGCG 382..390 CGACGCCGC
segment4 7bp 39..50 CCUAAAACAUGC 341..348 GCAGUUAG
segment5 12bp 53..71 GUCGAACGAGAAGCCCGGG 76..90 CCCGUGGACAGUGGC
segment6 3bp 97..99 GUG 299..301 CAC
segment7 17bp 106..127 ACGUGAGGAACCUGCCCCGAAG 208..226 CUUCGGGAUGGCCUCGCGG
segment8 11bp 129..143 CCGGGACAACACCGG 148..163 CCGGUGCUAAUACCGG
segment9 10bp 169..179 UUCACCGGAUC 184..194 GGUCUGGUGAA
segment10 2bp 199..200 UG 205..206 CC
segment11 15bp 227..246 CCUAUCAGCUUGUUGGUGAG 254..273 CUCACCAAGGCAACGACGGG
segment12 8bp 276..283 GCUGGUCU 288..298 GGAUGUCCAGC
segment13 6bp 303..308 CUGGGA 319..324 GCCCAG
segment14 4bp 326..329 CUCC 334..337 GGAG
segment15 9bp 354..366 UUGCGCAAUGGAC 371..380 GUCUGACGCA
segment16 8bp 393..406 GAGGGAUGAAGGCC 411..423 GGUUGUAAACCUC
segment17 8bp 427..437 CGACAGGGACG 445..459 CGGUACCUGCAGAAG
segment18 12bp 462..484 GCUCCGGCCAACUACGUGCCAGC 489..507 GCGGUGAUACGUAGGGAGC
segment19 4bp 529..537 GGCGUAAAG 841..844 CGCC
segment20 8bp 539..548 GCUCGUAGGC 717..726 GCUCAGGUGC
segment21 25bp 550..579 GCUCAGUAAGUCAGGUGUGAAAUCCUCAGG 585..614 CCUGGGGUCGCCAUCUGAUACUGCUGUGGC
segment22 15bp 618..634 AGCCCGGUAGGGCCCAC 696..713 GCUCACUGGCCCGGUACU
segment23 13bp 635..652 GGAAUUCCUGGUGUAGCG 659..679 UGCGCAGAUAUCAGGAGGAAC
segment24 3bp 684..688 GUGGC 693..695 GCG
segment25 12bp 731..748 NNNNGGGGAGCAAACAGG 758..772 CCUGGUAGUCCACGC
segment26 6bp 783..788 GUAUGC 835..840 GCAUCC
segment27 12bp 791..802 GCCGUUGGAGGG 807..818 CCCUUCAGUGGC
segment28 2bp 822..823 NN 828..829 GC
segment29 7bp 846..858 GGGGAGUACGGUC 863..873 GAUUAAAACUC
segment30 4bp 921..924 UUCG 933..936 CGAA
segment31 9bp 957..965 AUCCCGGUC 1002..1010 ACCGGAGAC
segment32 7bp 968..975 GGGAACGA 980..986 UCGAUCC
segment33 2bp 991..992 AG 998..999 CU
segment34 6bp 1033..1038 GCUCGU 1067..1072 ACGAGC
segment35 3bp 1039..1041 GUC 1046..1048 GAU
segment36 4bp 1051..1054 UGGG 1061..1064 CCCG
segment37 11bp 1083..1098 CCCUUAGUUGCCAUCA 1105..1119 UGGGCACUCUAAGGG
