import dataloader
from MyRNN import MyRNN
import torch
from torch.utils.data import DataLoader
import time

if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    hidden_size = 32
    num_epochs = 4
    batch_size = 12

    my_dataset = dataloader.FastaDataset('data')
    input_size = my_dataset.samples[0].size()[1]
    output_size = len(my_dataset.class_labels)
    model = MyRNN(input_size, hidden_size, output_size)

    print("Device:", device)

    some_data = dataloader.FastaDataset("data")
    dloader = DataLoader(some_data, batch_size=batch_size, collate_fn=dataloader.pad_zeros)

    criterion = torch.nn.CrossEntropyLoss()
    optim = torch.optim.SGD(params=model.parameters(), lr=0.1, momentum=0, weight_decay=0.)

    start_time = time.time()
    for epoch in range(num_epochs):
        model.train()
        for inputs, labels, _ in dloader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            optim.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optim.step()

        correct = 0
        total = 0

        # Set the model to evaluation mode
        model.eval()

        # Disable gradient computation
        with torch.no_grad():
            for inputs, labels, _ in dloader:
                inputs = inputs.to(device)
                labels = labels.to(device)

                outputs = model(inputs)
                predicted_probabilities = outputs[:, -1, :]

                max_values, _ = torch.max(predicted_probabilities, dim=1)  # Find the maximum value in the tensors
                predicted = torch.where(predicted_probabilities == max_values.unsqueeze(1), torch.tensor(1), torch.tensor(0))   # set the highest probability to one for prediction purpose
                # predicted = torch.tensor([[1 if i == int(torch.argmax(lst)) else 0 for i, el in enumerate(lst)] for lst in predicted_probabilities]).to(device)
                total += labels.size(0)

                eq_el = torch.eq(labels, predicted)
                eq_tensors = torch.all(eq_el, dim=1)
                sum_eq_tensors = torch.sum(eq_tensors)
                correct += sum_eq_tensors

        # Calculate the accuracy
        accuracy = (correct / total).item()

        # Print the accuracy for the current epoch
        print(f"Accuracy for epoch {epoch + 1}: {round(accuracy * 100, 2)}%")

    end_time = time.time()
    total_time = end_time - start_time
    print(f"Total training time: {round(total_time, 1)} seconds")