import torch


class MyRNN(torch.nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()

        self.hidden_size = hidden_size

        self.rnn = torch.nn.RNN(input_size, hidden_size, batch_first=True)

        self.out_layer = torch.nn.Linear(hidden_size, output_size)

        self.softmax = torch.nn.Softmax(dim=2)

    def forward(self, x):
        # batch_first ;)
        batch_size = x.size(0)
        hidden = self.init_hidden(batch_size)
        out, hidden = self.rnn(x, hidden)
        out = self.out_layer(out)
        out = self.softmax(out)

        return out

    def init_hidden(self, batch_size):
        hidden = torch.zeros(1, batch_size, self.hidden_size)
        return hidden
