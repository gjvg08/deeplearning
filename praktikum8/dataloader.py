import torch
import os
from torch.utils.data import Dataset


def load_data(dir_path):
    files = os.listdir(dir_path)
    class_labels = [file_path.split('.')[0] for file_path in files]
    samples = []
    labels = []
    for file_name in files:
        file_path = dir_path + '/' + file_name
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('>'):
                    continue
                elif len(line) > 0:
                    seq = line.rstrip()
                    one_hot_seq = encode_protein(seq)
                    samples.append(one_hot_seq)

                    # make labels onehot not strings
                    true_label = file_name.split('.')[0]
                    one_hot_label = [1 if label == true_label else 0 for label in class_labels]
                    labels.append(one_hot_label)

    return samples, labels, class_labels


def encode_protein(sequence):
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y',
                'Z', 'X']
    alphabet_to_encoding = {}
    for l_ind, letter in enumerate(alphabet):
        alphabet_to_encoding[letter] = [1 if letter == "X" else 1 if _ == l_ind else 0 for _ in
                                        range(len(alphabet) - 1)]
    one_hot = [alphabet_to_encoding[letter] for letter in sequence]
    return torch.tensor(data=one_hot, dtype=torch.float)


def pad_zeros(batch):
    batch = sorted(batch, key=lambda x: len(x[0]), reverse=True)

    # Separate inputs and labels
    inputs, labels = zip(*batch)

    # make label tensors
    labels = torch.tensor(labels)

    # Get the length of each sequence in the batch
    lengths = [len(seq) for seq in inputs]

    # Pad sequences to the length of the longest sequence in the batch
    padded_inputs = torch.nn.utils.rnn.pad_sequence(inputs, batch_first=True)

    return padded_inputs, labels, lengths


class FastaDataset(Dataset):
    def __init__(self, data_dir):
        self.data_dir = data_dir
        self.samples, self.labels, self.class_labels = load_data(data_dir)

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, index):
        sample = self.samples[index]
        label = self.labels[index]
        return sample, label
