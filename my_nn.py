import torch


class NeuralNetwork(torch.nn.Module):
    def __init__(self, channels_out, ):
        super().__init__()
        self.conv_in = torch.nn.Conv2d(in_channels=1, out_channels=channels_out, kernel_size=(3, 3), padding=(1, 1))
        self.conv_interm = torch.nn.Conv2d(in_channels=5, out_channels=1, kernel_size=(3, 3), padding=(1, 1))
        self.activ = torch.nn.ReLU()

    def forward(self, x):
        x = self.conv_in(x)
        x = self.activ(x)
        x = self.conv_interm(x)
        return x


if __name__ == '__main__':
    model = NeuralNetwork(channels_out=5)
    criterion = torch.nn.MSELoss()
    optim = torch.optim.SGD(params=model.parameters(), lr=0.0001, momentum=0, weight_decay=0.)
    image = torch.rand(size=(1, 1, 28, 28))
    target = torch.ones(size=(1, 1, 28, 28))
    for i in range(5):
        optim.zero_grad()
        pred = model(image)
        loss = criterion(pred, target)
        print(loss)
        loss.backward()
        optim.step()

    for param in model.named_parameters():
        print(param)
