import os
import pandas as pd


def read_files(dir):
    data = {}
    for file_name in os.listdir(dir):
        file_data = []
        with open(f"{dir}/{file_name}", 'r') as file:
            lines = file.readlines()
            if len(lines) < 1: continue
            lines = lines[2:-1]
            for line in lines:
                accuracy = float(line.split(': ')[1].rstrip().strip('%'))
                file_data.append(accuracy)
        index = file_name.strip('.txt')
        data[index] = file_data
    data = pd.DataFrame.from_dict(data, orient='index')
    return data


if __name__ == '__main__':
    dir = "results"
    data = read_files(dir)
    file_medians = data.median(axis=1)
    sorted_data = data.iloc[file_medians.argsort()]
    print(data)
