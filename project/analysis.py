from CustomDataset import load_data
import matplotlib.pyplot as plt
import os
import textwrap


def plot_length():
    train_dir = "bpRNA_train"
    test_dir = "bpRNA_test"
    train_samples, train_labels = load_data(train_dir)
    test_samples, test_labels = load_data(test_dir)
    samples = train_samples + test_samples
    labels = train_labels + test_labels

    len_samples = [len(sample) for sample in samples]
    count_lens = {}
    for sample_length in len_samples:
        if sample_length in count_lens:
            count_lens[sample_length] += 1
        else:
            count_lens[sample_length] = 1

    x = list(count_lens.keys())
    y = list(count_lens.values())

    plt.bar(x, y)
    plt.xlabel("Sequenzlänge")
    plt.ylabel("Häufigkeit")
    plt.title("Verteilung der Längen im gesamten Datenset")

    plt.show()


def get_accuracies(file_path):
    accs = []
    with open(file_path, 'r') as file:
        for line in file:
            accs.append(line.split(":")[1].strip(" \n%"))
    accs = accs[2:-1]
    accs = [float(acc) for acc in accs]
    return accs


def plot_accuracies(accs, file_name):
    plt.plot([i for i in range(1, len(accs)+1)], accs)
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy in %")
    plt.xlim(1,51)
    plt.ylim(0,100)
    plt.title(file_name)
    plt.show()


def plot_dir(dir):
    if not os.path.exists(dir + "/plots"):
        os.mkdir(dir + "/plots")
    for file_path in os.listdir(dir):
        if not file_path == "plots":
            accs = get_accuracies(dir + "/" + file_path)
            file_name = file_path.split("/")[-1].strip(".txt") + ".jpg"
            plt.plot([i for i in range(1, len(accs) + 1)], accs)
            plt.xlabel("Epoch")
            plt.ylabel("Accuracy in %")
            plt.xlim(1, 51)
            plt.ylim(0, 100)
            wrapped_title = textwrap.fill(file_name.strip(".jpg"), width=100)
            plt.title(wrapped_title, fontsize=8)
            plt.savefig(dir + "/plots/" + file_name)
            plt.clf()

if __name__ == '__main__':
    # file_path = "archive/grid_search/hsize17_bsize20_nlayers3_nepochs50.txt"
    # accs = get_accuracies(file_path)
    # plot_accuracies(accs, file_path.split("/")[2].strip(".txt"))
    plot_dir("results")
