import torch
import os
from torch.utils.data import Dataset
from itertools import chain

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
iupac_dict = {'A': torch.tensor([1., 0., 0., 0.]), 'C': torch.tensor([0., 1., 0., 0.]), 'G': torch.tensor([0., 0., 1., 0.]),
              'U': torch.tensor([0., 0., 0., 1.]), 'R': torch.tensor([1., 0., 1., 0.]), 'Y': torch.tensor([0., 1., 0., 1.]),
              'S': torch.tensor([0., 1., 1., 0.]), 'W': torch.tensor([1., 0., 0., 1.]), 'K': torch.tensor([0., 0., 1., 1.]),
              'M': torch.tensor([1., 1., 0., 0.]), 'B': torch.tensor([0., 1., 1., 1.]), 'D': torch.tensor([1., 0., 1., 1.]),
              'H': torch.tensor([1., 1., 0., 1.]), 'V': torch.tensor([1., 1., 1., 0.]), 'N': torch.tensor([1., 1., 1., 1.])}
iupac_dict = {key: value.to(device) for key, value in iupac_dict.items()}
# print(iupac_dict)
# reverse_iupac = {value: key for key, value in iupac_dict.items()}
# print(reverse_iupac)
iupac_keys = list(iupac_dict.keys())
iupac_values = list(iupac_dict.values())


def load_file(file_path):
    sample = ''
    label = ''
    sample_empty = True
    skip_brackets = False
    label_empty = True
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('#'):
                continue
            elif skip_brackets:
                skip_brackets = False
                continue
            elif sample_empty:
                sample = line.rstrip().upper()
                sample_empty = False
                skip_brackets = True
            elif label_empty:
                label = line.rstrip().upper()
                break

    return sample, label


def load_data(dir_path):
    tuples = []
    for file_path in os.listdir(dir_path):
        sample, label = load_file(f"{dir_path}/{file_path}")
        tuples.append((sample, label))
    tuples = sorted(tuples, key=lambda x: len(x[0]))
    samples, labels = zip(*tuples)
    return samples, labels


def base_to_one_hot(base):
    """ACGU in this order"""
    return iupac_dict[base]


def one_hot_to_base(one_hot):
    # return reverse_iupac[one_hot]
    return "ACGU"[torch.argmax(one_hot)]
    # print(one_hot.get_device())
    # print(iupac_dict["A"].get_device())
    # return {base for base in iupac_dict if iupac_dict[base] == one_hot}


def one_hot_to_rna(enc_seq):
    rna = "".join([one_hot_to_base(enc_base) for enc_base in enc_seq])
    return rna


def one_hot_samples(seqs):
    one_hot_seqs = [torch.stack([base_to_one_hot(base) for base in seq]).to(torch.float) for seq in seqs]
    return one_hot_seqs


def one_hot_labels(labels):
    unique_chars = sorted(list(set(chain.from_iterable(labels))))
    one_hot_labels = [
        torch.tensor([[1. if char == struct else 0. for char in unique_chars] for struct in label], dtype=torch.float) for
        label in labels]
    return one_hot_labels, unique_chars


def pad_zeros(batch):
    batch = sorted(batch, key=lambda x: len(x[0]), reverse=True)

    # Separate inputs and labels
    inputs, labels = zip(*batch)

    # Get the length of each sequence in the batch
    lengths = [len(seq) for seq in inputs]

    # Pad sequences to the length of the longest sequence in the batch
    padded_inputs = torch.nn.utils.rnn.pad_sequence(inputs, batch_first=True)
    padded_labels = torch.nn.utils.rnn.pad_sequence(labels, batch_first=True)

    return padded_inputs, padded_labels, lengths


class StructfileDataset(Dataset):
    def __init__(self, data_dir):
        self.samples, self.labels = load_data(data_dir)
        self.samples = one_hot_samples(self.samples)
        self.labels, self.struct_alphabet = one_hot_labels(self.labels)
        self.num_structs = len(self.struct_alphabet)

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, index):
        return self.samples[index], self.labels[index]

    def __split__(self, ratio):
        indices = torch.randperm(len(self))
        val_size = int(len(self) * ratio)
        train_size = len(self) - val_size

        train_indices = indices[:train_size]
        validation_indices = indices[train_size:]

        train_samples = [self.samples[i] for i in train_indices]
        train_labels = [self.labels[i] for i in train_indices]
        validation_samples = [self.samples[i] for i in validation_indices]
        validation_labels = [self.labels[i] for i in validation_indices]

        train = StructfileDataset.__new__(StructfileDataset)
        validation = StructfileDataset.__new__(StructfileDataset)

        train.samples = train_samples
        train.labels = train_labels
        train.struct_alphabet = self.struct_alphabet
        train.num_structs = self.num_structs

        validation.samples = validation_samples
        validation.labels = validation_labels
        validation.struct_alphabet = self.struct_alphabet
        validation.num_structs = self.num_structs

        return train, validation
