import torch


class MyLSTM(torch.nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_structs):
        super().__init__()

        self.hidden_size = hidden_size
        self.num_layers = num_layers

        self.lstm = torch.nn.LSTM(input_size, hidden_size, num_layers, batch_first=True, bidirectional=True)

        self.out_layer = torch.nn.Linear(hidden_size * 2, num_structs)

        self.softmax = torch.nn.Softmax(dim=-1)

    def forward(self, x):
        initial_hidden_state = torch.zeros(self.num_layers * 2, x.size(0), self.hidden_size).to(x.device)
        initial_cell_state = torch.zeros(self.num_layers * 2, x.size(0), self.hidden_size).to(x.device)
        out, (final_hidden_state, final_cell_state) = self.lstm(x, (initial_hidden_state, initial_cell_state))
        out = self.out_layer(out)
        out = self.softmax(out)

        return out
