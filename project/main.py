import CustomDataset
from NeuralNetwork import MyLSTM
import torch
from torch.utils.data import DataLoader, Subset
import time
import sys
import os
import numpy as np


def stdout_to_file(dir, **kwargs):
    stdout = sys.stdout
    if not os.path.exists(dir):
        os.makedirs(dir)
    trans_table = str.maketrans("{}<>':", "     _")
    file_path = (f"{dir}/{kwargs}".translate(trans_table) + ".txt").strip()
    out_file = open(file_path, 'w')
    sys.stdout = out_file
    print(kwargs)
    return stdout, out_file


def run(device, hidden_size_range, batch_size_range, num_layers_range, num_epochs_range, learning_rate_range,
        momentum_range, weight_decay_range, optim_lst, criterion, train, test, save_to_file, dir):
    input_size = train.samples[0].size()[1]
    output_size = train.num_structs


    if device == 'cuda':
        pin_memory = True
    else:
        pin_memory = False

    for batch_size in batch_size_range:
        dloader_train = DataLoader(train, batch_size=batch_size,
                                   collate_fn=CustomDataset.pad_zeros,
                                   shuffle=False, pin_memory=pin_memory)
        dloader_test = DataLoader(test, batch_size=batch_size,
                                  collate_fn=CustomDataset.pad_zeros,
                                  shuffle=False, pin_memory=pin_memory)
        for hidden_size in hidden_size_range:
            for num_layers in num_layers_range:
                for num_epochs in num_epochs_range:
                    for learning_rate in learning_rate_range:
                        for momentum in momentum_range:
                            for weight_decay in weight_decay_range:
                                for optim in optim_lst:
                                    try:
                                        if save_to_file:
                                            stdout, out_file = stdout_to_file(dir, hidden_size=hidden_size,
                                                                              batch_size=batch_size,
                                                                              num_layers=num_layers,
                                                                              num_epochs=num_epochs,
                                                                              learning_rate=learning_rate,
                                                                              momentum=momentum,
                                                                              weight_decay=weight_decay, optim=optim)
                                        print("Device:", device)

                                        model = MyLSTM(input_size, hidden_size, num_layers, output_size).to(device)
                                        optim = optim(params=model.parameters(), lr=learning_rate, momentum=momentum,
                                                      weight_decay=weight_decay)

                                        seqs = []
                                        accuracies = []
                                        all_losses = []

                                        start_time = time.time()
                                        for epoch in range(num_epochs):
                                            losses = []
                                            model.train()
                                            for inputs, labels, _ in dloader_train:
                                                inputs = inputs.to(device)
                                                labels = labels.to(device)

                                                optim.zero_grad(set_to_none=True)
                                                outputs = model(inputs)
                                                loss = criterion(outputs, labels)
                                                losses.append(loss)
                                                loss.backward()
                                                optim.step()

                                                seqs += [(CustomDataset.one_hot_to_rna(input),
                                                          label,
                                                          output) for input, label, output in zip(inputs, labels, outputs)]

                                            all_losses.append(losses)
                                            correct = 0
                                            total = 0

                                            model.eval()

                                            with torch.no_grad():
                                                for inputs, labels, _ in dloader_test:
                                                    inputs = inputs.to(device)
                                                    labels = labels.to(device)

                                                    outputs = model(inputs)

                                                    max_values, _ = torch.max(outputs, dim=2,
                                                                              keepdim=True)  # Find the maximum value in the tensors
                                                    predicted = torch.where(outputs == max_values, torch.tensor(1),
                                                                            torch.tensor(
                                                                                0))  # set the highest probability to one for prediction purpose
                                                    total += labels.size(0) * labels.size(1)

                                                    differences = labels - predicted
                                                    mask = (differences == 0)
                                                    amount_correct_tokens = torch.sum(mask.all(dim=2))
                                                    correct += amount_correct_tokens

                                            accuracy = (correct / total).item()
                                            accuracies.append(accuracy)

                                            # print(f"Accuracy for epoch {epoch + 1}: {round(accuracy * 100, 2)}%")

                                        end_time = time.time()
                                        total_time = end_time - start_time
                                        print(seqs)
                                        print(all_losses)
                                        print(accuracies)
                                        print(f"Total training time: {round(total_time, 1)} seconds")
                                    finally:
                                        if save_to_file:
                                            sys.stdout = stdout
                                            out_file.close()

                                    print(
                                        f"hsize{hidden_size}_bsize{batch_size}_nlayers{num_layers}_nepochs{num_epochs} done!")


if __name__ == '__main__':
    save_to_file = True
    dir = "results"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    dataset = CustomDataset.StructfileDataset('small_train')
    train, validation = dataset.__split__(0.2)
    test = CustomDataset.StructfileDataset('small_test')

    print("Data loaded!")

    input_size = train.samples[0].size()[1]
    output_size = train.num_structs

    hidden_size_range = range(8, 9)
    batch_size_range = range(32, 33)
    num_layers_range = range(1, 2)
    num_epochs_range = range(11, 12)
    learning_rate_range = [0.1]
    momentum_range = np.linspace(0.9, 1, 1)
    weight_decay_range = [0.]
    optim_lst = [torch.optim.SGD]
    criterion = torch.nn.CrossEntropyLoss()

    run(device, hidden_size_range, batch_size_range, num_layers_range, num_epochs_range, learning_rate_range,
        momentum_range, weight_decay_range, optim_lst, criterion, train, validation,
        save_to_file, dir)
    print("Done!")
