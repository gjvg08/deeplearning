Device: cuda
Accuracy for epoch 1: 11.54%
Accuracy for epoch 2: 11.54%
Accuracy for epoch 3: 11.54%
Accuracy for epoch 4: 11.54%
Accuracy for epoch 5: 11.54%
Accuracy for epoch 6: 11.54%
Accuracy for epoch 7: 11.54%
Accuracy for epoch 8: 11.54%
Accuracy for epoch 9: 11.63%
Accuracy for epoch 10: 58.08%
Accuracy for epoch 11: 58.06%
Accuracy for epoch 12: 58.07%
Accuracy for epoch 13: 58.08%
Accuracy for epoch 14: 58.08%
Accuracy for epoch 15: 58.08%
Accuracy for epoch 16: 58.08%
Accuracy for epoch 17: 58.08%
Accuracy for epoch 18: 58.08%
Accuracy for epoch 19: 58.08%
Accuracy for epoch 20: 58.08%
Accuracy for epoch 21: 55.68%
Accuracy for epoch 22: 0.67%
Accuracy for epoch 23: 0.67%
Accuracy for epoch 24: 0.67%
Accuracy for epoch 25: 0.67%
Accuracy for epoch 26: 11.67%
Accuracy for epoch 27: 0.67%
Accuracy for epoch 28: 11.6%
Accuracy for epoch 29: 11.66%
Accuracy for epoch 30: 11.66%
Accuracy for epoch 31: 0.67%
Accuracy for epoch 32: 0.67%
Accuracy for epoch 33: 11.24%
Accuracy for epoch 34: 0.67%
Accuracy for epoch 35: 0.67%
Accuracy for epoch 36: 0.67%
Accuracy for epoch 37: 0.67%
Accuracy for epoch 38: 0.67%
Accuracy for epoch 39: 0.67%
Accuracy for epoch 40: 0.67%
Accuracy for epoch 41: 0.67%
Accuracy for epoch 42: 0.67%
Accuracy for epoch 43: 0.67%
Accuracy for epoch 44: 0.67%
Accuracy for epoch 45: 0.67%
Accuracy for epoch 46: 0.67%
Accuracy for epoch 47: 0.67%
Accuracy for epoch 48: 0.67%
Accuracy for epoch 49: 0.67%
Accuracy for epoch 50: 0.67%
Total training time: 78.3 seconds
