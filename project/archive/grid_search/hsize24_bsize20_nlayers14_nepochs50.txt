Device: cuda
Accuracy for epoch 1: 3.6%
Accuracy for epoch 2: 3.6%
Accuracy for epoch 3: 3.6%
Accuracy for epoch 4: 3.59%
Accuracy for epoch 5: 11.24%
Accuracy for epoch 6: 11.24%
Accuracy for epoch 7: 58.04%
Accuracy for epoch 8: 58.07%
Accuracy for epoch 9: 58.08%
Accuracy for epoch 10: 2.13%
Accuracy for epoch 11: 58.08%
Accuracy for epoch 12: 58.08%
Accuracy for epoch 13: 58.08%
Accuracy for epoch 14: 58.08%
Accuracy for epoch 15: 58.08%
Accuracy for epoch 16: 58.08%
Accuracy for epoch 17: 58.1%
Accuracy for epoch 18: 0.67%
Accuracy for epoch 19: 0.67%
Accuracy for epoch 20: 58.11%
Accuracy for epoch 21: 58.1%
Accuracy for epoch 22: 56.64%
Accuracy for epoch 23: 56.49%
Accuracy for epoch 24: 0.67%
Accuracy for epoch 25: 0.67%
Accuracy for epoch 26: 0.67%
Accuracy for epoch 27: 0.67%
Accuracy for epoch 28: 0.67%
Accuracy for epoch 29: 0.67%
Accuracy for epoch 30: 0.67%
Accuracy for epoch 31: 0.67%
Accuracy for epoch 32: 0.67%
Accuracy for epoch 33: 0.67%
Accuracy for epoch 34: 0.67%
Accuracy for epoch 35: 0.67%
Accuracy for epoch 36: 0.67%
Accuracy for epoch 37: 0.67%
Accuracy for epoch 38: 0.67%
Accuracy for epoch 39: 0.67%
Accuracy for epoch 40: 0.67%
Accuracy for epoch 41: 0.67%
Accuracy for epoch 42: 0.67%
Accuracy for epoch 43: 0.67%
Accuracy for epoch 44: 0.67%
Accuracy for epoch 45: 0.67%
Accuracy for epoch 46: 0.67%
Accuracy for epoch 47: 0.67%
Accuracy for epoch 48: 0.67%
Accuracy for epoch 49: 0.67%
Accuracy for epoch 50: 0.67%
Total training time: 140.0 seconds
