Device: cuda
Accuracy for epoch 1: 3.6%
Accuracy for epoch 2: 3.6%
Accuracy for epoch 3: 3.6%
Accuracy for epoch 4: 3.6%
Accuracy for epoch 5: 3.6%
Accuracy for epoch 6: 3.59%
Accuracy for epoch 7: 11.24%
Accuracy for epoch 8: 11.24%
Accuracy for epoch 9: 11.24%
Accuracy for epoch 10: 57.98%
Accuracy for epoch 11: 0.67%
Accuracy for epoch 12: 0.67%
Accuracy for epoch 13: 0.67%
Accuracy for epoch 14: 0.67%
Accuracy for epoch 15: 2.02%
Accuracy for epoch 16: 2.0%
Accuracy for epoch 17: 2.0%
Accuracy for epoch 18: 11.6%
Accuracy for epoch 19: 11.6%
Accuracy for epoch 20: 11.6%
Accuracy for epoch 21: 11.63%
Accuracy for epoch 22: 11.64%
Accuracy for epoch 23: 0.67%
Accuracy for epoch 24: 0.67%
Accuracy for epoch 25: 0.67%
Accuracy for epoch 26: 11.65%
Accuracy for epoch 27: 11.54%
Accuracy for epoch 28: 11.54%
Accuracy for epoch 29: 11.54%
Accuracy for epoch 30: 11.54%
Accuracy for epoch 31: 11.27%
Accuracy for epoch 32: 0.67%
Accuracy for epoch 33: 0.67%
Accuracy for epoch 34: 0.67%
Accuracy for epoch 35: 0.67%
Accuracy for epoch 36: 5.18%
Accuracy for epoch 37: 0.67%
Accuracy for epoch 38: 0.67%
Accuracy for epoch 39: 0.67%
Accuracy for epoch 40: 0.67%
Accuracy for epoch 41: 0.67%
Accuracy for epoch 42: 0.67%
Accuracy for epoch 43: 0.67%
Accuracy for epoch 44: 0.67%
Accuracy for epoch 45: 0.67%
Accuracy for epoch 46: 0.67%
Accuracy for epoch 47: 0.67%
Accuracy for epoch 48: 0.67%
Accuracy for epoch 49: 0.67%
Accuracy for epoch 50: 0.67%
Total training time: 64.7 seconds
