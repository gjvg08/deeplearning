Device: cuda
Accuracy for epoch 1: 0.67%
Accuracy for epoch 2: 10.78%
Accuracy for epoch 3: 10.68%
Accuracy for epoch 4: 58.08%
Accuracy for epoch 5: 57.86%
Accuracy for epoch 6: 58.45%
Accuracy for epoch 7: 60.67%
Accuracy for epoch 8: 11.21%
Accuracy for epoch 9: 11.24%
Accuracy for epoch 10: 11.24%
Accuracy for epoch 11: 11.24%
Accuracy for epoch 12: 11.99%
Accuracy for epoch 13: 11.97%
Accuracy for epoch 14: 10.68%
Accuracy for epoch 15: 10.68%
Accuracy for epoch 16: 10.68%
Accuracy for epoch 17: 12.02%
Accuracy for epoch 18: 11.49%
Accuracy for epoch 19: 11.29%
Accuracy for epoch 20: 11.55%
Accuracy for epoch 21: 58.08%
Accuracy for epoch 22: 58.08%
Accuracy for epoch 23: 53.1%
Accuracy for epoch 24: 10.68%
Accuracy for epoch 25: 10.68%
Accuracy for epoch 26: 10.68%
Accuracy for epoch 27: 10.68%
Accuracy for epoch 28: 10.68%
Accuracy for epoch 29: 10.68%
Accuracy for epoch 30: 10.68%
Accuracy for epoch 31: 10.68%
Accuracy for epoch 32: 10.68%
Accuracy for epoch 33: 10.68%
Accuracy for epoch 34: 10.68%
Accuracy for epoch 35: 10.68%
Accuracy for epoch 36: 10.68%
Accuracy for epoch 37: 10.68%
Accuracy for epoch 38: 10.68%
Accuracy for epoch 39: 10.68%
Accuracy for epoch 40: 10.68%
Accuracy for epoch 41: 10.68%
Accuracy for epoch 42: 10.68%
Accuracy for epoch 43: 10.68%
Accuracy for epoch 44: 10.68%
Accuracy for epoch 45: 10.68%
Accuracy for epoch 46: 10.68%
Accuracy for epoch 47: 10.68%
Accuracy for epoch 48: 10.68%
Accuracy for epoch 49: 10.68%
Accuracy for epoch 50: 10.68%
Total training time: 47.7 seconds
